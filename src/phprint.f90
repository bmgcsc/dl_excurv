SUBROUTINE PHPRINT (IS,IF)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_F
  Use Common_ELS
  Use Common_POT
  Use Common_COMPAR
  Use Common_P
  Use Common_PRDSN
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	Print phaseshift files
  !
  !	Print phaseshifts for atoms IS to IF.
  !	If ATOM(int) and other parameters are defined, these are used to
  !	generate the member name and optional title records.
  !
  DIMENSION WSPACE(2*IPARLMAX+2)
  CHARACTER MEM1*12,ATOMS(6)*8,CDATE*10,CTIME*8,PM(-1:1)*1
  DATA ATOMS/'cent','atom2','atom3','atom4','atom5','atom6'/,PM/'-',' ','+'/
  !
  !	Print up to lmax+1 phaseshifts, unless mod(lmax+1,8) = 1
  !
  IDIV=2
  LMAX1=LMAX+1
  IF (IDIV.EQ.1.AND.MOD(LMAX1,8).EQ.1) LMAX1=LMAX1+1
  PRDSN=' '
  DO  I=IS,IF
     IF (NPH(I).LT.2) THEN
        IF (IS.EQ.IF) CALL WTEXTI ('Phaseshifts undefined for atom: ',I)
        cycle
     ENDIF
     CALL WTEXTI ('Atom number ',I)
     M=NPH(I)
     !
     !	Write titles if atomic numbers defined
     !
     !	V0EV(I) contains the MT zero
     !	IHCODE(I) contains the code for central atom approximation or 0
     !
     IF (V0EV(I).NE.0..AND.IATOM(I).NE.0) THEN
        WRITE (OUTTERM,80) &
             ELS(IATOM(I)),PM(ION(I)),IATOM(I),RMTR(I),ALF(I),CMAG(I),IHCODE(I),V0EV(I),FEEV(I),RHO0S(I),RLIFEEV(I)
     ENDIF
     !
     !	Phase shift files
     !
     IF (IATOM(I).LE.0.OR.IATOM(I).GT.103) THEN
        MEM1=ATOMS(I)
     ELSE
        INO=1
        IF (I.NE.1) THEN
           DO  J=1,I-1
              IF (IATOM(J).EQ.IATOM(I).AND.IHCODE(J).EQ.IHCODE(I)) INO=INO+1
           enddo
        ENDIF
        MEM1=ELS(IATOM(I))
        LM=2
        IF (MEM1(2:2).EQ.' ') LM=1
        IF (IHCODE(I).GT.0) THEN
           LM=LM+1
           MEM1(LM:LM)='C'
        ENDIF
        IF (INO.GE.2) THEN
           LM=LM+1
           MEM1(LM:LM)=CHAR(INO+48)
        ENDIF
     ENDIF
     !
     !	All options call OUTNAM to open output file
     !
     CALL OUTNAM (20,PRDSN,MEM1,*60)
     CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*60)
     CALL GETDAT (CDATE,CTIME,OUTFILE)
     IF (V0EV(I).NE.0..AND.IATOM(I).NE.0) THEN
        WRITE (OUTFILE,80) &
             ELS(IATOM(I)),PM(ION(I)),IATOM(I),RMTR(I),ALF(I),CMAG(I),IHCODE(I),V0EV(I),FEEV(I),RHO0S(I),RLIFEEV(I)
     ENDIF
     WRITE (OUTFILE,*) real(IATOM(I),kind=dp),real(IHCODE(I),kind=dp)
     DO  K=1,M
	IF (IDIV.EQ.2) THEN
           IF (CHAROPT(1:1).NE.'C') THEN
              WRITE (OUTFILE,70) EN(K,I),EREF(K,I)
           ELSE
              !	    EE=-E02
              !
              !	What do we do about this - need a number to correspond to the MTR really
              !
              EE=0.
              EEE=EN(K,I)+EE
              WRITE (OUTFILE,70) EEE,EE,VPI02(0)
           ENDIF
           DO  L=1,LMAX1
              JL=L+L-1
              WSPACE(JL)=PHS(K,I,L)
              IF (L.GT.1.AND.ABS(WSPACE(JL)).LT.0.000001) GOTO 39
              WSPACE(JL+1)=AIMAG(PHS(K,I,L))
           enddo
	ELSE
           WRITE (OUTFILE,70) EN(K,I)
           DO  L=1,LMAX1
              WSPACE(L)=PHS(K,I,L)
              IF (ABS(WSPACE(L)).LT.0.000001.AND.MOD(L,8).NE.2) GOTO 39
           enddo
	ENDIF
        !
        !	Write phaseshifts
        !
	L=LMAX1+1
39	if (charopt(1:1).ne.'P') then
           WRITE (OUTFILE,90) (WSPACE(J),J=1,(L-1)*IDIV)
	else
           WRITE (OUTFILE,91) (WSPACE(J),J=1,(L-1)*IDIV)
	endif
     enddo
     !
     !	Close file and issue message
     !
     CALL EFILE ('Phase shifts')
     LFS(I+IPARNSP+1)=PRDSN
  enddo
60 RETURN
  !
70 FORMAT (3F14.6)
80 FORMAT (A2,A1,' phaseshifts (Z=',I3,').'/'mtr',F7.4,' alpha',F7.3,' madelung const.',F6.2, &
       ' hole-code',I2,' V0',F9.4/' Ef',F9.4,' RHO0',F9.4,' Rlife',F9.4)
90 FORMAT (8F10.6)
91 FORMAT (52F10.6)
END SUBROUTINE PHPRINT
