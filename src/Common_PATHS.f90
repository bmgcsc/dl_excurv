MODULE Common_PATHS

!      COMMON /PATHS/ SPECTRE(IPARNPOINTS,IPARPATH,IPARNSP),
!     1NUMPATHS,PATHINDEX(IPARPATH),IPATHINDS(5,IPARPATH),
!     1JPATHINDS(5,IPARPATH),PATHPL(IPARPATH),PATHANG(IPARPATH),IPATHOCC(
!     2IPARPATH),PATHMAX(IPARPATH1,IPARNSP),SPECTRA(IPARNPOINTS,IPARPATH,
!     3IPARNSP),PL(0:25),NPATHS,PAT
!     4HFLAG(IPARPATH1),FILTER,PATHOVER,PATH1OVER
!      LOGICAL PATHFLAG,FILTER,PATHOVER,PATH1OVER
!      COMPLEX SPECTRE

Use Definition
Use Parameters
implicit none
private

complex(imag), dimension(:,:,:), public ::  SPECTRE(IPARNPOINTS,IPARPATH,IPARNSP)
integer(i4b), public :: NUMPATHS
real(dp),     dimension(:),     public ::   PATHINDEX(IPARPATH)
integer(i4b), dimension(:,:),   public :: IPATHINDS(5,IPARPATH)
integer(i4b), dimension(:,:),   public :: JPATHINDS(5,IPARPATH)
real(dp),     dimension(:),     public ::      PATHPL(IPARPATH)
real(dp),     dimension(:),     public ::     PATHANG(IPARPATH)
integer(i4b), dimension(:),     public ::    IPATHOCC(IPARPATH)
real(dp),     dimension(:,:),   public ::     PATHMAX(IPARPATH1,IPARNSP)
real(dp),     dimension(:,:,:), public ::    SPECTRA(IPARNPOINTS,IPARPATH,IPARNSP)
real(dp),     dimension(:),     public ::    PL(0:25)
integer(i4b), public :: NPATHS
logical,      dimension(:),     public :: PATHFLAG(IPARPATH1)
logical,      public :: FILTER,PATHOVER,PATH1OVER
end MODULE Common_PATHS
