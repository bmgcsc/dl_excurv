SUBROUTINE PLPAR (ITB)
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_F
  Use Common_ELS
  Use Index_IGF
  Use Common_PMTX
  Use Index_INDS
  Use Common_DISTANCE
  Use Common_VAR
  Use Include_IPA
  Use Include_PA1
  !
  !	Prepare and output parameter tables
  !
  CHARACTER*12 IST(3)
  DATA IST/'Experiment','Parameters','Phaseshifts'/
  !
  !	IGF(9)=4 , no parameter table required
  !	Hard copy requires parameters if VFORMAT is 2 however
  !
  CALL SETTAB (0)
  CALL PLACE (0,ITB)
  !	CALL TYPECS('1234567890123456789012345678901234567890')
  !	CALL CRLNFD
  !	CALL SPACE (20)
  !	CALL TYPECS ('1234567890')
  !	CALL CRLNFD
  IFIELD=5
  IF (IGF(9).GT.2) GOTO 100
  DO  JK=1,7+NPS
     J=JK
     IF (JK.EQ.1) J=inds(INDEF0)
     IF (JK.EQ.2) J=inds(INDVPI0)
     IF (JK.EQ.3) J=inds(INDAFAC0)
     IF (JK.EQ.4) J=inds(INDEMIN0)
     IF (JK.EQ.5) J=inds(INDEMAX0)
     IF (JK.EQ.6) J=inds(INDESUM0)
     IF (JK.EQ.7) J=inds(INDLMAX)
     IF (JK.GE.8) J=inds(INDMTR) +J-8
     CALL TYPECS (IVARS(J)(1:6))
     CALL TYPENF (PA1(J),-2,IFIELD)
     IF (MOD(JK,3).EQ.0.OR.JK.EQ.7+NPS) CALL CRLNFD
  enddo
  IF (iset(ISING).NE.0) THEN
     DO JK=1,NSPEC
        DO  J=inds(INDBETH0)+JK,inds(INDBEW0)+JK,IPARNSP+1
           CALL TYPECS (IVARS(J)(1:6))
           CALL TYPENF (PA1(J),-2,IFIELD)
        enddo
        CALL CRLNFD
     enddo
  ENDIF
  !
  !	See if any special options have been used :
  !
  NFLAG=0
  DO  I=1,NPS
     !
     !	If XE used, display XE
     !
     IF (XE(I).NE.0.) THEN
        NFLAG=NFLAG+1
        CALL TYPECS (IVARS(inds(INDXE)+I-1)(1:6))
        CALL TYPENF (XE(I),-2,IFIELD)
     ENDIF
     IF (NFLAG.NE.0.AND.(MOD(NFLAG,3).EQ.0.OR.I.EQ.NPS)) CALL CRLNFD
  enddo
  IF (WD.NE.0) THEN
     CALL TYPECS (IVARS(inds(INDWEX0))(1:6))
     CALL TYPENF (WEX0(0),-2,IFIELD)
     CALL TYPECS (IVARS(inds(INDWD))(1:6))
     CALL TYPENF (WD,-2,IFIELD)
     CALL TYPECS (IVARS(inds(INDWA))(1:6))
     CALL TYPENF (WA,-2,IFIELD)
  ENDIF
  CALL CRLNFD
  DO  I=1,NS
     IF (B(I).NE.0..OR.C(I).NE.0.OR.D(I).NE.0.) THEN
        CALL TYPECS (IVARS(inds(INDB)+I)(1:4))
        CALL TYPENF (B(I),3,6)
        CALL TYPECS (IVARS(inds(INDC)+I)(1:4))
        CALL TYPENF (C(I),4,6)
        CALL TYPECS (IVARS(inds(INDD)+I)(1:4))
        CALL TYPENF (D(I),3,6)
        CALL CRLNFD
     ENDIF
  enddo
  !  80	IF (IZZ.GT.1) THEN
  !	  CALL TYPECS ('FT (')
  !	  CALL SPACE (-1)
  !	  DO 90 I=1,7
  !	  CALL TYPENI (IFTSET(I),1)
  !  90	  CALL SPACE (-1)
  !	  CALL TYPECS (') ')
  !	ENDIF
  !	IF (IZZ.EQ.3) GOTO 160
100 CALL TYPECS ('FI')
  CALL TYPENF (FITINDEX,-5,8)
  CALL TYPECS ('R ')
  CALL TYPENF (RFAC,-3,6)
  IF (WD.NE.0.) THEN
     CALL TYPECS ('Rex ')
     CALL TYPENF (REXAFS,-3,6)
     CALL TYPECS ('Rd ')
     CALL TYPENF (RDISTANCE,-3,6)
     CALL CRLNFD
     CALL LINEFD (1)
     DO  I=1,NS
        II=0
        DO J=0,I-1
           IF (WEIGHTINGS(I,J).NE.0.) II=J+1
        ENDDO
        IF (II.EQ.0.) cycle
        CALL TYPECS ('D'//CHAR(48+I)//'n')
        DO  J=0,II-1
           CALL TYPENF (DISTANCES(I,J),3,7)
           CALL TYPECS('(')
           IF (J.EQ.0.) THEN
              DIST=R(I)
           ELSE
              DIST1=(UX(I)-UX(J))
              DIST2=(UY(I)-UY(J))
              DIST3=(UZ(I)-UZ(J))
              DIST=SQRT(DIST1*DIST1+DIST2*DIST2+DIST3*DIST3)
           ENDIF
           CALL TYPENF (DIST,3,7)
           CALL TYPECS (')')
        enddo
        CALL CRLNFD
        CALL LINECHECK (*200)
     enddo
     CALL CRLNFD
  ELSE
     CALL CRLNFD
     CALL LINEFD (1)
  ENDIF
  DO  J=0,NS
     CALL LINECHECK (*200)
     DO  I=inds(INDN),inds(INDA),IPARNS+1
	L=J+I
        !	NCHAR1=3
	CALL TYPECS (IVARS(L)(1:3))
	IF (I.NE.inds(INDT)) THEN
           NUM=3
           NUM1=6
           IF (I.EQ.inds(INDN)) THEN
              NUM=1
              NUM1=5
           ENDIF
           !
           !	PC ONLY
           !
           IF (I.EQ.inds(INDA)) NUM1=5
           CALL TYPENF (PA1(L),NUM,NUM1)
	ELSE
           NUM=1
           !
           !	Probably PC only
           !
           IF (IGF(2).EQ.1) NUM=2
           !	  call all_IPA
           CALL TYPENI (IPA(L),NUM)
           !	  call IPA_all
           IF (IATOM(IPA(L)).NE.0) THEN
              IF (IGF(2).EQ.1.AND.IGF(ITNUM).NE.5) CALL SPACE (-1)
              !	    call all_IPA
              CALL TYPECS ('('//ELS(IATOM(IPA(L)))//')')
              !	    call IPA_all
           ELSE
              CALL SPACE (3)
           ENDIF
	ENDIF
     enddo
     CALL TYPENI (ICLUS(J),2)
     CALL TYPENI (NU(J),2)
     CALL CRLNFD
  enddo
  CALL CRLNFD
  IF (IGF(9).EQ.3) GOTO 160
  J=1
  DO  I=1,NPS+IPARNSP+1
     IF (IGF(9).EQ.2.AND.I.GT.3) GOTO 160
     CALL LINECHECK (*200)
     IF (I.EQ.1.OR.I.EQ.4.OR.I.EQ.5) THEN
        CALL TYPECS (IST(J))
        J=J+1
     ELSE
        IF (LFS(I).EQ.' ') cycle
        CALL SPACE (24)
     ENDIF
     CALL TYPECS (LFS(I)(1:30))
     CALL CRLNFD
     IF (LFS(I)(31:).NE.' ') THEN
        CALL SPACE (12)
        CALL TYPECS (LFS(I)(31:60))
     ENDIF
  enddo
  !
  !	If an XADD spectrum is in use display titles
  !
160 IF (IXA.NE.3) GOTO 200
  CALL CRLNFD
  DO  I=1,IPARNS+2
     IF (MSTL(I).EQ.' ') GOTO 200
     CALL LINECHECK (*200)
     CALL TYPECS (MSTL(I))
     CALL CRLNFD
  enddo
  !	GOTO 200
  ! 180	DO 190 J=inds(INDATOM),inds(INDCMAG),IPARNP
  !	L=J+JJ-1
  !	CALL TYPECS (IVARS(L))
  !	CALL TYPENF (PA1(L),3,7)
  !	CALL SPACE (1)
  !	IF (J.EQ.inds(INDMTR)) THEN
  !	  CALL CRLNFD
  !	ENDIF
  ! 190	CONTINUE
200 RETURN
END SUBROUTINE PLPAR
