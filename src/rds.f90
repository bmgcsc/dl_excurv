SUBROUTINE RDS (KUN,ICOL,ISP,*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Include_IPA
  Use Include_PA1
  Use Include_XY
  !
  !	Subroutine to read in an experimental file using RDSPEC
  !
  !	   KUN (I)   Unit number for input spectrum
  !	  ICOL (I)   Column combination
  !	   ISP (I)   Point frequency
  !
  !	RETURN 1     Error return
  !
  DIMENSION TENER(IPARNPOINTS,IPARNSP)
  SAVE TENER
  CHARACTER*80 TITL
  !
  !	Read in file
  !
  ICODE=ISP
  !	IF (LHEAD.NE.0) ICODE=-ISP
  NP1=IPARNPOINTS
  IF (IEXP.EQ.1) THEN
     CALL RDSPEC (KUN,NP1,TITL,TENER,ICOL/10,XABS(1,IEXP),MOD(ICOL,10),ICODE,*10)
     DO I=1,NP1
        TENER(I,IEXP)=TENER(I,IEXP)-E0
     ENDDO
     CALL UCOPY (TENER,ENER,NP1)
     NP=NP1
     WRITE (7,'(A,I2,I5,2F9.3)') 'SPECTRUM : ',IEXP,NP,TENER(1,1),TENER(NP,1)
  ELSE
     CALL RDSPEC (KUN,NP1,TITL,TENER(1,IEXP),ICOL/10,XABS(1,IEXP),MOD(ICOL,10),ICODE,*10)
     DO I=1,NP1
        TENER(I,IEXP)=TENER(I,IEXP)-E0
     ENDDO
     WRITE (7,'(A,I2,I5,2(F9.3,F7.3))') 'SPECTRUM : ',IEXP,NP1,TENER(1,IEXP),XABS(1,IEXP),TENER(NP1,IEXP),XABS(NP1,IEXP)
     CALL SPLINT1 (NP1,TENER(1,IEXP),XABS(1,IEXP),NP,TENER,0,.TRUE.,*25)
     WRITE (7,'(A,2I5,4F9.3)') 'SPECTRUM IEXP:',IEXP,NP1,TENER(1,IEXP),XABS(1,IEXP),TENER(NP,IEXP),XABS(NP,IEXP)
  ENDIF
  CALL UCOPY (TENER,ENER,NP)
  IF (NP.GE.3) RETURN
  !
  !	Error condition
  !
10 CALL ERRS(*20)
20 CALL ERRMSG ('Less than three points in spectrum',*21)
21 WRITE (OUTTERM,30) ICOL,ISP
25 RETURN 1
30 FORMAT (' Column combination',I3,' Point frequency',I3)
END SUBROUTINE RDS
