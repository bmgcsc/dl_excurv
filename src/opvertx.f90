SUBROUTINE OPVERTX (APHI,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about plane //z at 0 to x
  !
  Use Common_convia
  APHI=2.*PI-APHI
  IS=IS+1
  RETURN 1
END SUBROUTINE OPVERTX
