SUBROUTINE MINT (RK,IPT)
  !======================================================================C
  Use Definition
  REAL(dp)  ARR(4,4),E(100),RK(*)
  IF (IPT.GT.100) WRITE (6,*) 'soddit, ipt>100',IPT
  DO I=1,IPT
     E(I)=real(I,kind=dp)
  enddo
  DO J=2,IPT,2
     N1=J-3
     IF (N1.LE.1) N1=1
     N2=IPT-2
     IF (N1+6.LT.N2) N2=N1+6
     N1=N2-6
     DO K=N1,N2,2
        KL=(K-N1+2)/2
        ARR(1,KL)=(RK(K)*(E(J)-E(K+2))-RK(2+K)*(E(J)-E(K)))/(E(K)-E(K+2))
     enddo
     DO M=2,4
        NMAX=N2-2*M+2
        DO  K=N1,NMAX,2
           KPM=K+M+M
           KL=(K-N1+2)/2
           ARR(M,KL)=(ARR(M-1,KL)*(E(J)-E(KPM))-ARR(M-1,KL+1)*(E(J)-E(K)))/(E(K)-E(KPM))
        enddo
     enddo
     RK(J)=ARR(4,1)
  enddo
  RETURN
END SUBROUTINE MINT
