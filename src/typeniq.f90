SUBROUTINE TYPENIQ (IIN,NIN)
  !======================================================================C
  Use definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_GRAPHICS

  CHARACTER USTRING*14
  WRITE (USTRING,'(I14)') IIN
  IF (QDQ) THEN
     CALL FQQGETGP2 (TXMIN,TYMIN)
     AAA=FQUEERY (USTRING(14-NIN+1:14))*XDIFF
     CALL FQQRMOVE2 (real(NIN,kind=dp)-AAA,0.)
     CALL FQQDRAWSTRF (USTRING(14-NIN+1:14))
     CALL FQQMOVE2 (TXMIN+real(NIN,kind=dp),TYMIN)
  ELSE
     CALL FDRAWSTRF (USTRING(14-NIN+1:14))
  ENDIF
  RETURN
END SUBROUTINE TYPENIQ
