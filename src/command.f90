SUBROUTINE COMMAND
  !======================================================================C
  Use Common_DLV
  Use Parameters
  Use Common_risch
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VTIME
  Use Common_PLFLAG
  Use Common_AIL
  Use Index_IGF
  Use Common_PMTX
  Use Common_COMPAR
  Use Common_ICF
  Use Common_PATHS
  Use Common_RCBUF
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  Use Include_FT
  !
  !	This routine requests commands and calls the appropriate command
  !
  !	Commands normally call a subroutine with the name of the command
  !	( or abbreviation )
  !	One exception is COPLOT ( call to GPLOT ).
  !
  !	Help and *command processing is part of CREAD/AREAD.
  !
  CHARACTER*120 :: TEMPSTR
  integer(i4b),parameter :: No_com = 48, No_abr = 9
  CHARACTER*8   :: COM(No_com)
  character*11  :: cdate
  character*10  :: ctime
  CHARACTER*4   :: ABBR(No_abr)
  integer(i4b)  ::  JOP(No_abr)
  LOGICAL       :: QUIET
  data COM/ &
       'CHANGE','COMPARE','COPLOT',  'END',  'INQUIRE','FFILTER ','RECOVER', 'FIT',     'FTSET',   'GSET', &
       'REFINE','LIST',   'MAP',     'RULE', 'SITE',   'EXPAND',  'PLOT',    'PRINT',   'READ',    'TIME', &
       'XADD',  'DISPLAY','CCHANGE', 'SET',  'DRAW',   'STATS',   'XANES',   'SYMMETRY','SUBMIT',  'EN',   &
       'ALIAS', 'SAVE',   'TABLE',   'SORT', 'CALC',   'SPARE',   'EXTRACT', 'PLANE',   'IDEALISE','SPIN', &
       'UNDO',  'INFO',   'GENERATE','ANGLE','PATH',   'DLV',     'PDLV',   'LDLV'/
  data ABBR/'C','CM','CP','FI','L','P','R','S','LD'/
  data  JOP/ 1 , 2  , 3  , 8  , 12, 17, 19, 24, 48/
  !
  !	Request and find command
  !
  ID=0
  NPROMPT=0
  GOTO 40
  !
  !	Here for an error in a keyword parameter.
  !
10 CALL ERRMSG ('Keyword not found: '//KEYWORD,*20)
  !
  !	Here for all SERIOUS errors.
  !	ERRS resets various flags, terminates macros, empties terminal
  !	buffers and current input string so as to avoid further problems.
  !
20 CALL ERRS (*40)
  !
  !	Set to alpha screen after blank/help etc. command if graphics
  !
30 IF (PLCMD) CALL ALPHAM
  !
  !	Enter command
  !
40 IF (LRIS.EQ.0) THEN
     IF (PLCMD.AND.IGF(2).EQ.1) THEN
        CALL TPOS (0,740)
        IF (IGF(12).EQ.1) THEN
           if(.not.DLV_flag) then
              CALL WTEXT('ENTER COMMAND:')
           elseif(DLV_flag)then
              CALL WTEXT('====================================================================================')
           endif
           CALL TPOS (200,740)
        ENDIF
     ELSE
        IF (PLCMD) CALL ALPHAM
        if(.not.DLV_flag) then
           CALL WTEXT('ENTER COMMAND:')
        elseif(DLV_flag)then
           CALL WTEXT('=====================================================================================')
        endif
     ENDIF
  ENDIF
  LEVAL=0
50 CALL CREAD (COMWORD,KEYWORD,CHAROPT,INTOPT,NUMFLAG,FPOPT,*30,*40)
  LKEYWORD=NCSTR(KEYWORD)
  IF (PLCMD.AND.LRIS.LE.0.AND.LMDSN.LE.0) THEN
     CALL ALPHAM
     IF (LSTBUF.NE.' ') THEN
        if(.not.DLV_flag) then
           CALL WTEXT('ENTER COMMAND:')
        elseif(DLV_flag)then
           CALL WTEXT('====================================================================================')
        endif
        CALL WTEXT (LSTBUF)
     ENDIF
  ENDIF
  DO I=1,No_abr
     IF (COMWORD.NE.ABBR(I)) GOTO 60
     IOP=JOP(I)
     GOTO 90
60   CONTINUE
  enddo
  !  print *,com,comword,iop
  !  pause
  CALL GETOPT (COM,No_com,COMWORD,IOP,*70)
  !  print *,com,comword,iop
  !  pause
  GOTO 90
70 IF (COMWORD(1:1).EQ.'?') GOTO 50
  IF (NALIAS.GT.0.AND.NALIAS.LE.IPARAL) THEN
     DO I=1,NALIAS
        IF (COMWORD(1:4).EQ.ALID(I)(1:4)) THEN
           TEMPSTR=RIS
           J=NCSTR(ALSTRING(I))
           K=LEN(RIS)
           RIS=ALSTRING(I)
           IF (LRIS.GT.0) THEN
              RIS(J+1:J+1)=';'
              RIS(J+2:K)=TEMPSTR
              LRIS=LRIS+1
           ENDIF
           LRIS=LRIS+J
           GOTO 80
        ENDIF
     ENDDO
  ENDIF
  IPR=1
  CALL WTEXT ('Command '//COMWORD//' not found.')
  GOTO 20
80 LEVAL=LEVAL+1
  IF (LEVAL.GT.20) THEN
     IPR=1
     CALL WTEXT ('Invalid recursive use of alias.')
     GOTO 20
  ELSE
     GOTO 50
  ENDIF
  !
  !	PROCESS OPTIONS
  !
90 NPROMPT=0
  !  print *,' IOP =', iop
  !  pause
  GOTO ( &
       100,110,120,130,140,150,160,170,180,190, &
       200,210,220,230,240,430,250,260,270,280, &
       290,350,300,310,360,320,330,340,370,380, &
       380,390,400,410,420,440,450,460,470,480, &
       490,500,501,609,700,710,720,730),IOP
  !
  !	OPTION 'CHANGE'
  !
100 CALL CHANGE(*20)
  OPEN (47,FILE='recover.par',FORM='UNFORMATTED',STATUS='UNKNOWN')
  WRITE (47) PA1
  CLOSE (47)
  GOTO 40
  !
  !	OPTION 'COMPARE'
  !
110 CALL COMPARE (*20)
  GOTO 40
  !
  !	OPTION 'COPLOT'
  !
120 J=5
  CENTRED=0.
  IF (IGF(1).EQ.1.AND.NSPEC.EQ.1) THEN
     !	  IF (IGF(ITNUM).EQ.5.OR.(IGF(2).NE.1.AND.IGF(IPLOTTR).EQ.2.OR.IGF(IPLOTTR).EQ.4)
     !     ) CENTRED=1.311/5.
     IF (IGF(ITNUM).EQ.5.OR.(IGF(2).NE.1)) CENTRED=1.311/5.
  ENDIF
  CALL GPLOT (J,INTOPT)
  CENTRED=0.
  GOTO 40
  !
  !	OPTION 'END'
  !
130 RETURN
  !
  !	OPTION 'INQUIRE'
  !
140 CALL INQU
  GOTO 40
  !
  !	OPTION 'FFILTER'
  !
150 CALL FFILTER
  GOTO 40
  !
  !	OPTION 'FILE'
  !
160 CALL RECOVER
  GOTO 40
  !
  !	OPTION 'FIT'
  !
170 IF (NUMFLAG.EQ.0) INTOPT=iset(IWEIGHT)
  CALL FIT (KEYWORD(1:1),INTOPT,*20)
  GOTO 40
  !
  !	OPTION 'FTSET'
  !
180 CALL FTSET (KEYWORD,CHAROPT,INTOPT,*40)
  GOTO 40
  !
  !	OPTION 'GSET'
  !
190 CALL GSET (KEYWORD,CHAROPT,INTOPT,*40)
  GOTO 40
  !
  !       OPTION 'ITERATE'
  !
200 CALL ITER
  GOTO 40
  !
  !       OPTION 'LIST'
  !
210 CALL LIST(KEYWORD,CHAROPT)
  GOTO 40
  !
  !       OPTION 'MAP'
  !
220 CALL MAPQ
  GOTO 40
  !
  !	Option 'RULE'
  !
230 CALL RULE
  GOTO 40
  !
  !	Option 'SITE'
  !
240 CALL SITE
  GOTO 40
  !
  !	Option 'PLOT'
  !
250 CALL PLOT
  GOTO 40
  !
  !	Option 'PRINT'
  !
260 CALL PRINT
  GOTO 40
  !
  !	Option 'READ'
  !
270 CALL READQ
  GOTO 40
  !
  !	OPTION 'TIME'
  !
280 CALL GETTIM (1,CPU,ELAPS)
  CALL GETDAT (CDATE,CTIME,0)
  WRITE (OUTTERM,510) CDATE,CTIME,CPU,ELAPS
  WRITE (LOGFILE,510) CDATE,CTIME,CPU,ELAPS
  WRITE (OUTTERM,520)
  WRITE (LOGFILE,520)
  !	VTIME(13)=VTIME(11)-VTIME(3)-VTIME(4)-VTIME(6)-VTIME(7)-VTIME(9)-VTIME(10)-VTIME(12)
  !-st	VTIME(13)=0.
  WRITE (OUTTERM,530) (VTIME(I),I=1,10)
  WRITE (LOGFILE,530) (VTIME(I),I=1,10)
  GOTO 40
  !
  !	OPTION 'XADD'
  !
290 CALL XADD (KEYWORD,INTOPT,*20)
  GOTO 40
  !
  !       OPTION 'CCHANGE'
  !
300 CALL CCHANGE(*20)
  OPEN (47,FILE='recover.par',FORM='UNFORMATTED',STATUS='UNKNOWN')
  WRITE (47) PA1
  CLOSE (47)
  GOTO 40
  !
  !	OPTION 'SET'
  !
310 CALL SET (KEYWORD,CHAROPT,INTOPT,*40)
  GOTO 40
  !
  !       OPTION 'STATS'
  !
320 CALL NEWSTAT
  GOTO 40
  !
  !       OPTION 'XANES'
  !
330 CALL XANES (*20)
  GOTO 40
340 QUIET=(CHAROPT(1:1).EQ.'Q'.OR.LMDSN.GT.0)
  CALL SYMMETRY (QUIET,.TRUE.,*20)
  GOTO 40
350 CALL DISPLAY (OUTTERM)
  GOTO 40
360 CALL DRAWQ (*20)
  GOTO 40
370 CALL SUBMITQ
  GOTO 40
  !
  !	Here for ALIAS
  !
380 CALL ALIAS
  GOTO 40
390 CALL SAVE
  GOTO 40
400 CALL TABLE
  GOTO 40
410 CALL SORT
  GOTO 40
  !
  !	OPTION 'CALC'
  !
420 CALL CALC
  GOTO 40
430 CALL EXPAND
  GOTO 40
440 CALL SPFIX
  GOTO 40
450 CALL EXTRACT
  GOTO 40
460 CALL PLANE
  GOTO 40
470 CALL IDEALISE
  GOTO 40
480 CALL SPIN
  GOTO 40
490 CALL UNDO
  GOTO 40
  !
  !	SPARE SLOT
  !
500 CALL INFO
  GOTO 40
  !
  !	SPARE SLOT
  !
501 CALL GENERATE
  GOTO 40
  !
  !	SPARE SLOT
  !
609 CALL ANGLE
  GOTO 40
  !
  !	
  !
700 CALL PATH
  GOTO 40
710 CALL DLV
  GOTO 40
720 CALL PDLV
  GOTO 40
730 CALL LDLV(KEYWORD,CHAROPT)
  GOTO 40
  !
510 FORMAT (/,' Date is ',A10,'   Time is ',A8,/,' Total CPU time used =',F10.2,'   elapsed time =',F10.2)
520 FORMAT ('   SETUP   EXCAL  MSCAL2  MSCAL  CALCPATH    IA1   ','TMATR MSCAL2I MSCALI      KA3')
530 FORMAT (13F8.3)
end subroutine COMMAND
