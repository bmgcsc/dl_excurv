SUBROUTINE CALCMAP (IARG)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_ICF
  Use Common_IP
  Use Index_INDS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  OPEN (77,FILE='MAP',STATUS='UNKNOWN')
  !
  !	Map
  !
  id=1
  INT=INTOPT
  JJ=NCALL
  NCALL=2
  DO L=1,JJ,2
     RV1=PA1(IVNUMS(L))
     RV2=PA1(IVNUMS(L+1))
     XSTEP=(XP(L)-U(L))/real(INT-1,kind=dp)
     YSTEP=(XP(L+1)-U(L+1))/real(INT-1,kind=dp)
     XSTART=U(L)
     YSTART=U(L+1)
     IVNUMS(1)=IVNUMS(L)
     IVNUMS(2)=IVNUMS(L+1)
     WRITE (77,310) INT,IVNUMS(1),IVNUMS(2),(U(I),I=L,L+1),(XP(I),I=L,L+1),IVCH(1),IVCH(2)
     SF(2)=YSTART
     !	    TIMELEFT=.TRUE.
     DO I=0,INT-1
        XP(2)=1.
        SF(1)=XSTART
        DO J=0,INT-1
           !	    CALL TCHECK (*150)
           !	    GOTO 170
           ! 150	    TIMELEFT=.FALSE.
           !	    DO 160 IW=PLOTFILE,OUTTERM,IDIFF
           ! 160	    WRITE (IW,*) 'Time limit exceeded during map at call ',ICALL
170        XP(1)=1.
           XP(2)=1.
           !	    IF (TIMELEFT) CALL FIT (' ',iset(IWEIGHT),*210)
           CALL FIT (' ',iset(IWEIGHT),*210)
           ICALL=ICALL+1
           WRITE (77,320) RFAC,SF(1),SF(2),AMIN1(FITINDEX*1.E3,9999.),RDISTANCE
           SF(1)=SF(1)+XSTEP
        enddo
        SF(2)=SF(2)+YSTEP
     enddo
     NUMFLAG=2
     CALL PCHANGE (IVCH(1),RV1)
     CALL PCHANGE (IVCH(2),RV2)
  enddo
210 CLOSE (77)
  RETURN
310 FORMAT (I3,2I5,4F13.4,2(1X,A6))
320 FORMAT (F15.5,4F10.4)
END SUBROUTINE CALCMAP
