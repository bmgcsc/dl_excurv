SUBROUTINE DRAWQQ (UXM,INCLUS,CLEAR,IUCLUS)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_PMTX
  Use Common_UPU
  Use Common_DISTANCE
  Use Common_VIEW
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  DIMENSION :: RADIUS(0:IPARATOMS),AMAG(0:IPARATOMS),AMAG1(0:IPARATOMS)
  dimension :: INDEX(0:IPARATOMS),TX(0:IPARATOMS),TY(0:IPARATOMS)
  dimension :: TX1(10),TY1(10),TX2(10),TY2(10),ANGLE(10),IATOVER(0:IPARATOMS)
  dimension :: XSIGN(8),YSIGN(8)
  dimension :: UXY(4)
  COMPLEX :: P3D,TXYZ,TXY2
  LOGICAL :: COND,REVLABEL(0:IPARATOMS),QUAD(0:IPARATOMS,8),NOLABEL(0:IPARATOMS)
  logical :: OVERLAPS(0:IPARATOMS),INTERCEPT,CLEAR,LBOND,TBOND,IUCLUS
  CHARACTER :: KABEL*5,ILB(4)*4
  CHARACTER*2 :: KP1,KP2,KP3
  !  EQUIVALENCE (UXMIN,UXY(1)), (UXMAX,UXY(2)),(UYMIN,UXY(3)),(UYMAX,UXY(4))
  !-st above EQUIVALENCE is in the scope of only this subroutine
  SAVE UXT
  DATA XSIGN/.707,.707,-.707,-1.,0.,1.,0.,-1./,YSIGN/.707,-.707,-.707,1.,1.,0.,-1.,0./
  DATA ILB/'XMIN','XMAX','YMIN','YMAX'/

  UXY(1)= UXMIN
  UXY(2)= UXMAX
  UXY(3)= UYMIN
  UXY(4)= UYMAX

  IF (IUCLUS) THEN
     JCLUS=INCLUS
     IUNIT=0
  ELSE
     JCLUS=IABS(ICLUS(NR(INCLUS)))
     IUNIT=INCLUS
  ENDIF
  CALL LUZERO (QUAD,(IPARATOMS+1)*8)
  TXYZ=P3D(0.,0.,0.)
  RADIUS(0)=AMAX1(RMTR(1)*SIZE,.0025*UXM)
  TX(0)=REAL(TXYZ)
  TY(0)=AIMAG(TXYZ)
  AMAG(0)=1.
  REVLABEL(0)=.FALSE.
  OVERLAPS(0)=.FALSE.
  NOLABEL(0)=.FALSE.
  IF (BOND.EQ.0.) THEN
     ABOND=AR(1,JCLUS)+.1
  ELSE
     ABOND=BOND
  ENDIF
  WRITE (7,*) 'BOND',ABOND,AR(1,JCLUS)
  V1=SQRT(VVX*VVX+VVY*VVY+VVZ*VVZ)
  IF (CLEAR) UXT=0.
  DO  I=1,NAT(JCLUS)
     REVLABEL(I)=.FALSE.
     OVERLAPS(I)=.FALSE.
     NOLABEL(I)=.FALSE.
     ASIZE=AMAX1(RMTR(IT(ISHELL(I,JCLUS)))*SIZE,.0025*UXM)
     VV1=VVX-AX(I,JCLUS)
     VV2=VVY-AY(I,JCLUS)
     VV3=VVZ-AZ(I,JCLUS)
     V2=SQRT(VV1*VV1+VV2*VV2+VV3*VV3)
     AMAG(I)=V1/V2
     ASIZE=ASIZE*AMAG(I)
     TXYZ=P3D(AX(I,JCLUS),AY(I,JCLUS),AZ(I,JCLUS))
     TTX=REAL(TXYZ)
     TTY=AIMAG(TXYZ)
     IF (CLEAR) UXT=AMAX1(UXT,ABS(TTX)+ASIZE*1.5,ABS(TTY)+ASIZE*1.5)
     TX(I)=TTX
     TY(I)=TTY
     RADIUS(I)=ASIZE
  enddo
  UXT=UXT*1.1
  CALL MAPP (-UXT,UXT,-UXT,UXT)
  IF (IGF(7).EQ.2.AND..NOT.BACKGROUND) THEN
20   DO  I=1,4
30      WRITE (OUTTERM,150) ILB(I)
        CALL CREAD (KP1,KP2,KP3,IN1,IC,UXY(I),*30,*130)
        IF (IC.NE.1) CALL ERRS (*30)
     enddo
     UXMIN=UXY(1)
     UXMAX=UXY(2)
     UYMIN=UXY(3)
     UYMAX=UXY(4)
     IF (UXMAX-UXMIN.LE.0.OR.UYMAX-UYMIN.LE.0) CALL ERRS(*20)
     CALL MAPP (UXMIN,UXMAX,UYMIN,UYMAX)
     UXY(1)= UXMIN
     UXY(2)= UXMAX
     UXY(3)= UYMIN
     UXY(4)= UYMAX
  ENDIF
  UXT=UXT/1.1
  IF (IGF(6).NE.2.AND.CLEAR) THEN
     CALL WHITEPEN
     TXYZ=P3D(UXT,0.,0.)
     CALL FCENTERTEXT (1)
     CALL PLOTCSX (REAL(TXYZ)*1.06,AIMAG(TXYZ)*1.06,'x')
     CALL POSITN (REAL(TXYZ),AIMAG(TXYZ))
     TXYZ=P3D(-UXT,0.,0.)
     CALL JOIN (REAL(TXYZ),AIMAG(TXYZ))
     TXYZ=P3D(0.,UXT,0.)
     CALL PLOTCSX (REAL(TXYZ)*1.06,AIMAG(TXYZ)*1.06,'y')
     CALL POSITN (REAL(TXYZ),AIMAG(TXYZ))
     TXYZ=P3D(0.,-UXT,0.)
     CALL JOIN (REAL(TXYZ),AIMAG(TXYZ))
     TXYZ=P3D(0.,0.,UXT)
     CALL PLOTCSX (REAL(TXYZ)*1.06,AIMAG(TXYZ)*1.06,'z')
     CALL POSITN (REAL(TXYZ),AIMAG(TXYZ))
     TXYZ=P3D(0.,0.,-UXT)
     CALL JOIN (REAL(TXYZ),AIMAG(TXYZ))
     CALL FCENTERTEXT (0)
  ENDIF
  !	CALL THICK(2)
  CALL UCOPY (AMAG,AMAG1,NAT(JCLUS)+1)
  CALL SORTTAB (AMAG1,NAT(JCLUS)+1,INDEX)
  IF (DEBUG) WRITE (LOGFILE,*) 'Sorted table:'
  DO I=0,NAT(JCLUS)
     INDEX(I)=INDEX(I)-1
     IF (DEBUG) WRITE (LOGFILE,140) I,AMAG1(I),INDEX(I),TX(I),TY(I),RADIUS(I)
  ENDDO
  IF (NAT(JCLUS).LT.2) GOTO 120
  DO II=1,NAT(JCLUS)
     I=INDEX(II)
     IF (.NOT.IUCLUS) THEN
        IF (NU(ISHELL(I,JCLUS)).NE.INCLUS.AND.ICLUS(ISHELL(I,JCLUS)).GT.0) cycle
     ENDIF
     !	WRITE (7,*) 'INDEX',II,I,iparns,iparatoms
     CALL GRNPEN
     DO JK=0,II-1
	J=INDEX(JK)
	IF (.NOT.IUCLUS) THEN
           IF (NU(ISHELL(J,JCLUS)).NE.INCLUS.AND.ICLUS(ISHELL(J,JCLUS)).GT.0) cycle
	ENDIF
	TXJ=AX(J,JCLUS)
	TYJ=AY(J,JCLUS)
	TZJ=AZ(J,JCLUS)
	DAX=ABS(AX(I,JCLUS)-TXJ)
	DAY=ABS(AY(I,JCLUS)-TYJ)
	DAZ=ABS(AZ(I,JCLUS)-TZJ)
	TR=SQRT(DAX**2+DAY**2+DAZ**2)
	III=I
	JJJ=J
	IF (I.EQ.0) THEN
           III=J
           JJJ=I
	ENDIF
	LBOND=TR.LE.ABOND.AND.TR.GT.0.
	TBOND=.FALSE.
	IF (I.LE.IPARNS.AND.J.LE.IPARNS) TBOND=BONDS(III,JJJ).NE.0
	LBOND=LBOND.OR.TBOND
	IF (LBOND) THEN
           IF (NS.LE.IPARNS) THEN
              IF (IGF(10).EQ.2.AND..NOT.TBOND) THEN
                 IBROK=2
                 CALL BROKEN
              ELSE
                 IBROK=1
                 CALL FULL
              ENDIF
              TXYZ=P3D(AX(I,JCLUS),AY(I,JCLUS),AZ(I,JCLUS))
              TXY2=P3D(TXJ,TYJ,TZJ)
              TTX=REAL(TXYZ)-REAL(TXY2)
              TTY=AIMAG(TXYZ)-AIMAG(TXY2)
              CALL GETQUAD (TTX,TTY,I,J,REVLABEL,QUAD)
              !
              !	I in front
              !
              RMFAC=RADIUS(I)
              DAJ=DAX*RADIUS(J)/TR
              DBJ=DAY*RADIUS(J)/TR
              DCJ=DAZ*RADIUS(J)/TR
              IF (AX(I,JCLUS).LT.TXJ) DAJ=-DAJ
              IF (AY(I,JCLUS).LT.TYJ) DBJ=-DBJ
              IF (AZ(I,JCLUS).LT.TZJ) DCJ=-DCJ
              TXYZ=P3D(AX(I,JCLUS),AY(I,JCLUS),AZ(I,JCLUS))
              TXY2=P3D(TXJ+DAJ,TYJ+DBJ,TZJ+DCJ)
              DX=REAL(TXYZ)-REAL(TXY2)
              DY=AIMAG(TXYZ)-AIMAG(TXY2)
              DIST=DX*DX+DY*DY
              IF (DIST.GT.RADIUS(I)*RADIUS(I)) THEN
                 IF (IGF(2).EQ.2) CALL THICK (3)
                 INTERCEPT=.FALSE.
                 KR=1
                 DO KK=JK+1,NAT(JCLUS)
                    IF (KK.NE.II) THEN
                       K=INDEX(KK)
                       DX1=REAL(TXYZ)-TX(K)
                       DY1=AIMAG(TXYZ)-TY(K)
                       DIST=DX1*DX1+DY1*DY1
                       DX2=REAL(TXY2)-TX(K)
                       DY2=AIMAG(TXY2)-TY(K)
                       DIST=DX2*DX2+DY2*DY2
                       CALL GETINT (DX1,DX2,DY1,DY2,RADIUS(K),NINTER,DX3,DX4,DY3,DY4)
                       IF (NINTER.GT.0) THEN
                          IF (DEBUG) WRITE (LOGFILE,*) ' Intersections: ',K,DX1,DY1,DX2,DY2
                          INTERCEPT=.TRUE.
                          KR=K
                       ENDIF
                    ENDIF
                 ENDDO
                 CALL JOINER (TXYZ,TXY2,RMFAC,UXT,INTERCEPT,TX(KR),TY(KR),RADIUS(KR),IBROK)
                 IF (IGF(2).EQ.2) CALL THICK (1)
              ENDIF
           ENDIF
	ENDIF
     enddo
  enddo
  !
  !	Now draw the balls, avoiding overlap
  !
  CALL FULL
  DO  I=NAT(JCLUS),0,-1
     J=INDEX(I)
     IF (.NOT.IUCLUS) THEN
        IF (NU(ISHELL(J,JCLUS)).NE.INCLUS.AND.ICLUS(ISHELL(J,JCLUS)).GT.0) cycle
     ENDIF
     ICOLOUR=IABS(IT(ISHELL(J,JCLUS)))
     IF (ICOLOUR.GT.6.OR.ICOLOUR.EQ.0) ICOLOUR=1
     CALL FCOLOR (ICOLOUR)
     NOVER=0
     IF (I.NE.NAT(JCLUS)) THEN
        DO  K=I+1,NAT(JCLUS)
           JK=INDEX(K)
           IF (.NOT.IUCLUS) THEN
              IF (NU(ISHELL(JK,JCLUS)).NE.INCLUS.AND.ICLUS(ISHELL(JK,JCLUS)).GT.0) cycle
           ENDIF
           DX=TX(J)-TX(JK)
           DY=TY(J)-TY(JK)
           DIST=DX*DX+DY*DY
           IF (DIST.NE.0) DIST=SQRT(DIST)
           DRAD=RADIUS(J)-RADIUS(JK)
           !
           !	First check for the concentric case
           !
           IF (DIST.LT.1.E-3.OR.DIST.LT.ABS(DRAD)) THEN
              IF (DRAD.LT.0.) THEN
                 NOLABEL(J)=.TRUE.
                 WRITE (7,*) 'DIST,DRAD',DIST,DRAD
                 !	      GOTO 90
              ENDIF
           ELSEIF (DIST.LT.RADIUS(J)+RADIUS(JK)) THEN
              IF (DEBUG) WRITE (LOGFILE,*) J,' AND',JK,' OVERLAP'
              IF (DIST.LE.RADIUS(JK)) OVERLAPS(J)=.TRUE.
              CALL GETQUAD (DX,DY,J,JK,REVLABEL,QUAD)
              IF (NOVER.LT.10) NOVER=NOVER+1
              IATOVER(NOVER)=JK
              CALL FINDINT(DX,DY,DIST,RADIUS(J),RADIUS(JK),TX1(NOVER),TX2(NOVER), &
                   TY1(NOVER),TY2(NOVER),ANGLE(NOVER),TTH2,TTH1)
              TTH1=360.-TTH1+90.
              TTH2=360.-TTH2+90.
              IF (TTH1.GE.360.) TTH1=TTH1-360.
              IF (TTH2.GE.360.) TTH2=TTH2-360.
              IF (TTH1.GT.TTH2)THEN
                 TTH1=TTH1-360.
              ENDIF
              IF (DEBUG) WRITE (7,*) 'TTH:',TTH1,TTH2
           ELSEIF (DIST.LT.(RADIUS(J)+RADIUS(JK))*1.5) THEN
              CALL GETQUAD (DX,DY,J,JK,REVLABEL,QUAD)
           ENDIF
        enddo
     ENDIF
     !	WRITE (7,*) 'I,J,NOVER',I,J,NOVER,ISHELL(J,JCLUS)
     IF (NOVER.EQ.0) THEN
        IF (IGF(2).EQ.1) THEN
	   CALL FPOLYFILL(1)
        ELSE
	   CALL THICK (3)
        ENDIF
        IF (J.NE.9999) CALL FCIRCLE (TX(J),TY(J),RADIUS(J))
        IF (IGF(2).EQ.1) THEN
           CALL FPOLYFILL(0)
        ELSE
           CALL THICK (1)
        ENDIF
     ELSEIF (NOVER.EQ.1) THEN
        IF (TTH1.NE.TTH2) CALL FARC (TX(J),TY(J),RADIUS(J),TTH1,TTH2)
     ELSE
        DO  I1=1,NOVER
           DO  I2=1,NOVER
              COND=.TRUE.
              REPX=(TX2(I1)+TX1(I2))*.5
              REPY=SQRT(RADIUS(J)*RADIUS(J)-REPX*REPX)
              IF (TX1(I2).GT.TX2(I1)) REPY=-REPY
              REPX=REPX+TX(J)
              REPY=REPY+TY(J)
              DO  ITEST=1,NOVER
                 IJ=IATOVER(ITEST)
                 IF (IJ.NE.J) THEN
                    TDIST=(REPX-TX(IJ))**2+(REPY-TY(IJ))**2
                    IF (TDIST.LT.RADIUS(IJ)**2) THEN
                       COND=.FALSE.
                       GOTO 80
                    ENDIF
                 ENDIF
                 IF (ITEST.NE.I1) CALL ARCCHECK (TX2(I1),TY2(I1),TX1(I2),TY1(I2), &
                      TX2(ITEST),TY2(ITEST),RADIUS(J),COND,ARCANG,TTH1,TTH2,*80)
                 IF (ITEST.NE.I2) CALL ARCCHECK (TX2(I1),TY2(I1),TX1(I2),TY1(I2), &
                      TX1(ITEST),TY1(ITEST),RADIUS(J),COND,ARCANG,TTH1,TTH2,*80)
              enddo
              IF (COND) THEN
                 TTH1=360.-TTH1+90.
                 TTH2=360.-TTH2+90.
                 IF (TTH1.GE.360.) TTH1=TTH1-360.
                 IF (TTH2.GE.360.) TTH2=TTH2-360.
                 IF (TTH1.GT.TTH2) THEN
                    TTH1=TTH1-360.
                 ENDIF
                 IF (TTH1.NE.TTH2) CALL FARC (TX(J),TY(J),RADIUS(J),TTH1,TTH2)
              ELSE
              ENDIF
80            CONTINUE
           enddo
        enddo
     ENDIF
  enddo
  CALL THICK (1)
  IF (IGF(13).NE.2) THEN
     CALL WHITEPEN
     DO I=0,NAT(JCLUS)
        JK=INDEX(I)
        JK=I
        IF (.NOT.IUCLUS) THEN
           IF (NU(ISHELL(JK,JCLUS)).NE.INCLUS.AND.ICLUS(ISHELL(JK,JCLUS)).GT.0) cycle
        ENDIF
        IF (NOLABEL(I)) cycle
        ASIZE=RADIUS(I)
        BSIZE=.05*UXM*AMAG(I)
        IF (BACKGROUND) BSIZE=BSIZE*3./5.
        IF (JJ.NE.0) BSIZE=BSIZE*1.5
        !
        !	Large labels option ( - no susbscripts )
        !
        IF (IGF(13).EQ.3) BSIZE=BSIZE*2.
        ITS=IATOM(IT(ISHELL(JK,JCLUS)))
        DO  J=0,I-1
           IF (NOLABEL(J)) cycle
           JK=INDEX(J)
           JK=J
           IF (.NOT.IUCLUS) THEN
              IF (NU(ISHELL(JK,JCLUS)).NE.INCLUS.AND.ICLUS(ISHELL(JK,JCLUS)).GT.0) cycle
           ENDIF
           TTX=TX(I)-TX(J)
           TTY=TY(I)-TY(J)
           IF (TTX*TTX+TTY*TTY.LT.BSIZE*5.)  THEN
              IF (DEBUG) WRITE (LOGFILE,*) 'Labels overlap',I,J,TTX,TTY
              CALL GETQUAD (TTX,TTY,I,J,REVLABEL,QUAD)
           ENDIF
        enddo
        TTX=TX(I)
        TTY=TY(I)
        IF (ITS.GT.0) THEN
           IQUAD=3
           IF (REVLABEL(I)) IQUAD=1
           IF (QUAD(I,IQUAD)) THEN
              DO J=8,1,-1
                 IF (.NOT.QUAD(I,J)) IQUAD=J
              ENDDO
           ENDIF
           KABEL=ELS(ITS)
           IF (KABEL(2:2).EQ.' ') THEN
              NDIGITS=1
           ELSE
              NDIGITS=2
              KABEL(2:2)=CHAR(ICHAR(KABEL(2:2))+32)
           ENDIF
           IF (OVERLAPS(I).OR.RADIUS(I).LT.BSIZE*2.) THEN
              XINC1=real(NDIGITS,kind=dp)*BSIZE*.75+ASIZE
              YINC1=BSIZE*.75+ASIZE
              TTX=TTX+XINC1*XSIGN(IQUAD)-.5*BSIZE
              TTY=TTY+YINC1*YSIGN(IQUAD)-.5*BSIZE
              TX(I)=TTX
              TY(I)=TTY
           ELSE
              TTX=TTX-BSIZE
           ENDIF
           IF (IGF(10).NE.2) THEN
              III=ISHELL(I,JCLUS)
           ELSE
              III=I
           ENDIF
           IF (IGF(13).EQ.1.AND.I.NE.0) THEN
              KABEL(NDIGITS+1:NDIGITS+1)='\\'
              IF (III.GT.99) THEN
                 NDIGITS=NDIGITS+1
                 KABEL(NDIGITS+1:)=CHAR(48+III/100)
                 III=MOD(III,100)
              ENDIF
              IF (III.GT.9) THEN
                 NDIGITS=NDIGITS+1
                 KABEL(NDIGITS+1:)=CHAR(48+III/10)
                 III=MOD(III,10)
              ENDIF
              KABEL(NDIGITS+2:)=CHAR(48+III)
              CALL PLOTCSX (TTX,TTY,KABEL)
              !	      TTX=TTX+BSIZE*(NDIGITS+1)
              !	      IF (II.LT.10) TTX=TTX-BSIZE*.5
           ENDIF
        ENDIF
     enddo
     CALL BLUPEN
  ENDIF
120 CALL THICK (1)
  !	IF (IGF(2).EQ.1) CALL GFLUSH
  IF (JJ.LE.1.AND.IGF(9).EQ.1.AND.CLEAR) CALL DRTAB(ABOND,JCLUS,IUNIT)
130 RETURN
140 FORMAT (I4,F7.3,I4,3F10.4)
150 FORMAT (' Enter ',A)
END SUBROUTINE DRAWQQ
