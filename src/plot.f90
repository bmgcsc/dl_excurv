SUBROUTINE PLOT
  !======================================================================C
  Use Parameters
  Use Index_IGF
  Use Common_PMTX
  Use Common_COMPAR

  CHARACTER*7 :: OPTS(21)
  DATA OPTS/ &
       'UNIT'   ,'EXAFS'  ,'BSF'   ,'FT'   ,'COPLOT', &
       'SHELLS' ,'PFT'    ,'WINDOW','O'    ,'RDF'   , &
       'WFX'    ,'SPECTRA','CAP'   ,'BSMAG','TPHASE', &
       'BSPHASE','PHASES' ,'REF'   ,'REFP' ,'SIGMA' , &
       'ATOMIC'/
  CALL FINDOPT (21,OPTS,IOPT,2,'Number of previous spectra',*10)
  !
  !	Default option is EXAFS
  !
  IF (IOPT.EQ.1) then
     CALL PU (INTOPT,*10)
  ELSE
     CALL GPLOT (IOPT,INTOPT)
  ENDIF
  IF (IGF(2).EQ.1.AND.IGF(ITNUM).EQ.5) CALL FGFLUSH
10 RETURN
END SUBROUTINE PLOT
