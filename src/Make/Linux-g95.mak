# Info for Portland Group Fortran/GNU C on Linux
#
CC = gcc
F90 = g95
LFLAGS = -L/usr/X11R6/lib -lX11
# PRODFLAGC = -DDOUBLE
PRODFLAGC =
# PRODFLAG90 = -fbounds-check
PRODFLAG90 = -O3 -std=f95 -fintrinsic-extensions=flush -fno-second-underscore
PRODFLAG77 = -O3  
# DEBUFLAG = -g
DEBUFLAG =
FREEFORM = -ffree-form
FIXFORM = -ffixed-form
