# Info for Portland Group Fortran/GNU C on Linux
#
CC = gcc
F90 = gfortran
LFLAGS = -L/usr/X11R6/lib -lX11
# PRODFLAGC = -DDOUBLE
PRODFLAGC =
# PRODFLAG90 = -fbounds-check
PRODFLAG90 = -O2 -finit-real=nan
PRODFLAG77 = -O2 -finit-real=nan
# DEBUFLAG = -g
DEBUFLAG =
FREEFORM = -ffree-form
FIXFORM = -ffixed-form
