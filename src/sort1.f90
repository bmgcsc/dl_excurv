SUBROUTINE SORT1 (INDEX)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Include_IPA

  DIMENSION INDEX(0:IPARNS),TR(0:IPARNS)
  DO I=0,NS
     INDEX(I)=I
  ENDDO
  IF (NS.GE.2) THEN
     !	  CALL UCOPY (RAD,TR,NS+1)
     DO K=0,NS
        TR(K)=RAD(K)
     ENDDO
     !
     !	If more than 1 shell is present, put them in order of ascending R
     !
     JSWOP=0
     DO  L=1,NS
        J=0
        DO  I=2,NS
           IF (TR(I).LT.TR(I-1)) THEN
              II=INDEX(I)
              RR=TR(I)
              INDEX(I)=INDEX(I-1)
              TR(I)=TR(I-1)
              INDEX(I-1)=II
              TR(I-1)=RR
              J=I
              JSWOP=JSWOP+1
           ENDIF
        enddo
        IF (J.EQ.0) GOTO 30
     enddo
30   IF (DEBUG) THEN
        WRITE (LOGFILE,*) 'Sorted index array:'
        DO I=1,NS
           WRITE (LOGFILE,*) I,INDEX(I),RAD(INDEX(I))/DC
        ENDDO
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE SORT1
