SUBROUTINE DIFFS (V,DX,N,VM)
  !======================================================================C
  Use Parameters
  DIMENSION V(IPARGRID), VM(IPARGRID)
  VM(1)=((6.0*V(2)+6.66666666667*V(4)+1.2*V(6))-(2.45*V(1)+7.5*V(3)+3.75*V(5)+.166666666667*V(7)))/DX
  VM(2)=((6.0*V(3)+6.66666666667*V(5)+1.2*V(7))-(2.45*V(2)+7.5*V(4)+3.75*V(6)+.166666666667*V(8)))/DX
  NM2=N-2
  DO  I=3,NM2
     VM(I)=((V(I-2)+8.0*V(I+1))-(8.0*V(I-1)+V(I+2)))/12.0/DX
  enddo
  VM(N-1)=(V(N)-V(N-2))/(2.0*DX)
  VM(N)=(V(N-2)*.5-2.0*V(N-1)+1.5*V(N))/DX
  RETURN
END SUBROUTINE DIFFS
