SUBROUTINE BSUB (ISTEP,DSTEP,IMSCB)
  !======================================================================C
  Use Parameters
  Use Common_extension
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_POT
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_IP
  Use Common_P
  Use Common_DISTANCE
  Use Common_PRDSN
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  !
  !	Submit an excurve background job. Used by commands ITER, MAP, XANES.
  !
  CHARACTER*12 IP1,IP2,IP3
  CHARACTER*60 ODSN2
  !
  !	Remember IXA in case it is changed
  !
  KTR=IXA
  !
  !	ALLOW ESCAPE
  !
  !  10	CALL WTEXT ('Enter Time in Seconds or "=" to Escape')
  !	CALL CREAD (IP1,IP2,IP3,NSEC,IC,V,*10,*50)
  !	IF (IC.NE.1) GOTO 10
  NSEC=999
  IF (KEYWORD(1:1).NE.'X') THEN
     !
     !	NON-XANES OPTIONS:
     !	CREATE A BINARY FILE FOR PARAMETER TRANSFER
     !	THE SUFFIX IT GENERATES WILL ALSO BE USED IN THE JOBNAME AND IN
     !	NAMING FILES CREATED BY THE JOB.
     !
     IF (IXA.NE.3) GOTO 30
     CALL CKXA (*60)
20   CALL WTEXT ('Should current xadd spectrum to be included ?')
     CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*20,*50)
     IF (IP1(1:1).EQ.'Y') GOTO 30
     IF (IP1(1:1).NE.'N') GOTO 20
     IXA=2
30   CALL WTEXT ('File for background job:')
     CALL OUTNAM (15,PRDSN,' ',*50)
     NC=NCROOT(PRDSN)
     ODSN2=PRDSN(1:NC)//EXTDATA
     CALL WTEXT ('Data will be returned in: '//ODSN2)
     CALL FILEOPEN (PRDSN,OUTFILE,'UNFORMATTED',' ',*50)
     ITEMP=IMSC
     IMSC=IMSCB
     WRITE (OUTFILE) PRDSN(NC-2:NC-1),NSEC,KEYWORD,ID,ISET,PGROUP,NPGR,EDGECODE,IEDGECODE,NINITIAL,LINITIAL,JINITIAL
     IMSC=ITEMP
     WRITE (OUTFILE) NCALL,IVNUMS,IVCH,XP,SF,U
     WRITE (OUTFILE) NSPEC,NP,NPS,KT,IXA,DSTEP,ISTEP
     WRITE (OUTFILE) (ENER(I),I=1,NP),(JS(IEXP),JF(IEXP),NPT(IEXP),(XABS(I,IEXP),I=1,NP),IEXP=1,NSPEC), &
          PA1,PA2,IGF,DISTANCES,WEIGHTINGS,LASTNEIGH,LASTHOLE,NTORA,NTORB,ATLABEL
     DO  J=1,NPS
        WRITE (OUTFILE) NPH(J),(EN(I,J),EREF(I,J),(PHS(I,J,K),K=1,LMAX+1),I=1,NPH(J))
     enddo
     !
     !       PCS added this line 4/12/98
     !
     WRITE( OUTFILE )( NCLUS(I), I = 1, 3 )
     !
     !	Extra record if current xadd spectrum is to be used.
     !
     IF (IXA.EQ.3) WRITE (OUTFILE) NX,XAE1,XAE2,(XA(I),I=1,NX),MSTL
     !
     !	Free EXOUT files, print message.
     !
     CALL EFILE ('Output for background job: ')
     ITYP=1
  ELSE
     !
     !	HERE FOR XANES OPTION
     !
     ITYP=6
     ODSN2=PRDSN(1:NCROOT(PRDSN))//EXTDATA
  ENDIF
  CALL SUBMIT (ITYP,PRDSN,ODSN2,NSEC,*60)
50 IXA=KTR
60 RETURN
END SUBROUTINE BSUB
