!
!	subroutine for calculating total potential including energy
!	dependent xc
!
! input:  indx   0 hedin-lundqvist
!	          1 dirac-hara + const imag part
!	          2 cnst imag part (disabled)
!	          3 X-alpha + cnst imag part (disabled)
!	          4 hedin-von barth + cnst imag part (disabled)
!	   en     real part of energy in rydbergs
!	   iref   1 for complex calculation
!	          0 for real part only (disabled)
!	   xmu    fermi level
!	   gamach core-hole lifetime
!	   vrc    coulomb potential * r**2
!	   vr     total potential including vxcgs * r**2
!	   edens  4*pi*density*r**2
!	          NB vr and edens must have correct interstitial value
!	          outside muffin-tin
!	   ie     index of energy level in phase
!
! output: ref    reference potential corrected for current energy
!	   v(450) total potential including energy dep xc, passed to
!	          FOVRG via common /fg/
!
!     version 1 written by j. mustre 9/1/87
!     version 2 3/22/88 index 9 included
!     version 3 3/30/88 only few options kept
!     version 4 4/13/88 if statement added to calculate exchange
!	  potential below the vacuum level, local momentum has been
!	  redefined.
!     version 5 1/12/89 excited state exchange correlation potential
!	  has been made to agree with ground state potential, at fermi
!	  level mu, according to j. rehr theory. local momentum has been
!	  redefined accordingly. vr is the total ground state potential,
!	  in previous versions vr was just the coulomb potential.
!     version 6  2/2/90 Energy reference changed to interstitial point.
!
!==========================================================================
SUBROUTINE XCPOT2 (IENER,GRID,EN,REF,XMU,GAMACH,VR,V,JRI,RS,SRS,WPFE,VXCRMU)
  !========================================================================
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  DIMENSION VR(*),VXCRMU(*),VXCIMU(IPARGRID),RS(*),WPFE(*),SRS(*)
  DIMENSION SAVEDELT(1000),GRID(*)
  COMPLEX*8 V(*),REF,DELTA
  COMPLEX SAVEDELT
  IF (DEBUG) WRITE (LOGFILE,*) 'XCPOT: en,xmu,gamach,vr(1)',EN,XMU,GAMACH,VR(1)
  !
  !     First calculate VXC to correct the local momentum dispersion
  !     relation calculating delta= vxc(e,k)-vxc(mu,k), and
  !     p^2=k^2-mu+kf^2-delta.
  !     In jr theory, v(e,r) = vcoul(r) + vxc(e,r) =
  !	                     = vcoul(r) + vxcgs(r) + delta(e,r).

  DO I = 1,JRI
     !
     !	EDENS = 4PI*R^2*RHO
     !	RS= (3.*R^2(I))/EDENS(I))**(1./3.)
     !
     RKF=1.9191583/RS(I)
     !
     !	  XK1 is the local momentum, P^2=K^2-MU+KF^2, K^2 represents
     !	  energy measured from vacuum (form. 2.15 in Lee and Beni's PAPER
     !	  paper with the last 2 terms neglected.
     !
     XK2=EN+RKF**2-XMU
     IF (XK2 .LE. XMU.OR.XK2.LT.0.OR.iset(IEXCH).EQ.2) THEN
        !
        !	This option sets the self-energy to constant below EF
        !
        !	IF (EN .LE. XMU.OR.XK2.LT.0.OR.iset(IEXCH).EQ.2) THEN
        IF (DEBUG) WRITE (7,*) 'XK2',I,XK2,EN,RKF**2,XMU
        DELTA=0.
     ELSE
        XK1=SQRT(XK2)/RKF
        CALL RHL (VXCR,VXCI,XK1,RS(I),SRS(I),WPFE(I))
        !
        !	  correct local momentum according to the formula
        !	  p^2=k^2-mu+kf^2-delta. we will ignore the imaginary part of
        !	  delta so p will still be a real quantity. jm 1/12/89
        !
        IF (ABS(VXCR-VXCRMU(I)).GE..005) THEN
           XK2=XK2-(VXCR-VXCRMU(I))
           IF (XK2.GT.0) THEN
              XK1=SQRT(XK2)/RKF
              !
              !	  recalc vxc(e,k) and vxc(mu,k) with corrected local momentum
              !
              CALL RHL (VXCR,VXCI,XK1,RS(I),SRS(I),WPFE(I))
           ENDIF
        ENDIF
        !
        !	  delta corrected calculated with new local momentum
        !
        !	  DELTA = CMPLX (VXCR-VXCRMU(I), VXCI-VXCIMU(I))
        DELTA = CMPLX (VXCR-VXCRMU(I), VXCI)
     ENDIF
     IF (DEBUG.AND.I.EQ.JRI) WRITE (7,*) 'JRI,XK2,EN,RKF**2,XMU,RS(I)',JRI,XK2,EN,RKF**2,XMU,RS(I)
     !
     !	  multiplication by 2 to convert to rydbergs
     !
     V(I)=VR(I)+2.*DELTA
     SAVEDELT(I)=DELTA
  enddo
  !	if (en.lt.1.) write (7,*) 'en,rkf**2,xmu',en/ec,rkf**2/ec,xmu/ec
  !	write (40,'(f8.3,50f8.4)') en/ec,(real(savedelt(i)),i=1,jri,10)
  !	write (41,'(f8.3,50f8.4)') en/ec,(aimag(savedelt(i)),i=1,jri,10)
  !	jener=mod(iener-1,7)+1
  !	if (jener.eq.1) then
  !	  junit=42+iener/7
  !	  do i=1,jri
  !	  write (junit,'(f14.7,2f9.4)') grid(i)/dc,real(savedelt(i)),aimag
  !     (savedelt(i))
  !	  enddo
  !	endif
  !
  !     reference the potential with respect to eref the MT potential
  !     (Vmt=0) ad1
  !	  reference equal to the complex value vmt. !!Note that
  !	  the reference does not contain the core hole lifetime since the
  !	  total atomic potential should have it, however in the
  !	  perturbation deltav=v-vmt it cancels out.
  !	  (deltav=vat-igamma-(vatmt-igamma)).
  !	  correct reference to include the coulomb part of the
  !	  potential. jm 4/25/88
  !	  ref=v(jri)/dcmplx(ri(jri)**2)
  !
  !	  reference changed to interstitial potential siz 2/2/90
  REF=V(JRI)
  !	WRITE (7,*) 'REF',V(JRI),VR(JRI),VR(JRI+1)
  DO I=1,JRI
     V(I)=V(I)-REF
  enddo
  !
  !     gamma added to reference so k^2=E-Eref. Eref=Vatmt-igamma/2
  !
  REF=REF+CMPLX(0.,-GAMACH*.5)
  IF (DEBUG) WRITE (LOGFILE,*) 'XCPOT: en,ref,vr(1),delta',EN,REF,VR(1),DELTA
  RETURN
END SUBROUTINE XCPOT2
