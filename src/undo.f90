SUBROUTINE UNDO
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_SPAREPA1
  Use Include_PA1
  !
  !	Restores parameters to before the previous CHANGE
  !
  CALL UCOPY (SPAREPA1,PA1,IPARNPARAMS)
  CALL FLIM (*173)
173 CALL UTAB (0)
  CALL WTEXT ('Old parameters restored')
  LC=IOR(LC,71_i4b)
  CALL IUZERO (NAT,IPARNCLUS)
  RETURN
END SUBROUTINE UNDO
