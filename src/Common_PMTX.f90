MODULE Common_PMTX

!      COMMON /PMTX/IGF(IPARGSET),RQ(2),JJ,LX,CSIZE,MPL,NPL,IPINDEX,PL
!     *OTXSCALE,PLOTYSCALE,PLOTTITLE,QRAT,IQFONT,IXMINOFF,IYMINOFF,PLOTXL
!     *AB,PLOTYLAB,LASTNCX,LASTNCY,QDQ,CENTRED,IGRAPH,USED3,USED4
!
!      CHARACTER*64 PLOTTITLE,PLOTXLAB,PLOTYLAB
!      LOGICAL QDQ,USED3,USED4
!
!       DATA JJ/0/,LX/0/,MPL/0/,NPL/0/                     ! in COMMON /PMTX/
!	data QDQ/.FALSE./,CENTRED/0./,CSIZE/.03/           ! in COMMON /PMTX/
!	DATA IGF/4*1,3,2*1,5,6*1,2/                        !   in COMMON /PMTX/
!	data IPINDEX/1/                                    !   in COMMON /PMTX/

  Use Definition
  Use Parameters
  implicit none
  private
  integer(i4b), dimension(IPARGSET), public :: IGF = (/1,1,1,1,3,1,1,5,1,1,1,1,1,1,2/)
  real(dp),     dimension(:),        public :: RQ(2)
  integer(i4b), public :: JJ = 0
  integer(i4b), public :: LX = 0
  real(dp),     public :: CSIZE = 0.03
  integer(i4b), public :: MPL = 0
  integer(i4b), public :: NPL = 0
  integer(i4b), public :: IPINDEX = 1
  real(dp),     public :: PLOTXSCALE,PLOTYSCALE
  character*64, public :: PLOTTITLE,PLOTXLAB,PLOTYLAB
  real(dp),     public :: QRAT
  integer(i4b), public :: IQFONT            ! never used ???
  integer(i4b), public :: IXMINOFF,IYMINOFF,LASTNCX,LASTNCY
  logical,      public :: QDQ = .false.
  real(dp),     public :: CENTRED = 0.0
  integer(i4b), public :: IGRAPH
  logical,      public :: USED3,USED4
end MODULE Common_PMTX
