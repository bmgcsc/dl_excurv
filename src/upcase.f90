SUBROUTINE UPCASE (STRING)
  !======================================================================C
  !
  !	 Convert a text string to upper case in situ
  !
  !	STRING (I/O) character string to be converted
  !
  CHARACTER*(*) STRING
  CHARACTER*26 LCASE,UCASE
  DATA LCASE/'abcdefghijklmnopqrstuvwxyz'/
  DATA UCASE/'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
  !
  !	 Convert string
  !
  LL=LEN(STRING)
  IF (LL.LE.0) RETURN
  !
  !      convex
  !
  DO  L=1,LL
     K=INDEX(LCASE,STRING(L:L))
     IF(K.EQ.0)cycle
     STRING(L:L)=UCASE(K:K)
  enddo
  RETURN
END SUBROUTINE UPCASE
