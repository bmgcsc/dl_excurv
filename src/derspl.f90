SUBROUTINE DERSPL (N,X,F,D,A)
  !======================================================================C
  !
  !	F(I) are the function values at the points X(I) for I=1,N and
  !	The spline derivatives D(I) are found.  The dimension of a must
  !	not be less than 3*N.
  !
  DIMENSION X(N),F(N),D(N),A(3*N)
  DO I=2,N
     IF (X(I)-X(I-1))10,10,30
10   WRITE(6,20)I
20   FORMAT(' Return from derspl  ',I3,' out of order')
     A(1)=1.0
     RETURN
30   CONTINUE
  enddo
  DO  I=1,N
     J=2
     IF (I-1)40,50,40
40   J=N-1
     IF (I.EQ.N) GOTO 50
     H1=1.0/(X(I)-X(I-1))
     H2=1.0/(X(I+1)-X(I))
     A(3*I-2)=H1
     A(3*I-1)=2.0*(H1+H2)
     A(3*I)=H2
     D(I)=3*(F(I+1)*H2*H2+F(I)*(H1*H1-H2*H2)-F(I-1)*H1*H1)
     GOTO 60
50   H1=1.0/(X(J)-X(J-1))
     H2=1.0/(X(J+1)-X(J))
     A(3*I-2)=H1*H1
     A(3*I-1)=H1*H1-H2*H2
     A(3*I)=-H2*H2
     D(I)=2 *(F(J)*(H2*H2*H2+H1*H1*H1)-F(J+1)*H2*H2*H2-F(J-1)*H1*H1*H1)
60   CONTINUE
  enddo
  P=A(4)/A(1)
  A(5)=A(5)-P*A(2)
  A(6)=A(6)-P*A(3)
  D(2)=D(2)-P*D(1)
  DO  I=3,N
     K=3*I-4
     P=A(K+2)/A(K)
     A(K+3)=A(K+3)-P*A(K+1)
     D(I)=D(I)-P*D(I-1)
     IF (I.NE.N-1) GOTO 70
     P=A(K+5)/A(K)
     A(K+5)=A(K+6)-P*A(K+1)
     A(K+6)=A(K+7)
     D(N)=D(N)-P*D(N-2)
70   CONTINUE
  enddo
  D(N)=D(N)/A(3*N-1)
  DO I=3,N
     J=N+2-I
     D(J)=(D(J)-A(3*J)*D(J+1))/A(3*J-1)
  enddo
  D(1)=(D(1)-D(2)*A(2)-D(3)*A(3))/A(1)
  A(1)=0.0
  RETURN
END SUBROUTINE DERSPL
