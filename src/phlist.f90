SUBROUTINE PHLIST (IS,IF)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_POT
  Use Common_COMPAR
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  CHARACTER BLOB*1
  !
  !	List parameters associated with phaseshifts
  !
  IW=OUTTERM
  IF (CHAROPT.EQ.'LOG') IW=LOGFILE
  DO  I=IS,IF
     BLOB=' '
     IF (LASTHOLE(I).NE.-99) BLOB='*'
     WRITE (IW,20) I,IATOM(I),ELS(IATOM(I)),BLOB,RION(I),RMTR(I),ALF(I),CMAG(I),XE(I)
  enddo
  WRITE (IW,30) NPS
  RETURN
  !
20 FORMAT (I2,' ATOM',I4,' (',A2,A1,') ION',F5.2,' MTR',F7.3,' ALF',F7.4,' CMAG',F7.3,' XE',F7.3)
30 FORMAT (/1X,I2,' Phaseshifts are defined')
END SUBROUTINE PHLIST
