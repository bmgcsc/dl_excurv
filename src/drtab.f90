SUBROUTINE DRTAB (ABOND,JCLUS,IUNIT)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_PMTX
  Use Common_DISTANCE
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1


  LINECOUNT=0
  CALL SETTAB (1)
  CALL PLACE (1,1)
  ILINE=3
  CALL TYPECS ('Bond distances:')
  CALL CRLNFD
  CALL TYPECS ('----------------')
  IISHELL=0
  DO  I=0,NS-1
     IF (IABS(ICLUS(I)).NE.JCLUS) cycle
     IF (IUNIT.NE.0.AND.NU(I).NE.IUNIT.AND.I.NE.0) cycle
     IF (RN(I).EQ.0.) cycle
     JSHELL=IISHELL+MAX(1,N(I))
     DO  J=I+1,NS
	IF (IABS(ICLUS(J)).NE.JCLUS) cycle
	IF (IUNIT.NE.0.AND.NU(J).NE.IUNIT) cycle
	IF (RN(J).EQ.0.) cycle
	IF (ICLUS(I).GT.0.AND.ICLUS(J).GT.0.AND.ICLUS(I).NE.ICLUS(J)) cycle
	DIST=(UX(I)-UX(J))**2+(UY(I)-UY(J))**2+(UZ(I)-UZ(J))**2
	IJ=IATOM(IT(I))
	IF (DIST.NE.0.) DIST=SQRT(DIST)
	IF (DIST.GT.ABOND.OR.DIST.EQ.0) cycle
	CALL PLACE (1,ILINE)
	CALL TYPELAB (IJ,I,IISHELL)
	CALL PLACE (5,ILINE)
	CALL TYPECS('-')
	CALL PLACE (7,ILINE)
	IJ=IATOM(IT(J))
	CALL TYPELAB (IJ,J,JSHELL)
	CALL PLACE (13,ILINE)
	CALL TYPENF (DIST,3,7)
	ILINE=ILINE+1
	CALL LINECHECK (*60)
   	JSHELL=JSHELL+MAX(1,N(J))
     enddo
     IISHELL=IISHELL+MAX(1,N(I))
  enddo
  ILINE=ILINE+1
  CALL LINECHECK (*60)
  CALL PLACE (1,ILINE)
  CALL TYPECS ('Bond angles:')
  CALL CRLNFD
  CALL TYPECS ('----------------')
  ILINE=ILINE+2
  IISHELL=0
  DO  I=0,NS
     IF (IABS(ICLUS(I)).NE.JCLUS) cycle
     IF (IUNIT.NE.0.AND.NU(I).NE.IUNIT.AND.I.NE.0) cycle
     IF (RN(I).EQ.0.) cycle
     I1=IATOM(IT(I))
     TUX=UX(I)
     TUY=UY(I)
     TUZ=UZ(I)
     JSHELL=0
     DO  J=0,NS-1
	IF (IABS(ICLUS(J)).NE.JCLUS) cycle
	IF (IUNIT.NE.0.AND.NU(J).NE.IUNIT.AND.J.NE.0) cycle
	IF (J.EQ.I) cycle
	IF (RN(J).EQ.0.) cycle
	J1=IATOM(IT(J))
	UXJ=UX(J)
	UYJ=UY(J)
	UZJ=UZ(J)
	DIFFIX=UXJ-TUX
	DIFFIY=UYJ-TUY
	DIFFIZ=UZJ-TUZ
	DIFFI=DIFFIX*DIFFIX+DIFFIY*DIFFIY+DIFFIZ*DIFFIZ
	DIFFI=SQRT(DIFFI)
	IF (DIFFI.GT.ABOND) cycle
	KSHELL=JSHELL+MAX(1,N(J))
	DO  K=J+1,NS
           IF (IABS(ICLUS(K)).NE.JCLUS) cycle
           IF (IUNIT.NE.0.AND.NU(K).NE.IUNIT) cycle
           IF (K.EQ.I) cycle
           IF (RN(K).EQ.0.) cycle
           K1=IATOM(IT(K))
           UXK=UX(K)
           UYK=UY(K)
           UZK=UZ(K)
           DIFFJX=UXK-TUX
           DIFFJY=UYK-TUY
           DIFFJZ=UZK-TUZ
           DIFFJ=DIFFJX*DIFFJX+DIFFJY*DIFFJY+DIFFJZ*DIFFJZ
           DIFFJ=SQRT(DIFFJ)
           IF (DIFFJ.GT.ABOND) cycle
           IF (ABS(DIFFI).LT.1.E-10.OR.ABS(DIFFJ).LT.1.E-10) THEN
              ANGLE=0.
           ELSE
              ANGLE=DIFFIX*DIFFJX+DIFFIY*DIFFJY+DIFFIZ*DIFFJZ
              ANGLE=ANGLE/(DIFFI*DIFFJ)
              IF (ABS(ANGLE).GT.1.) THEN
                 WRITE (7,*) 'Error in DRTAB:',ANGLE
                 ANGLE=0.
              ELSE
                 ANGLE=ACOS(ANGLE)/AC
              ENDIF
           ENDIF
           CALL PLACE (1,ILINE)
           CALL TYPELAB (J1,J,JSHELL)
           CALL PLACE (5,ILINE)
           CALL TYPECS ('- ')
           CALL TYPELAB (I1,I,IISHELL)
           CALL PLACE (11,ILINE)
           CALL TYPECS ('- ')
           CALL TYPELAB (K1,K,KSHELL)
           CALL PLACE (16,ILINE)
           CALL TYPENF (ANGLE,1,7)
           ILINE=ILINE+1
           CALL LINECHECK (*60)
           KSHELL=KSHELL+MAX(1,N(K))
        enddo
        JSHELL=JSHELL+MAX(1,N(J))
     enddo
     IISHELL=IISHELL+MAX(1,N(I))
  enddo
60 QDQ=.FALSE.
  RETURN
END SUBROUTINE DRTAB
