SUBROUTINE FILEXT (FILIN,FILOUT,*)
  !======================================================================C
  !
  !	Expand a unix file name input with reference to the current
  !	working directory into its full form
  !
  !	FILIN (I)   character variable holding name to be expanded
  !	FILOUT (O)  character variable returning expanded file name
  CHARACTER*(*) FILIN,FILOUT
  FILOUT = FILIN
  RETURN
END SUBROUTINE FILEXT
