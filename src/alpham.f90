SUBROUTINE ALPHAM
  !======================================================================C
  Use Parameters
  USE Common_B1
  USE Common_B2
  Use Common_PLFLAG
  Use Index_IGF
  Use Common_PMTX
  !       
  !	Set terminal to alpha mode (screen) or end page (plotter)
  !       
  INTEGER :: LUNIT(8)

  !	INTEGER SYSTEM
  IF (PLCMD) THEN
     !       
     !	Terminate page on plotter/printer
     !       
     IF (IGF(2).EQ.2) CALL GREND
     !       
     !	Machine specific code for switching from graphics to alpha mode
     !       
     !       
     !       UNIX only
     !       
     IF (IGF(ITNUM).EQ.4) THEN
        !       
        !       X-term operating in TEK mode
        !       
        WRITE (OUTTERM,10) CHAR(27)
        WRITE (OUTTERM,10) CHAR(3)
     ENDIF
     IF (IGF(ITNUM).EQ.1.OR.IGF(ITNUM).EQ.2.OR.IGF(ITNUM).EQ.4) THEN
        IF (IGF(ITNUM).EQ.2.OR.IGF(ITNUM).EQ.4) THEN
           !       
           !       Pericom/Kermit type - mode change
           !       
           WRITE (OUTTERM,10) CHAR(27)
           WRITE (OUTTERM,10) CHAR(12)
        ENDIF
        WRITE (OUTTERM,10) CHAR(24)
     ENDIF
     !       
     !       PC only
     !       
     !       CALL TEXT_MODE@
     PLCMD=.FALSE.
  ENDIF
  RETURN
10 FORMAT (A,$)
END SUBROUTINE ALPHAM
