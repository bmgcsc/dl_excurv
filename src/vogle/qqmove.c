#include "vogle.h"

/*
 * move
 *
 * Move the logical graphics position to the world coordinates x, y, z.
 *
 */
void
qqmove(x, y, z)
	float 	x, y, z;
{
	Token	*p;
	Vector	vect, result;

	if (!qqvdevice.initialised) 
		qqverror("move: vogle not initialised");

	qqvdevice.cpW[V_X] = x;
	qqvdevice.cpW[V_Y] = y;
	qqvdevice.cpW[V_Z] = z;

	qqvdevice.cpVvalid = 0;

	if (qqvdevice.inpolygon) {
		(*qqvdevice.pmove)(x, y, z);
		return;
	}

	if (qqvdevice.inobject) {
		p = qqnewtokens(4);

		p[0].i = MOVE;
		p[1].f = x;
		p[2].f = y;
		p[3].f = z;

		return;
	}

	if (qqvdevice.clipoff) {		/* update device coords as well */
		qqmultvector(qqvdevice.cpWtrans, qqvdevice.cpW, qqvdevice.transmat->m);
		qqvdevice.cpVx = qqWtoVx(qqvdevice.cpWtrans);
		qqvdevice.cpVy = qqWtoVy(qqvdevice.cpWtrans);
	}
}

/*
 * move2
 *
 * Move the logical graphics position to the world coords x, y, 0.0
 * (I.e. a 2D move is defined as a 3D move with the Z-coord set to zero)
 *
 */
void
qqmove2(x, y)
	float	x, y;
{
	if (!qqvdevice.initialised) 
		verror("move2: vogle not initialised");

	qqmove(x, y, 0.0);
}

/*
 * rmove
 *
 * moqqve the logical graphics position from the current world 
 * coordinates by dx, dy, dz 
 *
 */
void
qqrmove(dx, dy, dz)
	float	dx, dy, dz;
{
	if (!qqvdevice.initialised) 
		qqverror("rmove: vogle not initialised");

	qqmove((qqvdevice.cpW[V_X] + dx), (qqvdevice.cpW[V_Y] + dy), (qqvdevice.cpW[V_Z] + dz));
}

/*
 * rmove2
 *
 * Move Relative in 2D.
 *
 */
void
qqrmove2(dx, dy)
	float	dx, dy;
{
	if (!qqvdevice.initialised) 
		qqverror("rmove2: vogle not initialised");

	qqmove((qqvdevice.cpW[V_X] + dx), (qqvdevice.cpW[V_Y] + dy), 0.0);
}

/*
 * smove2
 *
 * Move directly as a fraction of the screen size.
 */
void
qqsmove2(xs, ys)
	float 	xs, ys;
{
	if (!qqvdevice.initialised) 
		qqverror("smove2: vogle not initialised");

	qqvdevice.cpVx = (xs / 2 + 0.5) * (qqvdevice.maxVx - qqvdevice.minVx);
	qqvdevice.cpVy = (0.5 + ys / 2) * (qqvdevice.maxVy - qqvdevice.minVy);
}

/*
 * rsmove2
 *
 * Relative move as a fraction of the screen size.
 */
void
qqrsmove2(dxs, dys)
	float	dxs, dys;
{
	if (!qqvdevice.initialised) 
		qqverror("rsmove2: vogle not initialised");

	qqvdevice.cpVx += dxs / 2 * (qqvdevice.maxVx - qqvdevice.minVx);
	qqvdevice.cpVy += dys / 2 * (qqvdevice.maxVy - qqvdevice.minVy);
}
