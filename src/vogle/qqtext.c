#include <fcntl.h>
#include <stdio.h>

#ifdef	TC

extern	double	cos();
extern	double	sin();

#else 

#include <math.h>

#endif

#ifdef PC
#  include <string.h>
#else
#  include <string.h>
#  define rindex strrchr
#endif
#include "vogle.h"

#define	MAX(a, b)	((a) < (b) ? (b) : (a))
#define	XCOORD(x)	((x) - 'R')
#define	YCOORD(y)	('R' - (y))

static	float	QQSCSIZEX = 1.0, QQSCSIZEY = 1.0;
static	int	qqLoaded = 0;
static	short	qqnchars;


/*
static	char	errmsg1[120] = "font: unable to open ";
*/
extern	char	errmsg1[120] = "font: unable to open ";

static	struct	{
	char	*p;	/* All the vectors in the font */
	char	**ind;	/* Pointers to where the chars start in p */
	int	as;	/* Max ascender of a character in this font */
	int	dec;	/* Max decender of a character in this font */
	int	mw;	/* Max width of a character in this font */
} qqftab;

float	qqstrlength();
static	void	qqactual_move();
static	void	qqdrawhardchar();
float	qqgetfontwidth();
float	qqgetfontheight();

extern char	*getenv();

/*
 * font
 * 	loads in a font.
 */
void
qqfont(name)
	char	*name;
{
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("font: vogle not initialised");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(2 + strlen(name) / sizeof(Token));
		tok[0].i = VFONT;
		strcpy((char *)&tok[1], name);

		return;
	}

	/*
	 * check we aren't loading the same font twice in a row
	 */
#ifdef PC
	if (*name == '\\') {
		if (strcmp(strrchr(name, '\\') + 1, qqvdevice.attr->a.font) == 0)
			return;
#else
	if (*name == '/') {
		if (strcmp(rindex(name, '/') + 1, qqvdevice.attr->a.font) == 0)
			return;
#endif
	} else if (strcmp(name, qqvdevice.attr->a.font) == 0)
		return;

	qqvdevice.attr->a.softtext = 0;
	if (qqhershfont(name)) {
		return;
	} else if (strcmp(name, "large") == 0) {
		if (!(*qqvdevice.dev.Vfont)(qqvdevice.dev.large)) 
			verror("font: unable to open large font");
	} else if (strcmp(name, "small") == 0) {
		if (!(*qqvdevice.dev.Vfont)(qqvdevice.dev.small))
			verror("font: unable to open small font");
	} else if (!(*qqvdevice.dev.Vfont)(name)) {
		strcat(errmsg1, "fontfile ");
		strcat(errmsg1, name);
		qqverror(errmsg1);
	}

#ifdef PC
	if (*name == '\\')
		strcpy(qqvdevice.attr->a.font, strrchr(name, '\\') + 1);
#else
	if (*name == '/')
		strcpy(qqvdevice.attr->a.font, rindex(name, '/') + 1);
#endif
	else 
		strcpy(qqvdevice.attr->a.font, name);
}

/*
 * numchars
 *
 *	return the number of characters in the currently qqLoaded hershey font.
 *	(The 128 is the number of chars in a hardware font)
 */
int
qqnumchars()
{
	if (qqvdevice.attr->a.softtext)
		return((int)qqnchars);

	return(128);
}

/*
 * hershfont
 *
 * Load in a hershey font. First try the font library, if that fails try
 * the current directory, otherwise return 0.
 */
int
qqhershfont(fontname)
	char	*fontname;
{
	FILE	*fp;
	int	i, j;
	short	nvects, n;
	char	path[120], *flib;
	
	if ((flib = getenv("VFONTLIB")) == (char *)NULL) {
		strcpy(path, FONTLIB);
#ifdef PC
		strcat(path, "\\");
#else
		strcat(path, "/");
#endif
		strcat(path, fontname);
	} else {
		strcpy(path, flib);
#ifdef PC
		strcat(path, "\\");
#else
		strcat(path, "/");
#endif
		strcat(path, fontname);
	}

#ifdef PC
	if ((fp = fopen(path, "r+b")) == (FILE *)NULL) 
		if ((fp = fopen(fontname, "r+b")) == (FILE *)NULL) 
#else
	if ((fp = fopen(path, "r")) == (FILE *)NULL)
		if ((fp = fopen(fontname, "r")) == (FILE *)NULL) 
#endif
			return (0);

	if (fread(&qqnchars, sizeof(qqnchars), 1, fp) != 1)
		return (0);

	if (fread(&nvects, sizeof(nvects), 1, fp) != 1)
		return(0);

	if (fread(&n, sizeof(n), 1,  fp) != 1)
		return(0);

	qqftab.as = (int)n;

	if (fread(&n, sizeof(n), 1, fp) != 1)
		return(0);

	qqftab.dec = (int)n;

	if (fread(&n, sizeof(n), 1, fp) != 1)
		return(0);

	qqftab.mw = (int)n;

	/*
	 *  Allocate space for it all....
	 */
	if (qqLoaded) {
		if (qqftab.ind[0])
			free(qqftab.ind[0]);
		if (qqftab.ind)
			free(qqftab.ind);
		qqLoaded = 0;
	}

	qqftab.ind = (char **)vallocate(sizeof(char *)*(qqnchars + 1));

	qqftab.p = (char *)vallocate((unsigned)(2 * nvects));

	/*
	 *  As we read in each character, figure out what ind should be
	 */

	for (i = 0; i < qqnchars; i++) {
		if ((j = fread(&n , sizeof(n), 1, fp)) != 1)
			return(0);

		if ((j = fread(qqftab.p, 1, (unsigned)n, fp)) != (unsigned)n)
			return(0);

		qqftab.ind[i] = qqftab.p;
		qqftab.p += n;
	}

	qqftab.ind[qqnchars] = qqftab.p;	/* To Terminate the last one */

	fclose(fp);
	qqvdevice.attr->a.softtext = qqLoaded = 1;

#ifdef PC
	if (*fontname == '\\')
		strcpy(qqvdevice.attr->a.font, strrchr(fontname, '\\') + 1);
#else
	if (*fontname == '/')
		strcpy(qqvdevice.attr->a.font, rindex(fontname, '/') + 1);
#endif
	else 
		strcpy(qqvdevice.attr->a.font, fontname);

	return(1);
}

/*
 * getcharsize
 *
 *	get the width and height of a single character. At the moment, for
 * the hershey characters, the height returned is always that of the
 * difference between the maximun descender and ascender.
 *
 */
void
qqgetcharsize(c, width, height)
	char	c;
	float	*width, *height;
{

	float	a, b;

	if (!qqvdevice.initialised)
		verror("getcharsize: vogle not initialised");

	if (qqvdevice.attr->a.softtext) {
		if (!qqLoaded)
			verror("getcharsize: no software font qqLoaded");
	
		*height = (float)(qqftab.as - qqftab.dec) * QQSCSIZEY;

		if (qqvdevice.attr->a.fixedwidth)
			*width = qqftab.mw * QQSCSIZEX;
		else
			*width = (qqftab.ind[c - 32][1] - qqftab.ind[c - 32][0]) * QQSCSIZEX;
	} else {
		qqVtoWxy(qqvdevice.hwidth, qqvdevice.hheight, width, height);
		qqVtoWxy(0.0, 0.0, &a, &b);
		*height -= b;
		*width -= a;
	}
}

/*
 * drawchar
 *
 * Display a character from the currently qqLoaded font.
 */
void
qqdrawchar(c)
	int	c;
{
	char	*p, *e;
	Token	*pr;
	int	Move, i, x, y, xt, yt, sync;
	float	xp, yp, tmp, xsave, ysave;
	float	a, b, tcos, tsin;

	if (qqvdevice.inobject) {
		pr = qqnewtokens(2);

		pr[0].i = DRAWCHAR;
		pr[1].i = c;

		return;
	}

	if (!qqvdevice.attr->a.softtext) {
		if (!qqvdevice.cpVvalid)
			qqactual_move();
		qqdrawhardchar(c);
		qqrmove(qqgetfontwidth(), 0.0, 0.0);
		return;
	}

	if (!qqLoaded)
		verror("drawchar: no font qqLoaded");

	if (sync = qqvdevice.sync)
		qqvdevice.sync = 0;

	tcos = qqvdevice.attr->a.textcos;
	tsin = qqvdevice.attr->a.textsin;


	if ((i = c - 32) < 0)
		i = 0;
	if (i >= qqnchars)
		i = qqnchars - 1;

	xsave = qqvdevice.cpW[V_X];
	ysave = qqvdevice.cpW[V_Y];

	Move = 1;
	xt = (qqvdevice.attr->a.fixedwidth ? -qqftab.mw / 2 : XCOORD(qqftab.ind[i][0]));
	yt = qqftab.dec;

	/* Justify in the x direction */
	if (qqvdevice.attr->a.justify & V_XCENTERED) {
		xt = 0;
	} else if (qqvdevice.attr->a.justify & V_RIGHT) {
		xt = (qqvdevice.attr->a.fixedwidth ? qqftab.mw / 2 : -XCOORD(qqftab.ind[i][0]));
	}

	/* Justify in the y direction */
	if (qqvdevice.attr->a.justify & V_YCENTERED) {
		yt = 0;
	} else if (qqvdevice.attr->a.justify & V_TOP) {
		yt = -qqftab.dec;
	}

	e = qqftab.ind[i+1];
	p = qqftab.ind[i] + 2;
	while(p < e) {
		x = XCOORD((int)(*p++));
		y = YCOORD((int)(*p++));
		if (x != -50) {			/* means move */
			xp = (float)(x - xt)*QQSCSIZEX;
			yp = (float)(y - yt)*QQSCSIZEY;
			tmp = xp;
			xp = tcos*tmp - tsin*yp + xsave;
			yp = tsin*tmp + tcos*yp + ysave;
			if (Move) {
				Move = 0;
				qqmove(xp, yp, qqvdevice.cpW[V_Z]);
			} else 
			        qqdraw(xp, yp, qqvdevice.cpW[V_Z]);
		} else
			Move = 1;
	}
	/*
	 * Move to right hand of character.
	 */
	
	tmp = qqvdevice.attr->a.fixedwidth ? (float)qqftab.mw : (float)(qqftab.ind[i][1] - qqftab.ind[i][0]);
	tmp *= QQSCSIZEX;
	xsave += tcos*tmp;
	ysave += tsin*tmp;
	qqmove(xsave, ysave, qqvdevice.cpW[V_Z]);

	if (sync) {
		qqvdevice.sync = 1;
		(*qqvdevice.dev.Vsync)();
	}
}

/*
 * qqdrawhardchar
 *
 *	Displays a hardware character.
 *	NOTE: Only does gross clipping to the viewport.
 *	      Current world position becomes undefined (ie you have
 *	      to do an explicit move after calling hardware text)
 */
static void
qqdrawhardchar(c)
	char	c;
{

	if (!qqvdevice.clipoff) {
		if (qqvdevice.cpVx - (int)qqvdevice.hwidth > qqvdevice.maxVx)
			return;

		if (qqvdevice.cpVx < qqvdevice.minVx)
			return;

		if (qqvdevice.cpVy - (int)qqvdevice.hheight > qqvdevice.maxVy)
			return;

		if (qqvdevice.cpVy < qqvdevice.minVy)
			return;
	}

	(*qqvdevice.dev.Vchar)(c);
}

/*
 * textsize
 *
 * set software character scaling values 
 *
 * Note: Only changes software char size. Should be called
 * after a font has been qqLoaded.
 *
 */
void
qqtextsize(width, height)
	float	width, height;
{
	float	a;
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("textsize: vogle not initialised");

	if (!qqvdevice.attr->a.softtext)
		return;

	if (!qqLoaded)
		verror("textsize: no font qqLoaded");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(3);

		tok[0].i = TEXTSIZE;
		tok[1].f = width;
		tok[2].f = height;

		return;
	}

	a = (float)MAX(qqftab.mw, (qqftab.as - qqftab.dec));
	qqvdevice.attr->a.fontwidth = width;
	qqvdevice.attr->a.fontheight = height;
	QQSCSIZEX = width / a;
	QQSCSIZEY = height / a;
}

/*
 * qqgetfontwidth
 *
 * Return the maximum Width of the current font.
 *
 */
float
qqgetfontwidth()
{
	float	a, b, c, d;

	if (!qqvdevice.initialised)
		verror("qqgetfontwidth: vogle not initialised");


	if (qqvdevice.attr->a.softtext) {
		if (!qqLoaded)
			verror("qqgetfontwidth: No font qqLoaded");

		return((float)(QQSCSIZEX * MAX(qqftab.mw, (qqftab.as - qqftab.dec))));
	} else {
		qqVtoWxy(qqvdevice.hwidth, qqvdevice.hheight, &c, &d);
		qqVtoWxy(0.0, 0.0, &a, &b);
		c -= a;
		return(c);
	}
}

/* 
 * qqgetfontheight
 *
 * Return the maximum Height of the current font
 */
float 
qqgetfontheight()
{
	float	a, b, c, d;

	if (!qqvdevice.initialised)
		verror("qqgetfontheight: vogle not initialized");

	if (qqvdevice.attr->a.softtext) {
		if (!qqLoaded)
			verror("qqgetfontheight: No font qqLoaded");

		return((float)(QQSCSIZEY * MAX(qqftab.mw, (qqftab.as - qqftab.dec))));
	} else {
		qqVtoWxy(qqvdevice.hwidth, qqvdevice.hheight, &c, &d);
		qqVtoWxy(0.0, 0.0, &a, &b);
		d -= b;
		return(d);
	}
}

/*
 * getfontsize
 *
 * get the current character size in user coords.
 * Hardware text may or may not be really that accurate,
 * depending on what type of font you are using on the device.
 * For software Hershey fonts, the character width is that of
 * a the widest character and the height the height of the tallest.
 *
 */
void
qqgetfontsize(cw, ch)
	float 	*cw, *ch;
{
	*cw = qqgetfontwidth();
	*ch = qqgetfontheight();
}

/*
 * drawhstr
 *
 * Display the text string using the currently qqLoaded Hershey font
 */
static void
qqdrawhstr(string)
	char	*string;
{
	char	c;
	int	i, sync, oldClipoff, NeedClip, oldJustify;
	float	p[4], q[4];
	float	qqstrlength(), qqgetfontheight();


	if (!qqvdevice.initialised) 
		qqverror("drawhstr: not initialized");

	/*
	 * For the duration of hershey strings, turn off
	 * "qqvdevice.attr->a.justify" as we have already compensated
	 * for it in drawstr()
	 */
	oldJustify = qqvdevice.attr->a.justify;
	qqvdevice.attr->a.justify = V_LEFT;

	/*
	 * Determine if we can get away with "clipoff"
	 */
	oldClipoff = qqvdevice.clipoff;
	if (!oldClipoff) {  /* Only do this if we have to ... ie. if clipping is on */
		q[0] = qqvdevice.cpW[V_X];
		q[1] = qqvdevice.cpW[V_Y];
		q[2] = qqvdevice.cpW[V_Z];
		q[3] = 1.0;
		multvector(p, q, qqvdevice.transmat->m);
		NeedClip = 0;
		for (i = 0; i < 3; i++)
			NeedClip = ((p[3] + p[i] < 0.0) ||
				    (p[3] - p[i] < 0.0)) || NeedClip;
		if (!NeedClip) {   	/* The other end, only if we have to */
			q[0] += qqstrlength(string);
			q[1] += qqgetfontheight();
			multvector(p, q, qqvdevice.transmat->m);
			NeedClip = 0;
			for (i = 0; i < 3; i++)
				NeedClip = ((p[3] + p[i] < 0.0) || 
					    (p[3] - p[i] < 0.0)) || NeedClip;
		}
		if (!NeedClip)
			qqvdevice.clipoff = 1; /* ie. Don't clip */

	}

	/*
	 * Now display each character
	 *
	 */
	if (sync = qqvdevice.sync)
		qqvdevice.sync = 0;

	while (c = *string++)
		qqdrawchar(c);
	
	if (sync) {
		qqvdevice.sync = 1;
		(*qqvdevice.dev.Vsync)();
	}

	/*
	 * Restore ClipOff
	 */
	qqvdevice.clipoff = oldClipoff;
	qqvdevice.attr->a.justify = oldJustify;
}

/*
 * drawstr
 *
 * Draw a string from the current pen position.
 *
 */
void
qqdrawstr(string)
	char 	*string;
{
	float	sl, width, height, cx, cy;
	float	tcos, tsin;
	char	*str = string, c;
	Token	*tok;

	if(!qqvdevice.initialised) 
		verror("drawstr: vogle not initialized");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(2 + strlen(str) / sizeof(Token));

		tok[0].i = DRAWSTR;
		strcpy((char *)&tok[1], str);

		return;
	}

#ifdef SUN_CC
	/* Note that SUN's unbundled ANSI C compiler bitches about this
	sl = (float)strlen(string);
	... so we change it to ... 
	(The (float) IS an explicit cast .. isn't it?) */

	sl = (float)(size_t)strlen(string);
#else
	sl = (float)strlen(string);
#endif

	tcos = qqvdevice.attr->a.textcos;
	tsin = qqvdevice.attr->a.textsin;

	height = qqgetfontheight();
	width = qqstrlength(string);

	/* Justify in the x direction */
	if (qqvdevice.attr->a.justify & V_XCENTERED) {
		width /= 2.0;
	} else if (qqvdevice.attr->a.justify & V_RIGHT) {
		;	/* NO change */
	} else {	/* V_LEFT as default */
		width = 0.0;
	}

	/* Justify in the y direction */
	if (qqvdevice.attr->a.justify & V_YCENTERED) {
		height /= 2.0;
	} else if (qqvdevice.attr->a.justify & V_TOP) {
		;	/* NO change */
	} else {	/* V_BOTTOM as default */
		height = 0.0;
	}

	cx = qqvdevice.cpW[V_X] + height * tsin - width * tcos;
	cy = qqvdevice.cpW[V_Y] - height * tcos - width * tsin;

	qqmove(cx, cy, qqvdevice.cpW[V_Z]);
		
		
	if (qqvdevice.attr->a.softtext) {
		/*  As we are using software text then call the routine 
			    to display it in the current font */
		qqdrawhstr(string);
	} else {
		qqactual_move();	/* Really move there */

		/*   If not clipping then simply display text and return  */

		if (qqvdevice.clipoff || (1 == 1))
			(*qqvdevice.dev.Vstring)(string);
		else { /* Check if string is within viewport */
			if (qqvdevice.cpVx > qqvdevice.minVx &&
			    qqvdevice.cpVx + (int)(sl * (qqvdevice.hwidth - 1)) < qqvdevice.maxVx &&
			    qqvdevice.cpVy - (int)qqvdevice.hheight < qqvdevice.maxVy &&
		            qqvdevice.cpVy > qqvdevice.minVy)
				(*qqvdevice.dev.Vstring)(string);
			else
				while (c = *str++) {
					qqdrawhardchar(c);
					qqvdevice.cpVx += qqvdevice.hwidth;
				}
		}

		qqmove(cx + qqgetfontwidth() * sl, cy, qqvdevice.cpW[V_Z]);

	}
}

/*
 * iqqstrlength
 *
 * Find out the length of a string in raw "Hershey coordinates".
 */
static	int
iqqstrlength(s)
	char	*s;
{
	char	c;
	int	i, len = 0;
	
	if (qqvdevice.attr->a.fixedwidth) 
		return((int)(strlen(s) * qqftab.mw));
	else {
		while (c = *s++) {
			if ((i = (int)c - 32) < 0 || i >= qqnchars)
				i = qqnchars - 1;

			len += (qqftab.ind[i][1] - qqftab.ind[i][0]);
		}
		return (len);
	}
}

/*
 * qqstrlength
 *
 * Find out the length (in world coords) of a string.
 *
 */
float
qqstrlength(s)
	char	*s;
{
	if (!qqvdevice.initialised)
		verror("qqstrlength: vogle not initialised");

	if (qqvdevice.attr->a.softtext)
		return((float)(iqqstrlength(s) * QQSCSIZEX));
	else
#ifdef SUN_CC
		/* Note that SUN's unbundled ANSI C compiler bitches here 
		return((float)(strlen(s) * qqgetfontwidth()));
		... so we write it as ... */
		return((float)((size_t)strlen(s) * qqgetfontwidth()));
#else
		return((float)strlen(s) * qqgetfontwidth());
#endif
}

/*
 * boxtext
 *
 * Draw text so it fits in a "box" - note only works with hershey text
 */
void
qqboxtext(x, y, l, h, s)
	float	x, y, l, h;
	char	*s;
{
	float	oQQSCSIZEx, oQQSCSIZEy;
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("boxtext: vogle not initialised");
	
	if (!qqvdevice.attr->a.softtext)
		verror("boxtext: need a hershey vector font qqLoaded");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(6 + strlen(s) / sizeof(Token));

		tok[0].i = BOXTEXT;
		tok[1].f = x;
		tok[2].f = y;
		tok[3].f = l;
		tok[4].f = h;
		strcpy((char *)&tok[5], s);

		return;
	}

	oQQSCSIZEx = QQSCSIZEX;
	oQQSCSIZEy = QQSCSIZEY;
	/*
	 * set width so string length is the same a "l" 
	 */
	QQSCSIZEX = l / (float)iqqstrlength(s);

	/* 
	 * set character height so it's the same as "h" 
	 */
	QQSCSIZEY = h / (float)(qqftab.as - qqftab.dec);
	qqmove(x, y, qqvdevice.cpW[V_Z]);
	
	qqdrawstr(s);

	QQSCSIZEX = oQQSCSIZEx;
	QQSCSIZEY = oQQSCSIZEy;
}

/*
 * boxfit
 *
 * Set up the scales etc for text so that a string of "qqnchars" characters
 * of the maximum width in the font fits in a box.
 */
void
qqboxfit(l, h, qqnchars)
	float	l, h;
	int	qqnchars;
{
	if (!qqvdevice.initialised) 
		verror("boxfit: vogle not initialised");

	if (!qqvdevice.attr->a.softtext) 
		verror("boxfit: cannot rescale hardware font");

	QQSCSIZEX = l / (float)(qqnchars * qqftab.mw);
	QQSCSIZEY = h / (float)(qqftab.as - qqftab.dec);
}

/*
 * centertext
 *
 *	Turns centering of text on or off
 *	Turns off all other justifying.
 *	(Just like in old VOGLE).
 */
void
qqcentertext(onoff)
	int	onoff;
{
	if (onoff)
		qqvdevice.attr->a.justify = V_XCENTERED | V_YCENTERED;
	else
		qqvdevice.attr->a.justify = V_LEFT | V_BOTTOM;
}

/*
 * textjustify
 *
 *	Directly turns on/off justification
 */
void
qqtextjustify(val)
	unsigned val;
{
	qqvdevice.attr->a.justify = val;
}

/*
 * xcentertext
 *
 *	Directly turns on xcentering
 */
void
qqxcentertext()
{
	qqvdevice.attr->a.justify |= V_XCENTERED;
	qqvdevice.attr->a.justify &= ~(V_LEFT | V_RIGHT);
}

/*
 * ycentertext
 *
 *	Directly turns on ycentering
 */
void
qqycentertext()
{
	qqvdevice.attr->a.justify |= V_YCENTERED;
	qqvdevice.attr->a.justify &= ~(V_TOP | V_BOTTOM);
}

/*
 * leftjustify
 *
 *	Turns on leftjustification
 */
void
qqleftjustify()
{
	/*
	 * If left justification is on, then V_XCENTER must be off
	 * and V_RIGHT must be off
	 */
	qqvdevice.attr->a.justify |= V_LEFT;
	qqvdevice.attr->a.justify &= ~(V_RIGHT | V_XCENTERED);
}

/*
 * rightjustify
 *
 *	Turns on rightjustification
 */
void
qqrightjustify()
{
	/*
	 * If right justification is on, then V_XCENTER must be off
	 * and V_LEFT must be off
	 */
	qqvdevice.attr->a.justify |= V_RIGHT;
	qqvdevice.attr->a.justify &= ~(V_LEFT | V_XCENTERED);
}

/*
 * topjustify
 *
 *	Turns on topjustification
 */
void
qqtopjustify()
{
	/*
	 * If top justification is on, then V_YCENTER must be off
	 * and V_BOTTOM must be off
	 */
	qqvdevice.attr->a.justify |= V_TOP;
	qqvdevice.attr->a.justify &= ~(V_BOTTOM | V_YCENTERED);
}


/*
 * bottomjustify
 *
 *	Turns on bottomjustification
 */
void
qqbottomjustify()
{
	/*
	 * If bottom justification is on, then V_YCENTER must be off
	 * and V_TOP must be off
	 */
	qqvdevice.attr->a.justify |= V_BOTTOM;
	qqvdevice.attr->a.justify &= ~(V_TOP | V_YCENTERED);
}

/*
 * fixedwidth
 *
 *	Turns fixedwidth text on or off
 */
void
qqfixedwidth(onoff)
	int	onoff;
{
	qqvdevice.attr->a.fixedwidth = onoff;
}

/*
 * textang
 *
 * set software character angle in degrees
 *
 * strings will be written along a line 'ang' degrees from the 
 * horizontal screen direction
 *
 * Note: only changes software character angle
 *
 */
void
qqtextang(ang)
	float	ang;
{
	Token	*tok;

	if (!qqvdevice.initialised) 
		verror("textang: vogle not initialised");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(3);

		tok[0].i = TEXTANG;
		tok[1].f = cos((double)(ang * D2R));
		tok[2].f = sin((double)(ang * D2R));

		return;
	}

	qqvdevice.attr->a.textcos = cos((double)(ang * D2R));
	qqvdevice.attr->a.textsin = sin((double)(ang * D2R));
}

/*
 * Actually do a move (multplying by the current transform and updating the
 * actual screen coords) instead of just setting the current spot in world
 * coords.
 */
static void
qqactual_move()
{
	Vector	v2;

	multvector(v2, qqvdevice.cpW, qqvdevice.transmat->m);
	qqvdevice.cpVvalid = 0;

	qqvdevice.cpVx = qqWtoVx(v2);
	qqvdevice.cpVy = qqWtoVy(v2);
	
	copyvector(qqvdevice.cpWtrans, v2);
}
