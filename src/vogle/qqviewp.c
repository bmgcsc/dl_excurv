#include <stdio.h>
#include "vogle.h"

static	Vstack	*qqvsfree = (Vstack *)NULL;

/*
 * pushviewport
 *
 * pushes the current viewport on the viewport stack
 *
 */
void
qqpushviewport()
{
	Vstack	*nvport;
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("pushviewport: vogle not initialised");
	
	if (qqvdevice.inobject) {
		tok = qqnewtokens(1);

		tok->i = PUSHVIEWPORT;

		return;
	}

	if (qqvsfree != (Vstack *)NULL) {
		nvport = qqvdevice.viewport;
		qqvdevice.viewport = qqvsfree;
		qqvsfree = qqvsfree->back;
		qqvdevice.viewport->back = nvport;
		qqvdevice.viewport->v.left = nvport->v.left;
		qqvdevice.viewport->v.right = nvport->v.right;
		qqvdevice.viewport->v.bottom = nvport->v.bottom;
		qqvdevice.viewport->v.top = nvport->v.top;
	} else {
		nvport = (Vstack *)vallocate(sizeof(Vstack));
		nvport->back = qqvdevice.viewport;
		nvport->v.left = qqvdevice.viewport->v.left;
		nvport->v.right = qqvdevice.viewport->v.right;
		nvport->v.bottom = qqvdevice.viewport->v.bottom;
		nvport->v.top = qqvdevice.viewport->v.top;
		qqvdevice.viewport = nvport;
	}
}

/*
 * popviewport
 *
 * pops the top viewport off the viewport stack.
 *
 */
void
qqpopviewport()
{
	Token	*tok;
	Vstack	*nvport;

	if (!qqvdevice.initialised)
		verror("popviewport: vogle not initialised");
	
	if (qqvdevice.inobject) {
		tok = qqnewtokens(1);

		tok->i = POPVIEWPORT;

		return;
	}

	if (qqvdevice.viewport->back == (Vstack *)NULL)
		verror("popviewport: viewport stack underflow");
	else {
		nvport = qqvdevice.viewport;
		qqvdevice.viewport = qqvdevice.viewport->back;
		nvport->back = qqvsfree;
		qqvsfree = nvport;
	}

	qqvdevice.maxVx = qqvdevice.viewport->v.right * qqvdevice.sizeX;
	qqvdevice.maxVy = qqvdevice.viewport->v.top * qqvdevice.sizeY;
	qqvdevice.minVx = qqvdevice.viewport->v.left * qqvdevice.sizeX;
	qqvdevice.minVy = qqvdevice.viewport->v.bottom * qqvdevice.sizeY;

	qqCalcW2Vcoeffs();
}

/*
 * viewport
 *
 * Define a Viewport in Normalized Device Coordinates
 *
 * The viewport defines that fraction of the screen that the window will
 * be mapped onto.  The screen dimension is -1.0 -> 1.0 for both X & Y.
 */
void
qqviewport(xlow, xhigh, ylow, yhigh)
	float	xlow, xhigh, ylow, yhigh;
{
	Token	*tok;
	char	buf[35];

	if (!qqvdevice.initialised) 
		verror("viewport: vogle not initialised");

	/*
	 *	A few preliminary checks ....
	 */
	
	if (xlow >= xhigh) {
		sprintf(buf,"viewport: xleft(%5.2f) >= xright(%5.2f)", xlow, xhigh);
		verror(buf);
	} 
	if (ylow >= yhigh) {
		sprintf(buf,"viewport: ybottom(%5.2f) >= ytop(%5.2f)", ylow, yhigh);
		verror(buf);
	} 

	if (qqvdevice.inobject) {
		tok = qqnewtokens(5);

		tok[0].i = VIEWPORT;
		tok[1].f = xlow;
		tok[2].f = xhigh;
		tok[3].f = ylow;
		tok[4].f = yhigh;

		return;
	}

	/*
	 * convert to 0.0 to 1.0
	 */
	xlow = xlow / 2 + 0.5;
	xhigh = xhigh / 2 + 0.5;
	ylow = ylow / 2 + 0.5;
	yhigh = yhigh / 2 + 0.5;

	/*
	 * Make sure the viewport stack knows about us.....
	 */
	qqvdevice.viewport->v.left = xlow;
	qqvdevice.viewport->v.right = xhigh;
	qqvdevice.viewport->v.bottom = ylow;
	qqvdevice.viewport->v.top = yhigh;

	qqvdevice.maxVx = xhigh * qqvdevice.sizeX;
	qqvdevice.maxVy = yhigh * qqvdevice.sizeY;
/*	fprintf(stderr," q: %d %d \n",qqvdevice.sizeX,qqvdevice.sizeY);*/
	qqvdevice.minVx = xlow * qqvdevice.sizeX;
	fprintf(stderr," q: %f %f \n",qqvdevice.minVx,qqvdevice.minVy);
	qqvdevice.minVy = ylow * qqvdevice.sizeY;

	qqCalcW2Vcoeffs();
}

/*
 * getviewport
 *
 *	Returns the left, right, bottom and top limits of the current
 *	viewport.
 */
void
qqgetviewport(left, right, bottom, top)
	float	*left, *right, *bottom, *top;
{
	*left = (qqvdevice.viewport->v.left - 0.5) * 2;
	*right = (qqvdevice.viewport->v.right - 0.5) * 2;
	*bottom = (qqvdevice.viewport->v.bottom - 0.5) * 2;
	*top = (qqvdevice.viewport->v.top - 0.5) * 2;
}
