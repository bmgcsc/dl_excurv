#include "vogle.h"

/*
 * getgp
 *
 *	return the current (x, y, z) graphics position
 */
void
qqgetgp(x, y, z)
	float	*x, *y, *z;
{
	*x = qqvdevice.cpW[V_X];
	*y = qqvdevice.cpW[V_Y];
	*z = qqvdevice.cpW[V_Z];
}

/*
 * getgp2
 *
 *	return the current (x, y) graphics position
 */
void
qqgetgp2(x, y)
	float	*x, *y;
{
	*x = qqvdevice.cpW[V_X];
	*y = qqvdevice.cpW[V_Y];
}

/* 
 * getgpt
 *
 *	return the current transformed graphics position.
 */
void    
qqgetgpt(x, y, z, w)
        float   *x, *y, *z, *w;
{
	qqmultvector(qqvdevice.cpWtrans, qqvdevice.cpW, qqvdevice.transmat->m);

        *x = qqvdevice.cpWtrans[V_X];
        *y = qqvdevice.cpWtrans[V_Y];
        *z = qqvdevice.cpWtrans[V_Z];
        *w = qqvdevice.cpWtrans[V_W];
}

/*
 * sgetgp2
 *
 *	return the current (x, y) graphics position in screen coordinates
 */
void
qqsgetgp2(x, y)
	float	*x, *y;
{
	float	sx, sy;

	sx = qqvdevice.maxVx - qqvdevice.minVx;
	sy = qqvdevice.maxVy - qqvdevice.minVy;

	qqmultvector(qqvdevice.cpWtrans, qqvdevice.cpW, qqvdevice.transmat->m);
	*x = 2.0 * WtoVx(qqvdevice.cpWtrans) / sx - 1.0;
	*y = 2.0 * WtoVy(qqvdevice.cpWtrans) / sy - 1.0;
}
