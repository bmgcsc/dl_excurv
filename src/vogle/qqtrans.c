#include "vogle.h"

#ifdef	TC

extern	double	cos();
extern	double	sin();

#else 

#include <math.h>

#endif

/*
 * translate
 * 
 * Set up a translation matrix and premultiply it and 
 * the top matrix on the stack.
 *
 */
void
qqtranslate(x, y, z)
	float 	x, y, z;
{
	Token	*tok;
	Matrix	tmat;

	if (!qqvdevice.initialised) 
		verror("translate: vogle not initialised");

        if (qqvdevice.inobject) {
		tok = qqnewtokens(4);

		tok[0].i = TRANSLATE;
		tok[1].f = x;
		tok[2].f = y;
		tok[3].f = z;

                return;
        }

	/*
	 * Do the operations directly on the top matrix of
	 * the stack to speed things up.
	 */
	qqvdevice.transmat->m[3][0] += x * qqvdevice.transmat->m[0][0]
				   + y * qqvdevice.transmat->m[1][0]
				   + z * qqvdevice.transmat->m[2][0];

	qqvdevice.transmat->m[3][1] += x * qqvdevice.transmat->m[0][1]
				   + y * qqvdevice.transmat->m[1][1]
				   + z * qqvdevice.transmat->m[2][1];

	qqvdevice.transmat->m[3][2] += x * qqvdevice.transmat->m[0][2]
				   + y * qqvdevice.transmat->m[1][2]
				   + z * qqvdevice.transmat->m[2][2];

	qqvdevice.transmat->m[3][3] += x * qqvdevice.transmat->m[0][3]
				   + y * qqvdevice.transmat->m[1][3]
				   + z * qqvdevice.transmat->m[2][3];
}

/*
 * rotate
 * 
 * Set up a rotate matrix and premultiply it with 
 * the top matrix on the stack.
 *
 */
void
qqrotate(r, axis)
	float	r;
	char	axis;
{
	Token		*tok;
	char		str[2];
	register float	costheta, sintheta, tmp;

	if (!qqvdevice.initialised)
		verror("rotate: vogle not initialised");

        if (qqvdevice.inobject) {
		tok = qqnewtokens(3);

		tok[0].i = ROTATE;
		tok[1].f = r;
		tok[2].i = axis;

                return;
        }

	/*
	 * Do the operations directly on the top matrix of
	 * the stack to speed things up.
	 */
	costheta = cos((double)(D2R * r));
	sintheta = sin((double)(D2R * r));

	switch(axis) {
	case 'x':
	case 'X':
		tmp = qqvdevice.transmat->m[1][0];
		qqvdevice.transmat->m[1][0] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[2][0];
		qqvdevice.transmat->m[2][0] = costheta * qqvdevice.transmat->m[2][0]
					  - sintheta * tmp;

		tmp = qqvdevice.transmat->m[1][1];
		qqvdevice.transmat->m[1][1] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[2][1];
		qqvdevice.transmat->m[2][1] = costheta * qqvdevice.transmat->m[2][1]
					  - sintheta * tmp;
		tmp = qqvdevice.transmat->m[1][2];
		qqvdevice.transmat->m[1][2] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[2][2];
		qqvdevice.transmat->m[2][2] = costheta * qqvdevice.transmat->m[2][2]
					  - sintheta * tmp;

		tmp = qqvdevice.transmat->m[1][3];
		qqvdevice.transmat->m[1][3] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[2][3];
		qqvdevice.transmat->m[2][3] = costheta * qqvdevice.transmat->m[2][3]
					  - sintheta * tmp;
		break;
	case 'y':
	case 'Y':
		tmp = qqvdevice.transmat->m[0][0];
		qqvdevice.transmat->m[0][0] = costheta * tmp
					  - sintheta * qqvdevice.transmat->m[2][0];
		qqvdevice.transmat->m[2][0] = sintheta * tmp
					  + costheta * qqvdevice.transmat->m[2][0];
		tmp = qqvdevice.transmat->m[0][1];
		qqvdevice.transmat->m[0][1] = costheta * tmp
					  - sintheta * qqvdevice.transmat->m[2][1];
		qqvdevice.transmat->m[2][1] = sintheta * tmp
					  + costheta * qqvdevice.transmat->m[2][1];
		tmp = qqvdevice.transmat->m[0][2];
		qqvdevice.transmat->m[0][2] = costheta * tmp
					  - sintheta * qqvdevice.transmat->m[2][2];
		qqvdevice.transmat->m[2][2] = sintheta * tmp
					  + costheta * qqvdevice.transmat->m[2][2];
		tmp = qqvdevice.transmat->m[0][3];
		qqvdevice.transmat->m[0][3] = costheta * tmp
					  - sintheta * qqvdevice.transmat->m[2][3];
		qqvdevice.transmat->m[2][3] = sintheta * tmp
					  + costheta * qqvdevice.transmat->m[2][3];
		break;
	case 'z':
	case 'Z':
		tmp = qqvdevice.transmat->m[0][0];
		qqvdevice.transmat->m[0][0] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[1][0];
		qqvdevice.transmat->m[1][0] = costheta * qqvdevice.transmat->m[1][0]
					  - sintheta * tmp;

		tmp = qqvdevice.transmat->m[0][1];
		qqvdevice.transmat->m[0][1] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[1][1];
		qqvdevice.transmat->m[1][1] = costheta * qqvdevice.transmat->m[1][1]
					  - sintheta * tmp;

		tmp = qqvdevice.transmat->m[0][2];
		qqvdevice.transmat->m[0][2] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[1][2];
		qqvdevice.transmat->m[1][2] = costheta * qqvdevice.transmat->m[1][2]
					  - sintheta * tmp;

		tmp = qqvdevice.transmat->m[0][3];
		qqvdevice.transmat->m[0][3] = costheta * tmp
					  + sintheta * qqvdevice.transmat->m[1][3];
		qqvdevice.transmat->m[1][3] = costheta * qqvdevice.transmat->m[1][3]
					  - sintheta * tmp;
		break;
	default:
		verror("rotate: illegal axis of rotation");
	}
}
