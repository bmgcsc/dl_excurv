#include <stdio.h>
#include "vogle.h"

static	Astack	*qqasfree = (Astack *)NULL;

/*
 * qqcopyattributes
 *
 *	Copies attribute stack entries from b to a
 */
static	void
qqcopyattributes(a, b)
	Attribute	*a, *b;
{
	if (b->style) {
		if (a->style)
			free(a->style);

		a->style = (unsigned char *)vallocate(strlen(b->style) + 1);
		strcpy(a->style, b->style);
	}

	a->style = b->style;
	a->dashp = b->dashp;
	a->dash = b->dash;
	a->adist = b->adist;
	a->color = b->color;
	a->fill = b->fill;
	a->hatch = b->hatch;
	a->backbuf = b->backbuf;
	a->textcos = b->textcos;
	a->textsin = b->textsin;
	a->hatchcos = b->hatchcos;
	a->hatchsin = b->hatchsin;
	a->hatchpitch = b->hatchpitch;
	a->justify = b->justify;
	a->fixedwidth = b->fixedwidth;
	a->fontwidth = b->fontwidth;
	a->fontheight = b->fontheight;
	a->softtext = b->softtext;
	strcpy(a->font, b->font);
}

/*
 * pushattributes
 *
 * save the current attributes on the matrix stack
 *
 */
void
qqpushattributes()
{
	Astack	*nattr;
	Token	*p;

	if (!qqvdevice.initialised)
		verror("pushattributes:  vogle not initialised");
	
	if (qqvdevice.inobject) {
		p = qqnewtokens(1);

		p[0].i = PUSHATTRIBUTES;

		return;
	}

	if (qqasfree != (Astack *)NULL) {
		nattr = qqvdevice.attr;
		qqvdevice.attr = qqasfree;
		qqasfree = qqasfree->back;
		qqvdevice.attr->back = nattr;
		qqcopyattributes(&qqvdevice.attr->a, &nattr->a);
	} else {	
		nattr = (Astack *)vallocate(sizeof(Astack));
		nattr->back = qqvdevice.attr;
		qqcopyattributes(&nattr->a, &qqvdevice.attr->a);
		qqvdevice.attr = nattr;
	}
}

/*
 * popattributes
 *
 * pop the top entry on the attribute stack 
 *
 */
void
qqpopattributes()
{
	Astack	*nattr;
	Token	*p;

	if (!qqvdevice.initialised)
		verror("popattributes: vogle not initialised");
	
	if (qqvdevice.inobject) {
		p = qqnewtokens(1);

		p[0].i = POPATTRIBUTES;

		return;
	}

	if (qqvdevice.attr->back == (Astack *)NULL) 
		verror("popattributes: attribute stack is empty");
	else {
		nattr = qqvdevice.attr;
		font(qqvdevice.attr->back->a.font);
		qqvdevice.attr = qqvdevice.attr->back;
		nattr->back = qqasfree;
		qqasfree = nattr;
	}

	/*
	 * Restore some stuff...
	 */
	qqcolor(qqvdevice.attr->a.color);

	if (qqvdevice.attr->a.backbuf)
		backbuffer();

	if (qqvdevice.attr->a.softtext)
		qqtextsize(qqvdevice.attr->a.fontwidth, qqvdevice.attr->a.fontheight);
}

#ifdef	DEBUG

qqprintattribs(s)
	char	*s;
{
	printf("%s\n", s);
	printf("clipoff    = %d\n", qqvdevice.clipoff);
	printf("color      = %d\n", qqvdevice.attr->a.color);
	printf("fill       = %d\n", qqvdevice.attr->a.fill);
	printf("hatch      = %d\n", qqvdevice.attr->a.hatch);
	printf("textcos    = %f\n", qqvdevice.attr->a.textcos);
	printf("textsin    = %f\n", qqvdevice.attr->a.textsin);
	printf("hatchcos   = %f\n", qqvdevice.attr->a.hatchcos);
	printf("hatchsin   = %f\n", qqvdevice.attr->a.hatchsin);
	printf("hatchpitch = %f\n", qqvdevice.attr->a.hatchpitch);
	printf("justify    = %d\n", qqvdevice.attr->a.justify);
	printf("fixedwidth = %d\n", qqvdevice.attr->a.fixedwidth);
	printf("fontwidth  = %f\n", qqvdevice.attr->a.fontwidth);
	printf("fontwidth  = %f\n", qqvdevice.attr->a.fontheight);
	printf("font       = %s\n", qqvdevice.attr->a.font);
}

#endif
