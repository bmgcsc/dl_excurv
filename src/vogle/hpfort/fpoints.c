#include "../vogle.h"

/*
 * point_
 */
void
fpoint_(x, y, z)
	float 	*x, *y, *z;
{
	point(*x, *y, *z);
}

/*
 * point2_
 */
void
fpoint2_(x, y)
	float	*x, *y;
{
	point(*x, *y, 0.0);
}

