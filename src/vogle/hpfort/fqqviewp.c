#include "../vogle.h"

/*
 * pushviewport_
 */
void
fqqpushviewport_()
{
	qqpushviewport();
}

/*
 * popviewport_
 */
void
fqqpopviewport_()
{
	qqpopviewport();
}

/*
 * viewport_
 */
void
fqqviewport_(xlow, xhigh, ylow, yhigh)
	float	*xlow, *ylow, *xhigh, *yhigh;
{
	qqviewport(*xlow, *xhigh, *ylow, *yhigh);
}

/*
 * getviewport_
 */
void
fqqgetviewport_(left, right, bottom, top)
	float	*left, *right, *bottom, *top;
{
	qqgetviewport(left, right, bottom, top);
}

