#include "../vogle.h"

/*
 * backbuffer_
 */
int
fbackbuffer_()
{
	return(backbuffer());
}

/*
 * frontbuffer_
 */
void
ffrontbuffer_()
{
	frontbuffer();
}

/*
 * swapbuffers_
 */
int
fswapbuffers_()
{
	return(swapbuffers());
}
