#include "../vogle.h"

/*
 * polarview_
 */
void
fqqpolarview_(dist, azim, inc, twist)
	float	*dist, *azim, *inc, *twist;
{
	qqpolarview(*dist, *azim, *inc, *twist);
}

/*
 * up_
 */
void
fqqup_(x, y, z)
	float	*x, *y, *z;
{
	qqup(*x, *y, *z);
}

/*
 * lookat_
 */
void
fqqlookat_(vx, vy, vz, px, py, pz, twist)
	float  *vx, *vy, *vz, *px, *py, *pz, *twist;
{
	qqlookat(*vx, *vy, *vz, *px, *py, *pz, *twist);
}

/*
 * perspective_
 */
void
fqqperspective_(fov, aspect, hither, yon)
	float 	*fov, *aspect, *hither, *yon;
{
	qqperspective(*fov, *aspect, *hither, *yon);
}

/*
 * window_
 */
void
fqqwindow_(left, right, bottom, top, hither, yon)
	float 	*left, *right, *bottom, *top, *hither, *yon;
{
	qqwindow(*left, *right, *bottom, *top, *hither, *yon);
}

/*
 * ortho_
 */
void
fqqortho_(left, right, bottom, top, hither, yon)
	float 	*left, *right, *bottom, *top, *hither, *yon;
{
	qqortho(*left, *right, *bottom, *top, *hither, *yon);
}

/*
 * ortho2_
 */
void
fqqortho2_(left, right, bottom, top)
	float	*left, *right, *bottom, *top;
{
	qqortho2(*left, *right, *bottom, *top);
}
