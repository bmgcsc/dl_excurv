#include <stdio.h>
#include "../vogle.h"

/*
 * voutput_
 */
void
fqqvoutput_(path, len)
	char	*path;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, path, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	qqvoutput(buf);
}

/*
 * vinit_
 */
int
fqqvinit_(dev, len)
	char	*dev;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, dev, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	return(qqvinit(buf));
}

/*
 * vnewdev_
 */
void
fqqvnewdev_(dev, len)
	char	*dev;
	int	len;
{
	char		buf[BUFSIZ];
	register char	*p;

	strncpy(buf, dev, len);
	buf[len] = 0;

	for (p = &buf[len - 1]; *p == ' '; p--)
		;

	*++p = 0;

	qqvnewdev(buf);
}

/*
 * vgetdev_
 */
void
fqqvgetdev_(buf, len)
	char	*buf;
	int	len;
{
	register char	*p;

	(void)qqvgetdev(buf);

	for (p = &buf[len - 1]; *p == ' '; p--)
		;
}
	
/*
 * vexit_
 */
void
fqqvexit_()
{
	qqvexit();
}

/*
 * clear_
 */
void
fqqclear_()
{
	qqclear();
}

/*
 * color_
 */
void
fqqcolor_(col)
	int	*col;
{
	qqcolor(*col);
}

/*
 * _mapcolor
 */
void
fqqmapcolor_(indx, red, green, blue)
	int	*indx, *red, *green, *blue;
{
	qqmapcolor(*indx, *red, *green, *blue);
}

/*
 * getkey_
 */
int
fqqgetkey_()
{
	return(qqgetkey());
}

/*
 * checkkey_
 */
int
fqqcheckkey_()
{
	return(qqcheckkey());
}

/*
 * getdepth_
 */
int
fqqgetdepth_()
{
	return(qqgetdepth());
}

/*
 * locator_
 */
int
fqqlocator_(xaddr, yaddr)
	float	*xaddr, *yaddr;
{
	return(qqlocator(xaddr, yaddr));
}

/*
 * slocator_
 */
int
fqqslocator_(xaddr, yaddr)
	float	*xaddr, *yaddr;
{
	return(qqslocator(xaddr, yaddr));
}
