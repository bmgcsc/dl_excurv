#include "../vogle.h"

/*
 * circleprecision_
 */
void
fcircleprecision_(prec)
	int	*prec;
{
	circleprecision(*prec);
}

/*
 * arcprecision_
 */
void
farcprecision_(prec)
	int	*prec;
{
	circleprecision(*prec);
}

/*
 * arc_
 */
void
farc_(x, y, radius, startang, endang)
	float	*x, *y, *radius;
	float	*startang, *endang;
{
	arc(*x, *y, *radius, *startang, *endang);
}

/*
 * sector_
 */
void
fsector_(x, y, radius, startang, endang)
	float	*x, *y, *radius;
	float	*startang, *endang;
{
	sector(*x, *y, *radius, *startang, *endang);
}

/*
 * circle_
 */
void
fcircle_(x, y, radius)
	float	*x, *y, *radius;
{
	circle(*x, *y, *radius);
}

