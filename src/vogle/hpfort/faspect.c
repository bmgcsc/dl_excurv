#include "../vogle.h"

/*
 * getaspect_
 */
float
fgetaspect_()
{
	return(getaspect());
}

/*
 * getfactors_
 */
void
fgetfactors_(xr, yr)
	float	*xr, *yr;
{
	getfactors(xr, yr);
}

/*
 * getdisplaysize_
 */
void
fgetdisplaysize_(xs, ys)
	float	*xs, *ys;
{
	getdisplaysize(xs, ys);
}
