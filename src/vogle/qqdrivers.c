#include <stdio.h>
#include "vogle.h"

extern char	*getenv();

struct vdev	qqvdevice;

static FILE	*qqfp = NULL;
/*static FILE	*qqfp = stdout;*/

static int	qqallocated = 0;

/* device-independent function routines */

/*
 * voutput
 *
 *	redirect output - only for postscript, hpgl (this is not a feature)
 */
void
qqvoutput(path)
	char	*path;
{
	char	buf[128];

	if ((qqfp = fopen(path, "w")) == (FILE *)NULL) {
		sprintf(buf, "voutput: couldn't open %s", path);
		qqverror(buf);
	}
}

/*
 * _voutfile
 *
 *	return a pointer to the current output file - designed for internal
 * use only.
 */
FILE *
_qqvoutfile()
{
	return(qqfp);
}

/*
 * verror
 *
 *	print an error on the graphics device, and then exit. Only called
 * for fatal errors. We assume that stderr is always there.
 *
 */
void
qqverror(str)
	char	*str;
{
#ifdef MSWIN
	mswin_verror(str);
	if (qqvdevice.initialised)
		qqvexit();
#else
	if (qqvdevice.initialised)
		qqvexit();

	fprintf(stderr, "%s\n", str);
#endif
	return;
/*
	exit(1);
*/
}

/*
 * qqgetdev
 *
 *	get the appropriate device table structure
 */
static
qqgetdev(dev)
	char	*dev;
{
	char	*device = "";
	char	buf[100];

	if (dev == (char *)NULL || *dev == 0) {
		if ((device = getenv("VDEVICE")) == (char *)NULL)
			device = "";
	} else 
		device = dev;
	
#ifdef TG
	/*
	 * We can have one of many device names for Tk Graphic widgets, luckily
	 * all tk widgets names start with a leading dot, This should keep
	 * them from clashing with other devices names.
	 */
	if ((*device == '.') && (_TG_devcpy(device) >= 0))
		/* NULL BODY */ ;
	else
#endif

#ifdef SUN
	if (strncmp(device, "sun", 3) == 0)
		_SUN_devcpy();
	else
#endif
#ifdef X11
	if (strncmp(device, "qqX11", 5) == 0)
		_qqX11_devcpy();
	else
#endif
#ifdef DECX11
	if (strncmp(device, "decX11", 6) == 0)
		_DECX11_devcpy();
	else
#endif
#ifdef NeXT
	if (strncmp(device, "NeXT", 4) == 0)
		_NeXT_devcpy();
	else
#endif
#ifdef LASERWRITER
	if (strncmp(device, "laser", 5) == 0) {
		_LASER_devcpy();
	} else
#endif
#ifdef POSTSCRIPT
	if (strncmp(device, "postscript", 10) == 0) {
		_PS_devcpy();
	} else
	if (strncmp(device, "ppostscript", 11) == 0) {
		_PSP_devcpy();
	} else
	if (strncmp(device, "cps", 3) == 0) {
		_CPS_devcpy();
	} else
	if (strncmp(device, "pcps", 4) == 0) {
		_PCPS_devcpy();
	} else
#endif
#ifdef HPGL
	if (strncmp(device, "hpgla1", 6) == 0)
		_HPGL_A1_devcpy();
	else if (strncmp(device, "hpgla3", 6) == 0)
		_HPGL_A3_devcpy();
	else if (strncmp(device, "hpgla4", 6) == 0)
		_HPGL_A4_devcpy();
	else if (strncmp(device, "hpgla2", 6) == 0 || strncmp(device, "hpgl", 4) == 0)
		_HPGL_A2_devcpy();
	else
#endif
#ifdef DXY
	if (strncmp(device, "dxy", 3) == 0)
		_DXY_devcpy();
	else
#endif
#ifdef HPGT
	if (strncmp(device, "hpgt", 3) == 0)
		_HPGT_devcpy();
	else
#endif
#ifdef TEK
	if (strncmp(device, "tek", 3) == 0)
		_TEK_devcpy();
	else
#endif
#ifdef HERCULES
	if (strncmp(device, "hercules", 8) == 0)
		_hgc_devcpy();
	else
#endif
#ifdef MSWIN
	if (strncmp(device, "mswin", 5) == 0)
		_mswin_devcpy();
	else
#endif
#ifdef CGA
	if (strncmp(device, "cga", 3) == 0)
		_cga_devcpy();
	else
#endif
#ifdef EGA
	if (strncmp(device, "ega", 3) == 0)
		_ega_devcpy();
	else
#endif
#ifdef VGA
	if (strncmp(device, "vga", 3) == 0)
		_vga_devcpy();
	else
#endif
#ifdef SIGMA
	if (strncmp(device, "sigma", 5) == 0)
		_sigma_devcpy();
	else
#endif
	{
		if (*device == 0)
			sprintf(buf, "vogle: expected the enviroment variable qqvdevice to be set to the desired device.\n");
		else
			sprintf(buf, "vogle: %s is an invalid device type\n", device);

#ifdef MSWIN
		mswin_verror(buf);
#else
		fputs(buf, stderr);
		fprintf(stderr, "The devices compiled into this library are:\n");
#ifdef TG
		fprintf(stderr, "Tk Graphic widgets\n");
#endif
#ifdef SUN
		fprintf(stderr, "sun\n");
#endif
#ifdef X11
		fprintf(stderr, "X11\n");
		fprintf(stderr, "qqX11\n");
#endif
#ifdef DECX11
		fprintf(stderr, "decX11\n");
#endif
#ifdef NeXT
		fprintf(stderr, "NeXT\n");
#endif
#ifdef POSTSCRIPT
		fprintf(stderr, "postscript\n");
		fprintf(stderr, "ppostscript\n");
		fprintf(stderr, "cps\n");
		fprintf(stderr, "pcps\n");
#endif
#ifdef HPGL
		fprintf(stderr, "hpgla1\n");
		fprintf(stderr, "hpgla2 (or hpgl)\n");
		fprintf(stderr, "hpgla3\n");
		fprintf(stderr, "hpgla4\n");
#endif
#ifdef DXY
		fprintf(stderr, "dxy\n");
#endif
#ifdef TEK
		fprintf(stderr, "tek\n");
#endif
#ifdef HERCULES
		fprintf(stderr, "hercules\n");
#endif
#ifdef CGA
		fprintf(stderr, "cga\n");
#endif
#ifdef EGA
		fprintf(stderr, "ega\n");
#endif
#ifdef VGA
		fprintf(stderr, "vga\n");
#endif
#ifdef SIGMA
		fprintf(stderr, "sigma\n");
#endif
#endif
		exit(1);
	}
}

/*
 * vinit
 *
 * 	initialise VOGLE
 *
 */
int
qqvinit(device)
	char	*device;
{
	if (qqfp == NULL)
		qqfp = stdout;
	qqgetdev(device);

	if (qqvdevice.initialised)
		qqvexit();

	if (!qqallocated) {
		qqallocated = 1;
		qqvdevice.transmat = (Mstack *)vallocate(sizeof(Mstack));
		qqvdevice.transmat->back = (Mstack *)NULL;
		qqvdevice.attr = (Astack *)vallocate(sizeof(Astack));
		qqvdevice.attr->back = (Astack *)NULL;
		qqvdevice.viewport = (Vstack *)vallocate(sizeof(Vstack));
		qqvdevice.viewport->back = (Vstack *)NULL;
	}

	qqvdevice.clipoff = 1;
	qqvdevice.sync = 1;
	qqvdevice.upset = 0;
	qqvdevice.cpW[V_W] = 1.0;			/* never changes */

	qqvdevice.attr->a.font[0] = '\0';
	qqvdevice.attr->a.fill = 0;
	qqvdevice.attr->a.hatch = 0;
	qqvdevice.attr->a.backface = 0;
	qqvdevice.attr->a.justify = V_LEFT | V_BOTTOM;
	qqvdevice.attr->a.textcos = 1.0;
	qqvdevice.attr->a.textsin = 0.0;
	qqvdevice.attr->a.softtext = 0;
	qqvdevice.attr->a.fixedwidth = 0;
	qqvdevice.attr->a.hatchcos = 1.0;
	qqvdevice.attr->a.hatchsin = 0.0;
	qqvdevice.attr->a.hatchpitch = 0.1;
	qqvdevice.attr->a.style = (unsigned char *)NULL;
	qqvdevice.attr->a.dashp = (unsigned char *)NULL;
	qqvdevice.attr->a.adist = 0.0;
	qqvdevice.attr->a.dash = 0.0;


	if ((*qqvdevice.dev.Vinit)()) {
		qqvdevice.initialised = 1;
		qqvdevice.inobject = 0;
		qqvdevice.inpolygon = 0;

		qqviewport(-1.0, 1.0, -1.0, 1.0);

		qqidentmatrix(qqvdevice.transmat->m);
		qqidentmatrix(qqvdevice.transmat->p);
		qqmove(0.0, 0.0, 0.0);
		if (!qqhershfont("futura.l")) /* Try a Hershey font */
			qqfont("small");	/* set up default font */

		qqtextsize(0.05, 0.05);
	} else {
		fprintf(stderr, "vogle: error while setting up device\n");
/*
		exit(1);
*/
	return(4);
	}
}

/*
 * Hacky new device changing routines...
 */
#define	DEVSTACK	8
static	Device	qqvdevstk[DEVSTACK];
static	int	qqvdevindx = 0;

qqpushdev(device)
	char	*device;
{
	/*
	 * Save the old qqvdevice structure
	 */
	qqpushattributes();
	qqpushviewport();

	qqvdevstk[qqvdevindx] = qqvdevice;

	if (qqvdevindx++ > DEVSTACK)
		qqverror("vogle: pushdev: Device stack overflow");

	qqvdevice.initialised = 0;

	qqgetdev(device);

	(*qqvdevice.dev.Vinit)();

	qqvdevice.initialised = 1;

	qqpopviewport();
	qqpopattributes();

}

qqpopdev()
{
	/*
	 * Restore the old qqvdevice structure
	 */
	qqpushattributes();
	qqpushviewport();

	(*qqvdevice.dev.Vexit)();
	if (qqvdevindx-- < 0)
		qqverror("vogle: popdev: Device stack underflow");

	qqvdevice = qqvdevstk[qqvdevindx];

	qqpopviewport();
	qqpopattributes();
}

/*
 * vnewdev
 *
 * reinitialize vogle to use a new device but don't change any
 * global attributes like the window and viewport settings.
 */
void
qqvnewdev(device)
	char	*device;
{
	if (!qqvdevice.initialised)
		qqverror("vnewdev: vogle not initialised\n");

	qqpushviewport();	

	(*qqvdevice.dev.Vexit)();

	qqvdevice.initialised = 0;

	qqgetdev(device);

	(*qqvdevice.dev.Vinit)();

	qqvdevice.initialised = 1;

        /*
         * Need to update font for this device if hardware font is what was
         * being used previously.
         */

	if (!strcmp(qqvdevice.attr->a.font, "small")) {
		if (!(*qqvdevice.dev.Vfont)(qqvdevice.dev.small))
			verror("font: unable to open small font");
	} else if (!strcmp(qqvdevice.attr->a.font, "large")) {
		if (!(*qqvdevice.dev.Vfont)(qqvdevice.dev.large))
			verror("font: unable to open large font");
	}

	qqpopviewport();
}

/*
 * vqqgetdev
 *
 *	Returns the name of the current vogle device 
 *	in the buffer buf. Also returns a pointer to
 *	the start of buf.
 */
char	*
qqvgetdev(buf)
	char	*buf;
{
	/*
	 * Note no exit if not initialized here - so that vexit
	 * can be called before printing the name.
	 */
	if (qqvdevice.dev.devname)
		strcpy(buf, qqvdevice.dev.devname);
	else
		strcpy(buf, "(no device)");

	return(&buf[0]);
}


/*
 * getkey
 *
 *	returns the next key pressed.
 */
int
qqgetkey()
{
	if (!qqvdevice.initialised)
		verror("getkey: vogle not initialised\n");

	return((*qqvdevice.dev.Vgetkey)());
}

/*
 * checkkey
 *
 *	returns true if a key has been hit, or 0 otherwise
 *	(doesn't wait around like getkey)
 */
int
qqcheckkey()
{
	if (!qqvdevice.initialised)
		verror("checkkey: vogle not initialised\n");

	return((*qqvdevice.dev.Vcheckkey)());
}

/*
 * locator
 *
 *	returns the current position of the crosshair or equivalent
 * in world coordinates, and the mouse buttons pressed (if any).
 */
int
qqlocator(wx, wy)
	float	*wx, *wy;
{
	int	a, b, c;

	if (!qqvdevice.initialised)
		verror("locator: vogle not initialised");

	c = (*qqvdevice.dev.Vlocator)(&a, &b);
	qqVtoWxy((float)a, (float)b, wx, wy);

	return(c);
}

/*
 * slocator
 *
 *	returns the current position of the crosshair or equivalent
 * in screen coordinates, and the mouse buttons pressed (if any).
 */
int
qqslocator(wx, wy)
	float	*wx, *wy;
{
	int	a, b, c;
	float	sx, sy;

	if (!qqvdevice.initialised)
		verror("slocator: vogle not initialised");

	c = (*qqvdevice.dev.Vlocator)(&a, &b);
	sx = qqvdevice.sizeX;
	sy = qqvdevice.sizeY;

	*wx = a / (0.5 * sx) - 1.0;
	*wy = b / (0.5 * sy) - 1.0;

	return(c);
}

/*
 * clear
 *
 *	clears the screen to the current colour, excepting devices
 * like a laser printer where it flushes the page.
 *
 */
void
qqclear()
{
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("clear: vogle not initialised");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(1);
		tok->i = CLEAR;

		return;
	}

	(*qqvdevice.dev.Vclear)();
}

/*
 * vexit
 *
 *	exit the vogle system
 *
 */
void
qqvexit()
{
	if (!qqvdevice.initialised)
		verror("vexit: vogle not initialised");

	(*qqvdevice.dev.Vexit)();

	qqvdevice.initialised = 0;
	qqfp = stdout;
}

/*
 * color
 *
 *	set the current colour to colour index number i.
 *
 */
void
qqcolor(i)
	int	i;
{
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("color: vogle not initialised");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(2);

		tok[0].i = COLOR;
		tok[1].i = i;
		return;
	}

	qqvdevice.attr->a.color = i;
	(*qqvdevice.dev.Vcolor)(i);
}

/*
 * mapcolor
 *
 *	set the color of index i.
 */
void
qqmapcolor(i, r, g, b)
	int	i;
	short	r, g, b;
{
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("mapcolor: vogle not initialised");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(5);

		tok[0].i = MAPCOLOR;
		tok[1].i = i;
		tok[2].i = r;
		tok[3].i = g;
		tok[4].i = b;

		return;
	}

	(*qqvdevice.dev.Vmapcolor)(i, r, g, b);
}

/*
 * getdepth
 *
 *	Returns the number if bit planes on a device.
 */
int
qqgetdepth()
{
	if (!qqvdevice.initialised)
		verror("getdepth: vogle not initialised\n");

	return(qqvdevice.depth);
}

/*
 * vsetflush
 *
 * Controls flushing of the display - we can get considerable
 * Speed up's under X11 using this...
 */
void
qqvsetflush(yn)
	int	yn;
{
	qqvdevice.sync = yn;
}

/*
 * vflush
 *
 * Explicitly call the device flushing routine...
 * This is enabled for object so that you can force an update
 * in the middle of an object, as objects have flushing off
 * while they are drawn anyway.
 */
void
qqvflush()
{
	Token	*tok;

	if (!qqvdevice.initialised)
		verror("vflush: vogl not initialised");

	if (qqvdevice.inobject) {
		tok = qqnewtokens(1);
		tok->i = VFLUSH;

		return;
	}

	(*qqvdevice.dev.Vsync)();
}

/*
 * linewidth
 *
 *	Because it's so difficult to do this device independently,
 *	(it looks so different on each device) we will just have 
 *	THICK(1) or THIN(0).
 */
void
qqlinewidth(w)
	int	w;
{
	(*qqvdevice.dev.Vsetlw)(w);
}
