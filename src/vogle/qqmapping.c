#include "vogle.h"

static	float	qqVcx, qqVcy, qqVsx, qqVsy;

/*
 * VtoWxy
 *
 * Return the world x and y values corresponding to the input
 * screen x and y values.
 */
void
qqVtoWxy(xs, ys, xw, yw)
	float	xs, ys;
	float	*xw, *yw;
{
	float	d, a1, a2, b1, b2, c1, c2, A, B;

	A = (xs - qqVcx) / qqVsx;
	B = (ys - qqVcy) / qqVsy;

	a1 = qqvdevice.transmat->p[0][0] - qqvdevice.transmat->p[0][3] * A;
	a2 = qqvdevice.transmat->p[0][1] - qqvdevice.transmat->p[0][3] * B;

	b1 = qqvdevice.transmat->p[1][0] - qqvdevice.transmat->p[1][3] * A;
	b2 = qqvdevice.transmat->p[1][1] - qqvdevice.transmat->p[1][3] * B;

	c1 = qqvdevice.transmat->p[3][3] * A - qqvdevice.transmat->p[3][0];
	c2 = qqvdevice.transmat->p[3][3] * B - qqvdevice.transmat->p[3][1];

	d = (a2 * b1 - b2 * a1);

	if (d != 0.0) {
		*xw = (b1 * c2 - c1 * b2) / d;
		*yw = (c1 * a2 - a1 * c2) / d;
	} else {
		*xw = A;
		*yw = B;
	}

/*
	if (*xw == 0.0)
		*xw = A;

	if (*yw == 0.0)
		*yw = B;
*/
}

/*
 * calcW2Vcoeffs
 *
 *	Calculate the linear coeffs defining the mapping of world
 *	space to actual device space
 */
void
qqCalcW2Vcoeffs()
{
	qqVcx = (float)(qqvdevice.maxVx + qqvdevice.minVx) * 0.5;
	qqVcy = (float)(qqvdevice.maxVy + qqvdevice.minVy) * 0.5;

	qqVsx = (float)(qqvdevice.maxVx - qqvdevice.minVx) * 0.5;
	qqVsy = (float)(qqvdevice.maxVy - qqvdevice.minVy) * 0.5;
}

/*
 * WtoVx
 *
 * return the Screen X coordinate corresponding to world point 'p' 
 * (does the perspective division as well)
 */
int
qqWtoVx(p)
	float	p[];
{
	return((int)(p[0] * qqVsx / p[3] + qqVcx + 0.5));
}

/*
 * WtoVy
 *
 * return the Screen Y coordinate corresponding to world point 'p' 
 * (does the perspective division as well)
 */
int
qqWtoVy(p)
	float	p[];
{
	return((int)(p[1] * qqVsy / p[3] + qqVcy + 0.5));
}


