SUBROUTINE PAPER
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PLFLAG
  Use Index_IGF
  Use Common_PMTX
  Use Common_GRAPHICS
  Use Common_PLTDSN

  CHARACTER FILE*8,DEVICE*6
  INTEGER FVINIT,FQQVINIT
  IF (IGF(2).EQ.1) THEN
     IF (IGF(ITNUM).EQ.5) THEN
        DEVICE='X11'
     ELSE
        DEVICE='tek'
     ENDIF
     ISTAT=FVINIT(DEVICE)
     WRITE (7,*) 'FVINIT:',ISTAT
     CALL FFONT ('times.r')
     IF (IGF(ITNUM).EQ.5) THEN
        DEVICE='qqX11'
        ISTAT=FQQVINIT(DEVICE)
     ENDIF
  ELSE
     CALL OUTNAM (14,PLTDSN,' ',*100)
     CALL FVOUTPUT (PLTDSN)
     IF (IGF(IPLOTTR).EQ.1.OR.IGF(IPLOTTR).EQ.3) THEN
        !
        !	HP
        !
        FILE='hpgl.hdr'
        CALL FFOPENQ (INFILE,FILE,3,1,*100)
        READ (INFILE,'(A)') LINE1
        READ (INFILE,'(A)') LINE2
        CLOSE (INFILE)
        CALL FILEOPEN (PLTDSN,PLOTFILE,'FORMATTED',' ',*100)
        WRITE (PLOTFILE,'(A)') LINE1
        WRITE (PLOTFILE,'(A)') LINE2
        CLOSE (PLOTFILE)
        ISTAT=FVINIT('hpgla4')
     ELSE
        !
        !	Postscript
        !
        ISTAT=FVINIT('postscript')
     ENDIF
     CALL FFONT ('times.r')
  ENDIF
  WRITE (7,*) 'FVINIT:',ISTAT
  PLFLAG=.TRUE.
100 RETURN
END SUBROUTINE PAPER
