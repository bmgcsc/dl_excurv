SUBROUTINE GETINT (DX1,DX2,DY1,DY2,RADIUS,NINTER,DX3,DX4,DY3,DY4)
  !======================================================================C
  NINTER=0
  IF (DX1.NE.DX2) THEN
     TA=(DY2-DY1)/(DX2-DX1)
     TB=DY2-DX2*TA
     VC=TB*TB-RADIUS*RADIUS
     VB=2.*TA*TB
     VA=1.+TA*TA
     VAC=VB*VB-4.*VA*VC
     IF (VAC.GT.0.) THEN
        DY=SQRT(VAC)
        VX1=(-VB+DY)/(2.*VA)
        VX2=(-VB-DY)/(2.*VA)
        VY1=TA*VX1+TB
        VY2=TA*VX2+TB
        IF (DX1.LT.DX2) THEN
           DDX1=DX1
           DDX2=DX2
           DDY1=DY1
           DDY2=DY2
        ELSE
           DDX1=DX2
           DDX2=DX1
           DDY1=DY2
           DDY2=DY1
        ENDIF
        IF (VX1.GE.DDX1.AND.VX1.LE.DDX2) THEN
           NINTER=NINTER+1
           DX3=VX1
           DY3=VY1
        ELSE
           DX3=0.
           DY3=0.
        ENDIF
        IF (VX2.GE.DDX1.AND.VX2.LE.DDX2) THEN
           NINTER=NINTER+1
           DX4=VX2
           DY4=VY2
        ELSE
           DX4=0.
           DY4=0.
        ENDIF
     ENDIF
  ELSEIF (DX1.GT.-RADIUS.AND.DX1.LT.RADIUS) THEN
     VY2=RADIUS*RADIUS-DX1*DX1
     VY1=-VY2
     IF (DY1.LT.DY2) THEN
        DDY1=DY1
        DDY2=DY2
     ELSE
        DDY1=DY2
        DDY2=DY1
     ENDIF
     IF (VY1.LT.DDY2.AND.VY1.GT.DDY1) THEN
        NINTER=NINTER+1
        DX3=DX1
        DY3=VY1
     ENDIF
     IF (VY2.LT.DDY2.AND.VY2.GT.DDY1) THEN
        NINTER=NINTER+1
        DX4=DX1
        DY4=VY2
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE GETINT
