SUBROUTINE OCTRIAD (DN,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - rotation about triad //(111)
  !
  DIMENSION DN(36,*)
  TU=DN(1,6)
  DN(1,6)=DN(1,5)
  DN(1,5)=DN(1,4)
  DN(1,4)=TU
  CALL CARTOPOL (DN(1,4),DN(1,5),DN(1,6),DN(1,1),DN(1,2),DN(1,3))
  IS=IS+1
  RETURN 1
END SUBROUTINE OCTRIAD
