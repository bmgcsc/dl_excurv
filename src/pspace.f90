SUBROUTINE PSPACE (UXMIN,UXMAX,UYMIN,UYMAX)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Index_IGF     ! because of comented line only
  Use Common_PMTX
  Use Common_GRAPHICS

  SCALY=1.
  !
  !      Next line not PC
  !
  !      IF (IGF(2).EQ.1.AND.IGF(ITNUM).NE.5) SCALY=1.311
  TX1=UXMIN*2.*SCALY-1.
  TX2=UXMAX*2.*SCALY-1.
  TY1=UYMIN*2.-1.
  TY2=UYMAX*2.-1.
  IF (QDQ) THEN
     CALL FQQVIEWPORT (TX1,TX2,TY1,TY2)
  ELSE
     CALL FVIEWPORT (TX1,TX2,TY1,TY2)
  ENDIF
  !	CALL MAPP (0.,1.,0.,1.)
  !	CALL POSITN (0.,0.)
  !	CALL LINE (0.,1.)
  !	CALL LINE (1.,0.)
  !	CALL LINE (0.,-1.)
  !	CALL LINE (-1.,0.)
  RETURN
END SUBROUTINE PSPACE
