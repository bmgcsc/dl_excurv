SUBROUTINE DZERO (ARRAY,LEN)
  !======================================================================
  Use Definition
  !	SET THE CONTENTS OF A REAL ARRAY TO ZERO
  !       
  !       ARRAY (O)   ARRAY TO BE ZEROED
  !       LEN (I)   THE NUMBER OF WORDS TO BE ZEROED
  !       
  real(float) ARRAY(*)
  IF (LEN.LE.1) RETURN
  DO L=1,LEN/2
     ARRAY(L)=0
  enddo
  RETURN
END SUBROUTINE DZERO
