SUBROUTINE ONSX (I,TPHI,TTHETA,JCLUS,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SYMOPS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2

  JSX=1
  IF (PGROUP(JCLUS)(1:1).NE.'I') JCX=1
  !
  !	First get rid of the Dnd case - one plane only at (PI/jaxis)/2
  !
  IF (PGROUP(JCLUS)(3:3).EQ.'d'.OR.PGROUP(JCLUS).EQ.'Td') THEN
     !
     !	Dont forget that we are using a 2n improper rotation
     !
     TP=PI2/real(JAXIS,kind=dp)
     TF=AMOD(RADPHI(I),TP)
     TP=TP*.5
     IF (ABS(TF-TP).GT..0001) THEN
        WRITE (OUTTERM,10) I,TP/AC
     ENDIF
     TPHI=TP
     TPHI=RADPHI(I)
  ELSEIF (PGROUP(JCLUS)(1:2).EQ.'Oh') THEN
     TPHI=PID2/2.
  ELSE
     !
     !	Test for one of possible sets.
     !
     TP=PI/real(JAXIS,kind=dp)
     TF=AMOD(RADPHI(I),TP)
     IF (NPgr(JCLUS).EQ.3) TF=TF+PID2
     IF (ABS(TF-TP).GT..0001.AND.ABS(TF).GT..0001) THEN
        CALL WTEXTI ('Shell: ',I)
        CALL WTEXT ('PHI invalid for special position.')
        RETURN 1
     ENDIF
     !
     !	Select the required set. Criteria depend on whether atom is
     !	on its own, the nearest atom in the unit or a remote atom in a unit.
     !
     TPHI=RADPHI(I)
  ENDIF
  !
  !	Now fix the theta's
  !
  TTHETA=RADTH(I)
  RETURN
10 FORMAT ('Shell:',I3/'PHI invalid for special position.',F6.2,' assumed.')
END SUBROUTINE ONSX
