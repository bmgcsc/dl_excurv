MODULE Index_ISET
  USE Definition
  implicit none
  integer(I4B),parameter,  public ::  NFIN       =  1
  integer(I4B),parameter,  public ::  IQUINN     =  2
  integer(I4B),parameter,  public ::  IUC        =  3
  integer(I4B),parameter,  public ::  IWEIGHT    =  4
  integer(I4B),parameter,  public ::  ISING      =  5
  integer(I4B),parameter,  public ::  IEXTYP     =  6
  integer(I4B),parameter,  public ::  ICOR       =  7
  integer(I4B),parameter,  public ::  IATABS     =  8
  integer(I4B),parameter,  public ::  IAVERAGE   =  9
  integer(I4B),parameter,  public ::  IREL       = 10
  integer(I4B),parameter,  public ::  IWAVE      = 11
  integer(I4B),parameter,  public ::  JRCOR      = 12
  integer(I4B),parameter,  public ::  IMSC       = 13
  integer(I4B),parameter,  public ::  MAPSIZE    = 14
  integer(I4B),parameter,  public ::  IEXCH      = 15
  integer(I4B),parameter,  public ::  IDEBUG     = 16  !           never used
  integer(I4B),parameter,  public ::  IGROUND    = 17 
  integer(I4B),parameter,  public ::  ITORTOISE  = 18
  integer(I4B),parameter,  public ::  I_MSSYM    = 19  ! dummy;    never used  
  integer(I4B),parameter,  public ::  I_USESIGMA = 20  ! dummy;    never used 
  integer(I4B),parameter,  public ::  IVIEW      = 21  !           never used
END MODULE Index_ISET
