SUBROUTINE MAKELEG1(MUJ,MUM,GAMNU,GAMSQUIG,RL,KKMAX,IDIM,ITYP,INDY,IEP,INDANG, &
     INDRK,INDSK,NEED,MEE,MEG,MJ,MU,lastpoint)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ATMAT
  Use Common_FMAT
  Use Include_IPA
  !	Use Common_IPA

  COMPLEX GAMNU(-3:3,0:2,0:IPARLMAX,*),GAMSQUIG(-3:3,0:2,0:IPARLMAX,*)
  !     ,FK(9,9,*),FNEW(9,12,*),FTERM(12,9,*),FMAT(12,12,*)
  COMPLEX*16 RL(-2:2,-2:2,0:IPARLMAX,*)
  LOGICAL NEED(0:IDIM,0:IDIM),MEE(*),MEG(*),LMUA,LMUB,lastpoint
  DIMENSION ITYP(0:*),INDY(0:IDIM,0:IDIM),INDANG(0:IDIM,0:IDIM,0:IDIM)
  dimension INDRK(0:IDIM,0:IDIM),INDSK(0:IDIM,0:IDIM,0:IDIM)
  MM=MIN(IDLMAX,MUM)
  DO J1=0,KKMAX
     DO J2=0,KKMAX
	IF (NEED(J1,J2)) THEN
           IT2=ITYP(J2)
           DO J3=0,KKMAX
              IF (NEED(J2,J3)) THEN
                 JJ=INDANG(J1,J2,J3)
                 LL=INDSK(J1,J2,J3)
                 LK=INDRK(J1,J3)
                 J=INDY(J1,J2)
                 K=INDY(J2,J3)
                 IF (J1.EQ.0.AND.J2.NE.0.AND.J3.NE.0) THEN
                    L1=0
                    DO MU1=-MUJ,MUJ
                       DO NU1=MAX(0,-MU1),MIN(MUJ,IEDGE-MU1)
                          L1=L1+1
                          L2=0
                          JL1=MAX(0,MU1+NU1)
                          DO MU2=-MUM,MM
                             LMUA=MU1.NE.-MU2
                             LMUB=MU1.NE.MU2
                             DO NU2=MAX(0,-MU2),MIN(MUM,IDLMAX-MU2)
                                L2=L2+1
                                FNEW(L1,L2,LL)=0.
                                IF (MEE(JJ)) THEN
                                   IF (LMUA) GOTO 5
                                ELSEIF (MEG(JJ)) THEN
                                   IF (LMUB) GOTO 5
                                ENDIF
                                JL2=MAX(0,MU2+NU2)
                                JL3=MAX(JL1,JL2)
                                IF (IDLMAX.GE.JL3) THEN
                                   DO JL=JL3,IDLMAX
                                      FNEW(L1,L2,LL)= &
                                           FNEW(L1,L2,LL)+TMAT(IEP,JL,IT2)*GAMNU(MU2,NU2,JL,K) &
                                           *GAMSQUIG(MU1,NU1,JL,J)*RL(MU2,MU1,JL,JJ)
                                   ENDDO
                                ENDIF
5                               CONTINUE
                             ENDDO
                          ENDDO
                       ENDDO
                    ENDDO
                    MJ=L1
                    MU=L2
                 ENDIF
                 IF (J1.NE.0.AND.J2.NE.0.AND.J3.EQ.0) THEN
                    L1=0
                    DO MU1=-MUM,MM
                       DO NU1=MAX(0,-MU1),MIN(MUM,IDLMAX-MU1)
                          L1=L1+1
                          L2=0
                          JL1=MAX(0,MU1+NU1)
                          DO MU2=-MUJ,MUJ
                             LMUA=MU1.NE.-MU2
                             LMUB=MU1.NE.MU2
                             DO NU2=MAX(0,-MU2),MIN(MUJ,IEDGE-MU2)
                                L2=L2+1
                                FTERM(L1,L2,LL)=0
                                IF (MEE(JJ)) THEN
                                   IF (LMUA) GOTO 10
                                ELSEIF (MEG(JJ)) THEN
                                   IF (LMUB) GOTO 10
                                ENDIF
                                JL2=MAX(0,MU2+NU2)
                                JL3=MAX(JL1,JL2)
                                IF (IDLMAX.GE.JL3) THEN
                                   DO JL=JL3,IDLMAX
                                      FTERM(L1,L2,LL)= &
                                           FTERM(L1,L2,LL)+TMAT(IEP,JL,IT2)*GAMNU(MU2,NU2,JL,K) &
                                           *GAMSQUIG(MU1,NU1,JL,J)*RL(MU2,MU1,JL,JJ)
                                   ENDDO
                                ENDIF
10                              CONTINUE
                             ENDDO
                          ENDDO
                       ENDDO
                    ENDDO
                 ENDIF
                 IF (J1.NE.0.AND.J2.EQ.0.AND.J3.NE.0) THEN
                    L1=0
                    DO MU1=-MUJ,MUJ
                       DO NU1=MAX(0,-MU1),MIN(MUJ,IEDGE-MU1)
                          L1=L1+1
                          L2=0
                          DO MU2=-MUJ,MUJ
                             LMUA=MU1.NE.-MU2
                             LMUB=MU1.NE.MU2
                             DO NU2=MAX(0,-MU2),MIN(MUJ,IEDGE-MU2)
                                L2=L2+1
                                IF (MEE(JJ)) THEN
                                   IF (LMUA) THEN
                                      FK(L1,L2,LK)=0.
                                      GOTO 20
                                   ENDIF
                                ELSEIF (MEG(JJ)) THEN
                                   IF (LMUB) THEN
                                      FK(L1,L2,LK)=0.
                                      GOTO 20
                                   ENDIF
                                ENDIF
                                FK(L1,L2,LK)=GAMNU(MU2,NU2,IEDGE,K)*GAMSQUIG(MU1,NU1,IEDGE,J)*RL(MU2,MU1,IEDGE,JJ)
20                              CONTINUE
                             ENDDO
                          ENDDO
                       ENDDO
                    ENDDO
                 ENDIF
                 IF (J1.NE.0.AND.J2.NE.0.AND.J3.NE.0) THEN
                    L1=0
                    DO MU1=-MUM,MM
                       DO NU1=MAX(0,-MU1),MIN(MUM,IDLMAX-MU1)
                          L1=L1+1
                          L2=0
                          JL1=MAX(0,MU1+NU1)
                          DO MU2=-MUM,MM
                             LMUA=MU1.NE.-MU2
                             LMUB=MU1.NE.MU2
                             DO NU2=MAX(0,-MU2),MIN(MUM,IDLMAX-MU2)
                                L2=L2+1
                                FMAT(L1,L2,LL)=0
                                IF (MEE(JJ)) THEN
                                   IF (LMUA) GOTO 30
                                ELSEIF (MEG(JJ)) THEN
                                   IF (LMUB) GOTO 30
                                ENDIF
                                JL2=MAX(0,MU2+NU2)
                                JL3=MAX(JL1,JL2)
                                IF (IDLMAX.GE.JL3) THEN
                                   DO JL=JL3,IDLMAX
                                      FMAT(L1,L2,LL)= &
                                           FMAT(L1,L2,LL)+TMAT(IEP,JL,IT2)*GAMNU(MU2,NU2,JL,K) &
                                           *GAMSQUIG(MU1,NU1,JL,J)*RL(MU2,MU1,JL,JJ)
                                      !	if (lastpoint) write (7,*) 'makeleg:',l1,l2,ll,tmat(iep,jl,it2), &
                                      ! gamnu(mu2,nu2,jl,k),gamsquig(mu1,nu1,jl,j),rl(mu2,mu1,jl,jj)
                                   ENDDO
                                ENDIF
30                              CONTINUE
                             ENDDO
                          ENDDO
                       ENDDO
                    ENDDO
                 ENDIF
              ENDIF
           ENDDO
	ENDIF
     ENDDO
  ENDDO
  RETURN
END SUBROUTINE MAKELEG1
