SUBROUTINE CHANGE (*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_CONVIA
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_VAR
  Use Common_RULES
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_POT
  Use Common_UPU
  Use Common_COMPAR
  Use Common_SOUPER
  Use Common_ICF
  Use Index_INDS
  Use Common_DISTANCE
  Use Common_SPAREPA1
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  !
  !	Change parameter values or sets of parameter values
  !
  !	KEYWORD  Parameter name etc. If blank then keyword will be
  !	         read in within CHANGE.
  !	NUMFLAG  0 Option parameter was not numeric
  !	         1 Option parameter was numeric
  !	         2 Call from various subroutines to change parameters
  !	           under program control
  !	CHAROPT  Character string containing non-numeric option
  !
  !	RETURN 1 Return after an error
  !
  INTEGER ILIST(IPARNS+1)
  DIMENSION VEC(10),SRULE(10),oldr(0:iparns)
  CHARACTER*12 RKEYWORD
  CHARACTER*4 IP1,IP2,IP3
  CHARACTER OPTS(5)*28
  LOGICAL TERMOUT,UCALL
  DATA OPTS/'(parameter name - eg R1)','(parameter list - eg N[1-3])','(shell symbol - eg S1)', &
       '(table symbol - eg D1:0, D:)','DF1 orDF2'/

  call eqv_convia

  do i=0,iparns
     oldr(i)=r(i)
  enddo
  IF (NUMFLAG.EQ.-1.AND.CHAROPT.NE.' ') NUMFLAG=1
  TERMOUT=NUMFLAG.NE.2.AND.LRIS+LMDSN.EQ.0
  IF (LF.LT.2) LF=2
  ICODE=1
  UCALL=.FALSE.
  IF (NRULES.GT.0) THEN
     DO I=1,NRULES
        VEC(1)=0.
        SRULE(I)=EXPRESS(RULEY(I),IC,VEC)
     ENDDO
  ENDIF
5 IF (KEYWORD.EQ.' ') THEN
10   CALL WTEXT ('Parameter name ?')
     CALL CREAD (KEYWORD,CHAROPT,IP3,INTOPT,NUMFLAG,FPOPT,*10,*480)
     LKEYWORD=NCSTR(KEYWORD)
  ENDIF
  RKEYWORD=KEYWORD
  CALL FINDOPT (5,OPTS,IOPT,0,'New Value',*500)
  CALL GETLIST (KEYWORD,LKEYWORD,NLIST,ILIST)
  !
  !  check for normal ordinary change - (a) in documentation
  !
  CALL UCOPY (PA1,SPAREPA1,IPARNPARAMS)
  CALL FINDPAR (KEYWORD,J,*11)
  GOTO 300
  !
  !	Here for C DF1 and C DF2 - (d) in documentation
  !
11 DO  J=1,2
     IF (KEYWORD.EQ.DVARS(J)) GOTO 50
  enddo
  GOTO 60
40 CALL WTEXT ('Enter value')
  CALL CREAD (CHAROPT,IP2,IP3,INTOPT,NUMFLAG,FPOPT,*40,*480)
50 IF (NUMFLAG.EQ.0) GOTO 40
  IF (J.EQ.1) THEN
     J1=ID1A
     J2=ID1B
  ELSE
     J1=ID2A
     J2=ID2B
  ENDIF
  JT1=(J1-11)/(IPARNS+1)+1
  JT2=(J2-11)/(IPARNS+1)+1
  IF (JT1.NE.JT2) CALL ERRMSG ('Parameters not the same type',*500)
  MS1=MOD(J1-11,IPARNS+1)
  MS2=MOD(J2-11,IPARNS+1)
  AVE=PA1(J1)*RN(MS1)+PA1(J2)*RN(MS2)
  IF (RN(MS1).EQ.0.OR.RN(MS2).EQ.0.) CALL ERRMSG('Occupation number is zero',*500)
  VTOT=RN(MS1)+RN(MS2)
  V1=(AVE-FPOPT*RN(MS2))/VTOT
  V2=(AVE+FPOPT*RN(MS1))/VTOT
  PA1(J1)=AMIN1(RMX(J1),AMAX1(RMS(J1),V1))
  PA1(J2)=AMIN1(RMX(J2),AMAX1(RMS(J2),V2))
  PA2(J1)=PA1(J1)*CONVIA(ICF(J1))
  PA2(J2)=PA1(J2)*CONVIA(ICF(J2))
  !	call all_IPA
  IPA(J1)=PA1(J1)
  !	call IPA_all
  !	call all_IPA
  IPA(J2)=PA1(J2)
  !	call IPA_all
  CALL UTAB (0)
  UCALL=.TRUE.
  CALL IUZERO (NAT,IPARNCLUS)
  LC=IOR(LC,71_i4b)
  IF (.NOT.TERMOUT) GOTO 480
  WRITE (OUTTERM,520) IVARS(J1),PA1(J1)
  WRITE (OUTTERM,520) IVARS(J2),PA1(J2)
  GOTO 480
  !
  !	Here for C Sn - (b) in documentation
  !
60 DO  MS=1,IPARNS
     IF (KEYWORD.EQ.SVARS(MS)) GOTO 240
  enddo
  !
  !	Here for C Dn:m, Wn:m (e), An:m, (f) Bn:m, Vn:m - (g) in documentation
  !
  K=0
  DO  I=1,IPARNS
     DO  J=0,I-1
	IF (KEYWORD.EQ.DISTVARS(I,J)) GOTO 220
     enddo
  enddo
  K=1
  DO  I=1,IPARNS
     DO  J=0,I-1
	IF (KEYWORD.EQ.WEIGHTVARS(I,J)) GOTO 220
     enddo
  enddo
  K=2
  DO  I=1,IPARNS
     DO  J=0,I-1
	IF (KEYWORD.EQ.BONDANGVARS(I,J)) GOTO 220
     enddo
  enddo
  K=3
  DO  I=1,IPARNS
     DO  J=0,I-1
	IF (KEYWORD.EQ.BAWTVARS(I,J)) GOTO 220
     enddo
  enddo
  K=4
  DO I=1,IPARNS
     DO J=0,I-1
	IF (KEYWORD.EQ.BONDVARS(I,J)) GOTO 220
     enddo
  enddo
  !
  !	Here for C D:, W: (e), A:, (f) B:, V: - (g) in documentation
  !
  IF (KEYWORD.EQ.'D:') THEN
     DO  I=1,NS
        DO  J=0,I-1
           V1=UX(I)-UX(J)
           V2=UY(I)-UY(J)
           V3=UZ(I)-UZ(J)
           V=V1*V1+V2*V2+V3*V3
           DISTANCES(I,J)=SQRT(V)
        enddo
     enddo
     IF (TERMOUT) CALL LIST (KEYWORD,'T')
     GOTO 480
  ENDIF
  IF (KEYWORD.EQ.'W:') THEN
     WMIN=-1.
     WMAX=999.
     IF (NUMFLAG.EQ.0) THEN
        V=1.
     ELSE
        V=FPOPT
        IF (FPOPT.LT.0.) THEN
           V=-V
140        IF (TERMOUT) CALL WTEXT ('Enter Minimum Distance')
           CALL CREAD (IP1,IP2,IP3,INTOPT,NUMFLAG,WMIN,*140,*140)
           IF (NUMFLAG.EQ.0) GOTO 140
150        IF (TERMOUT) CALL WTEXT ('Enter Maximum Distance')
           CALL CREAD (IP1,IP2,IP3,INTOPT,NUMFLAG,WMAX,*150,*150)
           IF (NUMFLAG.EQ.0) GOTO 150
        ENDIF
     ENDIF
     DO  I=1,NS
        DO  J=0,I-1
           IF (DISTANCES(I,J).GE.WMIN.AND.DISTANCES(I,J).LE.WMAX)  WEIGHTINGS(I,J)=V
        enddo
     enddo
     IF (TERMOUT) CALL LIST (KEYWORD,'T')
     GOTO 480
  ENDIF
  IF (KEYWORD.EQ.'A:') THEN
     DO  I=1,NS
        DO  J=0,I-1
           BONDANGS(I,J)=ANG(I)
        enddo
     enddo
     IF (TERMOUT) CALL LIST (KEYWORD,'T')
     GOTO 480
  ENDIF
  IF (KEYWORD.EQ.'B:') THEN
     DO  I=1,IPARNS
        DO  J=0,I-1
           BONDS(I,J)=V
           IF (J.NE.0) BONDS(J,I)=BONDS(I,J)
        enddo
     enddo
     IF (TERMOUT) CALL LIST (KEYWORD,'T')
     GOTO 480
  ENDIF
  IF (KEYWORD.EQ.'V:') THEN
     WMIN=-1.
     WMAX=999.
     IF (NUMFLAG.EQ.0) THEN
        V=1.
     ELSE
        V=FPOPT
        IF (FPOPT.LT.0.) THEN
           V=-V
190        IF (TERMOUT) CALL WTEXT ('Enter Minimum Angle')
           CALL CREAD (IP1,IP2,IP3,INTOPT,NUMFLAG,WMIN,*190,*190)
           IF (NUMFLAG.EQ.0) GOTO 190
200        IF (TERMOUT) CALL WTEXT ('Enter Maximum Angle')
           CALL CREAD (IP1,IP2,IP3,INTOPT,NUMFLAG,WMAX,*200,*200)
           IF (NUMFLAG.EQ.0) GOTO 200
        ENDIF
     ENDIF
     DO  I=1,NS
        DO  J=0,I-1
           IF (BONDANGS(I,J).GE.WMIN.AND.BONDANGS(I,J).LE.WMAX)  BAWT(I,J)=V
        enddo
     enddo
     IF (TERMOUT) CALL LIST(KEYWORD,'T')
     GOTO 480
  ENDIF
  GOTO 490
  !
  !	Finished search for options - error if something not found
  !
220 IF (NUMFLAG.NE.0) GOTO 230
  IF (TERMOUT) THEN
     IF (K.EQ.0) WRITE (OUTTERM,510) DISTVARS(I,J)
     IF (K.EQ.1) WRITE (OUTTERM,510) WEIGHTVARS(I,J)
     IF (K.EQ.2) WRITE (OUTTERM,510) BONDANGVARS(I,J)
     IF (K.EQ.3) WRITE (OUTTERM,510) BAWTVARS(I,J)
     IF (K.EQ.4) WRITE (OUTTERM,510) BONDVARS(I,J)
  ENDIF
  CALL CREAD (CHAROPT,IP2,IP3,IDUM,NUMFLAG,FPOPT,*220,*290)
230 IF (K.EQ.0) THEN
     IF (TERMOUT) WRITE (OUTTERM,580) DISTVARS(I,J),DISTANCES(I,J)
     DISTANCES(I,J)=FPOPT
     WRITE (OUTTERM,580)  DISTVARS(I,J),DISTANCES(I,J)
  ELSEIF (K.EQ.1) THEN
     IF (TERMOUT) WRITE (OUTTERM,580) WEIGHTVARS(I,J),WEIGHTINGS(I,J)
     WEIGHTINGS(I,J)=FPOPT
     WRITE (OUTTERM,580) WEIGHTVARS(I,J),WEIGHTINGS(I,J)
  ELSEIF (K.EQ.2) THEN
     IF (TERMOUT) WRITE (OUTTERM,580) BONDANGVARS(I,J),BONDANGS(I,J)
     BONDANGS(I,J)=FPOPT
     WRITE (OUTTERM,580) BONDANGVARS(I,J),BONDANGS(I,J)
  ELSEIF (K.EQ.3) THEN
     IF (TERMOUT) WRITE (OUTTERM,580) BAWTVARS(I,J),BAWT(I,J)
     BAWT(I,J)=FPOPT
     WRITE (OUTTERM,580) BAWTVARS(I,J),BAWT(I,J)
  ELSEIF (K.EQ.4) THEN
     IF (TERMOUT) WRITE (OUTTERM,580) BONDVARS(I,J),BONDS(I,J)
     BONDS(I,J)=FPOPT
     IF (J.NE.0) BONDS(J,I)=FPOPT
     WRITE (OUTTERM,580) BONDVARS(I,J),BONDS(I,J)
  ENDIF
  GOTO 480

240 IF (INTOPT.GT.IPARNS) INTOPT=IPARNS
  IF (TERMOUT) CALL LIST(KEYWORD,'T')
  !	IF (INTOPT.LE.0) THEN
  IF (NUMFLAG.LE.0) THEN
     UN(MS)=0.
     NU(MS)=0
     DO  I=inds(INDN),inds(INDZ),IPARNS+1
        K=MS+I
250     IF (TERMOUT) WRITE (OUTTERM,510) IVARS(K)
        CALL CREAD (CHAROPT,IP2,IP3,IDUM,NUMFLAG,FPOPT,*250,*290)
        !
        !	First check for alphabetic options if T(I) is being changed
        !
        IF (NUMFLAG.EQ.0) THEN
           IF (I.EQ.inds(INDT)) THEN
	      DO  IATYPE=IPARNP,1,-1
                 IF (CHAROPT.EQ.ELS(IATOM(IATYPE))) THEN
                    FPOPT=IATYPE
                    GOTO 270
                 ENDIF
              enddo
	      CALL READBASE (CHAROPT,MS,.FALSE.,*250)
           ELSE
	      GOTO 250
           ENDIF
        ENDIF
        IF (I.EQ.inds(INDANG).AND.FPOPT.GT.360.) THEN
           PA1(K)=MOD(FPOPT,360.)
        ELSEIF (I.EQ.inds(INDANG).AND.FPOPT.LT.0.) THEN
           PA1(K)=360.+FPOPT
        ENDIF
270     PA1(K)=AMIN1(AMAX1(FPOPT,RMS(K)),RMX(K))
        PA2(K)=PA1(K)*CONVIA(ICF(K))
        !	  call all_IPA
        IPA(K)=PA2(K)
        !	  call IPA_all
     enddo
  ELSE
     CALL SWOP (MS,INTOPT,.FALSE.)
     CALL FLIM (*290)
  ENDIF
290 CALL UTAB (0)
  UCALL=.TRUE.
  LC=IOR(LC,71_i4b)
  LF=2
  CALL IUZERO (NAT,IPARNCLUS)
  IF (LMDSN.EQ.0) CALL LIST (KEYWORD,'T')
  GOTO 480
  !
  !	Here for normal change
  !
  !	For parameters indexed by shell, JT the parameter type, and JTN is
  !	is the index of JT0.
  !
300 IF (NLIST.LE.0) THEN
     NLIST=1
     ILIST(1)=J
  ELSE
     IOFF=J-ILIST(1)
     DO I=1,NLIST
        ILIST(I)=ILIST(I)+IOFF
     ENDDO
  ENDIF
  DO  IIJ=1,NLIST
     J=ILIST(IIJ)
     JT=(J-11)/(IPARNS+1)+1
     IF (J.LE.10.OR.JT.GT.IPARNSV) THEN
        JT=0
        JTN=0
        MS=0
        JCLUS=0
     ELSE
        JTN=(JT-1)*(IPARNS+1)+11
        !
        !	MS is the shell number for this parameter
        !
        MS=MOD(J-11,IPARNS+1)
        JCLUS=IABS(ICLUS(MS))
     ENDIF
     !
     !	Special case for PANG
     !
     IF (JTN.EQ.inds(INDPANG)) THEN
        IF (MS.EQ.0.OR.IPLA(MS).LE.0) GOTO 480
        MS =IPLA(MS)
        JTN=inds(INDANG)
        J  =inds(INDANG)+MS
     ENDIF
     !
     !	Save the original value in case correlation option required
     !
     OLD=PA1(J)
     OLD2=PA2(J)
     OLDOUT=OLD
     IF (JTN.EQ.inds(INDANG)) OLDOUT=TANG(MS)
310  IF (NUMFLAG.GT.0) GOTO 360
     !
     !	Three situations where a symbol not a number may be provided
     !
     IF (CHAROPT.NE.' '.AND.IVARS(J)(1:4).EQ.'ATOM') THEN
        K=J-inds(INDATOM)+1
        LCHAR=NCSTR(CHAROPT)
        IF (CHAROPT(LCHAR:LCHAR).EQ.'*') THEN
           CHAROPT(LCHAR:LCHAR)=' '
           LASTHOLE(K)=-98
        ELSE
           LASTHOLE(K)=-99
        ENDIF
        DO  IMT=-NSITES,103
           IF (CHAROPT.EQ.ELS(IMT)) THEN
              FPOPT=IMT
              GOTO 360
           ENDIF
        enddo
        CALL ERRMSG ('Element '//CHAROPT(1:2)//' not found',*350)
     ELSEIF (CHAROPT.NE.' '.AND.JTN.EQ.inds(INDT)) THEN
        DO  IATYPE=IPARNP,1,-1
           IF (CHAROPT.EQ.ELS(IATOM(IATYPE))) THEN
              FPOPT=IATYPE
              GOTO 360
           ENDIF
        enddo
        CALL READBASE (CHAROPT,MS,.FALSE.,*350)
        TERMOUT=.FALSE.
        GOTO 360
        !
        !	Check for change D1A,D1B etc
        !
     ELSEIF (J.GE.inds(INDD1A).AND.J.LE.inds(INDD2B)) THEN
        DO IMT=inds(INDN),inds(INDTWST)+IPARNS
           IF (CHAROPT.EQ.IVARS(IMT)) THEN
              FPOPT=IMT
              GOTO 360
           ENDIF
        enddo
     ENDIF
350  IF (TERMOUT.AND.(IIJ.EQ.1.OR.IIJ.EQ.NLIST)) THEN
        IF (J.GE.inds(INDD1A).AND.J.LE.inds(INDD2B)) THEN
           WRITE (OUTTERM,560) IVARS(J),IVARS(IPA(J))
        ELSEIF (J.GE.inds(INDATOM).AND.J.LT.inds(INDATOM)+IPARNP) THEN
           WRITE (OUTTERM,570) IVARS(J),ELS(IPA(J)),IPA(J)
        ELSE
           OUT=PA1(J)
           IF (JTN.EQ.inds(INDANG)) OUT=TANG(MS)
           WRITE (OUTTERM,540) IVARS(J),OUT
        ENDIF
     ENDIF
     CALL CREAD (CHAROPT,IP2,IP3,INTOPT,NUMFLAG,FPOPT,*350,*480)
     GOTO 310

360  IF (DEBUG) WRITE (LOGFILE,*) 'J,JTN,MS,FPOPT',J,JTN,MS,FPOPT
     !
     !	Test for ANG
     !
     IF (JTN.EQ.inds(INDANG).AND.FPOPT.GT.360.) THEN
        FPOPT=MOD(FPOPT,360.)
     ELSEIF (JTN.EQ.inds(INDANG).AND.FPOPT.LT.0.) THEN
        FPOPT=360.+FPOPT
     ENDIF
     PA1(J)=AMIN1(AMAX1(FPOPT,RMS(J)),RMX(J))
     PA2(J)=PA1(J)*CONVIA(ICF(J))
     IPA(J)=PA2(J)
     IF (IVARS(J)(1:5).EQ.'PERCA') THEN
        IF (PA1(J+IPARNP).NE.0.) THEN
           PA1(J+IPARNP)=PA1(J+IPARNP)-FPOPT+OLD
           PA2(J+IPARNP)=PA1(J+IPARNP)
        ENDIF
     ENDIF
     !
     !	Check for a whole load of options that change things other than themselves
     !	Branch to 462 means UTAB must be called, to 461 that it must not
     !
     !	Here for ATOM and ION - changes default MTR's
     !
     IF (J.GE.inds(INDATOM).AND.J.LT.inds(INDION)+IPARNP) THEN
        MS=MOD(J-inds(INDATOM),IPARNP)+1
        IDJ=IATOM(MS)
        !
        !	Set default MTR according to ION
        !
        CALL SETMTR1 (MS,IDJ)
        GOTO 461
     ELSEIF (JTN.EQ.inds(INDLINK).AND.PA1(J).NE.0.) THEN
        IF (.NOT.ICOMMON) CALL WTEXT ('Links switched on')
        ICOMMON=.TRUE.
     ELSEIF (J.EQ.inds(INDDLMAX).OR.J.EQ.inds(INDTLMAX))THEN
        !
        !	Here for DLMAX, TLMAX
        !
        GOTO 461
        !
        !	Check for KMIN,KMAX
        !
     ELSEIF (J.EQ.inds(INDKMIN).OR.J.EQ.inds(INDKMAX)) THEN
        JEXP=1
        IF (J.EQ.inds(INDKMIN)) THEN
           KMARK=1
           DO IE=NP,1,-1
              IF (RKMIN.GE.RK(IE,JEXP)/RKC) THEN
                 KMARK=IE
                 GOTO 432
              ENDIF
           ENDDO
           KMARK=1
432        DO I=0,IPARNSP
	      EMIN0(0)=(ENER(KMARK)-.001)/EC
	      EMIN02(0)=(ENER(KMARK)-.001)
           ENDDO
        ELSE
           KMARK=NP
           DO IE=NP-1,1,-1
              IF (RKMAX.GE.RK(IE,JEXP)/RKC) THEN
                 KMARK=IE+1
                 GOTO 433
              ENDIF
           ENDDO
           KMARK=NP
433        EMAX0(0)=(ENER(KMARK)+.001)/EC
           EMAX02(0)=ENER(KMARK)+.001
        ENDIF
        GOTO 461
        !
        !	Check for EMIN,EMAX
        !
     ELSEIF (J.GE.inds(INDEMIN0).AND.J.LE.inds(INDEMAX0)+IPARNSP)THEN
        JEXP=1
        IF (J.LT.inds(INDEMAX0)) THEN
           KMARK=1
           DO IE=NP,1,-1
              IF (EMIN0(0).GE.ENER(IE)/EC) THEN
                 KMARK=IE
                 GOTO 434
              ENDIF
           ENDDO
           KMARK=1
434        RKMIN=(RK(KMARK,JEXP)-.001)/RKC
           RKMIN2=(RK(KMARK,JEXP)-.001)
        ELSE
           KMARK=NP
           DO IE=NP-1,1,-1
              IF (EMAX0(0).LT.ENER(IE)/EC) THEN
                 KMARK=IE+1
                 GOTO 435
              ENDIF
           ENDDO
           KMARK=1
435        RKMAX=(RK(KMARK,JEXP)+.001)/RKC
           RKMAX2=RK(KMARK,JEXP)+.001
        ENDIF
        GOTO 461
     ELSEIF (J.EQ.inds(INDCLE)) THEN
        !
        !	If the corrficient of linear expansion and temperature are defined,
        !	calculate the assymetric terms in the DW factor by the anharmonic
        !	oscillator model.
        !
        IF (CLE.GT.0.) THEN
           DO I=1,NS
              B(I)=A(I)*CLE*R(I)*TEMPQ*1.E-6*10.
              B2(I)=B(I)*S3C
           ENDDO
        ENDIF
        GOTO 461
        !
        !	Options which modify cartesian coordinates
        !
     ELSEIF (J.EQ.inds(INDSURREL)) THEN
        !
        !	Here for SURREL
        !
        DO I=1,IPARNS
           IF (UZ(I).GE.ZMIN.AND.UZ(I).LE.ZMAX.AND.ICLUS(I).EQ.1) THEN
              RTRANS=-99.
              DO II=1,IPARNS
                 IF (UZ(II).LT.ZMIN.AND.UZ(II).GT.RTRANS) RTRANS=UZ(II)
              ENDDO
              WRITE (6,*) 'OLD DISTANCE BETWEEN LAYERS:',UZ(I)-RTRANS
              RMULT=(UZ(I)-RTRANS)*SURREL/OLD
              WRITE (6,*) 'NEW DISTANCE BETWEEN LAYERS:',RMULT
              RTRANS=(UZ(I)-RMULT)-RTRANS
              WRITE (6,*) 'TRANSLATION THIS STEP',RTRANS
              GOTO 345
           ENDIF
        ENDDO
        GOTO 461
345     DO I=1,IPARNS
           IF (ICLUS(I).EQ.1.AND.UZ(I).LT.ZMIN) THEN
              UZ(I)=UZ(I)-RTRANS
           ENDIF
        ENDDO
	JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDACELL)) THEN
        !
        !	Here for ACELL
        !
        DO I=1,IPARNS
           !	  IF (ICLUS(I).EQ.1) THEN
           UX(I)=UX(I)*ACELL/OLD
           IF(ICLASS(NPGR(1)).EQ.0.OR.ICLASS(NPGR(1)).GE.4.OR.iset(ICOR).EQ.2)THEN
              UY(I)=UY(I)*ACELL/OLD
              BCELL=ACELL
           ENDIF
           IF (ICLASS(NPGR(1)).EQ.7.OR.iset(ICOR).EQ.2) THEN
              UZ(I)=UZ(I)*ACELL/OLD
              CCELL=ACELL
           ENDIF
        ENDDO
        JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDBCELL)) THEN
        !
        !	Here for BCELL
        !
        IF (ICLASS(NPGR(1)).EQ.0.OR.ICLASS(NPGR(1)).GE.4) THEN
           BCELL=OLD
           CALL ERRMSG ('BCELL invalid for space group'//PGROUP(1),*462)
        ENDIF
        DO I=1,IPARNS
           !	  IF (ICLUS(I).EQ.1) UY(I)=UY(I)*BCELL/OLD
           UY(I)=UY(I)*BCELL/OLD
        ENDDO
	JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDCCELL)) THEN
        !
        !	Here for CCELL
        !
        IF (ICLASS(NPGR(1)).EQ.7) THEN
           CCELL=OLD
           CALL ERRMSG ('CCELL invalid for space group'//PGROUP(1),*462)
        ENDIF
        DO I=1,IPARNS
           !	  IF (ICLUS(I).EQ.1) UZ(I)=UZ(I)*CCELL/OLD
           UZ(I)=UZ(I)*CCELL/OLD
        ENDDO
	JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDTRANSX)) THEN
        !
        !	Here for TRANSX
        !
        IF (NPGR(1).EQ.0.OR.NPGR(1).GT.3) THEN
           TRANSX=OLD
           CALL ERRMSG ('deltax invalid for space group'//PGROUP(1),*462)
        ENDIF
        DO I=1,IPARNS
           IF (ABS(UZ(I)).GT.0.001) THEN
              UX(I)=UX(I)+TRANSX-OLD
           ENDIF
        ENDDO
	JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDTRANSY)) THEN
        !
        !	Here for TRANSY
        !
        IF (NPGR(1).EQ.0.OR.NPGR(1).GT.3) THEN
           TRANSY=OLD
           CALL ERRMSG ('deltay invalid for space group'//PGROUP(1),*462)
        ENDIF
        DO I=1,IPARNS
           IF (ABS(UZ(I)).GT.0.001) THEN
              UY(I)=UY(I)+TRANSY-OLD
           ENDIF
        ENDDO
	JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDTRANSZ)) THEN
        !
        !	Here for TRANSZ
        !
        IF (NPGR(1).EQ.0.OR.NPGR(1).EQ.2.OR.NPGR(1).EQ.4.OR.(NPGR(1).GT.11.AND. &
             NPGR(1).LT.17).OR.(NPGR(1).GT.21.AND.NPGR(1).NE.46)) THEN
           TRANSZ=OLD
           CALL ERRMSG ('deltaz invalid for space group'//PGROUP(1),*462)
        ENDIF
        DO I=1,IPARNS
           IF (ABS(UZ(I)).GT.0.001) THEN
              UZ(I)=UZ(I)+TRANSZ-OLD
           ENDIF
        ENDDO
	JCLUS=-1
        GOTO 462
     ELSEIF (J.EQ.inds(INDSURROT)) THEN
        !
        !	Here for SURROT
        !
        DO  I=1,NS
           IF (UZ(I).GT.ZMAX.OR.UZ(I).LT.ZMIN) cycle
           CALL FRACCORD (I,FRACX,FRACY,FRACZ,INTX,INTY,INTZ,0)
           CALL XROTATE (0.,0.,1.,SURROT2-OLD2,FRACX,FRACY,FRACZ)
           CALL CARTCORD (I,FRACX+INTX,FRACY+INTY,FRACZ+INTZ)
        enddo
        JCLUS=-1
        GOTO 462
     ELSEIF (J.GE.inds(INDSDX).AND.J.LT.inds(INDSDZ)+IPARNP) THEN
        !
        !	Here for SDX, SDY, SDZ
        !
        JUG=MOD(J-inds(INDSDX),IPARNP)+1
        JOG=   (J-inds(INDSDX))/IPARNP
        DO I=1,NS
           IF (UZ(I).GT.ZMAX.OR.UZ(I).LT.ZMIN) cycle
           IF (IT(I).EQ.JUG) THEN
              CALL FRACCORD (I,FRACX,FRACY,FRACZ,INTX,INTY,INTZ,6)
              IF (JOG.EQ.0) THEN
                 FRACX=FRACX+PA1(J)-OLD
              ELSEIF (JOG.EQ.1.) THEN
                 FRACY=FRACY+PA1(J)-OLD
              ELSEIF (JOG.EQ.2) THEN
                 FRACZ=FRACZ+PA1(J)-OLD
              ENDIF
              CALL CARTCORD (I,FRACX+INTX,FRACY+INTY,FRACZ+INTZ)
           ENDIF
        enddo
        JCLUS=-1
        GOTO 462
     ENDIF
     !
     !	Test for options requiring call to UTAB +/- PCOR
     !
     IF (J.EQ.inds(INDNS).OR.JTN.EQ.inds(INDN).OR.JTN.EQ.inds(INDT).OR. &
          JTN.EQ.inds(INDR).OR.JTN.GE.inds(INDUN))  THEN
        !
        !	Check for CORRELLATION ON
        !
        !   1    2    3    4    5    6    7    8    9   10   11   12   13   14
        !   N    T    R    A    B    C    D   UN  ANG   TH  PHI   UX   UY   UZ
        !
        !  15   16   17   18   19   20   21   22   23   24   25   26   27
        ! ROT  PIV  PLA  PLB TORA TORB TORC  PAT PANG  UOC TWST TILT CLUS
        !
        !
        IG=NU(MS)
        IF (IG.GT.0) THEN
           NF=NR(IG)
        ELSE
           NF=0
        ENDIF
        JRAD=IRAD(IABS(ICLUS(MS)))
        ICODE=0
        IF (JTN.EQ.inds(INDUOC)) THEN
           !
           !	Change to UOC
           !
           DO I=1,IPARNS
              IF (NU(I).EQ.MS) THEN
                 RN(I)=FPOPT
              ENDIF
           ENDDO
        ENDIF
        IF (DEBUG) WRITE (6,*) 'ICOR,IG,JTN,INDR',iset(ICOR),IG,JTN,inds(INDR)
        IF ((iset(ICOR)*IG.GT.0.OR.iset(ICOR).EQ.2).AND.JTN.GE.inds(INDR))THEN
           IF (MS.EQ.JRAD) THEN
	      CALL WTEXT ('Correlation ignored for central atom')
           ELSEIF (JTN.LT.inds(INDTORA).OR.JTN.GT.inds(INDTORC)) THEN
	      IF (DEBUG) WRITE (6,*) 'NR,MS',NR(IG),MS
              !
              !	PCOR/PCOR2 update cartesian coordinates so ICODE is set to 1
              !
	      IF (iset(ICOR).EQ.2.OR.NR(IG).EQ.MS) THEN
                 CALL PCOR (J,OLD,OLD2,JTN,MS,JCLUS)
	      ELSE
                 CALL PCOR2 (J,OLD,OLD2,JTN,MS,JCLUS)
	      ENDIF
	      ICODE=1
           ENDIF
           !
           !	Note that ROT and TORTION angle refinement cannot at present be used if CORRELATION is on/all
           !
        ELSEIF (JTN.EQ.inds(INDROT)) THEN
           !
           !	ROT
           !
           CALL RROTATE (MS,ROT(MS)-OLD,IVECA,IVECB)
        ELSEIF (JTN.GE.inds(INDTORA).AND.JTN.LE.inds(INDTORC)) THEN
           !
           !	Change to Torsion angle
           !
           IF (JTN.EQ.inds(INDTORA)) THEN
	      JVECA=NTORA(2,MS)
	      JVECB=NTORA(3,MS)
           ELSEIF (JTN.EQ.inds(INDTORB)) THEN
	      JVECA=NTORB(2,MS)
	      JVECB=NTORB(3,MS)
           ELSE
	      JVECA=IPIV(MS)
	      JVECB=IPLA(MS)
           ENDIF
           IF (JVECA.GT.0.AND.JVECB.GT.0) THEN
	      DO I=1,IPARNS
                 IF (NU(I).NE.MS) cycle
                 JCLUS=IABS(ICLUS(I))
                 IF (JTN.NE.inds(INDTORC)) THEN
                    IF (ATLABEL(I).EQ.'C'.OR.ATLABEL(I).EQ.'N'.OR.ATLABEL(I).EQ.'O'.OR.ATLABEL(I).EQ.'CA') cycle
                 ELSE
                    IF (I.EQ.IRAD(JCLUS)) cycle
                 ENDIF
                 CALL RROTATE (I,PA1(J)-OLD,JVECA,JVECB)
              enddo
           ENDIF
        ELSEIF (JTN.EQ.inds(INDANG).OR.JTN.EQ.inds(INDTILT).OR.JTN.EQ.inds(INDTWST))THEN
           !
           !	ANG, TILT or TWST
           !
           CALL GETAXIS (MS,JTN,NF,IG,RX,RY,RZ,FANG,*480)
           IF (MS.EQ.NF) THEN
	      IF (JTN.EQ.inds(INDANG)) THEN
                 ANG(MS)=0.
                 ANG2(MS)=0.
                 GOTO 480
	      ENDIF
	      CX=UX(JRAD)
	      CY=UY(JRAD)
	      CZ=UZ(JRAD)
           ELSE
	      CX=UX(NF)
	      CY=UY(NF)
	      CZ=UZ(NF)
           ENDIF
           IF (DEBUG) WRITE (7,*) 'ANGCHANG',MS,UX(MS),UY(MS),CX,CY,PA1(J),OLD2
           UX(MS)=UX(MS)-CX
           UY(MS)=UY(MS)-CY
           UZ(MS)=UZ(MS)-CZ
           IF (DEBUG) WRITE (6,*) 'RX,RY,RZ',RX,RY,RZ
           CALL XROTATE (RX,RY,RZ,PA2(J)-OLD2,UX(MS),UY(MS),UZ(MS))
           UX(MS)=CX+UX(MS)
           UY(MS)=CY+UY(MS)
           UZ(MS)=CZ+UZ(MS)
           UX2(MS)=UX(MS)*DC
           UY2(MS)=UY(MS)*DC
           UZ2(MS)=UZ(MS)*DC
        ENDIF
        !
        !	Call to UTAB required
        !
        IF (JTN.GT.inds(INDR).AND.JTN.NE.inds(INDTH).AND.JTN.NE.inds(INDPHI)) ICODE=1
     ELSE
        GOTO 461
     ENDIF
462  IF (IIJ.EQ.NLIST) THEN
        CALL UTAB (ICODE)
        CALL FLIM (*99)
99      UCALL=.TRUE.
        IF (ICOMMON.AND.JCLUS.GT.0.AND.iset(ICOR).NE.2) THEN
           DO I=0,NS
              KCLUS=IABS(ICLUS(I))
              IF (KCLUS.NE.JCLUS) THEN
                 K=LINK(I)
                 LCLUS=IABS(ICLUS(K))
                 aa=coffset(1,kclus)-coffset(1,lclus)*0.
                 bb=coffset(2,kclus)-coffset(2,lclus)*0.
                 cc=coffset(3,kclus)-coffset(3,lclus)*0.
                 aa=sqrt(aa**2+bb**2+cc**2)
                 IF (DEBUG) WRITE (6,'(A,5I3,F8.3)') 'UPDATING CLUSTER FOR',JCLUS,KCLUS,LCLUS,I,K,AA
                 IF (K.NE.0) THEN
                    IF (JCLUS.EQ.1) THEN
                       UX(I)=UX(K)-COFFSET(1,KCLUS)+COFFSET(1,LCLUS)*0.
                       UY(I)=UY(K)-COFFSET(2,KCLUS)+COFFSET(2,LCLUS)*0.
                       UZ(I)=UZ(K)-COFFSET(3,KCLUS)+COFFSET(3,LCLUS)*0.
                    ELSE
                       !	          UX(I)=UX(K)+COFFSET(1,JCLUS)
                       !	          UY(I)=UY(K)+COFFSET(2,JCLUS)
                       !	          UZ(I)=UZ(K)+COFFSET(3,JCLUS)
                    ENDIF
                 ENDIF
              ENDIF
           ENDDO
           CALL UTAB (1)
           UCALL=.TRUE.
        ENDIF
     ENDIF
461  IF (TERMOUT.AND.(IIJ.EQ.1.OR.IIJ.EQ.NLIST)) THEN
        IF (J.GE.inds(INDATOM).AND.J.LT.inds(INDATOM)+IPARNP) THEN
           IOLD=int(OLD)
           WRITE (OUTTERM,550) IVARS(J),ELS(IOLD),IOLD,ELS(IPA(J)),IPA(J)
        ELSEIF (JTN.EQ.inds(INDT)) THEN
           IOLD=IATOM(int(OLD))
           WRITE (OUTTERM,550) IVARS(J),ELS(IOLD),int(OLD),ELS(IATOM(IPA(J))),IPA(J)
        ELSEIF (J.EQ.inds(INDD1A).OR.J.EQ.inds(INDD1B)) THEN
           WRITE (OUTTERM,*) IVARS(IPA(J))
        ELSE
           OUT=PA1(J)
           IF (JTN.EQ.inds(INDANG)) OUT=TANG(MS)
           WRITE (OUTTERM,530) IVARS(J),OLDOUT,OUT
        ENDIF
     ENDIF
     DO I=0,IPARNS
	IF (ABS(OLDR(I)-R(I)).GT..1) THEN
           IF (DEBUG) WRITE (OUTTERM,*) 'SHELL',I,' DISTANCE CHANGED FROM',OLDR(I),' TO',R(I)
	ENDIF
     ENDDO
  enddo
  !
  !	Set condition codes etc for last variable in list
  !
  LC=IOR(LC,LCS(J))
  !
  !	Set to zero if rmin, rmax, wp, wind or wave vector changed
  !
  IF (J.LT.11.AND.J.NE.2) LF=2
  IF ((J.LT.inds(INDVX)  .OR. J.GT.inds(INDVZ)).AND.J.NE.inds(INDSIZE).AND.J.NE.inds(INDBOND)) then
     CALL IUZERO (NAT,IPARNCLUS)
  endif
  !
  !	Tmatrix requires update for xe etc
  !
  IF (IAND(LC,8_i4b).NE.0) CALL FLIM (*480)
480 IF (NRULES.GT.0) THEN
     DO I=1,NRULES
        J=IRULE(I)
        VEC(1)=0.
        PA1(J)=EXPRESS(RULEY(I),IC,VEC)
        PA2(J)=PA1(J)*CONVIA(ICF(J))
        !	  call all_IPA
        IPA(J)=PA1(J)
        !	  call IPA_all
        IF (TERMOUT.AND.PA1(J).NE.SRULE(I)) THEN
           L1=NCSTR(RULEX(I))
           L2=NCSTR(RULEY(I))
           WRITE (6,'(1X,A,''='',A,''   ['',F9.4,'']'')') RULEX(I)(1:L1),RULEY(I)(1:L2),EXPRESS(RULEY(I),IC,VEC)
        ENDIF
     ENDDO
     IF (UCALL) THEN
        CALL UTAB (ICODE)
        CALL FLIM (*299)
     ENDIF
  ENDIF
299 RETURN
490 CALL ERRMSG ('Invalid parameter: '//KEYWORD,*500)
500 RETURN 1
510 FORMAT (' ',A6,'=? ')
520 FORMAT (1X,A6,' =',6F9.4/7X,6F9.4/7X,4F9.4)
530 FORMAT (1X,A6,' changed from: ',F9.4,' to: ',F9.4)
540 FORMAT (1X,'Old value of ',A6,' = ',F9.4,' - new value ?')
550 FORMAT (1X,A6,' changed from: ',A,'(',I3,') to: ',A,'(',I3,')')
560 FORMAT (1X,'Old value of ',A6,' = ',A,' - new value ?')
570 FORMAT (1X,'Old value of ',A6,' = ',A,'(',I3,') - new value ?')
580 FORMAT (1X,A6,' =',F9.4)
END SUBROUTINE CHANGE
