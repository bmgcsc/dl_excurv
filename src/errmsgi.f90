SUBROUTINE ERRMSGI (STR,II,*)
  !======================================================================C
  !
  !	Output an error message of the form ***ERROR*** STR II
  !	where STR is a character string passed to the routine and where
  !	II is an integer value passed to the routine.
  !	The max length of the output message is 80 characters
  !
  !	    STR (I)   Character string to be output in message. This
  !	              string will be truncated if necessary to reduce
  !	              the total length of the output message to 80
  !	              characters.
  !	     II (I)   Integer value to be output (note no extra spaces
  !	              will be inserted by the subroutine)
  !
  CHARACTER :: STR*(*),MSG*80
  CHARACTER*15 :: VALSTR
  !
  !	Prepare to output error message
  !
  L=LEN(STR)
  WRITE (VALSTR,30) II
  DO  I1=1,15
     IF (VALSTR(I1:I1).NE.' ') GOTO 20
  enddo
20 L=MIN0(L,52+I1)
  MSG=STR
  CALL ERRMSG (MSG(1:L)//VALSTR(I1:15),*25)
25 RETURN 1
  !
30 FORMAT(I15)
END SUBROUTINE ERRMSGI
