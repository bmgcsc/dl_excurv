SUBROUTINE OCVERTY (DN,IS,*)
  !======================================================================C
  !
  ! Performs symmetry operation - reflection about plane //z at 90 to x
  !
  Use Common_convia
  DIMENSION DN(36,*)
  DN(1,3)=PI-DN(1,3)
  DN(1,4)=-DN(1,4)
  IS=IS+1
  RETURN 1
END SUBROUTINE OCVERTY
