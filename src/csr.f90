FUNCTION CSR (RN,XN,ANGLE)
  !======================================================================C
  CSR=RN*RN+XN*XN-2.*XN*RN*COS(ANGLE)
  IF (CSR.LT.0.) CSR=0.
  CSR=SQRT(CSR)
  RETURN
END FUNCTION CSR
