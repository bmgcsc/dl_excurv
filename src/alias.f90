SUBROUTINE ALIAS
  !======================================================================C
  Use Parameters
  Use Common_risch
  USE Common_B1
  USE Common_B2
  Use Common_AIL
  Use Common_COMPAR
  Use Common_RCBUF

  CHARACTER :: OPTS(3)*27
  LOGICAL   :: SAME
  DATA OPTS/'(alias name - define alias)','LIST','DELETE - (no abbrevition)'/
  CALL FINDOPT(3,OPTS,IOPT,2,'Number of deletions - (DELETE only)',*40)
  IF (KEYWORD.EQ.' '.OR.KEYWORD.EQ.'LIST') THEN
     IF (NALIAS.LE.0) THEN
        CALL WTEXT ('No aliases defined.')
        CALL WTEXT ('Enter ALIAS (aliasname) to define an alias.')
     ELSE
        CALL WTEXTI ('Number of aliases defined: ',NALIAS)
        CALL WTEXT (' ')
        DO I=1,NALIAS
           CALL WTEXT (ALID(I)//' = '//ALSTRING(I)(1:70))
        enddo
     ENDIF
  ELSEIF (KEYWORD.EQ.'DELETE') THEN
     NALIAS=MAX(0,NALIAS-MAX(INTOPT,1))
  ELSE
     IF (NALIAS.GE.IPARAL) THEN
        IPR=1
        CALL WTEXT ('No more space for aliases.')
        CALL WTEXT ('Enter ALIAS DELETE n to delete last n aliases')
        RETURN
     ENDIF
20   CALL WTEXT ('Enter definition of '//KEYWORD//':')
     K=NALIAS+1
     SAME=.FALSE.
     IF (NALIAS.NE.0) THEN
        DO I=1,NALIAS
           IF (ALID(I).EQ.KEYWORD) THEN
              K=I
              SAME=.TRUE.
              GOTO 30
           ENDIF
        ENDDO
     ENDIF
30   ALID(K)=KEYWORD
     CALL SREAD (*20,*40)
     IF (.NOT.SAME) NALIAS=NALIAS+1
     ALSTRING(K)=BUF
     IF (LRIS.GT.0) THEN
        I=NCSTR(ALSTRING(K))
        ALSTRING(K)(I+1:)=';'
        ALSTRING(K)(I+2:)=RIS
        LRIS=0
     ENDIF
     J=NCSTR(ALSTRING(K))
     IALLEN(K)=J
     IF (ALSTRING(K)(1:1).EQ.'''') THEN
        ALSTRING(K)(1:1)=' '
        IF (ALSTRING(K)(J:J).EQ.'''') THEN
           IALLEN(K)=J-1
           ALSTRING(K)(J:J)=' '
        ENDIF
     ENDIF
  ENDIF
40 RETURN
END SUBROUTINE ALIAS
