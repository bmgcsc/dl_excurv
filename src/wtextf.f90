SUBROUTINE WTEXTF (STR,FF)
  !======================================================================C
  !
  !	Subroutine to output a text string and floating point number to
  !	the terminal and if required to the output file using the
  !	subroutine 'wtext'
  !
  ! PARAMETERS
  !
  !	   STR (I)    TEXT STRING TO BE OUTPUT (TYPE = CHARACTER)
  !	              MAX OF 65 CHARACTERS IN LENGTH (ANY EXCESS
  !	              CHARACTERS WILL BE LOST)
  !	    II (I)    THE INTEGER VALUE TO BE OUTPUT FOLLOWING THE
  !	              TEXT STRING (NO SPACES INSERTED)
  !
  CHARACTER*(*) STR
  CHARACTER*15 VALSTR
  CHARACTER*80  TXT
  L=LEN(STR)
  TXT=STR
  VALSTR=' '
  WRITE(VALSTR,30) FF
  DO  I=1,15
     I1=I
     IF (VALSTR(I:I).NE.' ') GOTO 20
  enddo
20 LL=64+I1
  L=MIN0(L,LL)
  TXT(L+1:)=VALSTR(I1:15)
  CALL WTEXT (TXT)
30 FORMAT(F10.3)
  RETURN
END SUBROUTINE WTEXTF
