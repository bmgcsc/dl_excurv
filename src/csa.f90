FUNCTION CSA (A,B,C)
  !======================================================================C
  Use Common_convia
  !
  !	Cosine rule calculation given sides a,b,c
  !
  IF (B.EQ.0..OR.C.EQ.0.) THEN
     CSA=0.
  ELSE
     CSA=(B*B+C*C-A*A)/(2.*B*C)
     IF (ABS(CSA).GT.1.) THEN
        IF (ABS(CSA).GT.1.001) WRITE (6,*) 'CSA error',A,B,C,CSA
        IF (CSA.GT.0) THEN
           CSA=0
        ELSE
           CSA=PI
        ENDIF
     ELSE
        CSA=ACOS(CSA)
     ENDIF
  ENDIF
  RETURN
END FUNCTION CSA
