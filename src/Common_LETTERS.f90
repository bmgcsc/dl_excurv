MODULE Common_LETTERS

  !      COMMON /LETTERS/LETTERS(48),NUMBERS(0:9)
  !      CHARACTER LETTERS*1,NUMBERS*1
  !
  !	DATA LETTERS/' ','A','B','C','D','E','F','G','H','I','J','K','L',
  !     1'M','N','O','P','Q','R','S','T','U','V','W','X','a','b','c','d',
  !     2'e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t',
  !     3'u','v','w'/,NUMBERS/'0','1','2','3','4','5','6','7','8','9'/

  !!!!  NUMBERS is also defined in symmetry.f so we rename NUMBERS here in NUMBERS  !!!!

  USE Definition
  USE Parameters
  implicit none
  private
  character*1, dimension(48), public :: LETTERS = (/' ', &
       'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X', &
       'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w'/)
  character*1, dimension(0:9), public :: NUMBERS = (/'0','1','2','3','4','5','6','7','8','9'/)
END MODULE Common_LETTERS
