SUBROUTINE POLTOCARD (R,THETA,PHI,X,Y,Z)
  !======================================================================C
  Use Common_CONVIA
  IF (ABS(THETA-90.).LT.1.E-6) THEN
     SINTH=1.
     COSTH=0.
  ELSE
     SINTH=SIN(THETA*PI/180.)
     COSTH=COS(THETA*PI/180.)
  ENDIF
  IF (ABS(PHI).LT.1.E-6) THEN
     SINPHI=0.
     COSPHI=1.
  ELSEIF (ABS(PHI-180.).LT.1.E-6) THEN
     SINPHI=0.
     COSPHI=-1.
  ELSE
     SINPHI=SIN(PHI*PI/180.)
     COSPHI=COS(PHI*PI/180.)
  ENDIF
  X=R*SINTH*COSPHI
  Y=R*SINTH*SINPHI
  Z=R*COSTH
  RETURN
END SUBROUTINE POLTOCARD
