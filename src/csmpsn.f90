SUBROUTINE CSMPSN (XINTVL,XONE,AX,BX,YY,AREA,NPOINT)
  !======================================================================C
  !
  !	General purpose integrator: integrates YY(x) w.r.t. X, which
  !	is given on a mesh of constant spacing XINTVL. Npoint is
  !	the # of points in array YY, XONE is the x-value corresponding
  !	to YY(1), AX & BX are the beginning & end of the integration
  !	range respectively (n.b. AX & BX do not have to fall on mesh
  !	points, but AX should be just greater than XONE and BX just
  !	less than XONE+XINTVL*(NPOINT-1).
  !
  COMPLEX YY(*),AREA
  !
  IF (NPOINT.EQ.3) GOTO 110
  K=2
10 IF (K.EQ.NPOINT) GOTO 20
  IF (K.GT.NPOINT) GOTO 30
  K=K+2
  GOTO 10
20 L=NPOINT-1
  GOTO 40
30 L=NPOINT
  !
40 AREA=0.0
  IF (NPOINT.EQ.2) GOTO 80
  NST=L+1
  N=2
50 AREA=AREA+(4./3.)*XINTVL*YY(N)
  N=N+2
  IF (N.NE.NST) GOTO 50
  !
  N=3
  IF (L.EQ.3) GOTO 70
  NST=L
60 AREA=AREA+(2./3.)*XINTVL*YY(N)
  N=N+2
  IF (N.NE.NST) GOTO 60
  !
70 AREA=AREA+(1./3.)*XINTVL*(YY(1)+YY(L))
  !
80 FA=YY(2)+(YY(2)-YY(1))*(AX-XONE-XINTVL)/XINTVL
  AFA=0.5*(AX-XONE)*(YY(1)+FA)
  !
  XNPT=XONE+(NPOINT-1)*XINTVL
  LL=NPOINT
  KK=NPOINT-1
  FB=YY(LL)+(YY(LL)-YY(KK))*(BX-XNPT)/XINTVL
  !
  IF (L.EQ.NPOINT) GOTO 90
  AFB=0.5*(BX-XNPT+XINTVL)*(YY(KK)+FB)
  AFB=-AFB
  GOTO 100
  !
90 AFB=0.5*(XNPT-BX)*(YY(LL)+FB)
  !
100 AREA=AREA-AFA-AFB
  !
  RETURN
  !
110 FA=YY(2)+(YY(2)-YY(1))*(AX-XONE-XINTVL)/XINTVL
  LL=NPOINT
  KK=NPOINT-1
  XNPT=XONE+(NPOINT-1)*XINTVL
  FB=YY(LL)+(YY(LL)-YY(KK))*(BX-XNPT)/XINTVL
  AREA=0.5*(YY(2)+FA)*(XONE+XINTVL-AX)
  AREA=AREA+0.5*(FB+YY(2))*(BX-XONE-XINTVL)
  RETURN
  !
END SUBROUTINE CSMPSN
