SUBROUTINE SIMPSN (XX,FX,NX,I,AX)
  !======================================================================C
  !
  !	IMPLICIT REAL*8(A-H,O-Z)
  DIMENSION XX(NX),FX(NX),AX(NX)
  !
  !	****************************************************************
  !	*                                                              *
  !	*  SMPSN INTEGRATES REAL FX WRT. XX USING SIMPSON'S RULE.      *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  INPUT :                                                     *
  !	*  =====                                                       *
  !	*                                                              *
  !	*  XX(I),I=1,NX  : INDEPENDENT VARIABLE.                       *
  !	*  FX(I),I=1,NX  : REAL FUNCTION TO BE INTEGRATED.             *
  !	*  I : CONTROLS DIRECTION OF INTEGRATION; >0 FOR FORWARDS      *
  !	*                                         <0 FOR BACKWARDS.    *
  !	*                                                              *
  !	****************************************************************
  !	*                                                              *
  !	*  OUTPUT :                                                    *
  !	*  ======                                                      *
  !	*                                                              *
  !	*  AX(I),I=1,NX  : INTEGRAL OF FX WRT. XX.                     *
  !	*                                                              *
  !	****************************************************************
  !
  !	INTEGRATE FORWARDS
  !
  IF (I.GT.0) THEN
     AX(1)=0.0E0
     DO  IX=2,NX,2
        D1=XX(IX)-XX(IX-1)
        AX(IX)=AX(IX-1)+D1/2.0E0*(FX(IX)+FX(IX-1))
        D2=XX(IX+1)-XX(IX-1)
        D3=D2/D1
        A2=D3/6.0E0*D2**2/(XX(IX+1)-XX(IX))
        A3=D2/2.0E0-A2/D3
        AX(IX+1)=AX(IX-1)+(D2-A2-A3)*FX(IX-1)+A2*FX(IX)+A3* FX(IX+1)
     enddo
  ENDIF
  !
  !	Integrate backwards
  !
  IF (I.LT.0) THEN
     AX(NX)=0.
     DO  IX=2,NX,2
        IC=NX+1-IX
        D1=XX(IC+1)-XX(IC)
        AX(IC)=AX(IC+1)+D1/2.0E0*(FX(IC+1)+FX(IC))
        D2=XX(IC+1)-XX(IC-1)
        D3=D2/(XX(IC)-XX(IC-1))
        A2=D3/6.0E0*D2**2/D1
        A3=D2/2.0E0-A2/D3
        AX(IC-1)=AX(IC+1)+(D2-A2-A3)*FX(IC-1)+A2*FX(IC)+ A3*FX(IC+1)
     enddo
  ENDIF
  RETURN
END SUBROUTINE SIMPSN
