MODULE Common_extension
  implicit none
  private
!-st  character*4,            public :: EXTENSION(6)
  CHARACTER*4, parameter, public :: EXTATOM = 'atom' 
  CHARACTER*4, parameter, public :: EXTCONT = 'cont' 
  CHARACTER*4, parameter, public :: EXTDATA = 'data' 
  CHARACTER*3, parameter, public :: EXTLOG  = 'log' 
  CHARACTER*4, parameter, public :: EXTUNFM = 'unfm' 
!-st  CHARACTER*4, parameter, public :: EXTTAB  = 'tab' 
! contains
!-st  subroutine eqv_extension
!-st    extension(1) = extatom
!-st    extension(2) = extcont
!-st    extension(3) = extdata
!-st    extension(4) = extlog
!-st    extension(5) = extunfm
!-st    extension(5) = exttab
!-st    return
!-st  end subroutine eqv_extension
END MODULE Common_extension
