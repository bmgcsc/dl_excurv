SUBROUTINE GPLOT (INN,INX1)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B1
  Use Common_LETTERS
  Use Common_TL
  Use Common_F
  Use Common_PMTCH
  Use Index_IGF
  Use Common_POT
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_MATEL
  Use Common_P
  Use Common_PATHS
  Use Common_ATMAT
  Use Common_WFX
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  Use Include_FT
  Use Include_OLD
  !  include 'Common_IPA.inc'
  !  include 'Common_PA1.inc'
  !  include 'Common_PA2.inc'
  !  include 'Common_XY.inc'
  !  include 'Common_FT.inc'
  !  include 'Common_OLD.inc'
  !
  !	Used by the PLOT, COPLOT and COMPARE commands to plot spectra.
  !
  DIMENSION W(IPARNPOINTS),XXF(IPARNPOINTS),SX(IPARNPOINTS)
  dimension XTEMP(IPARNPOINTS),WMAG(IPARNSP)
  CHARACTER FTOPTS(0:4)*6,IP1*4,IP2*4,IP3*4
  DATA FTOPTS/' ','k','k^2^','k^3^','k/fpim'/
  IN1=INX1
  JZ=IGF(5)-1
  INT=INN
  IF (INT.EQ.12) GOTO 10
  CALL FIT ('Q',iset(IWEIGHT),*170)
  IF (LC.NE.0) RETURN
  IF (INT.LT.4.OR.INT.EQ.6) GOTO 10
  !
  !	Calc FT for COPLOT if required
  !
  IF (LF.NE.0.OR.IGF(3).EQ.4) CALL FOURS (IGF(3))
10 PLOTXSCALE=1.
  PLOTYSCALE=1.
  PLOTXLAB='X-axis'
  PLOTYLAB='Y-axis'
  IPINDEX=1
  JJ=0
  QRAT=1.
  LX=3*IPARNPOINTS*IPARNSP+1*IPARNPOINTS+1
  IF (IGF(4).NE.1) LX=1
  !
  !	1   PLOT
  !	2   PLOT EXAFS
  !	3   PLOT BSF
  !	4   PLOT FT
  !	5   COPLOT
  !	6   PLOT SHELLS
  !	7   PLOT PFT
  !	8   PLOT WINDOW
  !	9   PLOT RDF
  !	11  PLOT WFX
  !	12  PLOT SPECTRA
  !	17  PLOT PHASESHIFTS
  !
  GOTO (30,30,60,40,20,80,120,130,30,180,190,210,260,270,280,290, &
       310,360,370,380,395),INT
  !
20 JJ=1
30 IF (NSPEC.GT.1) JJ=1
  IF (NSPEC.GT.3) JJ=5
  RQ(1)=0.
  RQ(2)=0.
  ISPEC=0
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  PLOTXSCALE=RKC
  IF (IGF(4).NE.1) PLOTXSCALE=EC
  PLOTYSCALE=RKC**JZ
  PLOTYLAB='k^'//NUMBERS(JZ)//'^ chi(k)'
  IF (IGF(3).EQ.4) THEN
     DO IEXP=1,NSPEC
        ISPEC=ISPEC+1
        DO I=JS(IEXP),JF(IEXP)
           UBS(I,ISPEC)=XABT(I,1)-XABS(I,1)
        ENDDO
        CALL WEIGHT (UBS(JS(IEXP),ISPEC),UBS(JS(IEXP),ISPEC),RK(JS(IEXP),IEXP),NPT(IEXP))
     ENDDO
  ELSE
     DO IEXP=1,NSPEC
        IF (IGF(3).NE.3) THEN
           ISPEC=ISPEC+1
           CALL WEIGHT (XABS(JS(IEXP),IEXP),UBS(JS(IEXP),ISPEC),RK(JS(IEXP),IEXP),NPT(IEXP))
        ENDIF
        IF (IGF(3).NE.2) THEN
           DO IGEN=0,IN1
              ISPEC=ISPEC+1
              IF (IGEN.EQ.0) THEN
                 CALL WEIGHT (XABT(JS(IEXP),IEXP),UBS(JS(IEXP),ISPEC),RK(JS(IEXP),IEXP),NPT(IEXP))
              ELSEIF (IGEN.EQ.1) THEN
                 CALL WEIGHT (XABO(JS(IEXP),IEXP),UBS(JS(IEXP),ISPEC),RK(JS(IEXP),IEXP),NPT(IEXP))
              ELSE
                 CALL WEIGHT (XABO(JS(IEXP)+IPARNPOINTS/2,IEXP),UBS(JS(IEXP),ISPEC),RK(JS(IEXP),IEXP),NPT(IEXP))
              ENDIF
           ENDDO
        ENDIF
     ENDDO
  ENDIF
  ISPEC=0
  IF (IGF(3).EQ.4) THEN
     DO IEXP=1,NSPEC
        CALL CTHEORY (IEXP,1,ISPEC)
        PLOTTITLE = ITL(12)
        CALL PLT (XY(LX),UBS(1,ISPEC),JS(IEXP),JF(IEXP),1,*170)
     ENDDO
  ELSE
     DO IEXP=1,NSPEC
        JQ=1
        !!	  NPL=IEXP
        !	  IF (IGF(1).NE.1) JQ=1
        IF (IGF(3).NE.3) THEN
           CALL CEXPER(IEXP,ISPEC)
           CALL PLT (XY(LX),UBS(1,ISPEC),JS(IEXP),JF(IEXP),JQ,*170)
           JQ=0
        ENDIF
        IF (IGF(3).NE.2) THEN
           DO IGEN=0,IN1
              CALL CTHEORY(IEXP,IGEN,ISPEC)
              CALL PLT (XY(LX),UBS(1,ISPEC),JS(IEXP),JF(IEXP),JQ,*170)
              JQ=0
           ENDDO
        ENDIF
        IF (IEXP.NE.NSPEC) JJ=JJ+1
        IF (JJ.EQ.5) THEN
           JJ=1
           CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*53,*150)
           IF (IGF(2).EQ.2) CALL FCLEAR
53         CONTINUE
        ENDIF
     ENDDO
     NPL=0
  ENDIF
  IF (INT.EQ.6) GOTO 100
  IF (INT.NE.5) GOTO 150
  JJ=JJ+1
  IF (JJ.EQ.4) JJ=JJ+1
  IF (JJ.GT.5) JJ=10
  IF (JJ.EQ.5) THEN
     JJ=1
     CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*54,*150)
     IF (IGF(2).EQ.2) CALL FCLEAR
54   CONTINUE
  ENDIF
  GOTO 42
  !
  !	Here for PLOT FT
  !
40 IF (NSPEC.GT.1) JJ=1
  IF (NSPEC.GT.3) JJ=5
42 RQ(2)=0.
  PLOTXLAB='r/Angstroms'
  K=IFTSET(2)
  PLOTYLAB='FT '//FTOPTS(K)(1:NCSTR(FTOPTS(K)))//' chi(k)'
  DO IEXP=1,NSPEC
     IF (IGF(3).NE.3) CALL ARRMAXA (FE(1,IEXP),IPARNA,RQ(2))
     IF (IGF(3).NE.2) CALL ARRMAXA (FT(1,IEXP),IPARNA,RQ(2))
  ENDDO
  IF (IN1.GE.1) THEN
     CALL ARRMAXA (OFT,IPARNA,RQ(2))
     IF (IN1.GE.2) THEN
        CALL ARRMAXA (OFT(IPARNA+1),IPARNA,RQ(2))
     ENDIF
  ENDIF
  IF (IFTSET(7).EQ.1) THEN
     RQ(1)=0.
  ELSE
     RQ(1)=-RQ(2)
  ENDIF
  PLOTXSCALE=DC
  PLOTYSCALE=1.
  JQ=1
  DO IEXP=1,NSPEC
     !!	NPL=IEXP
     IF (IGF(1).NE.1) JQ=1
     if ( IGF(3) .eq. 4 ) then
        plottitle=itl(13)
        call plt (af,fe(1,iexp),1,iparna,jq,*170)
     else
	IF (IGF(3).NE.3) THEN
           PLOTTITLE=ITL(5)
           CALL PLT (AF,FE(1,IEXP),1,IPARNA,JQ,*170)
           JQ=0
           PLOTTITLE=ITL(7)
           IF (IFTSET(7).EQ.2) CALL PLT (AF,FEI(1,IEXP),1,IPARNA,0,*170)
	ENDIF
	IF (IGF(3).NE.2) THEN
           PLOTTITLE=ITL(6)
           CALL PLT (AF,FT(1,IEXP),1,IPARNA,JQ,*170)
           JQ=1
           PLOTTITLE=ITL(8)
           IF (IFTSET(7).EQ.2) CALL PLT (AF,FTI(1,IEXP),1,IPARNA,0,*170)
	ENDIF
	JJ=JJ+1
	IF (JJ.EQ.5.AND.IEXP.NE.NSPEC) THEN
           JJ=1
           CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*55,*150)
           IF (IGF(2).EQ.2) CALL FCLEAR
55         CONTINUE
	ENDIF
     endif
  ENDDO
  NPL=0
  PLOTTITLE=ITL(6)
  IF (IN1.GT.0) CALL PLT (AF,OFT,1,IPARNA,0,*170)
  IF (IN1.GT.1) CALL PLT (AF,OFT(IPARNA+1),1,IPARNA,0,*170)
  GOTO 150
  !
  !	Here for PLOT BSF
  !
60 M=1
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  IEXP=1
  DO I=9,11
     JJ=I-8
     RQ(2)=-9999.
     RQ(1)=+9999.
     DO IE=JS(IEXP),JF(IEXP)
	IF (I.EQ.9) THEN
           XXX=FPIM(IE,IEXP)
	ELSEIF (I.EQ.10) THEN
           PLOTYLAB='radians'
           XXX=FPIP(IE,IEXP)
	ELSE
           XXX=BSCP(IE,IEXP)
	ENDIF
	IF (XXX.LT.RQ(1)) THEN
           RQ(1)=XXX
	ELSEIF (XXX.GT.RQ(2)) THEN
           RQ(2)=XXX
	ENDIF
     ENDDO
     PLOTTITLE=ITL(I)
     PLOTXSCALE=RKC
     IF (IGF(4).EQ.2) PLOTXSCALE=EC
     IF (I.EQ.9) THEN
        CALL PLT (XY(LX),FPIM(1,IEXP),JS(IEXP),JF(IEXP),1,*170)
     ELSEIF (I.EQ.10) THEN
        CALL PLT (XY(LX),FPIP(1,IEXP),JS(IEXP),JF(IEXP),1,*170)
     ELSE
        CALL PLT (XY(LX),BSCP(1,IEXP),JS(IEXP),JF(IEXP),1,*170)
     ENDIF
     M=M+IPARNPOINTS
  enddo
  !   70	CONTINUE
  GOTO 150
  !
  !	Here for PLOT SHELLS
  !
80 CALL UCOPY (XABT(JS(IEXP),1),OLDENER,NPT(IEXP))
  CALL UCOPY (RN2(1),XTEMP,NS)
  RQ(1)=0.
  RQ(2)=0.
  DO J=1,NS
     CALL UZERO (RN2(1),NS)
     RN2(J)=XTEMP(J)
     LC=IOR(LC,3_i4b)
     CALL SETUP (*170)
     !
     !	Store UBT for the shell
     !
     CALL UCOPY (XABT(JS(IEXP),1),OLDXY(NPT(IEXP)*J+1),NPT(IEXP))
  enddo
  CALL UCOPY (XTEMP,RN2,NS)
  !
  !	Retrieve composite theory
  !
  CALL UCOPY (OLDENER,XABT(JS(IEXP),1),NPT(IEXP))
  GOTO 30
100 DO I=1,NS
     CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*110,*150)
     PLOTTITLE=ITL(2)
     IEXP=1
     ISPEC=1
     IGEN=0
     CALL WEIGHT (OLDXY(I*NPT(IEXP)+1),UBS(JS(IEXP),ISPEC),RK(JS(IEXP),IEXP),NPT(IEXP))
     CALL CTHEORY (IEXP,IGEN,ISPEC)
     CALL PLT (XY(LX),UBS(1,1),JS(IEXP),JF(IEXP),0,*170)
110  CONTINUE
  enddo
  GOTO 150
  !
  !	Here for PLOT PFT - Call FOURS for experiment only
  !
120 CALL FOURS (2)
  RQ(1)=0.
  RQ(2)=0.
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  IEXP=1
  CALL ARRMAXA (PFT(JS(IEXP)),NPT(IEXP),RQ(2))
  IF (iset(IATABS).EQ.0) RQ(1)=-RQ(2)
  PLOTTITLE='Experiment * window function'
  CALL PLT (XY(LX),PFT,JS(IEXP),JF(IEXP),1,*170)
  IWC=-1
  GOTO 140
  !
  !	Here for PLOT WINDOW
  !
130 IWC=1
  IN1=1
  !
  !	Entry point for PLOT PFT - P PFT 1 plots window as well on top of
  !	everything else ( IWC = -1 ).
  !
140 RQ(2)=1.1
  RQ(1)=0.
  IEXP=1
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  PLOTTITLE=ITL(14)
  IF (IN1.EQ.1) CALL PLT (XY(LX),W,JS(IEXP),JF(IEXP),IWC,*170)
150 IF (IGF(2).EQ.1.AND.IGF(ITNUM).EQ.5) CALL FGFLUSH
  CALL PLIST
170 RETURN
  !
  !	Here for PLOT RDF
  !
180 CALL PLRDF (IN1)
  RETURN
  !
  !	Here for PLOT WFX
  !
190 RQ(2)=0.
  DO I=JS(IEXP),JF(IEXP)
     RQ(2)=AMAX1(WFX(I-JS(IEXP)+1),RQ(2))
  enddo
  RQ(1)=-RQ(2)
  PLOTTITLE='Calculated RDF'
  CALL PLT (XY(LX+JS(IEXP)-1),WFX,1,NPT(IEXP),1,*170)
  RETURN
  !
  !	Here for PLOT SPECTRA
  !
210 IF (NUMPATHS.LE.0) CALL ERRMSG ('No paths defined',*420)
  JJMIN=5
  JJMAX=19
  IF (IN1.EQ.1) THEN
     JJMIN=1
     JJMAX=4
  ENDIF
  JJ=JJMIN-1
  IGF9=IGF(9)
  IGF(9)=5
  JMAX=MIN(NUMPATHS,IPARPATH)
  OPEN (39,FILE='plotspec',STATUS='UNKNOWN')
  WRITE (39,430) GTITLE
  WRITE (39,440)
  DO J=1,JMAX
     DO IORD=5,1,-1
	IF (IPATHINDS(IORD,J).NE.-1) GOTO 15
     ENDDO
     IORD=5
15   write (7,*) pathindex(j),pathflag(ifix(pathindex(j))),iord
     IF (.NOT.PATHFLAG(ifix(PATHINDEX(J)))) GOTO 240
     !  15	IF (IPATHOCC(J).LE.0.OR.IORD.LT.IOMIN.OR.IORD.GT.IOMAX.OR.PATHPL(J).LT.PLMIN.OR.PATHPL(J) &
     !           .GT.PLMAX.OR.PATHANG(J).LT.RMINANG) GOTO 240
     JJ=JJ+1
     IF (JJ.GT.JJMAX) THEN
        IF (IGF(2).NE.3) THEN
           CALL CREAD (IP1,IP2,IP3,INT,IC,V,*220,*250)
220        CONTINUE
           IF (IGF(2).EQ.2) CALL FCLEAR
        ENDIF
        JJ=JJMIN
     ENDIF
     IPINDEX=J
     JCLUS=IABS(ICLUS(IPATHINDS(1,J)))
     IEXP=KEXP(JCLUS)
     DO KSPEC=1,MAX(IPOL(IEXP),1)
	RQ(2)=0.
	WMAG(KSPEC)=0.
	DO I=1,NPT(IEXP)
           XTEMP(I)=SPECTRA(I,J,KSPEC)*RK(I+JS(IEXP)-1,IEXP)**(IGF(5)-1)
           WMAG(KSPEC)=AMAX1(ABS(XTEMP(I)),WMAG(KSPEC))
        enddo
        !	WRITE (7,*) 'path',J,NPT(IEXP),(XTEMP(I),I=1,NPT(IEXP))
	RQ(2)=WMAG(KSPEC)
	IF (WMAG(KSPEC).EQ.0.) RQ(2) = 1.
	RQ(1)=-RQ(2)
	CALL CTHEORY(1,1,ISPEC)
	CALL PLT (XY(LX+JS(IEXP)-1),XTEMP,1,NPT(IEXP),1,*250)
	IF (CHAROPT(1:1).NE.'N') CALL FFT2 (XTEMP,JJMIN,JJMAX,*250)
     ENDDO
     CALL WRPATH (39,J,' ',WMAG,0)
240  CONTINUE
  enddo
250 IGF(9)=IGF9
  CLOSE (39)
  RETURN
  !
  !	Here for PLOT SOMETHING ELSE
  !
260 RQ(1)=REAL(CAP(1,1,1))
  RQ(2)=RQ(1)
  ICODE=1
  DO IEXP=1,NSPEC
     DO IE=1,NPT(IEXP)
	W(IE)=REAL(CAP(IE,1,1))
	SX(IE)=AIMAG(CAP(IE,1,1))
	IF (W(IE).LT.RQ(1)) THEN
           RQ(1)=W(IE)
	ELSEIF (W(IE).GT.RQ(2)) THEN
           RQ(2)=W(IE)
	ENDIF
	IF (SX(IE).LT.RQ(1)) THEN
           RQ(1)=SX(IE)
	ELSEIF (SX(IE).GT.RQ(2)) THEN
           RQ(2)=SX(IE)
	ENDIF
     ENDDO
     CALL PLT (XY(LX),W,1,NPT(IEXP),ICODE,*170)
     CALL PLT (XY(LX),SX,1,NPT(IEXP),0,*170)
     ICODE=0
  ENDDO
  RETURN
  !
  !	Plot BS phase, Magnitude etc. separately - EFEE is equivalnced to
  !	the three arrays FPIM,FPIP,FBSC
  !
270 M=1
  IWC=9
  GOTO 300
280 M=IPARNPOINTS+1
  IWC=10
  GOTO 300
290 M=IPARNPOINTS*2+1
  IWC=11
300 RQ(2)=EFEE(M+JS(IEXP)-1)
  RQ(1)=RQ(2)
  DO IE=JS(IEXP),JF(IEXP)
     IF (EFEE(M-1+IE).LT.RQ(1)) THEN
        RQ(1)=EFEE(M-1+IE)
     ELSEIF (EFEE(M-1+IE).GT.RQ(2)) THEN
        RQ(2)=EFEE(M-1+IE)
     ENDIF
  ENDDO
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  PLOTXSCALE=RKC
  PLOTTITLE=ITL(IWC)
  CALL PLT (XY(LX),EFEE(M),JS(IEXP),JF(IEXP),1,*170)
  GOTO 150
  !
  !	Here for phaseshift plotting
  !
310 IF (INTOPT.EQ.0.OR.INTOPT.GT.IPARNP) THEN
     IS=1
     IF=IPARNP
  ELSE
     IS=INTOPT
     IF=INTOPT
  ENDIF
  II=0
  PLOTXSCALE=EC
  PLOTXLAB='energy/eV'
  PLOTYLAB='radians'
  DO J=IS,IF
     NM=NPH(J)
     IF (NM.LT.2.OR.NM.GT.IPARPHPOINTS) GOTO 350
     IF (NPS.GT.1.AND.IS.NE.IF) II=II+1
     IF (II.EQ.5) THEN
        CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*56,*170)
56      II=1
        IF (IGF(2).EQ.2) CALL FCLEAR
     ENDIF
     !	IF (II.LE.1.AND.CHAROPT(1:1).NE.'N') CALL GRINIT
     JJ=II
     RQ(1)=0.
     RQ(2)=10.
     IF (CHAROPT(1:1).EQ.'I') THEN
        RQ(1)=-.2
        RQ(2)=.2
     ENDIF
     IF (CHAROPT(1:1).EQ.'A') THEN
        IMAX=LMAX+1
     ELSE
        IMAX=7
     ENDIF
     DO I=1,IMAX
	IF (CHAROPT(1:1).EQ.'M') THEN
           DO K=1,NM
              XXF(K)=CABS(PHS(K,J,I))
           enddo
	ELSEIF (CHAROPT(1:1).EQ.'I') THEN
           DO K=1,NM
              XXF(K)=AIMAG(PHS(K,J,I))
           enddo
	ELSE
           DO K=1,NM
              XXF(K)=REAL(PHS(K,J,I))
           enddo
	ENDIF
	IF (I.NE.1) THEN
           ICOD=0
           PLOTTITLE=' '
	ELSE
           ICOD=1
           PLOTTITLE=LFS(J+IPARNSP+1)(1:NCSTR(LFS(J+IPARNSP+1)))//' - l=0 to 6 phaseshifts'
           IF (JJ.EQ.2.OR.JJ.EQ.4) ICOD=3
	ENDIF
	CALL PLT (EN(1,J),XXF,1,NM,ICOD,*170)
     enddo
350  CONTINUE
  enddo
  RETURN
  !
  !	Here for PLOT REF
  !
360 CONTINUE
  RQ(2)=-99.
  RQ(1)=99.
  IF (IN1.LT.1) THEN
     IN1=1
  ELSEIF (IN1.GT.NPS) THEN
     IN1=NPS
  ENDIF
  DO I=1,NPH(IN1)
     IF (REAL(EREF(I,IN1)).GT.RQ(2)) THEN
        RQ(2)=REAL(EREF(I,IN1))
     ELSEIF (REAL(EREF(I,IN1)).LT.RQ(1)) THEN
        RQ(1)=REAL(EREF(I,IN1))
     ENDIF
     W(I)=REAL(EREF(I,IN1))
     IF (EN(I,IN1).GT.XMAX2) GOTO 88
  ENDDO
  I=NPH(IN1)
88 K=I
  PLOTTITLE='Real(EREF) up to XMAX'
  PLOTXSCALE=EC
  PLOTYSCALE=EC
  CALL PLT (EN(1,IN1),W,1,K,1,*170)
  !-st	CALL POSITN (EN(1,IN1)/EC,SNGL(V0S(1)*.5/EC))
  CALL POSITN (EN(1,IN1)/EC,real(V0S(1)*.5/EC, kind=sp))
  !-st	CALL JOIN (EN(K,IN1),SNGL(V0S(1)*.5/EC))
  CALL JOIN (EN(K,IN1),real(V0S(1)*.5/EC, kind=sp))
  CALL POSITN (0.,YMIN)
  CALL JOIN (0.,YMAX)
  CALL BROKEN
  CALL POSITN (FEEV(IN1),YMIN)
  CALL JOIN (FEEV(IN1),YMAX)
  CALL FULL
  RQ(2)=-99.
  RQ(1)=99.
  DO I=1,K
     IF (AIMAG(EREF(I,IN1)).GT.RQ(2)) THEN
        RQ(2)=AIMAG(EREF(I,IN1))
     ELSEIF (AIMAG(EREF(I,IN1)).LT.RQ(1)) THEN
        RQ(1)=AIMAG(EREF(I,IN1))
     ENDIF
     W(I)=AIMAG(EREF(I,IN1))
  ENDDO
  PLOTTITLE='Imag(EREF) - new scale'
  CALL PLT (EN(1,IN1),W,1,K,-1,*170)
  RETURN
  !
  !	Plot EREFP
  !
370 CONTINUE
  RQ(2)=0.
  RQ(1)=0.
  IF (IN1.LT.1) THEN
     IN1=1
  ELSEIF (IN1.GT.NPS) THEN
     IN1=NPS
  ENDIF
  DO I=1,NPS
     IF (REAL(EREFP(I,IN1)).GT.RQ(2)) THEN
        RQ(2)=REAL(EREFP(I,IN1))
     ELSEIF (REAL(EREFP(I,IN1)).LT.RQ(1)) THEN
        RQ(1)=REAL(EREFP(I,IN1))
     ENDIF
     W(I)=REAL(EREFP(I,IN1))
  ENDDO
  CALL PLT (ENER(JS(IEXP)),W,1,NPT(IEXP),1,*170)
  RQ(2)=0.
  RQ(1)=0.
  DO I=1,NPT(IEXP)
     IF (AIMAG(EREFP(I,IN1)).GT.RQ(2)) THEN
        RQ(2)=AIMAG(EREFP(I,IN1))
     ELSEIF (AIMAG(EREFP(I,IN1)).LT.RQ(1)) THEN
        RQ(1)=AIMAG(EREFP(I,IN1))
     ENDIF
     W(I)=AIMAG(EREFP(I,IN1))
  ENDDO
  CALL PLT (ENER(JS(IEXP)),W,1,NPS,-1,*170)
  RETURN
  !
  !	Here for SIGMA
  !
380 CONTINUE
  RQ(2)=0.
  RQ(1)=1.E6
  IF (NSIG.LE.0) RETURN
  IEXP=1
  CALL ARRMINMAX (SIGMA(JS(IEXP)),NPT(IEXP),RQ(1),RQ(2))
  !	DO I=JS(IEXP),JF(IEXP)
  !	IF (ABS(SIGMA(I)).GT.RQ(2)) THEN
  !	  RQ(2)=ABS(SIGMA(I))
  !	ELSEIF (ABS(SIGMA(I)).LT.RQ(1)) THEN
  !	  RQ(1)=ABS(SIGMA(I))
  !	ENDIF
  !	ENDDO
  PLOTTITLE='Sigma'
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  CALL PLT (XY(LX),SIGMA,JS(IEXP),JF(IEXP),1,*170)
  RETURN
395 CONTINUE
  IF (NME.LE.3) RETURN
  RQ(1)=0.
  RQ(2)=0.
  CALL ARRMAXA (ATABS(1,2),NPT(IEXP),RQ(2))
  PLOTXLAB='k/Angstroms^-1'
  ICODE=1
  IF (LINITIAL(IEXP).NE.0) THEN
     PLOTTITLE='L-1 matrix elements'
     IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
     CALL PLT (XY(LX+JS(IEXP)),ATABS(1,1),1,NPT(IEXP),1,*170)
     ICODE=0
  ENDIF
  PLOTTITLE='L+1 matrix elements'
  CALL PLT (XY(LX+JS(IEXP)),ATABS(1,2),1,NPT(IEXP),ICODE,*170)
  RQ(2)=0.
  CALL ARRMAXA (RATIO(1,2),NPT(IEXP),RQ(2))
  ICODE=-1
  IF (LINITIAL(IEXP).NE.0) THEN
     PLOTTITLE='L-1 weighting (scale: 0-1.2)'
     CALL PLT (XY(LX+JS(IEXP)),RATIO(1,1),1,NPT(IEXP),-1,*170)
     ICODE=0
  ENDIF
  PLOTTITLE='L+1 weighting (scale: 0-1.2)'
  CALL PLT (XY(LX+JS(IEXP)),RATIO(1,2),1,NPT(IEXP),ICODE,*170)
  RETURN
  !
  !	Here for compare
  !
  ENTRY CPLOT(INN,*)
  JJ=0
  JZ=IGF(5)-1
  LX=3*IPARNPOINTS*IPARNSP+1*IPARNPOINTS+1
  IF (IGF(4).NE.1) LX=1
  PLOTXLAB='k/Angstroms^-1'
  IF (IGF(4).NE.1) PLOTXLAB='energy/eV'
  PLOTYLAB='k^'//NUMBERS(JZ)//'^ chi(k)'
  PLOTXSCALE=RKC
  IF (IGF(4).NE.1) PLOTXSCALE=EC
  PLOTYSCALE=RKC**JZ
  INT=INN
  ICNT=0
  RE0=EF0(0)
390 IF (ICNT.NE.0) CALL TPOS (0,740)
  CALL WTEXT ('Enter Quit, EXAFS or FT')
  IF (ICNT.NE.0) CALL TPOS (100,740)
  CALL CREAD (IP1,IP2,IP3,IN,IC,V,*390,*410)
  IF (IP1(1:1).EQ.'Q') GOTO 410
  !
  !	The shift option only applies with xaxis=k
  !
  XSHIFT=0.
  IF (IGF(4).EQ.1) THEN
     CALL WTEXT ('Enter X-shift for spectrum 2 [0]')
     CALL CREAD (IP3,IP2,IP3,IN,IC,XSHIFT,*400,*410)
400  CALL PCHANGE ('EF',XSHIFT+RE0)
  ELSE
     CALL WTEXT ('Note : X-shift not permitted for X-AXIS=2')
  ENDIF
  WRITE (7,*) 'JZ=',JZ
  IF (LC.NE.0) CALL FIT ('Q',iset(IWEIGHT),*410)
  !	CALL GRINIT
  RQ(2)=0.
  ICNT=ICNT+1
  IEXP=1
  IF (IP1(1:1).EQ.'E') THEN
     CALL WEIGHT (OLDXABS(IOLDJS(1),1),UBS(IOLDJS(1),1),OLDRK(IOLDJS(1),1),IOLDJF(1)-IOLDJS(1)+1)
     CALL WEIGHT (XABS(JS(IEXP),1),    UBS(JS(IEXP),2), RK(JS(IEXP),IEXP), NPT(IEXP))
     IF (INT.EQ.1) CALL WEIGHT (XABT(JS(IEXP),1),UBS(JS(IEXP),3),RK(JS(IEXP),IEXP),NPT(IEXP))
     PLOTTITLE=LFS(1)
     CALL PLT  (OLDXY(LX),UBS(1,1),IOLDJS(1),IOLDJF(1),1,*170)
     PLOTTITLE=LFSAV
     CALL ADDSHIFT (PLOTTITLE,XSHIFT)
     CALL PLT (XY(LX),UBS(1,2),JS(IEXP),JF(IEXP),0,*170)
     IF (INT.EQ.1) THEN
        PLOTTITLE='theory'
        CALL PLT (XY(LX),UBS(1,3),JS(IEXP),JF(IEXP),0,*170)
     ENDIF
  ELSE
     !
     !	FT option in COMPARE
     !
     IF (LF.NE.0.) CALL FOURS (IGF(3))
     RQ(2)=0.
     PLOTXLAB='r/Angstroms'
     K=IFTSET(2)
     PLOTYLAB='FT '//FTOPTS(K)(1:NCSTR(FTOPTS(K)))//' chi(k)'
     CALL ARRMAXA (OLDFE,IPARNA,RQ(2))
     DO IEXP=1,1
	IF (IGF(3).NE.3) CALL ARRMAXA (FE(1,IEXP),IPARNA,RQ(2))
	IF (IGF(3).NE.2) CALL ARRMAXA (FT(1,IEXP),IPARNA,RQ(2))
     ENDDO
     IF (IFTSET(7).EQ.1) THEN
        RQ(1)=0.
     ELSE
        RQ(1)=-RQ(2)
     ENDIF
     PLOTXSCALE=DC
     PLOTYSCALE=1.
     PLOTTITLE=LFSAV
     CALL PLT (AF,OLDFE,1,IPARNA,1,*170)
     PLOTTITLE=LFS(1)
     CALL ADDSHIFT (PLOTTITLE,XSHIFT)
     CALL PLT (AF,FE(1,1),1,IPARNA,0,*170)
     PLOTTITLE='theory'
     PLOTTITLE=ITL(6)
     IF (INT.EQ.1) CALL PLT (AF,FT,1,IPARNA,0,*170)
  ENDIF
  GOTO 390
410 CALL PCHANGE ('EF',RE0)
420 RETURN
430 FORMAT (/A/)
440 FORMAT (/'  N  Index    Path              Length  Angle     Mag        Max        wMax'/)
  ! 530	FORMAT (I3,F8.2,3X,A,F6.3,F6.1,' | ',2E11.2,E11.2)
END SUBROUTINE GPLOT
