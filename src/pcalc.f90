SUBROUTINE PCALC (LMAX1,INT,V0,ZED,H,GRID,G2,VMT,COR,MATRIX,INCR,XSTART, &
     IENMAX,EFERMI,RLIFE,RHO,RMT,AUTO)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_MATEL
  Use Common_P
  Use Include_PA1
  !
  !	Finds wave-functions and log derivative (=dlogcx) at the mtr
  !	by outward integration of Dirac equation.
  !	Constructs l-wave phase-shifts up to (lmax1-1)
  !
  COMPLEX :: &
       AA,BB,CC,DD,TT,UU,TF,ARG,ARG1,ARG2,BETA,VPOT1,VPOT2,CX,SX, &
       TERM1,TERM2,TERM3,TERM4,EE,GNU1,DLOGCX,ETA,RL,CTEMP,V1,Y,DY,WI,DWI,  &
       D2Y,AK1,AK2,AK3,AK4,V(IPARGRID),REF,TPOT(IPARGRID),CTERM1(IPARGRID), &
       CTERM4(IPARGRID),CTERM5(IPARGRID),CTERM6(IPARGRID),CTERM7(IPARGRID), &
       CTERM8(IPARGRID),AREA,RCOR(IPARGRID),YS,TYS,HR(2),VE,RNU,BESSI(0:26),&
       BESSR(0:26),VPOT(IPARGRID),BETANEW,TWOZ,TWOZSQ,SA1,SA2,SB0,SB1,SB2,  &
       DRCOR(IPARGRID)
  DIMENSION &
       COR(1000),GRID(*),G2(*),VMT(*),BREL(IPARGRID),VM(IPARGRID), &
       BRELP1(IPARGRID),CTERM2(IPARGRID),CTERM3(IPARGRID),CTERM6A(IPARGRID), &
       RRS(IPARGRID),RHO(*),WPFE(IPARGRID),VXCRMU(IPARGRID),SRS(IPARGRID),   &
       XXL1(1),XXL1H3(1),XXL1H9(1),XXL1H32(1),XXL1H34(1),XXL1H94(1),H3(1),   &
       TL(27),TL1(27),TL2(27),TL3(27),TL4(27)
  LOGICAL WAVE,AUTO
  SAVE CTEMP

  DATA TL /3.,5.,7.,9.,11.,13.,15.,17.,19.,21.,23.,25.,27.,29.,31.,33.,35.,37.,39.,41.,43.,45.,47., &
       49.,51.,53.,55./
  DATA TL1/5.,7.,9.,11.,13.,15.,17.,19.,21.,23.,25.,27.,29.,31.,33.,35.,37.,39.,41.,43.,45.,47.,49., &
       51.,53.,55.,57./
  data TL2/14.,18.,22.,26.,30.,34.,38.,42.,46.,50.,54.,58.,62.,66.,70.,74.,78.,82.,86.,90.,94.,98., &
       102.,106.,110.,114.,118./
  data TL3/27.,33.,39.,45.,51.,57.,63.,69.,75.,81.,87.,93.,99.,105.,111.,117.,123.,129.,135.,141.,147., &
       153.,159.,165.,171.,177.,183./
  data TL4/44.,52.,60.,68.,76.,84.,92.,100.,108.,116.,124.,132.,140.,148.,156.,164.,172.,180.,188.,196., &
       204.,212.,220.,228.,236.,244.,252./
  !
  !	X(I)-VALUES OF ARG. ON LOG GRID
  !	Y-SOLUTION TO D2Y/DR2-(V-E+L(L+1)/R2)Y=0 AFTER SUB. X=LOG(R)
  !	DY-DY/DX OR (1/R)DY/DR
  !	VMT(I)- POTENTIAL EVALUATED ON LOG GRID
  !
  !	rewind (75)
  !	rewind (76)
  !	rewind (77)
  NERRS=0
  CALL GETTIM (1,CPU1,ELAP)
  CPUXC=0.
  CPULOOP=0.
  CPUSTART=0.
  IF (MATRIX.EQ.1) WRITE (7,*) 'Final state: L=',LINITIAL(1)-1,LINITIAL(1)+1
  IF (DEBUG) THEN
     WRITE (LOGFILE,*) 'PCALC: LMAX1,INT,LINITIAL(1),V0,Z,H,XST',LMAX1,INT,LINITIAL(1),V0,ZED,H,XSTART
     WRITE (LOGFILE,*) 'PCALC: GRID(1/INCR-1/INCR),VMT(1/INCR-1/INCR)',GRID(1),GRID(INCR-1), &
          GRID(INCR),VMT(1),VMT(INCR-1),VMT(INCR)
  ENDIF
  CSQ=274.07446*274.07446
  !	IF (IENMAX.EQ.74) THEN
  ILOW=1
  IHIGH=IENMAX
  !	ELSE
  !	  XE0=XE(INT)+EF
  !	  WRITE (6,*) EMIN,EMAX
  !	  EEMIN=EMIN0(0)+XE(INT)+EF0(0)
  !	  EEMAX=EMAX0(0)+XE(INT)+EF0(0)
  !	  IF (EEMIN.LT.0) EEMIN=0.
  !	  IF (EEMAX.LT.0) EEMAX=1999.
  !	  ILOW=(ALOG(EEMIN+3.35)-1.5)/.15-1
  !	  IHIGH=(ALOG(EEMAX+3.35)-1.5)/.15+3
  !	  WRITE (6,*) ILOW,IHIGH
  !	ENDIF
  H2=H/2.
  H3(1)=H/3.
  H6=H/6.
  H8=H/8.
  H43=H*4./3.
  HR(2)=GRID(1)
  CALL DIFFS (VMT,H,INCR,VM)
  !	RCONST=1.9191583**-2*SQRT(12.)
  RCONST=.940522145
  IF (DEBUG) WRITE (7,*) 'INCR,G2(INCR),RHO(INCR)',INCR,G2(INCR),RHO(INCR)
  DO I=1,INCR
     IF (RHO(I).LE.0.) RHO(I)=1.E-6
     RRS(I)=(3.*G2(I)/RHO(I))**(1./3.)
     SRS(I)=SQRT(RRS(I))
     WPFE(I)=RCONST*SRS(I)
  ENDDO
  DO I=1,INCR
     CALL RHL (VXCRMU(I),VXCIMU,1.,RRS(I),SRS(I),WPFE(I))
     IF (VXCIMU.NE.0.) WRITE (6,*) 'VXCI',I,VXCIMU
  ENDDO
  CONST=(GRID(1)+GRID(2))*.5
  CONST1=CONST*CONST
  RVZ=-2.*ZED
  RVZ2=RVZ*RVZ
  RVZ3=RVZ*RVZ2
  DO  IENER=1,IENMAX
     IF (IENMAX.EQ.74) THEN
        EE=(EXP(1.5+.08*real(IENER,kind=dp))-3.00)/13.605825
     ELSE
        EE=(EXP(1.5+.066*real(IENER,kind=dp))-3.35)/13.605825
     ENDIF
     E=EE+V0
     EN(IENER,INT)=E*.5
     !	IF (IENER.LT.ILOW.OR.IENER.GT.IHIGH) goto 130
     !	CALL GETTIM (1,CPU2,ELAP)
     CALL XCPOT2 (IENER,GRID,E,REF,EFERMI,RLIFE,VMT,V,INCR,RRS,SRS,WPFE,VXCRMU)
     !	CALL DIFFS (VMT,H,INCR,VM)
     !	CALL GETTIM (1,CPU3,ELAP)
     !	CPUXC=CPUXC+CPU3-CPU2
     IF (E.LT.EFERMI.OR.iset(IEXCH).EQ.2) THEN
        EREF(IENER,INT)=CMPLX(V0,AIMAG(REF))
        EREF(IENER,INT)=REF
        !	  IF (IENER.EQ.1) WRITE (7,*) 'EREF',EREF(IENER,INT)/EC*.5,V0/EC*.5,E
     ELSE
        EREF(IENER,INT)=REF
     ENDIF
     IF (DEBUG) WRITE (LOGFILE,*) 'PCALC: EREF(',IENER,')',EREF(IENER,INT)
     V1=(V(1)*GRID(1)+V(2)*GRID(2))*.5*CONST
     VM1=(VM(1)+VM(2))*.5
     GNU1=1.-(V(1)-EE)/CSQ
     IF (iset(IREL).EQ.0) GNU1=1.
     EE=E-EREF(IENER,INT)
     EREF(IENER,INT)=EREF(IENER,INT)*.5
     !	IF (IENER.GT.74) goto 130
     DO  I=1,INCR
	VE=V(I)-EE
	VPOT(I)=VE*G2(I)
	BREL(I)=VM(I)/(CSQ-VE)
	IF (iset(IREL).EQ.0) BREL(I)=0.
	RNU=1.-VE/CSQ
	IF (iset(IREL).EQ.0) RNU=1.
	VPOT(I)=(VPOT(I)*RNU+BREL(I))*H3(1)
	TPOT(I)=VPOT(I)*H3(1)
	BREL(I)=(BREL(I)-1.)*H3(1)
   	BRELP1(I)=BREL(I)+1.
     enddo
     DO  I=3,INCR
	CTERM2(I)=(BRELP1(I)-BREL(I-1))*H43
	CTERM3(I)=(2.+BREL(I-1)-BREL(I-2))*H3(1)
   	CTERM6A(I)=-4.*BREL(I-1)
     enddo
     V1=V1-EE*CONST1
     RNU=CSQ-V1/CONST1
     IF (iset(IREL).EQ.0) RNU=CSQ
     BREL1=VM1/RNU
     IF (iset(IREL).EQ.0) BREL1=0.
     V1=V1*RNU/CSQ+BREL1
     BREL1=1.-BREL1
     RNU=(V(1)-EE)*G2(1)
     RNU=CSQ-RNU/G2(1)
     IF (iset(IREL).EQ.0) RNU=CSQ
     BREL0=1.-VM(1)/RNU
     IF (iset(IREL).EQ.0) BREL0=1.
     RNU=(V(2)-EE)*G2(2)
     RNU=CSQ-RNU/G2(2)
     IF (iset(IREL).EQ.0) RNU=CSQ
     BREL2=1.-VM(2)/RNU
     IF (iset(IREL).EQ.0) BREL2=1.
     BETA=SQRT(EE)
     ARG=BETA*RMT
     LMAX=ARG+1
     IF (LMAX.LT.2) THEN
        LMAX=2
     ELSEIF (LMAX.GE.LMAX1) THEN
        LMAX=LMAX1-1
     ENDIF
     LMIN=LMAX-2
     IF (LMIN.LT.1) LMIN=1
     ARG2=0.5*ARG*ARG
     SX=SIN(ARG)
     CX=COS(ARG)
     AA=SX/ARG
     BB=-CX/ARG
     CC=(AA-CX)/ARG
     DD=(BB-SX)/ARG
     BESSI(0)=AA
     BESSI(1)=CC
     BESSR(0)=BB
     BESSR(1)=DD
     FF=3.
     IF (REAL(ARG).LE..84) THEN
        DO  L=2,LMAX+1
           ARG1=1.0
           DO I=1,L
              ARG1=ARG1*ARG/TL(I)
           enddo
           TERM1=ARG2/TL1(L)
           TERM2=TERM1*ARG2/TL2(L)
           TERM3=TERM2*ARG2/TL3(L)
           TERM4=TERM3*ARG2/TL4(L)
           AA=ARG1*(1.0-TERM1+TERM2-TERM3+TERM4)
           BESSI(L)=AA
           UU=BB
           BB=DD
           DD=FF/ARG*BB-UU
           BESSR(L)=DD
           FF=FF+2.
        enddo
     ELSE
        DO  I=2,LMAX+1
           TT=AA
           UU=BB
           AA=CC
           BB=DD
           TF=FF/ARG
           CC=TF*AA-TT
           DD=TF*BB-UU
           BESSI(I)=CC
           BESSR(I)=DD
           FF=FF+2.
        enddo
     ENDIF
     VPOT1=VPOT(1)/H3(1)
     VE=VPOT1/G2(1)-RVZ/GRID(1)
     VPOT2=VPOT(2)/H3(1)
     DO  L=0,LMAX
        !	CALL GETTIM (1,CPU2,ELAP)
	WAVE=MATRIX.EQ.1.AND.(L.EQ.LINITIAL(1)-1.OR.L.EQ.LINITIAL(1)+1)
	XL=L
	XL2=L+2
	XXL=L+L
	XXL1(1)=L*(L+1)
	XXL1H3(1)=XXL*H3(1)
	XXL1H32(1)=XXL1H3(1)*2.
	XXL1H34(1)=XXL1H3(1)*4.
	XXL1H9(1)=XXL1H3(1)*H3(1)
	XXL1H94(1)=XXL1H9(1)*4.
	XL1=L+1
	X2L3=2*L+3
	X3L4=3*L+4
        !	ZZ=XL*BESSR(L)-ARG*BESSR(L+1)
        !	YY=XL*BESSI(L)-ARG*BESSI(L+1)
        !
        !	Find starting values
        !
	IHIGHACC=99
	IF (IHIGHACC.EQ.99) THEN
           HR(1)=GRID(1)*(real(L+1,kind=dp)/real(L+2,kind=dp))
           DO I=1,2
              IF (I.EQ.2) THEN
                 QTERM=(XL1/XL2)**(L+1)
                 DRCOR(1)=RCOR(1)*QTERM
              ENDIF
              RCOR(1)=XL1+RVZ*HR(I)+(RVZ2+VE*XL1)*HR(I)*HR(I)/X2L3+(RVZ3+RVZ*VE*X3L4)*HR(I)**3/(3.*XL2*X2L3)
           ENDDO
           !	  RCOR(1)=RCOR(1)*1.E-15
           !	  DRCOR(1)=DRCOR(1)*1.E-15
           DRCOR(1)=(RCOR(1)-DRCOR(1))*XL2
	ELSE
           TWOZ=-V(1)*GRID(1)
           TWOZSQ=TWOZ*TWOZ
           RCOR(1)=1.E-15
           BETANEW=SQRT(XXL1(1)+1.0-TWOZSQ/CSQ)
           SB0=(BETANEW-XL1)*CSQ/TWOZ
           SA1=(3.*BETANEW-TWOZSQ/CSQ)/(2.*BETANEW+1.)
           SB1=CSQ/TWOZ*((BETANEW-XL)*SA1-1.)-SB0
           SA2=((BETANEW+3.*XL1)*SA1-3.*XL+TWOZ/CSQ*(BETANEW+XL1+3.)*SB1)/(BETANEW+1.)/4.
           SB2=(CSQ/TWOZ*(2.*XL*(BETANEW+2.-XL1)-L-TWOZSQ/CSQ)*SA1-3.*XL*CSQ/TWOZ*(BETANEW+2.-XL1) &
                +(BETANEW+3.-2.*XL1-TWOZSQ/CSQ)*SB1)/(BETANEW+1.0)/4.0
           DELTA=GRID(1)*CSQ/TWOZ
           DRCOR(1)=(SB0+DELTA*(SB1+DELTA*SB2))/(1.+DELTA*(SA1+DELTA*SA2))*RCOR(1)
           DRCOR(1)=DRCOR(1)*GRID(1)*GNU1+XL1*RCOR(1)
	ENDIF
        !	CALL GETTIM (1,CPU3,ELAP)
        !	CPUSTART=CPUSTART+CPU3-CPU2
        !
	IF (L.EQ.0) THEN
           DO N=3,INCR
              CTERM1(N)=(bRELP1(N)+TPOT(N-2))
              CTERM4(N)=(4.*TPOT(N-1))
              CTERM5(N)=(VPOT(N)+VPOT(N-2))
              CTERM6(N)=(CTERM6A(N)+4.*TPOT(N))
              CTERM7(N)=(1.-bREL(N-2)+TPOT(N))
              CTERM8(N)=(4.*VPOT(N-1))
           ENDDO
	ELSE
           !	  DO I=1,INCR
           !	  VPOT(I)=VPOT(I)+XXL1H3(1)
           !	  TPOT(I)=TPOT(I)+XXL1H9(1)
           !	  ENDDO
           DO N=3,INCR
              CTERM1(N)=CTERM1(N)+XXL1H9(1)
              CTERM4(N)=CTERM4(N)+XXL1H94(1)
              CTERM5(N)=CTERM5(N)+XXL1H32(1)
              CTERM6(N)=CTERM6(N)+XXL1H94(1)
              CTERM7(N)=CTERM7(N)+XXL1H9(1)
              CTERM8(N)=CTERM8(N)+XXL1H34(1)
              TPOT(N)=TPOT(N)+XXL1H9(1)
           ENDDO
	ENDIF
	DO N=3,INCR
           BETAR=BRELP1(N)-REAL(TPOT(N))
           BETAI=AIMAG(TPOT(N))
           BETAD=BETAR*BETAR+BETAI*BETAI
           DRCOR(n)=CMPLX(BETAR/BETAD,BETAI/BETAD)
	ENDDO
        !
        !	Find next point of solution using Runge-Kutta
        !
	DWI=DRCOR(1)*BREL0
	D2Y=DWI+(VPOT1+XXL1(1))*RCOR(1)
	AK1=H*D2Y
        !
	WI=RCOR(1)+H2*DRCOR(1)+H8*AK1
	DWI=(DRCOR(1)+0.5*AK1)*BREL1
	D2Y=DWI+(V1+XXL1(1))*WI
	AK2=H*D2Y
        !
	DWI=(DRCOR(1)+0.5*AK2)*BREL1
	D2Y=DWI+(V1+XXL1(1))*WI
	AK3=H*D2Y
        !
	WI=RCOR(1)+H*DRCOR(1)+H2*AK3
	DWI=(DRCOR(1)+AK3)*BREL2
	D2Y=DWI+(VPOT2+XXL1(1))*WI
	AK4=H*D2Y
        !
	RCOR(2)=H*(DRCOR(1)+(AK1+AK2+AK3)/6.)+RCOR(1)
	DRCOR(2)=(AK1+2.*AK2+2.*AK3+AK4)/6.+DRCOR(1)
        !
        !	Solve differential equ. ( Fox and Goodwin method V)
        !
        !	CALL GETTIM (1,CPU2,ELAP)
	DO N=3,INCR
           RCOR(N)=(CTERM1(N)*RCOR(N-2)+CTERM2(N)*DRCOR(N-1)+CTERM3(N)*DRCOR(N-2)+CTERM4(N)*RCOR(N-1))*DRCOR(N)
           DRCOR(N)=(CTERM5(N)*RCOR(N-2)+CTERM6(N)*DRCOR(N-1)+CTERM7(N)*DRCOR(N-2)+CTERM8(N)*RCOR(N-1))*DRCOR(N)
           if (REAL(RCOR(n)).GT.1.E15) THEN
              RCOR(N-1)=RCOR(N-1)*1.E-30
              RCOR(N)=RCOR(N)*1.E-30
              DRCOR(N-1)=DRCOR(N-1)*1.E-30
              DRCOR(N)=DRCOR(N)*1.E-30
           ENDIF
        enddo
        !	CALL GETTIM (1,CPU3,ELAP)
        !	CPULOOP=CPULOOP+CPU3-CPU2
        !
	DLOGCX=XL1-DRCOR(INCR)/RCOR(INCR)
        !	IF (IENER.EQ.1) WRITE (7,*) 'L,DLOGCX',L,DLOGCX,DRCOR(INCR),RCOR(INCR)
        !
	TT=BESSR(L)*DLOGCX-ARG*BESSR(L+1)
	UU=BESSI(L)*DLOGCX-ARG*BESSI(L+1)
	CTEMP=UU/TT
	XX=REAL(CTEMP)
	WW=AIMAG(CTEMP)
	XW=XX*XX+WW*WW
	IF (XX .NE. 0.0) THEN
           ALPH=(1.-XW)
           ALPH=SQRT(ALPH*ALPH+4.*XX*XX)-ALPH
           ALPH=ALPH/2./XX
           ALPH=ATAN(ALPH)
           !	  ALPH=(2.*XX/(1.-XW))
           !	  ALPH =.5* ATAN(ALPH)
	ELSE
           ALPH = 0.0
	ENDIF
	GAMMA=XW*.5+.5
	ARGLOG=(GAMMA+WW)/(GAMMA-WW)
	IF (ARGLOG.LE.0.) THEN
           IF (NERRS.LT.10) WRITE (6,*) 'ARGLOG,GAMMA,WW,CTEMP',ARGLOG,GAMMA,WW,CTEMP
           nerrs=NERRS+1
           ARGLOG=1.E-30
	ENDIF
	GAMMA=.25*LOG(ARGLOG)
	ETA = CMPLX(ALPH,GAMMA)
        !	if (iener.eq.1.and.l.le.2) then
        !	do i=1,incr
        !	write (75+L,*) grid(i),real(rcor(i)),real(drcor(i))
        !	enddo
        !	endif
	IF (WAVE) THEN
           IFINAL=1
           IF (L.GT.LINITIAL(1)) IFINAL=2
           RL=BESSI(L)*COS(ETA)-BESSR(L)*SIN(ETA)
           RL=RL/RCOR(INCR)*RMT
           DO  I=1,INCR
              RCOR(I)=RCOR(I)*RL/GRID(I)
              IF (SPARE11.EQ.1) RCOR(I)=CMPLX(REAL(RCOR(I)),0.)
              RCOR(I)=RCOR(I)*COR(I)
           enddo
           CALL CSMPSN (H,XSTART,XSTART,ALOG(RMT),RCOR,AREA,INCR)
           IF (SPARE11.EQ.2) AREA=CMPLX(REAL(AREA),0.)
           RME(IENER,IFINAL)=ABS(AREA)
           IF (SPARE11.EQ.3) RME(IENER,IFINAL)=CABS(AREA)
           !	  WRITE (7,*) 'AREA',RME(IENER,IFINAL),AREA
	ENDIF
	LL=L
        !
        !	None of this really works
        !
	IF (L.GE.5.AND.REAL(ETA).GT.PI-0.1) THEN
           ETA=ETA-PI
	ENDIF
	IF (REAL(ETA).LT.0.) THEN
           IF (L.GE.3.AND.REAL(ETA).GT.-0.1) THEN
              LL=-1
           ELSEIF (L.GE.5.AND.REAL(ETA).GT.-0.31) THEN
              LL=-1
           ELSE
              ETA=ETA+PI
           ENDIF
	ENDIF
	PHS(IENER,INT,L+1)=ETA
	IF (ABS(REAL(ETA)).LT.0.001.AND.LL.GT.LMIN) goto 130
     enddo
130  CONTINUE
  enddo
  CALL GETTIM (1,CPU2,ELAP)
  IF (.NOT.BACKGROUND.AND..NOT.AUTO) THEN
     WRITE (LOGFILE,*) 'Time in PCAL  :',CPU2-CPU1
     WRITE (LOGFILE,*) 'Time in XCPOT :',CPUXC
     WRITE (LOGFILE,*) 'Time in LOOP  :',CPULOOP
     WRITE (LOGFILE,*) 'Time in START :',CPUSTART
     WRITE (LOGFILE,*) 'Time in REST  :',CPU2-CPU1-CPUXC-CPULOOP-CPUSTART
  ENDIF
  RETURN
END SUBROUTINE PCALC
