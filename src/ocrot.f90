SUBROUTINE OCROT (DN,KROT,IS,*)
  !======================================================================
  !	Performs symmetry operation - KROT fold rotation about z
  Use Definition
  Use Common_CONVIA
  real(dp) DN(36,*)
  DN(1,3)=DN(1,3)+PI2/real(KROT,kind=dp)
  IF (KROT.EQ.2) THEN
     DN(1,4)=-DN(1,4)
     DN(1,5)=-DN(1,5)
  ELSEIF (KROT.EQ.4) THEN
     TU=DN(1,4)
     DN(1,4)=-DN(1,5)
     DN(1,5)=TU
  ELSE
     CALL POLTOCAR (DN(1,1),DN(1,2),DN(1,3),DN(1,4),DN(1,5),DN(1,6))
  ENDIF
  IS=IS+2
  RETURN 1
END SUBROUTINE OCROT
