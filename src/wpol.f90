SUBROUTINE WPOL (JSHELL,ZZ,CEXAFS,IFILE,IEXP)
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_POL
  Use Include_IPA
  Use Include_PA1

  COMPLEX ZZ(-3:3,-3:3),CEXAFS
  IF (iset(ISING).NE.0) THEN
     WRITE (IFILE,30) 'Beam parameters: TH=',BETH0(IEXP),' PHI=', BEPHI0(IEXP),' OMEGA=',BEW0(IEXP), &
          ' ISING=',iset(ISING)
  ENDIF
  DO  M=-IEDGE,IEDGE
     IF (ABS(M).LE.LINITIAL(IEXP)) THEN
        WRITE (IFILE,50) (CCANG2(I,M,IEXP),I=-IEDGE,IEDGE),(ZZ(M,MM),MM=-1,1),(CCANG1(J,M,IEXP), &
             J=-LINITIAL(IEXP),LINITIAL(IEXP)),CEXAFS
     ELSE
        WRITE (IFILE,60) (ZZ(M,MM),MM=-1,1),CCANG1(M,M,IEXP)
     ENDIF
  enddo
  WRITE (IFILE,40) (ZZ(-1,-1)+ZZ(0,0)+ZZ(1,1))/1.5
  RETURN
30 FORMAT (A,F10.4,A,F10.4,A,F10.4,A,I3/)
40 FORMAT (/'2/3* z(-1-1)+z(00)+z(11)',3(2E12.4,2X))
50 FORMAT (3(F5.1,F4.1),'|',3(2E9.2,2X),'|',F5.1,F4.1,' =',12E12.4)
60 FORMAT (27(' '),'|',3(2E9.2,2X),'|',F5.1,F4.1)
END SUBROUTINE WPOL
