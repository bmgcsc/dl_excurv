SUBROUTINE PRINT
  !======================================================================
  Use Common_DLV
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_F
  Use Common_POT
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Common_P
  Use Common_ATMAT
  Use Common_PRDSN
  Use Include_IPA
  Use Include_PA2
  Use Include_XY
  Use Include_FT
  !======================================================================
  !       
  !	Output spectra, parameters, etc.
  !
  PARAMETER (IPAR0=IPARNPOINTS*4)
  PARAMETER (IPAR1=IPARNPOINTS*(1+IPARNSP  ))
  PARAMETER (IPAR2=IPARNPOINTS*(1+IPARNSP*3))
  PARAMETER (IPAR3=IPARNPOINTS*2)
  PARAMETER (IPAR4=IPARNPOINTS*(1+3*IPARNSP)+IPARNA)
  PARAMETER (IPAR5=IPARNPOINTS*(1+3*IPARNSP)+IPARNA*(1+IPARNSP))
  PARAMETER (IPAR6=IPARNPOINTS*(1+3*IPARNSP)+IPARNA*(1+IPARNSP*2))
  PARAMETER (IPAR7=IPARNPOINTS*(1+3*IPARNSP)+IPARNA*(1+IPARNSP*3))
  !
  !	    INTOPT    <= 11  No column codes for printing spectra
  !	              > 11   Column codes for printing spectra
  !	    INTOPT     =     1,2,3 Spectrum number
  !
  DIMENSION    :: KN(4),XOUT(IPARNPOINTS*4)
  integer(i4b),dimension(:) :: JY(15)
  CHARACTER*13 :: OPTS(11)
  CHARACTER*12 :: KP1,KP2,KP3
  CHARACTER*10 :: CDATE
  character*8  :: CTIME
  !
  !	Order of array offsets is: ENER,K,R(for FT),EXPER,THEORY,
  !	FT(exp),FT(th),Imag FT(exp),Imag FT(th),BS Mag,BS Phase,Phase,
  !	Exper (for DIFFERENCE).
  !
  DATA JY/ &
       0          , IPAR2,       IPAR2, IPARNPOINTS,       IPAR1, &
       IPARNPOINTS, IPAR1,       IPAR4,       IPAR6,       IPAR5, &
       IPAR7      ,     0, IPARNPOINTS,       IPAR3, IPARNPOINTS/
  DATA OPTS/ &
       'SPECTRUM', 'PARAMETERS', 'ATOMIC_COORDS', 'VARIABLES', 'GEOMETRY', &
       'TMATRIX' ,  'POTENTIAL',        'PHASES',     'PATHS',      'REF', &
       'BACK'/

  CALL FINDOPT (11,OPTS,IOPT,1,'Spectrum no. or column codes',*110)
  JQ=iset(IWEIGHT)
  !
  !	Here for SPECTRUM, PARAMETERS, TMATRIX, REF, BACK
  !	  - update fit index, FT and theory.
  !
  IF (IOPT.LE.2.OR.IOPT.EQ.6.OR.IOPT.GT.9) CALL FIT ('F',JQ,*111)
111 IF (IOPT.EQ.1.OR.IOPT.EQ.4.OR.IOPT.GE.6) THEN
     ITYP=11
  ELSEIF (IOPT.EQ.2) THEN
     ITYP=13
  ELSEIF (IOPT.EQ.3.OR.IOPT.EQ.5) THEN
     ITYP=19
  ENDIF
  !
  !	All options except PHASES call OUTNAM to open output file
  !
  IF (IOPT.NE.8) THEN
     CALL OUTNAM(ITYP,PRDSN,' ',*110)
     IF (IOPT.NE.9) THEN
        CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*110)
        CALL GETDAT (CDATE,CTIME,OUTFILE)
     ELSE
        CALL FILEOPEN (PRDSN,OUTFILE,'UNFORMATTED',' ',*110)
     ENDIF
  ENDIF
  !
  !	Separate code for each IOPT
  !
  IF (IOPT.EQ.1) THEN
     IEXP=1
     IF (INTOPT.LE.IPARNSP) IEXP=MAX(1,INTOPT)
     IF (INTOPT.GE.11) THEN
        L=4
        IF (INTOPT.LT.1000) INTOPT=INTOPT*10
        IF (INTOPT.LT.1000) INTOPT=INTOPT*10
        DO I=4,1,-1
           KN(I)=MOD(INTOPT,10)
           INTOPT=INTOPT/10
           IF (KN(I).EQ.0) L=I-1
        enddo
     ELSE
        CALL IUZERO (KN,4)
20      IF (LRIS+LMDSN.EQ.0) THEN
           if(.not.DLV_flag)then
              CALL WTEXT ('COLUMN 1')
              CALL WTEXT (' 1. ENERGY')
              CALL WTEXT (' 2. WAVE NUMBER')
              CALL WTEXT (' 3. DISTANCE')
              CALL WTEXT ('ENTER CODE FOR COLUMN 1')
           endif
        ENDIF
        CALL CREAD (KP1,KP2,KP3,KN(1),IC,V,*20,*100)
        IF (LRIS+LMDSN.EQ.0) THEN
           if(.not.DLV_flag)then
              CALL WTEXT ('COLUMNS 2 TO 4')
              CALL WTEXT (' 0. OMIT')
              CALL WTEXT (' 1. EXAFS(experiment)')
              CALL WTEXT (' 2. EXAFS(theory)')
              CALL WTEXT (' 3. EXAFS(experiment)*k^n')
              CALL WTEXT (' 4. EXAFS(theory)*k^n')
              CALL WTEXT (' 5. Fourier Transform (experiment)')
              CALL WTEXT (' 6. Fourier Transform (theory)')
              CALL WTEXT (' 7. Sine Transform (experiment)')
              CALL WTEXT (' 8. Sine Transform (theory)')
              CALL WTEXT (' 9. Backscattering Magnitude')
              CALL WTEXT ('10. Backscattering Phase')
              CALL WTEXT ('11. Phase of Backscattering Factor')
              CALL WTEXT ('12. EXAFS (difference)')
           endif
        ENDIF
        L=1
        DO  I=2,4
30         continue
           if(.not.DLV_flag)then
              IF (LRIS+LMDSN.EQ.0) WRITE (OUTTERM,140) I
           endif
           CALL CREAD (KP1,KP2,KP3,KN(I),IC,V,*30,*100)
           IF (KN(I).EQ.0) GOTO 50
           L=L+1
        enddo
     ENDIF
50   IF (L.LT.2) GOTO 100
     NPTS1=NPT(IEXP)
     IF (KN(1).EQ.3) NPTS1=IPARNA
     NPP=NPTS1*L
     DO J=1,L
        K=KN(J)
        IF (J.NE.1) K=K+3
        KS=JY(K)
        IF (K.GT.3.AND.K.LE.7.OR.K.EQ.15) THEN
           KS=KS+(IEXP-1)*IPARNPOINTS
        ELSEIF (K.GT.7.AND.K.LE.11) THEN
           KS=KS+(IEXP-1)*IPARNA
        ENDIF
        IF ((K.LT.8.OR.K.GT.11).AND.K.NE.3) KS=KS+JS(IEXP)-1
        IF ((K.LT.8.OR.K.EQ.15).AND.K.NE.3) THEN
           !
           !	These come from common /XY/
           !
           DO I=1,NPTS1
              M=L*I-L+J
              XOUT(M)=XY(KS+I)
              IF (K.EQ.1)           XOUT(M)=XOUT(M)/EC
              IF (K.EQ.2)           XOUT(M)=XOUT(M)/RKC
              IF (K.EQ.6.OR.K.EQ.7) XOUT(M)=XOUT(M)*RK(I+JS(IEXP)-1,IEXP)**JQ/(RKC**JQ)
              IF (K.EQ.15)          XOUT(M)=XOUT(M)-XY(IPAR1+JS(IEXP)-1+I)
           enddo
        ELSE
           !
           !	These come from common /FT/
           !
           DO  I=1,NPTS1
              M=L*I-L+J
              IF (KS.EQ.0) THEN
                 XOUT(M)=FPIM(I,IEXP)
              ELSEIF (KS.EQ.IPARNPOINTS) THEN
                 XOUT(M)=FPIP(I,IEXP)
              ELSEIF (KS.EQ.IPARNPOINTS*2) THEN
                 XOUT(M)=BSCP(I,IEXP)
              ELSE
                 XOUT(M)=EFEE(KS+I)
              ENDIF
              IF (K.EQ.3)  XOUT(M)=XOUT(M)/DC
           enddo
        ENDIF
     enddo
     IGF5R=IGF(5)
     IGF(5)=1
     CALL CEXPER (IEXP,ISPEC)
     ITL(1)=PLOTTITLE
     CALL CTHEORY (IEXP,0,ISPEC)
     ITL(2)=PLOTTITLE
     IGF(5)=JQ+1
     CALL CEXPER (IEXP,ISPEC)
     ITL(3)=PLOTTITLE
     CALL CTHEORY (IEXP,0,ISPEC)
     ITL(4)=PLOTTITLE
     IGF(5)=IGF5R
     WRITE (OUTFILE,120) (I,ITL(KN(I)),I=2,L)
     IF (L.EQ.4) WRITE (OUTFILE,150) (XOUT(I),I=1,NPP)
     IF (L.EQ.3) WRITE (OUTFILE,160) (XOUT(I),I=1,NPP)
     IF (L.EQ.2) WRITE (OUTFILE,170) (XOUT(I),I=1,NPP)
     WRITE (OUTFILE,*)
     WRITE (OUTTERM,130) KN                    ! writes column codes on screen
     CALL EFILE ('Spectrum '//CHAR(48+IEXP))
     RETURN
  ELSEIF (IOPT.EQ.2) THEN
     WRITE (OUTFILE,180) LFS(1),JQ,FITINDEX,RFAC,CHISQU*1.E6,REXAFS,RDISTANCE
     IF (CHAROPT(1:1).EQ.'B') THEN
        CALL WRBROOK (.FALSE.)
     ELSEIF (CHAROPT(1:1).EQ.'R') THEN
        CALL WRBROOK (.TRUE.)
     ELSE
        CALL WRNL (OUTFILE)
     ENDIF
     LFS(IPARNSP+1)=PRDSN
     CALL EFILE ('Parameters')
     RETURN
  ELSEIF (IOPT.EQ.3) THEN
     DO JCLUS=1,MAXCLUS
        CALL GEOMCAL (OUTFILE,.FALSE.,JCLUS,*10)
     ENDDO
     CALL EFILE ('Atomic coordinates')
10     RETURN
  ELSEIF (IOPT.EQ.4) THEN
     CALL WRITEVAR
     CALL EFILE ('Variable list')
     RETURN
  ELSEIF (IOPT.EQ.5) THEN
     KEYWORD=' '
     CALL DISPLAY (OUTFILE)
     CALL EFILE ('Molecular geometry')
     RETURN
  ELSEIF (IOPT.EQ.6) THEN
     IEXP=1
     DO I=1,NPT(IEXP)
        WRITE (OUTFILE,'(9F8.3)') ENER(I+JS(IEXP)-1),CAP(I,1,1),(TMAT(I,LL,IT(1) ),LL=0,2)
     ENDDO
     CALL EFILE ('CAP + Scattering atom T-matrix for shell '//CHAR(IT(1)+48))
     RETURN
  ELSEIF (IOPT.EQ.7) THEN
     IF (INTOPT.EQ.0) INTOPT=1
     NPTS=NPOTS(INTOPT)
     IF (NPTS.LE.3) RETURN
     CALL CALCGRID (INTOPT)
     FZ=NINT(ABS(POT(1,INTOPT))*.5)
     WRITE (6,*) 'Atom number ',INTOPT
     IF (CHAROPT(1:1).EQ.'C') THEN
        WRITE (OUTTERM,*) 'rV(r) and r**2 rho(r) - r in bohr radii,','V in Hartrees, rho in electrons/bohr radius.'
     ELSE
        WRITE (OUTTERM,*) 'rV(r) - r in bohr radii, V in hartrees'
     ENDIF
     WRITE (OUTFILE,*) FZ,POT(NPTS,INTOPT)*.5/GRID(NPTS),RHO0S(INTOPT),IHCODE(INTOPT) !,' z,v0,4pi rho0,hole'
     WRITE (OUTFILE,*) NPTS,GRID(1),GRID(NPTS),' NPTS,R(1),R(',NPTS,')'
     IF (CHAROPT(1:1).NE.'C') THEN
        WRITE (OUTFILE,'(6F13.6)') (POT(I,INTOPT)*.5,I=1,NPTS)
        WRITE (OUTFILE,'(6F13.6)') (RHO(I,INTOPT),I=1,NPTS)
     ELSE
        WRITE (OUTFILE,'(3F13.6)') (XGD(I),POT(I,INTOPT)*.5, RHO(I,INTOPT),I=1,NPTS)
     ENDIF
     CALL EFILE ('Potential')
     RETURN
  ELSEIF (IOPT.EQ.8) THEN
     !
     !	Phaseshifts
     !
     IF (INTOPT.EQ.0.OR.INTOPT.GT.IPARNP) THEN
        IS=1
        IF=IPARNP
     ELSE
        IS=INTOPT
        IF=INTOPT
     ENDIF
     CALL PHPRINT (IS,IF)
     RETURN
  ELSEIF (IOPT.EQ.9) THEN
     CALL WRSPEC (OUTFILE)
     CALL EFILE ('Spectra')
     RETURN
  ELSEIF (IOPT.EQ.10) THEN
     IF (INTOPT.LT.1) INTOPT=1
     DO I=1,NPH(INTOPT)+16
        WRITE (OUTFILE,160) EN(I,INTOPT),EREF(I,INTOPT)
     ENDDO
     CALL EFILE ('Eref')
     RETURN
  ELSEIF (IOPT.EQ.11) THEN
     IF (INTOPT.LT.1) INTOPT=1
     DO IEXP=1,NSPEC
        DO I=JS(IEXP),JF(IEXP)
           WRITE (OUTFILE,145) ENER(I)/EC,RK(I,IEXP)/RKC,AG1(I,IEXP),AG2(I,IEXP),CG1(I,IEXP),CG2(I,IEXP),CG3(I,IEXP)
        ENDDO
     ENDDO
     CALL EFILE ('Background/Chi')
     RETURN
  ENDIF
100 CLOSE (OUTFILE)
110 RETURN

120 FORMAT (' Column ',I4,': ',A24)
130 FORMAT ('Column codes: ',4I2)
140 FORMAT ('Code for column ? ',I3)
145 FORMAT (1X,7E15.7)
150 FORMAT (1X,4E15.7)
160 FORMAT (1X,3E15.7)
170 FORMAT (1X,2E15.7)
180 FORMAT ( /&
       'Experiment in : ',A//'Fit index with K**',I1,' weight:',G14.4, &
       ' R-factor :',F8.4,' Chisqu :',F9.5,' R-exafs :',F8.4,' R-distance :',F8.4)
END SUBROUTINE PRINT
