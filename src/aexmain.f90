PROGRAM AEXMAIN
  !======================================================================C
  Use Definition
  USE Parameters
  Use Common_convia
  USE Common_B1
  USE Common_B2
  Use Common_FACTORIALS
  Use Common_PMTCH
  Use Common_UPU
  Use Common_COMPAR
  Use Common_SOUPER
  Use Common_CDT
  Use Common_ICF
  Use Common_IP
  Use Common_MATEL
  Use Common_P
  Use Common_PATHS
  Use Index_INDS
  Use Common_DLV
  Use Include_PA1
  Use Include_XY
  Use Include_XA
  !	include 'Common_PA1.inc'
  !	include 'Common_XY.inc'
  !	include 'Common_XA.inc'
  !
  !  integer(i4b) :: LUNIT(8)
  real(float)  :: ff
  !
  !       Open termfile if required - LUNIT(5), input, LUNIT(6), output.
  !
  WRITE (OUTTERM,'(4(A,I7/),2(A,I7/))')      &
       ' max points          ', IPARNPOINTS, &
       ' max angular momentum', IPARLMAX,    &
       ' max shells          ', IPARNS,      &
       ' max phases          ', IPARNP,      &
       ' max paths           ', IPARPATH,    &
       ' max path-numbers    ', IPARPATH1

  OUTDIFF=LOGFILE-OUTTERM
  !       
  !	Indicate no matrix elements present
  !       
  NME=0
  FILTER    = .FALSE.
  PATHOVER  = .FALSE.
  PATH1OVER = .FALSE.
  CALL READVAR
  !       
  !	Generate fake experiment - prevents problems if no expt read in
  !	and useful for program testing.
  !       
  NP=IPARNPOINTS
  NSIG=0
  IENER=3
  IF (IPARNPOINTS.LE.500) IENER=6
  IF (IPARNPOINTS.LE.100) IENER=20
  DO I=1,NP
     ENER(I)=(I*IENER)*EC
     DO IEXP=1,IPARNSP
        RK(I,IEXP)   = SQRT(2.*ENER(I)+.5)
        TRK(I,IEXP)  = SQRT(2.*ENER(I)+.5)
        XABS(I,IEXP) = SIN(6.*RK(I,IEXP))/RK(I,IEXP)*.01
     ENDDO
  ENDDO
  GTITLE=' '
  CALL UZERO (XDT,4*IPARNSP*IPARNPOINTS)
  CENTCORD(:,:) = 0.
  REME(:)       = 0.
  IEXGROUP(1)   = 1
  UNAME(:)      = ' '

  CALL SETPATH (.TRUE.)
  !       
  !	Generate fake phaseshifts - allows use of FT without phaseshifts
  !       
  PHS(:,:,:) = czero1
  EREF(:,:)  = czero1
  NPH(:)     = 0
  NPH(1)=IPARPHPOINTS
  DO I=1,IPARPHPOINTS
     DX=real(I-1, kind=dp)/real(IPARPHPOINTS, kind=dp)
     EN(I,1)=DX*63.
     PHS(I,1,1)=PI*(1.-DX)**2
  ENDDO
  PATHMAX(:,:) = 0.
  !	CALL IUZERO (LINK,IPARNS+1)
  !	CALL UZERO (RLINK,IPARNS+1)
  !       
  !	Open terminal and log files using machine dependent macro
  !     Open logfile - LUNIT(7)
  ILOG=LOGFILE
  OPEN(ILOG,FILE='exout98.lis',STATUS='UNKNOWN')
  !       
  !	Generate variable names dependent on IPARNS etc.
  !       
  CALL INITVAR
  !       
  !	Search for indices corresponding to variable names.
  !       
  CALL GETIND
  DO I=0,IPARNSP
     RMS(inds(INDEMIN0)+I)=IENER
     RMX(inds(INDEMAX0)+I)=NP*IENER
  ENDDO
  RMX(inds(INDNS))  = IPARNS
  RMX(inds(INDVECA))= IPARNS
  RMX(inds(INDVECB))= IPARNS
  !       
  !	DISPHEAD initialises displays header, initialises timers, etc.
  !       
  CALL DISPHEAD
  FF=1.
  DO I=1,70
     !       WRITE (7,*) I-1,FAC(I-1),FF,FAC(I-1)-FF
     FAC(I-1)=FF
     FF=FF*real(I,kind=float)
  ENDDO
  !       
  !	FLIM and UTAB check parameters etc.
  !       
  CALL FLIM (*40)
  CALL UTAB (0)
  !       
  !	C3J calculates tables of 3J and Clebsch-Gordan coefficients
  !       
  CALL C3J
  !       
  !	Enter and process program commands
  !       
  CALL COMMAND
  !       
  !	Tidy up at the end
  !       
  CALL EXITQ
  STOP
40 CALL WTEXT ('Error in initial parameters.')
  STOP
END PROGRAM AEXMAIN
