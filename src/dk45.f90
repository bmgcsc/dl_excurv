SUBROUTINE DK45 (I,JSYM,JCLUS,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  !
  !	Decide between vertical or horizontal mirror plane (Dnh) or those
  !	normal to or interleaved with axes (Oh).
  !
  IF (ABS(RADTH(I)-PID2).LT..001) THEN
     JSYM=5
     IF (DEBUG) WRITE (6,'(A,I3,A)') 'Shell',I,' is on a mirror plane'
     RETURN 1
  ENDIF
  IF (DEBUG) WRITE (6,'(A,I3,A)') 'Shell',I,' is on a diad'
  IF (NPGCLASS(JCLUS).EQ.7) THEN
     TP=PID2
     TF=AMOD(RADPHI(I),TP)
     IF (ABS(TF-TP).LT..001.OR.ABS(TF).LT..001) THEN
        JSYM=5
        RETURN 1
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE DK45
