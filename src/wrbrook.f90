SUBROUTINE WRBROOK (REPLACE)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Include_IPA
  Use Include_PA1

  LOGICAL ERR,FOUNDIT,REPLACE
  CHARACTER CARD*4,ATYPE(10000)*3,RES(10000)*4,K1*1,K2*1,K3*1,LINE*80,SPARE*60
  CHARACTER RESNAME*4,ITORAT*3,RESN*4,IPLANE*3,EL*3,ANAME*12,WSPACE*60
  DIMENSION XX(10000),YY(10000),ZZ(10000),AA(10000),BB(10000),JRESNUM(10000)
  IF (REPLACE) THEN
10   IF (LRIS+LMDSN.EQ.0) CALL WTEXT ('File to be modified ?')
     CALL AREAD (2,WSPACE,LD,*10,*60)
     CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','pdb',*60)
     KK=1
     DO I=1,10000
        CALL READLINE(LINE,CARD,IATNO,ATYPE(KK),RES(KK),JRESNUM(KK),XX(KK),YY(KK),ZZ(KK),AA(KK),BB(KK),*30,*60)
        DO J=0,NS
           JCLUS=IABS(ICLUS(J))
           IF (ICOMMON.AND.JCLUS.NE.1.AND.(LINK(J).LT.NS.AND.LINK(J).GT.0.OR.J.EQ.LINK(0))) cycle
           IF (KK.EQ.ISEQNUM(J)) THEN
              IF (NU(J).GT.0) THEN
                 IK=IRESNUM(NU(J))
              ELSEIF (J.EQ.IRAD(JCLUS)) THEN
                 IK=ICRESNUM(JCLUS)
              ELSE
                 IK=0
              ENDIF
              WRITE (OUTTERM,'(2(A,I5),A)') 'Replacing atom',I,' ( residue',IK,')'
              XX(KK)=UX(J)+CENTCORD(1,JCLUS)
              YY(KK)=UY(J)+CENTCORD(2,JCLUS)
              ZZ(KK)=UZ(J)+CENTCORD(3,JCLUS)
              GOTO 20
           ENDIF
        enddo
20      WRITE (OUTFILE,70) CARD,IATNO,ATYPE(KK),RES(KK),LINE(22:22),JRESNUM(KK),XX(KK),YY(KK),ZZ(KK),AA(KK),BB(KK),LINE(67:80)
        KK=KK+1
        cycle
30      WRITE (OUTFILE,'(A)') LINE
     enddo
     CLOSE (INFILE)
  ELSE
     KEYWORD=' '
     CALL SYMMETRY (.TRUE.,.TRUE.,*60)
     IF (NAT(1).LT.1) RETURN
     DO JCLUS=1,MAXCLUS
        NAT1=NAT(JCLUS)
        IF (NAT1.EQ.0) cycle
        NAT(JCLUS)=0
        DO I=0,NAT1
           J=ISHELL(I,JCLUS)
           IF (ICOMMON.AND.JCLUS.NE.1.AND.(LINK(J).LT.NS.AND.LINK(J).GT.0.OR.J.EQ.LINK(0))) cycle
           K=IT(J)
           KK=NU(J)
           IF (IRESNUM(KK).GT.0.AND.IRESNUM(KK).LT.10000) THEN
              KR=IRESNUM(KK)
           ELSE
              KR=KK
              IF (J.EQ.IRAD(JCLUS)) KR=ICRESNUM(JCLUS)
           ENDIF
           IF (ATLABEL(J).NE.' ') THEN
              EL=ATLABEL(J)
           ELSE
              EL=ELS(IATOM(K))
           ENDIF
           II=ISEQNUM(J)
           IF (II.EQ.0) II=J
           IF (KK.LE.0) THEN
              ANAME=' '
              IF (J.EQ.IRAD(JCLUS)) ANAME=CUNAME(JCLUS)
           ELSE
              ANAME=UNAME(KK)
           ENDIF
           XX1=AX(I,JCLUS)+CENTCORD(1,JCLUS)
           YY1=AY(I,JCLUS)+CENTCORD(2,JCLUS)
           ZZ1=AZ(I,JCLUS)+CENTCORD(3,JCLUS)
           WRITE (OUTFILE,70) 'ATOM',II,EL,ANAME,'A',KR,XX1,YY1,ZZ1,1.,0.
        enddo
     enddo
  ENDIF
60 RETURN
70 FORMAT (A4,2X,I5,2X,A3,1X,A4,A,I4,4X,3F8.3,2F6.2,A)
END SUBROUTINE WRBROOK
