SUBROUTINE RDBROOK (*,JCLUS)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_UPU
  Use Common_ICF
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  DIMENSION IRES(100),RESN(100),ITORAT(5,10),RESNAME(10),IPLANE(3,10)
  !-st  LOGICAL ERR,FOUNDIT,OFFSET
  LOGICAL FOUNDIT
  CHARACTER CARD*4,ATYPE(10000)*3,RES(10000)*4,K1*1,K2*1,K3*1,LINE*66,SPARE*60
  CHARACTER RESNAME*4,ITORAT*3,RESN*4,IPLANE*3
  DIMENSION XX(10000),YY(10000),ZZ(10000),AA(10000),BB(10000),JRESNUM(10000)
  DATA RESNAME/'HIS','TYR','ASP','CO3','GLY','GLU','CYS','MET','AAA','BBB'/
  DATA ITORAT &
       /'N','CA','CB','CG','ND1','N','CA','CB','CG','CD1','N','CA','CB','CG','OD1','N','CA','CB', &
       'CG','CD1','N','CA','CB','CG','CD1','N','CA','CB','CG','CD','N','CA','CB','SG','Q','N',    &
       'CA','CB','CG','SD','Q','Q','Q','Q','Q','Q','Q','Q','Q','Q'/
  data IPLANE &
       /'NE2','CG','ND1','OH','CD2','CE2','CG','OD1','OD2','C','O1','O2','O','C','CA','OE1','CD', &
       'OE2','SG','CB','CA','SD','CE','CG','Q','Q','Q','Q','Q','Q'/
  call eqv_convia
  IF (JCLUS.NE.1) THEN
     IOFF=NS+1
     MOFF=MG
  ELSE
     IOFF=0
     MOFF=0
  ENDIF
  DIST2=RMAXRAD**2
  NRES=0
  FOUNDIT=.FALSE.
10 CALL WTEXT ('Central atom number')
  CALL CREAD (K1,K2,K3,ICENTAT,IC,V,*10,*30)
  KK=1
  DO  I=1,10000
     CALL READLINE(LINE,CARD,IATNO,ATYPE(KK),RES(KK),JRESNUM(KK),XX(KK),YY(KK),ZZ(KK),AA(KK),BB(KK),*20,*30)
     IF (IATNO.EQ.ICENTAT) THEN
        XX1=XX(KK)
        YY1=YY(KK)
        ZZ1=ZZ(KK)
        FOUNDIT=.TRUE.
        CENTCORD(1,JCLUS)=XX1
        CENTCORD(2,JCLUS)=YY1
        CENTCORD(3,JCLUS)=ZZ1
        CUNAME(JCLUS)=RES(KK)
        ICRESNUM(JCLUS)=JRESNUM(KK)
        JCENTAT=KK
     ENDIF
     KK=KK+1
20   CONTINUE
  enddo
30 IF (.NOT.FOUNDIT.OR.KK.EQ.0) RETURN 1
  DO  I=1,KK
     XX(I)=XX(I)-XX1
     YY(I)=YY(I)-YY1
     ZZ(I)=ZZ(I)-ZZ1
     IF (XX(I)*XX(I)+YY(I)*YY(I)+ZZ(I)*ZZ(I).LE.DIST2) THEN
        IF (NRES.NE.0) THEN
           DO J=1,NRES
              IF (IRES(J).EQ.JRESNUM(I).AND.RESN(J).EQ.RES(I)) GOTO 40
           ENDDO
        ENDIF
        NRES=NRES+1
        IRES(NRES)=JRESNUM(I)
        RESN(NRES)=RES(I)
     ENDIF
40   CONTINUE
  enddo
  WRITE (OUTTERM,60) NRES,SQRT(DIST2),ICENTAT,RES(ICENTAT),XX1,YY1,ZZ1
60 FORMAT (I4,' residues within',F4.1,' angstroms of atom',I5,' (',A,')',3F6.2/)
  IF (NRES.NE.0) THEN
     DO J=1,NRES
        WRITE (6,*) J,IRES(J),' ',RESN(J)
     ENDDO
  ENDIF
  NS=IOFF
  MG=MOFF
  IOLDRES=0
  CLUS(NS)=-JCLUS
  UN(NS)=0.
  R(NS)=0.
  UX(NS)=0.
  UY(NS)=0.
  UZ(NS)=0.
  T(NS)=1.
  NS1=NS
  DO I=1,KK
     IF (I.EQ.JCENTAT) THEN
	ISEQNUM(NS1)=I
        cycle
     ENDIF
     DO J=1,NRES
	IF (IRES(J).EQ.JRESNUM(I).AND.RESN(J).EQ.RES(I)) GOTO 70
     ENDDO
     cycle
70   IF (NS.LT.IPARNS) THEN
        NS=NS+1
        UX(NS)=XX(I)
        UY(NS)=YY(I)
        UZ(NS)=ZZ(I)
        CLUS(NS)=JCLUS
        IF (JRESNUM(I).NE.IOLDRES) THEN
           IF (MG.LT.IPARNUNITS) THEN
	      MG=MG+1
	      UNAME(MG)=RES(I)
	      IRESNUM(MG)=JRESNUM(I)
           ENDIF
        ENDIF
        IOLDRES=JRESNUM(I)
        UN(NS)=MG
        DO J=1,8
           IF (RES(I).EQ.RESNAME(J)) THEN
              DO K=1,4
                 IF (ATYPE(I).EQ.ITORAT(K,J)) NTORA(K,MG)=NS
                 IF (ATYPE(I).EQ.ITORAT(K+1,J)) NTORB(K,MG)=NS
              ENDDO
              IF (ATYPE(I).EQ.IPLANE(1,J)) PIV(MG)=NS
              IF (ATYPE(I).EQ.IPLANE(2,J)) PLA(MG)=NS
              IF (ATYPE(I).EQ.IPLANE(3,J)) PLB(MG)=NS
           ENDIF
        ENDDO
        T(NS)=1
        RN(NS)=1
        ISEQNUM(NS)=I
        ATLABEL(NS)=ATYPE(I)
        DO II=2,1,-1
           DO K=IPARNP,2,-1
              IF (ATYPE(I)(1:II).EQ.ELS(IATOM(K))) THEN
                 T(NS)=K
                 GOTO 80
              ENDIF
           ENDDO
        ENDDO
     ENDIF
80   CONTINUE
  enddo
  RNS=NS
  DO I=1,IPARNPARAMS
     PA2(I)=PA1(I)*CONVIA(ICF(I))
     !	call all_IPA
     IPA(I)=PA1(I)
     !	call IPA_all
  ENDDO
  CALL UTAB (1)
  DO I=1,NS
     IF (RAD(I).LT.2.5) THEN
        A(I)=A(1)
     ELSE
        A(I)=A(1)*1.5
     ENDIF
     A2(I)=A(I)*DWC
  ENDDO
  RETURN
END SUBROUTINE RDBROOK
