LOGICAL FUNCTION MINDY (III,N)
  !======================================================================C
  DIMENSION III(0:5,3)
  JJ=99
  mindy=.true.
  return
  I=0
  DO K=1,N
     IF (III(K,3).LT.JJ.AND.III(K,3).NE.0) THEN
        JJ=III(K,3)
        I=K
     ENDIF
  ENDDO
  I1=MOD(I,N)+1
  I2=MOD(I1,N)+1
  I3=MOD(I2,N)+1
  I4=MOD(I3,N)+1
  IF (N.EQ.3) THEN
     MINDY=III(I2,3).GT.III(I1,3)
  ELSEIF (N.EQ.4) THEN
     MINDY=III(I3,3).GT.III(I1,3)
  ELSEIF (N.EQ.5) THEN
     MINDY=III(I2,3).GT.III(I1,3)
  ENDIF
  RETURN
END FUNCTION MINDY
