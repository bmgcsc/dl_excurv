MODULE Common_NUMF

  !      COMMON /NUMF/ NUMF(13)
  !      data NUMF/13*0/
  Use Definition
  implicit none
  private
  integer(I4B), dimension(13), public :: NUMF
  data  NUMF /13*0/
end MODULE Common_NUMF
