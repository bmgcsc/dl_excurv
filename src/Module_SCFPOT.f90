Module Module_SCFPOT
  !
  use Definition
  use Common_ELS
  implicit none
  !
contains
  !
  subroutine READSCFPOT
    integer, parameter :: IOTMP = 30
    integer :: NM
    integer :: NQ, NT, NS, NE, IREL, IBZINT, NKTAB
    integer :: ITRSCF, KMROT, BRAVAIS, BASIS

    character(50) :: filename

    character(10) :: dummy_c10
    character(10),allocatable, dimension (:) :: atype
    character*10 :: dummy_d
    character*8  :: dummy_t
    character :: separator
    character :: title1, title2, title3,title4
    character :: c_INFO, c_SCFVXC, c_SCFALG
    character :: ORBPOL

    integer :: IQ, IM, IT, I, J, npts_new, nsc, jrx
    real(dp), allocatable :: RWS(:),RMT(:), R1(:), dx(:)
    integer,  allocatable :: JRWS(:), JRMT(:), Z(:),NOM(:),IMT(:)
    real(dp), allocatable :: VT(:,:), BT(:,:), RHOCHR(:,:), RHOSPN(:,:)
    real(dp), allocatable :: scr(:,:)
    real(dp), allocatable :: dx_new(:)
    real(dp) :: rhorad, scrho(1000), scpot(1000), scr_new(1000)
    real(dp) :: r_cur(1000), rho_cur(1000), pot_cur(1000)
    real(dp) :: scfmix, SCFTOL,RMSAVV,RMSAVB
    logical :: SEMICORE, BREITINT
    logical :: EXTFIELD, BLCOUPL
    real(dp) :: BEXT, QMVEC(1:3), EFERMI, ALAT, BOA, COA
    real(dp) ::  BRX(3), BRY(3), BRZ(3)
    integer :: ca, i_ca, i_option
    character(1) :: central
    logical :: flag_c
    !
    !
    flag_c = .false.
    !
    CALL WTEXT ('Read file with SCF potential [1] or set central atom [2]?')
    read(*,'(i2)') i_option

    if (i_option==1)then
       CALL WTEXT ('Filename with SCF potential ?')
       read(*,140) filename
       open (IOTMP, file =trim(filename))

       !
       !
       !
       read (IOTMP,99012) title1
       read (IOTMP,99012) title1
       read (IOTMP,99012) title3
       read (IOTMP,99012) title4
       READ (IOTMP,99014) dummy_c10,NM
       READ (IOTMP,99014) dummy_c10,NQ
       READ (IOTMP,99014) dummy_c10,NT
       READ (IOTMP,99014) dummy_c10,NS
       READ (IOTMP,99014) dummy_c10,NE
       READ (IOTMP,99014) dummy_c10,IREL
       READ (IOTMP,99014) dummy_c10,IBZINT
       READ (IOTMP,99014) dummy_c10,NKTAB
       READ (IOTMP,99012) dummy_c10,c_INFO
       READ (IOTMP,99012) dummy_c10,c_SCFVXC
       READ (IOTMP,99012) dummy_c10,c_SCFALG
       READ (IOTMP,99014) dummy_c10,ITRSCF
       READ (IOTMP,99015) dummy_c10,SCFMIX
       READ (IOTMP,99017) dummy_c10,SCFTOL,RMSAVV,RMSAVB
       READ (IOTMP,99018) dummy_c10,SEMICORE
       READ (IOTMP,99018) dummy_c10,BREITINT
       READ (IOTMP,99012) dummy_c10,ORBPOL
       READ (IOTMP,99018) dummy_c10,EXTFIELD
       READ (IOTMP,99018) dummy_c10,BLCOUPL
       READ (IOTMP,99015) dummy_c10,BEXT
       READ (IOTMP,99014) dummy_c10,KMROT
       READ (IOTMP,99015) dummy_c10,(QMVEC(I),I=1,3)
       READ (IOTMP,99015) dummy_c10,EFERMI
       READ (IOTMP,99014) dummy_c10,BRAVAIS
       READ (IOTMP,99014) dummy_c10,BASIS
       READ (IOTMP,99015) dummy_c10,ALAT
       READ (IOTMP,99015) dummy_c10,BOA
       READ (IOTMP,99015) dummy_c10,COA
       DO I = 1,3
          READ (IOTMP,99015) dummy_c10, BRX(I),BRY(I),BRZ(I)
       ENDDO
       !
       allocate(atype(NM))
       allocate(RWS(NM),RMT(NM), R1(NM), dx(NM))
       allocate(JRWS(NM), JRMT(NM), Z(NM),NOM(NM),IMT(NM))
       allocate(VT(1000,NM), BT(1000,NM), RHOCHR(1000,NM), RHOSPN(1000,NM))
       allocate(scr(1000,NM))
       allocate(dx_new(NM))


       read(IOTMP,'(A)') separator
       read (IOTMP,99012) title1
       read (IOTMP,99012) title2


       DO IM = 1,NM
          read (IOTMP,99014) dummy_c10 ,NOM(IM)  ! MESH
          read (IOTMP,99015) dummy_c10, RWS(IM)  ! RWS
          read (IOTMP,99015) dummy_c10, RMT(IM)  ! RMT
          read (IOTMP,99014) dummy_c10, JRWS(IM) ! JRWS
          read (IOTMP,99014) dummy_c10, JRMT(IM) ! JRMT
          read (IOTMP,99015) dummy_c10, R1(IM)   ! R(1)
          read (IOTMP,99015) dummy_c10, DX(IM)   ! DX
       enddo
       read(IOTMP,'(A)') separator
       read (IOTMP,99014) dummy_c10, NQ
       DO IQ = 1,NQ
          read (IOTMP,*)
       END DO
       DO IQ = 1,NQ
          read (IOTMP,*) 
       END DO
       read(IOTMP,'(A)') separator
       read(IOTMP,'(A10)') dummy_c10
       DO IQ = 1,NQ
          read (IOTMP,*) 
       END DO
       read(IOTMP,'(A)') separator
       do IT = 1, NM
          read (IOTMP,*)
          read (IOTMP,'(A10)') atype(IT)
          read (IOTMP,99014) dummy_c10, Z(IT)
          read (IOTMP,*) 
          read (IOTMP,*) 
          read (IOTMP,99014) dummy_c10, IMT(IT)
          read (IOTMP,*) 
          read (IOTMP,*)
          read (IOTMP,99019) (VT(J,IT),J=1,JRWS(IT))
          read (IOTMP,99019) (BT(J,IT),J=1,JRWS(IT))
          read (IOTMP,'(A)') separator
       enddo
       !
       close(IOTMP)
       do it = 1, NM  
          print *, ELS(z(it))
       enddo
       print *, NM
       !
    elseif (i_option==2)then
       !
       !       CALL WTEXT ('Filename with SCF potential ?')
       !       read(*,140) filename
       open (IOTMP, file =trim(filename))

       read (IOTMP,99012) title1
       read (IOTMP,99012) title1
       read (IOTMP,99012) title3
       read (IOTMP,99012) title4
       READ (IOTMP,99014) dummy_c10,NM
       READ (IOTMP,99014) dummy_c10,NQ
       READ (IOTMP,99014) dummy_c10,NT
       READ (IOTMP,99014) dummy_c10,NS
       READ (IOTMP,99014) dummy_c10,NE
       READ (IOTMP,99014) dummy_c10,IREL
       READ (IOTMP,99014) dummy_c10,IBZINT
       READ (IOTMP,99014) dummy_c10,NKTAB
       READ (IOTMP,99012) dummy_c10,c_INFO
       READ (IOTMP,99012) dummy_c10,c_SCFVXC
       READ (IOTMP,99012) dummy_c10,c_SCFALG
       READ (IOTMP,99014) dummy_c10,ITRSCF
       READ (IOTMP,99015) dummy_c10,SCFMIX
       READ (IOTMP,99017) dummy_c10,SCFTOL,RMSAVV,RMSAVB
       READ (IOTMP,99018) dummy_c10,SEMICORE
       READ (IOTMP,99018) dummy_c10,BREITINT
       READ (IOTMP,99012) dummy_c10,ORBPOL
       READ (IOTMP,99018) dummy_c10,EXTFIELD
       READ (IOTMP,99018) dummy_c10,BLCOUPL
       READ (IOTMP,99015) dummy_c10,BEXT
       READ (IOTMP,99014) dummy_c10,KMROT
       READ (IOTMP,99015) dummy_c10,(QMVEC(I),I=1,3)
       READ (IOTMP,99015) dummy_c10,EFERMI
       READ (IOTMP,99014) dummy_c10,BRAVAIS
       READ (IOTMP,99014) dummy_c10,BASIS
       READ (IOTMP,99015) dummy_c10,ALAT
       READ (IOTMP,99015) dummy_c10,BOA
       READ (IOTMP,99015) dummy_c10,COA
       DO I = 1,3
          READ (IOTMP,99015) dummy_c10, BRX(I),BRY(I),BRZ(I)
       ENDDO





       allocate(atype(NM))
       allocate(RWS(NM),RMT(NM), R1(NM), dx(NM))
       allocate(JRWS(NM), JRMT(NM), Z(NM),NOM(NM),IMT(NM))
       allocate(VT(1000,NM), BT(1000,NM), RHOCHR(1000,NM), RHOSPN(1000,NM))
       allocate(scr(1000,NM))
       allocate(dx_new(NM))


       read(IOTMP,'(A)') separator
       read (IOTMP,99012) title1
       read (IOTMP,99012) title2


       DO IM = 1,NM
          read (IOTMP,99014) dummy_c10 ,NOM(IM)  ! MESH
          read (IOTMP,99015) dummy_c10, RWS(IM)  ! RWS
          read (IOTMP,99015) dummy_c10, RMT(IM)  ! RMT
          read (IOTMP,99014) dummy_c10, JRWS(IM) ! JRWS
          read (IOTMP,99014) dummy_c10, JRMT(IM) ! JRMT
          read (IOTMP,99015) dummy_c10, R1(IM)   ! R(1)
          read (IOTMP,99015) dummy_c10, DX(IM)   ! DX
       enddo
       read(IOTMP,'(A)') separator
       read (IOTMP,99014) dummy_c10, NQ
       DO IQ = 1,NQ
          read (IOTMP,*)
       END DO
       DO IQ = 1,NQ
          read (IOTMP,*) 
       END DO
       read(IOTMP,'(A)') separator
       read(IOTMP,'(A10)') dummy_c10
       DO IQ = 1,NQ
          read (IOTMP,*) 
       END DO
       read(IOTMP,'(A)') separator
       do IT = 1, NM
          read (IOTMP,*)
          read (IOTMP,'(A10)') atype(IT)
          read (IOTMP,99014) dummy_c10, Z(IT)
          read (IOTMP,*) 
          read (IOTMP,*) 
          read (IOTMP,99014) dummy_c10, IMT(IT)
          read (IOTMP,*) 
          read (IOTMP,*)
          read (IOTMP,99019) (VT(J,IT),J=1,JRWS(IT))
          read (IOTMP,99019) (BT(J,IT),J=1,JRWS(IT))
          read (IOTMP,'(A)') separator
       enddo
       read (IOTMP,*)
       DO IT = 1,NM
          IM = IMT(IT)
          read (IOTMP,'(A)') separator
          read (IOTMP,*)
          read (IOTMP,*)
          read (IOTMP,99019) (RHOCHR(I,IT),I=1,JRWS(IM))
          read (IOTMP,99019) (RHOSPN(I,IT),I=1,JRWS(IM))
       END DO
       !
       do IT = 1, NM
          do i = 1, JRWS(IT)
             scr(i,IT) = R1(IT)*exp(dx(IT)*(i-1))
             VT(i,it)  = scr(i,it)*VT(i,it)
          enddo
       enddo
       !
       ! interpolate to new grid
       !

       do it = 1, NM  
          print *, ' Z = ', z(it),'; Atom = ', ELS(z(it)), '; SCF pot. filename = ', 'p2dlex'//char(48 + it)//'.dat'
       enddo
       !
       CALL WTEXT ('Set central atom ?')
       read(*,'(I2)') i_ca
       !
       npts_new = 350
       do it = 1, NM
          dx_new(it)   = log(RMT(it)/R1(it))/(npts_new -1)
          nsc = npts_new + 1
          do i = 1, nsc  
             scr_new(i) = R1(it)*exp(dx_new(it)*(i-1))
          enddo
          jrx = JRMT(it) + 1
          ! interpolate charge density
          r_cur(:) = 0.d0
          rho_cur(:) = 0.d0
          pot_cur(:) = 0.d0
          r_cur(1:jrx)   = scr(1:jrx,it)
          rho_cur(1:jrx) = RHOCHR(1:jrx,it)
          do i = 1, jrx
             pot_cur(i) = VT(i,it)/scr(i,it)
          enddo
          !
          call CHGRID(rho_cur,r_cur,jrx,scrho,scr_new,nsc)
          rhorad = scrho(npts_new)
          !
          call CHGRID(pot_cur,r_cur,jrx,scpot,scr_new,nsc)
          !

          ca = 0
          if(it.eq.i_ca) ca = 1
          !       if(.not.flag_c)then
          !          print *, ' Z = ', z(it),'; Atom = ', ELS(z(it)), '; SCF pot. filename = ', 'p2dlex'//char(48 + it)//'.dat'
          !          CALL WTEXT ('Central atom (y/n) ?')
          !          read(*,141) central
          !          if(central.eq.'y'.or.central.eq.'Y')then
          !             ca = 1
          !             flag_c = .true.
          !          endif
          !       end if
          !

          open (700+it, file ='p2dlex'//char(48 + it)//'.dat')
          open (800+it, file ='pot'//char(48 + it)//'_kkr.dat')
          open (801+it, file ='pot'//char(48 + it)//'_new.dat')
          open (900+it, file ='rho'//char(48 + it)//'_kkr.dat')
          open (901+it, file ='rho'//char(48 + it)//'_new.dat')
          !-------------------------------------------------------------------------------
          write (800+it,'(2f14.8)') (scr(i,it), 0.5d0*VT(i,it)              ,i=1,JRWS(it))
          write (900+it,'(2f14.8)') (scr(i,it),scr(i,it)**2*RHOCHR(i,it)    ,i=1,JRWS(it))
          !-------------------------------------------------------------------------------
          write (801+it,'(2f14.8)') (scr_new(i),0.5d0*scpot(i)*scr_new(i)   ,i=1,npts_new)
          write (901+it,'(2f14.8)') (scr_new(i),scrho(i)*scr_new(i)**2      ,i=1,npts_new)
          !-------------------------------------------------------------------------------
          call getdat(dummy_d,dummy_t,700+it)
          write (700+it,'(3e14.5, I5)') real(z(it)) , 0.5d0*scpot(npts_new)*scr_new(npts_new)/RMT(it), rhorad, ca
          write (700+it,'(i5,2e14.5)') npts_new, R1(it) , RMT(it)
          write (700+it,'(6f13.6)') (0.5d0*scpot(i)*scr_new(i)   ,i=1,npts_new)
          write (700+it,'(6f13.6)') (scrho(i)*scr_new(i)**2      ,i=1,npts_new)
          !-------------------------------------------------------------------------------
          close(700+it)
          close(800+it)
          close(900+it)
       enddo

       close (IOTMP)
       deallocate(atype)
       deallocate(RWS,RMT, R1, dx)
       deallocate(JRWS, JRMT, Z,NOM,IMT)
       deallocate(VT, BT, RHOCHR, RHOSPN)
       deallocate(scr)
       deallocate(dx_new)
    endif

140 FORMAT(A)
141 format(A1)

99001 FORMAT (3F12.8)
99002 FORMAT (10I5)
99003 FORMAT (1p,4D20.13)
99004 FORMAT (f10.5,/,f10.5,2F15.10)
99005 FORMAT (i3,/,2D15.8,/,i2)
99006 FORMAT (i5,1p,d15.6)
99027 FORMAT (80('*'),/,'V-ORB',5X,A)
99007 FORMAT (80('*'),/,'RHO    SCF-charge density')
99008 FORMAT (80('*'),/,'TYPE      ',I10,/,A4)
99009 FORMAT (80('*'),/,'MESH INFORMATION     ')
99010 FORMAT (80('*'),/,'SITES     ',I10)
99011 FORMAT (80('*'),/,'MADELUNG MATRIX  SMAD')
99012 FORMAT (A10,A)
99013 FORMAT (A10,A,I5)
99014 FORMAT (A10,7I10,:,/,(10X,7I10))
99015 FORMAT (A10,5F20.10)
99016 FORMAT (A10,I5,10(2X,A2))
99017 FORMAT (A10,F20.10,'    RMSAVV',F20.10,'    RMSAVB',F20.10)
99018 FORMAT (A10,L3)
99019 FORMAT (1P,5E16.9)
99020 FORMAT (I4,' Q=(',2(F14.10,','),F14.10,')   NOQ=',I3,'  IT,CONC=', 20(I3,F6.3))
99021 FORMAT (I4,' QMTET = ',F15.10,' QMPHI = ',F15.10)
  end subroutine READSCFPOT
  !
  subroutine CHGRID(fx,x,nx,fy,y,ny)
    use Definition
    implicit none
    !*--CHGRID4
    !
    !*** Start of declarations rewritten by SPAG
    !
    ! Dummy arguments
    !
    integer :: nx , ny
    real(dp), dimension(nx) :: fx , x
    real(dp), dimension(ny) :: fy , y
    intent (IN) fx , nx , x , y
    intent (OUT) fy
    intent (INOUT) ny
    !
    ! Local variables
    !
    real(dp) :: a1 , a12 , a13 , a2 , a3 , yy
    integer  :: ix , iy
    !
    !*** End of declarations rewritten by SPAG
    !
    !  PIECEWISE QUADRATIC INTERPOLATION FROM GRID X TO GRID Y,  BY
    !  AITKEN'S DIVIDED DIFFERENCE SCHEME.   NX,NY ARE ARRAY DIMENSIONS?
    !  NOTE THAT NY IS RESET IN CHGRID
    iy = 1
    do ix = 3 , nx
       do while ( iy<=ny )
          yy = y(iy)
          if ( yy>x(ix) ) GO TO 100
          a1 = x(ix-2) - yy
          a2 = x(ix-1) - yy
          a3 = x(ix) - yy
          a12 = (fx(ix-2)*a2-fx(ix-1)*a1)/(x(ix-1)-x(ix-2))
          a13 = (fx(ix-2)*a3-fx(ix)*a1)/(x(ix)-x(ix-2))
          fy(iy) = (a12*a3-a13*a2)/(x(ix)-x(ix-1))
          if ( iy>ny ) exit
          iy = iy + 1
       end do
       exit
100 end do
    ny = iy - 1
  end subroutine CHGRID

SUBROUTINE READSPOT
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_POT
  Use Common_COMPAR
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  CHARACTER*60 WSPACE,TITLE,PLFS(IPARNP)
  CHARACTER*12 KP1,KP2,KP3
  CHARACTER*8 CTEMP
  integer :: ix
  !
  integer :: ic, int, kay, j
  real(dp) :: v, rz, vo, rho0, tv0, rst, rmt, xst, XLRRR
  !
10 IF (INTOPT.GT.0.AND.INTOPT.LE.IPARNP) GOTO 40
20 CALL WTEXT ('Enter Atom Number : ')
  CALL CREAD (KP1,KP2,KP3,INTOPT,IC,V,*20,*80)
  GOTO 10
  !
30 CALL ERRS(*40)
  !
  !	read potential file
  !
40 CALL WTEXT ('index for scf potential ?')
  read(*,'(I5)') ix
  !  CALL AREAD (1,WSPACE,LD,*40,*80)
  WSPACE = 'p2dlex'//char(48 + ix)//'.dat'
  CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','dat',*30)
  PLFS(INTOPT)=WSPACE
  WRITE (OUTTERM,*) 'Potential in : ',PLFS(INTOPT)
  READ (INFILE,'(a60)') TITLE
  READ (INFILE,*) RZ,VO,RHO0,IHCODE(INTOPT)
  V0EV(INTOPT)=VO/EC
  WRITE (CTEMP,'(F8.3)') V0EV(INTOPT)
  CALL WTEXT ('Enter V0 ['//CTEMP//']')
  CALL CREAD (KP1,KP2,KP3,INT,IC,TV0,*50,*80)
  IF (TV0.GT.0) TV0=-TV0
  V0EV(INTOPT)=TV0
  VO=V0EV(INTOPT)*EC
50 continue
  !-st  50 READ (INFILE,*) KAY,RST,RMT
  READ (INFILE,*) KAY,RST,REMRMTR2(INTOPT)
  RMT = REMRMTR2(INTOPT)
  XST=LOG(RST)
  XLRRR=LOG(RMT)
  DXLS(INTOPT)=(XLRRR-XST)/real(KAY-1,kind=dp)
  WRITE (CTEMP,'(F8.3)') RMT/DC
  CALL WTEXT ('Enter RMT ['//CTEMP//']')
  CALL CREAD (KP1,KP2,KP3,INT,IC,TV0,*60,*80)
  IF (TV0.LT.0) TV0=-TV0
  RMT=TV0*DC
60 READ (INFILE,*) (POT(J,INTOPT),J=1,KAY)
  READ (INFILE,*) (RHO(J,INTOPT),J=1,KAY)
  V0S(INTOPT)=2.*VO
  RHO0S(INTOPT)=RHO0
  DO  J=1,KAY
     POT(J,INTOPT)=2.0*POT(J,INTOPT)
  enddo
  RMTR2(INTOPT)=RMT
  RMTR(INTOPT)=RMT/DC
  IATOM(INTOPT)=NINT(RZ)
  ATOM(INTOPT)=IATOM(INTOPT)
  ATOM2(INTOPT)=IATOM(INTOPT)
  XSTARTS(INTOPT)=XST
  NPOTS(INTOPT)=KAY
  NENDS(1,INTOPT)=KAY
  CLOSE (INFILE)
80 RETURN
END SUBROUTINE READSPOT



end Module Module_SCFPOT















