SUBROUTINE RDCAR (*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_ICF
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  DIMENSION IRES(100),RESN(100),ITORAT(5,4),RESNAME(4),IPLANE(3,4)
  LOGICAL ERR,FOUNDIT
  CHARACTER CARD(10000)*4,ATYPE(10000)*3,RES(10000)*4,K2*1,K3*1,LINE*80,JRESNUM(0:10000)*2,CENTAT*4
  CHARACTER RESNAME*4,ITORAT*3,RESN*4,IPLANE*3
  DIMENSION XX(10000),YY(10000),ZZ(10000)
  DATA RESNAME &
       /'HIS','TYR','ASP','CO3'/,ITORAT/'N','CA','CB','CG','ND1','N','CA','CB','CG','CD1', &
       'N','CA','CB','CG','OD1','Q','Q','Q','Q','Q'/
  data IPLANE/'NE2','CG','ND1','OH','CD2','CE2','CG','OD1','OD2','C','O1','O2'/
  call eqv_convia
  DIST2=RMAXRAD**2
  DIST2=99.**2
  NRES=0
  JRESNUM(0)=' '
  FOUNDIT=.FALSE.
  JCLUS=1
10 CALL WTEXT ('Central atom label')
  CALL CREAD (CENTAT,K2,K3,IDUM,IC,V,*10,*30)
  KK=1
  DO  I=1,10000
     CALL READCAR(LINE,CARD(KK),IATNO,ATYPE(KK),RES(KK),JRESNUM(KK),XX(KK),YY(KK),ZZ(KK),*20,*30)
     IF (JRESNUM(KK).NE.JRESNUM(KK-1)) THEN
        NRES=NRES+1
        RESN(NRES)=RES(KK)
        IRES(NRES)=NRES
     ENDIF
     IF (CARD(KK).EQ.CENTAT) THEN
        XX1=XX(KK)
        YY1=YY(KK)
        ZZ1=ZZ(KK)
        FOUNDIT=.TRUE.
        CENTCORD(1,JCLUS)=XX1
        CENTCORD(2,JCLUS)=YY1
        CENTCORD(3,JCLUS)=ZZ1
     ENDIF
     KK=KK+1
20   CONTINUE
  enddo
30 IF (.NOT.FOUNDIT.OR.KK.LE.1) RETURN 1
  KK=KK-1
  DO  I=1,KK
     XX(I)=XX(I)-XX1
     YY(I)=YY(I)-YY1
     ZZ(I)=ZZ(I)-ZZ1
  enddo
  WRITE (OUTTERM,60) NRES,SQRT(DIST2),CENTAT,XX1,YY1,ZZ1
60 FORMAT (I4,' residues within',F4.1,' angstroms of atom ',A,3F6.2/)
  IF (NRES.NE.0) THEN
     DO J=1,NRES
        WRITE (6,*) J,IRES(J),' ',RESN(J)
     ENDDO
  ENDIF
  NS=0
  MG=0
  IOLDRES=0
  DO  I=1,KK
     IF (CARD(I).EQ.CENTAT) cycle
     !	DO J=1,NRES
     !	IF (IRES(J).EQ.JRESNUM(I).AND.RESN(J).EQ.RES(I)) GOTO 70
     !	ENDDO
     !	cycle
70   IF (NS.LT.IPARNS) THEN
        NS=NS+1
        UX(NS)=XX(I)
        UY(NS)=YY(I)
        UZ(NS)=ZZ(I)
        IF (RES(I).NE.'FRAG') THEN
           IF (JRESNUM(I).NE.JRESNUM(I-1)) THEN
	      IF (MG.LT.IPARNUNITS) THEN
                 MG=MG+1
                 UNAME(MG)=RES(I)
                 !	        IRESNUM(MG)=JRESNUM(I)
	      ENDIF
           ENDIF
           !	    IOLDRES=JRESNUM(I)
           UN(NS)=MG
           !	    DO J=1,4
           !	    IF (RES(I).EQ.RESNAME(J)) THEN
           !	    DO K=1,4
           !	    IF (ATYPE(I).EQ.ITORAT(K,J)) NTORA(K,MG)=NS
           !	    IF (ATYPE(I).EQ.ITORAT(K+1,J)) NTORB(K,MG)=NS
           !	    ENDDO
           !	    IF (ATYPE(I).EQ.IPLANE(1,J)) PIV(MG)=NS
           !	    IF (ATYPE(I).EQ.IPLANE(2,J)) PLA(MG)=NS
           !	    IF (ATYPE(I).EQ.IPLANE(3,J)) PLB(MG)=NS
        ENDIF
        !	  ENDDO
        T(NS)=IPARNP
        RN(NS)=1
        !	  CLUS(I)=JCLUS
        CLUS(I)=1
        LINK(I)=0
        RLINK(I)=0
        ISEQNUM(NS)=I
        ATLABEL(NS)=ATYPE(I)
        DO II=2,1,-1
           DO K=IPARNP,2,-1
              IF (ATYPE(I)(1:II).EQ.ELS(IATOM(K))) THEN
                 T(NS)=K
                 GOTO 80
              ENDIF
           ENDDO
        ENDDO
     ENDIF
80   CONTINUE
  enddo
  RNS=NS
  DO I=1,IPARNPARAMS
     PA2(I)=PA1(I)*CONVIA(ICF(I))
     !	call all_IPA
     IPA(I)=PA1(I)
     !	call IPA_all
  ENDDO
  CALL IUZERO (NAT,IPARNCLUS)
  NPGR(1)=1
  CALL UTAB (1)
  DO I=1,NS
     IF (RAD(I).LT.2.5) THEN
        A(I)=A(1)
     ELSE
        A(I)=A(1)*1.5
     ENDIF
     A2(I)=A(I)*DWC
  ENDDO
  RETURN
END SUBROUTINE RDCAR
