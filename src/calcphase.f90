SUBROUTINE CALCPHASE (AUTO,IS,IF,CENTRAL)
  !======================================================================C
  Use Common_DLV
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_TL
  Use Common_SETKW
  Use Common_F
  Use Common_ELS
  Use Common_POT
  Use Common_PMTX
  Use Common_GAMMA_EDGES
  Use Common_ICF
  Use Common_MATEL
  Use Common_P
  Use Index_INDS
  Use Common_PRDSN
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  !
  !	Generate phase shifts and optionally dipole matrix elements
  !
  !	DXL     = Log. grid interval.
  !	EDXL    = EXP (DXL)
  !	NEND(I) = Maximum effective radius for atom type I.
  !	NPTS    = Number of points in radial grid (should be odd).
  !	RHO(I)  = Charge density * 4 pi. * R**2.
  !
  !	Input units are angstroms and ev. output units are bohr radii
  !	and rydbergs, as are working units.
  !	For rho the log grid has log(estart) =-8.8 , log(int) =.05
  !
  !	LMAX is the maximum angular momentum for phaseshifts ( up to 26)
  !	Must be at least 3.
  !

  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***

  CHARACTER :: IP1*12,IP2*12,IP3*12,CHTEMP*8,WE*6

  DIMENSION :: CORE(1000),RLASTLIFE(IPARNCLUS)
  LOGICAL  :: AUTO,CENTRAL
  SAVE RLASTLIFE
  DATA RLASTLIFE/IPARNCLUS*-1./
  IWM=7
  NV=0
  V0=0
  RHO0=0
  MATRIX=0
  !
  !	IS is 0 or +ve, IF is +ve, >= IS
  !	If IS is 0, the routine is called only to calculate a core state with a real
  !	potential. Only 1 matrix element can be calculated at present, as CENTRAL is
  !	set to FALSE as soon as 1 has been calculated.
  !
  DO INT=MAX(IS,1),IF
     IF (IATOM(INT).GT.0.AND.NPOTS(INT).GT.0) THEN
        NV=NV+1
        V0=V0+V0S(INT)
        RHO0=RHO0+RHO0S(INT)
     ENDIF
  ENDDO
  IF (NV.EQ.0) RETURN
  V0=V0/real(NV,kind=dp)
  RHO0=RHO0/real(NV,kind=dp)
  !
  !	AUTO is true if phaseshift variables are being REFINED - no prompts
  !
  IF (IS.NE.IF.AND..NOT.AUTO) then
     if(.not.DLV_flag) CALL WTEXT ('Calculating phaseshifs for all atoms:')
  end IF
  CALL UZERO (RME,IPARPHPOINTS*2)
  DO  IINT=IS,IF
     INT=MAX(IINT,1)
     !
     !	At present, the matrix elements must be calculated for atom 1 only
     !
     INT1=1
     JEXP=1
     IF (IINT.GT.1) THEN
        DO II=2,IINT
           IF (LASTHOLE(II).NE.-99) THEN
              INT1=II
              JEXP=JEXP+1
           ENDIF
        ENDDO
     ENDIF
     IF (IS.NE.IF) THEN
        !
        !	If the scattering or central om type is undefined, or the number of potential points
        !	is zero, no phaseshifts can be calculated
        !
        IF (IATOM(INT1).LE.0.OR.IATOM(INT).LE.0.OR.NPOTS(IINT).EQ.0) &
             GOTO 100
     ELSE
        IF (IATOM(INT1).LE.0.OR.IATOM(INT).LE.0.OR.NPOTS(IINT).EQ.0) &
             CALL ERRMSG ('Potentials not defined',*110)
     ENDIF
     V0=V0S(IINT)
     RHO0=RHO0S(IINT)
     DO  L=1,(IPARLMAX+1)
   	CALL CZERO (PHS(1,INT,L),IPARPHPOINTS*2)
     enddo
     !     print *, phs
     !     pause
     CALL CZERO (EREF(1,INT),IPARPHPOINTS*2)
     NPTS=NPOTS(IINT)
     RMT=REMRMTR2(IINT)
     CALL CALCGRID (IINT)
     DO I=1,NPTS
	VMT(I)=POT(I,IINT)/GRID(I)
     ENDDO
     DO I=NPTS+1,IPARGRID
	VMT(I)=VMT(NPTS)
	POT(I,IINT)=VMT(NPTS)*GRID(I)
	RHO(I,IINT)=RHO0S(IINT)*G2(I)
     ENDDO
     IF (.NOT.CENTRAL) GOTO 40
     WE='no] ?'
     IF (AUTO) THEN
        IF (iset(IATABS).EQ.0) GOTO 40
        MATRIX=1
        GOTO 123
     ELSEIF(iset(IATABS).GT.0.OR.(iset(NFIN).EQ.0.AND.LINITIAL(1).NE.0))THEN
        WE='yes] ?'
        MATRIX=1
     ENDIF
     IF (LRIS+LMDSN.EQ.0) then
        if(.not.DLV_flag) CALL WTEXT ('Do You Require the Atomic Absorption ['//WE)
     end IF
     CALL CREAD (IP1,IP2,IP3,IVL,IC,V,*41,*110)
     IF (IP1(1:1).EQ.'Y') THEN
        MATRIX=1
     ELSE
        MATRIX=0
     ENDIF
41   IF (MATRIX.EQ.0) GOTO 40
     CALL OUTNAM (16,PRDSN,' ',*110)
     CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*110)
     !
     !	Calculate core wavefunctions for both final states
     !
123  CALL COREX (NINITIAL(1),LINITIAL(1),ATOM(INT1),CORE,IINT,NPTS)
     CALL WTEXT ('Core state calculated')
     IF (IINT.EQ.0) GOTO 100
     !
     !	Calculate EFERMI in Rydbergs from RHO0 (4*pi*interstitial
     !	charge density) - Lee+Beni equ 2.13, Phys.Rev.B15,2862,1977.
     !
40   EFERMI=V0+(.75*PI*RHO0)**(2./3.)
     FEEV(INT)=EFERMI/EC*.5
     !	IF (.NOT.BACKGROUND) THEN
     !	  WRITE (OUTTERM,150) RHO0S(INT),FEEV(INT),V0EV(INT),LMAX
     !	  WRITE (LOGFILE,150) RHO0S(INT),FEEV(INT),V0EV(INT),LMAX
     !	ENDIF
     JMAX=MIN(90,IPARPHPOINTS)
     IF (INT.EQ.MAX(IS,1).OR.LASTHOLE(INT).NE.-99) THEN
        IF (IEDGECODE(JEXP).EQ.1) THEN
           RLIFE=GAMMAK(IATOM(INT1))
        ELSEIF (IEDGECODE(JEXP).EQ.2) THEN
           RLIFE=GAMMAL1(IATOM(INT1))
        ELSEIF (IEDGECODE(JEXP).EQ.3.OR.IEDGECODE(JEXP).EQ.4) THEN
           RLIFE=GAMMAL3(IATOM(INT1))
        ELSEIF (IEDGECODE(JEXP).EQ.5) THEN
           RLIFE=GAMMAM1(IATOM(INT1))
        ELSEIF (IEDGECODE(JEXP).EQ.6.OR.IEDGECODE(JEXP).EQ.7) THEN
           RLIFE=GAMMAM3(IATOM(INT1))
        ELSEIF (IEDGECODE(JEXP).EQ.8.OR.IEDGECODE(JEXP).EQ.9) THEN
           RLIFE=GAMMAM5(IATOM(INT1))
        ENDIF
        !
        !	Users arent allowed to know about SPARE7
        !
        PARE7=-1.
        IF (ESUM0(0).NE.-1.) THEN
           PARE72=ESUM02(0)
           PARE7=ESUM0(0)
        ELSEIF (ESUM0(JEXP).NE.-1.) THEN
           PARE72=ESUM02(JEXP)
           PARE7=ESUM0(JEXP)
        ENDIF
        IF (PARE7.GT.-1.) RLIFE=PARE72*2.
        !
        !	The efective core width, RLIFE is in Rydbergs not Hartrees
        !
        IF (AUTO) THEN
           IF (RLASTLIFE(JEXP).NE.-1..AND.PARE7.LE.-1.) RLIFE=RLASTLIFE(JEXP)
           GOTO 50
        ENDIF
        WRITE (CHTEMP,'(F8.3)') RLIFE/EC*.5
        if(.not.DLV_flag) CALL WTEXT ('Enter core width (FWHM eV) ['//CHTEMP//']')
        CALL CREAD (IP1,IP2,IP3,INTDUM,IC,V,*50,*110)
        RLIFE=V*EC*2.
     ENDIF
50   RLIFEEV(INT)=RLIFE/EC*.5
     !	WRITE (6,*) 'Core width used for cluster',JEXP,'=',RLIFEEV(INT)
     IF (.NOT.BACKGROUND) THEN
        WRITE (OUTTERM,150) RHO0S(INT),FEEV(INT),V0EV(INT),LMAX,RLIFEEV(INT)
        !	  WRITE (LOGFILE,150) RHO0S(INT),FEEV(INT),V0EV(INT),LMAX,RLIFEEV(INT)
     ENDIF
     RLASTLIFE(JEXP)=RLIFE
     CALL PCALC (LMAX+1,INT,V0S(INT),ATOM(INT),DXLS(INT),GRID,G2,VMT,CORE,MATRIX, &
          NPTS,XSTARTS(INT),JMAX,EFERMI,RLIFE,RHO(1,INT),RMT,AUTO)
     IF (DEBUG) CALL WTEXT ('phaseshift calculated')
     NPH(INT)=JMAX
     CALL FIXPI (INT,.TRUE.)
     IF (MATRIX.NE.1) GOTO 80
     NME=JMAX
     DO IFINAL=1,2
	DO I=1,NME
           XME(I)=EN(I,INT)
           RME(I,IFINAL)=RME(I,IFINAL)*RME(I,IFINAL)
        enddo
     ENDDO
     IF (AUTO) GOTO 75
     K=IATOM(INT)
     K1=ISET(IREL)    + MINUS(10)
     K2=ISET(IEXCH)   + MINUS(15)
     K3=ISET(IGROUND) + MINUS(17)
     WRITE (OUTFILE,160) ELS(K),EDGECODE(JEXP)(1:NCSTR(EDGECODE(JEXP))),LINITIAL(1),IHCODE(INT), &
          ' Rel ',SETKWO(K1,10),' Ex ',SETKWO(K2,15),' Gr ',SETKWO(K3,17)
     WRITE (OUTFILE,120) K,ION(INT),RMTR(INT),ALF(INT),CMAG(INT),RLIFEEV(INT)
     WRITE (OUTFILE,*) '   ENERGY    ME-1      AT-1      K        ME+1      AT+1'
     DO I=1,NME
	RK2=2.*(XME(I)-REAL(EREF(I,INT)))
	IF (RK2.GT.0.) THEN
           RK2=SQRT(RK2)
	ELSE
           WRITE (7,*) 'RK2<0',XME(I)/EC,REAL(EREF(I,INT))/EC
           RK2=0.
	ENDIF
	WRITE (OUTFILE,170) XME(I),RME(I,1),2.*RK2*RME(I,1),RK2,RME(I,2),2.*RK2*RME(I,2)
     enddo
     DO IW=OUTTERM,LOGFILE,OUTDIFF
   	WRITE (IW,140) NINITIAL(1),LINITIAL(1),LINITIAL(1)-1,LINITIAL(1)+1
     enddo
     CALL EFILE ('Atomic absorption')
75   MATRIX=0
80   LFS(IPARNSP+1+INT)=ELS(IATOM(INT))
85   NPS=MAX(NPS,INT)
     CALL FLIM (*100)
100  CENTRAL=.FALSE.
  enddo
110 RETURN
120 FORMAT (' Z =',I4,' ION=',I3,' MTR=',F6.3,' ALPHA=',F6.3,' CMAG=',F8.3,' RLIFE=',F6.3)
  ! 130	FORMAT (' Central atom approximation : code ',I2)
140 FORMAT (' Matrix elements calculated for the transition'/' N(core) =',I2,' L(core) =',I2,' to final states L =',I2,' and',I2)
150 FORMAT ('Rho0: ',F7.4,' Efermi:',F7.3,' V0:',F8.3,' LMAX:',I3,' CW:',F8.3)
160 FORMAT (1X,A,A,' to l=',I1,'+/-1 HC:',I2,3(A,A3))
170 FORMAT (6F10.5)
END SUBROUTINE CALCPHASE
