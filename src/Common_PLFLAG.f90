MODULE Common_PLFLAG

!      COMMON /PLFLAG/ PLFLAG,PLCMD,IRC,NPROMPT
!      LOGICAL PLFLAG,PLCMD

  Use Definition
  Use Parameters
  implicit none
  private
  logical, public :: PLFLAG = .false.
  logical, public :: PLCMD  = .false.
  integer(i4b), public :: IRC = 1
  integer(i4b), public :: NPROMPT
end MODULE Common_PLFLAG
