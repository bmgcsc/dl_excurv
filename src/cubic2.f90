SUBROUTINE CUBIC2 (XK,WPSQ,QPLUS,QMINUS,*)
  !======================================================================C
  COMPLEX*8 :: S1
  !	COMMON/INTER1/WP,WPSQ
  PARAMETER (THIRD=1./3.)
  !       
  !	this subroutine finds the roots of the equation
  !	4xk*q^3+(alph-4xk^2)q^2+wp^2=0.
  !	see abramowitz and stegun for formulae.
  !       
  A2=(THIRD/XK-XK)*THIRD
  A0=WPSQ/(8.*XK)
  A2CUBED=A2**3
  R=-A0-A2CUBED
  RAD=A2CUBED*A2CUBED-R*R
  IF (RAD.LT.0.) RETURN 1
  S1=CMPLX(R,SQRT(RAD))**THIRD
  QZ1=2.0*REAL(S1)-A2
  QZ3=1.732050808*AIMAG(S1)-A2-REAL(S1)
  QPLUS=QZ1*QZ1
  QMINUS=QZ3*QZ3
  RETURN
END SUBROUTINE CUBIC2
