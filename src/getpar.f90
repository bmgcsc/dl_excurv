FUNCTION GETPAR (CPAR)
  !======================================================================C
  Use Parameters
  Use Common_VAR
  Use Include_PA1

  CHARACTER*(*) CPAR
  DO I=1,IPARNPARAMS
     IF (CPAR.EQ.IVARS(I)) GOTO 10
  ENDDO
10 GETPAR=PA1(I)
  RETURN
END FUNCTION GETPAR
