SUBROUTINE INTPOL (A,B,FA,FB,FMA,FMB,X,FX,FMX)
  !======================================================================C
  Use Definition
  IMPLICIT real(float) (A-H,O-Z)
  SAVE
  COMPLEX(imag2) FA,FB,FMA,FMB,FX,FMX
  DX=B-A
  D=(X-A)/DX
  !	IF (D*(1.0-D).LT.0.0) STOP 'Error in INTPOL'
  IF (D*(1.0-D).LT.0.0) d=1.
  C2=3.0*(FB-FA)-(FMB+2.0*FMA)*DX
  C3=2.0*(FA-FB)+(FMA+FMB)*DX
  FX=FA+D*(DX*FMA+D*(C2+D*C3))
  FMX=FMA+D*(2.0*C2+3.0*C3*D)/DX
  RETURN
END SUBROUTINE INTPOL
