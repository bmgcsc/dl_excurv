SUBROUTINE SETDIST
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Common_DISTANCE
  Use Include_IPA
  Use Include_PA1

  DO I=0,NS-1
     IF (N(I).EQ.0) cycle
     DO  J=I+1,NS
	IF (N(J).EQ.0) cycle
	ICLUSI=IABS(ICLUS(I))
	ICLUSJ=IABS(ICLUS(J))
	IF (ICLUSI.GT.0.AND.ICLUSJ.GT.0.AND.ICLUSI.NE.ICLUSJ) cycle
	DIST=(UX(I)-UX(J))**2+(UY(I)-UY(J))**2+(UZ(I)-UZ(J))**2
	IJ=IATOM(IT(I))
	IF (DIST.NE.0.) DIST=SQRT(DIST)
        !	WRITE (7,*) 'i,j,n(j),dist',I,J,N(J),DIST
	IF (DIST.LT.3.) THEN
           IZI=IATOM(IT(I))
           IZJ=IATOM(IT(J))
           IF (IZI.EQ.26) IZI=29
           IF (IZJ.EQ.26) IZJ=29
           DISTANCES(J,I)=2.777
           WEIGHTINGS(J,I)=1.
           IF (I.EQ.0) WEIGHTINGS(J,I)=2.
           IF (IZI.EQ.29.AND.IZJ.EQ.29) DISTANCES(J,I)=2.606
           IF (IZI.EQ.46.AND.IZJ.EQ.29) DISTANCES(J,I)=2.645
           IF (IZI.EQ.29.AND.IZJ.EQ.46) DISTANCES(J,I)=2.645
           IF (IZI.EQ.46.AND.IZJ.EQ.46) DISTANCES(J,I)=2.680
	ELSE
           DISTANCES(J,I)=0.
	ENDIF
     enddo
  enddo
  RETURN
END SUBROUTINE SETDIST
