MODULE Common_F

  !      COMMON /F/ LFS(0:IPARNP+4),LFSAV
  !      CHARACTER*60 LFS,LFSAV
  !	 DATA LFS/IPARNP*' ',IPARNSP*' ',2*' '/,

  USE Definition
  USE Parameters
  implicit none
  private
  character*60, dimension(0:IPARNP+IPARNSP+1), public :: LFS = ' '
  character*60, public :: LFSAV
end MODULE Common_F
