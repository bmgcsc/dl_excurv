SUBROUTINE GETOPT (OPTS,NOPT,STRING,IOP,*)
  !======================================================================C
  !
  !	Check an input option against a list of possible options. Only
  !	the number of characters in the input option string are compared
  !	and if they match an entry in the option list then that option
  !	is selected unless the same string matches more than one option
  !
  !	   OPTS (I)   CHARACTER ARRAY DIMENSIONED FOR NOPT CONTAINING
  !	              THE OPTIONS LIST. THE ELEMENT LENGTH IN THE
  !	              ARRAY CAN HAVE ANY VALID VALUE
  !	   NOPT (I)   THE NUMBER OF OPTIONS IN THE OPTIONS LIST
  !	 STRING (I)   A CHARACTER VARIABLE CONTAINING THE OPTION TO
  !	              BE LOOKED FOR IN THE OPTIONS LIST. ANY TRAILING
  !	              BLANKS IN THIS STRING WILL BE IGNORED.
  !	    IOP (O)   THE NUMBER IN THE LIST OF THE REQUESTED OPTION
  !	              (=0 IF NOT FOUND)
  !	    MSG (I)   FLAG =0, DO NOT OUTPUT ERROR MESSAGES
  !	                   >0, OUTPUT ERROR MESSAGES
  !	    ERR (O)   ERROR FLAG =.FALSE.  OPTION FOUND
  !	                         =.TRUE.   OPTION NOT FOUND OR AMBIGUOUS
  !	                                   OPTION
  !
  LOGICAL FOUND
  CHARACTER*(*) OPTS(NOPT),STRING
  CHARACTER*21 MSG1,MSG2
  CHARACTER*20 SORTED(50),T
  CHARACTER*10 BLANKS
  DATA BLANKS/' '/
  !
  !	print *, nopt, kind(nopt)
  !	pause
  IF (STRING(1:1).EQ.'?') THEN
     CALL WTEXT ('These commands are available - type HELP for more information:')
     CALL WTEXT (' ')
     !
     !	sort the options into alphabetical order
     !
     IF ( NOPT .LE. 50) THEN
        DO  I = 1, NOPT
           SORTED(I) = OPTS(I)
        enddo
        !
        DO  I=1,NOPT-1
           MIN=I
           DO  J=I+1,NOPT
              IF (LLT(SORTED(J),SORTED(MIN)) ) MIN=J
           enddo
           T=SORTED(MIN)
           SORTED(MIN)=SORTED(I)
           SORTED(I)=T
        enddo
        !
        DO I=1,NOPT,2
           MSG1=' '//SORTED(I)
           IF (I.EQ.NOPT) THEN
	      CALL WTEXT (MSG1)
           ELSE
	      MSG2=' '//SORTED(I+1)
	      CALL WTEXT (MSG1//BLANKS//MSG2)
           ENDIF
        ENDDO
        CALL WTEXT (' ')
        RETURN 1
     ELSE
        DO I=1,NOPT,2
           MSG1=' '//OPTS(I)
           IF (I.EQ.NOPT) THEN
	      CALL WTEXT (MSG1)
           ELSE
	      MSG2=' '//OPTS(I+1)
	      CALL WTEXT (MSG1//BLANKS//MSG2)
           ENDIF
        ENDDO
        CALL WTEXT (' ')
        RETURN 1
     ENDIF
  ENDIF
  FOUND=.FALSE.
  IOP=0
  !
  !	Search for matching option
  !
  LL=LEN(OPTS(1))
  MM=NCSTR(STRING)
  IF (MM.GT.LL)MM=LL
  DO  N=1,NOPT
     IF (STRING(1:MM).EQ.OPTS(N)(1:MM)) THEN
        IF (FOUND) THEN
           IOP=0
           MSG1=STRING
           CALL ERRMSG ('AMBIGUOUS CODE: '//MSG1,*33)
        ELSE
           IOP=N
           FOUND=.TRUE.
        ENDIF
     ENDIF
  enddo
  IF (FOUND)RETURN
  IOP=0
33 RETURN 1
END SUBROUTINE GETOPT
