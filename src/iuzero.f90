SUBROUTINE IUZERO (IARRAY,LEN)
  !======================================================================C
  Use Parameters
  Use Common_B1
  !
  !	Set the contents of a real array to zero
  !
  !	ARRAY (O)   Array to be zeroed
  !	LEN (I)     The number of words to be zeroed
  !
  DIMENSION IARRAY(*)
  IF (LEN.LE.0) RETURN
  !	IF (DEBUG) WRITE (7,'(A,I6)') 'Iuzero:',LEN
  DO  L=1,LEN
     IARRAY(L)=0
  enddo
  RETURN
END SUBROUTINE IUZERO
