SUBROUTINE TYPELAB (J1,I,J)
  !======================================================================C
  Use Parameters
  Use Common_ELS
  Use Common_PMTX
  CHARACTER LABEL*5,TLAB*3

  IF (J1.NE.0) THEN
     LABEL=ELS(J1)
     IF (ELS(J1)(2:2).EQ.' ') THEN
        NPLACES=1
     ELSE
        NPLACES=2
     ENDIF
  ELSE
     LABEL=' '
     NPLACES=0
  ENDIF
  K=I
  IF (IGF(10).EQ.2) K=J
  NIN=1
  IF (K.GT.9) NIN=2
  IF (K.GT.99) NIN=3
  WRITE (TLAB,'(I3)') K
  LABEL(NPLACES+1:)=TLAB(4-NIN:)
  CALL TYPECS (LABEL(1:NPLACES+NIN))
  RETURN
END SUBROUTINE TYPELAB
