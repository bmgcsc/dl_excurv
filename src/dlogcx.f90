FUNCTION DLOGCX (L,E,INCR,ZED,UPPP,INT)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_POT
  !
  !	DLOGCX finds wave-functions and log derivatives (=dlogcx) by
  !	outward integration of schrodinger equation.
  !
  DIMENSION Y(IPARGRID),DY(IPARGRID),YS(30)
  !
  !	XGD(I)-VALUES OF ARG. ON LOG GRID
  !	Y(I)-SOLUTION TO D2Y/DR2-(V-E+L(L+1)/R2)Y=0 AFTER SUB. X=LOG(R)
  !	DY(I)-DY/DX OR (1/R)DY/DR
  !	VMT(I)- POTENTIAL EVALUATED ON LOG GRID
  !
  H=DXLS(INT)
  RVZERO=-2.*ZED
  D=EXP(XSTARTS(INT))
  DRVO=RVZERO/D-VMT(1)
  DRVO=-DRVO
  LP2=L+2
  HSUB=D/real(LP2,kind=dp)
  B=HSUB*RVZERO
  C=HSUB*HSUB*(DRVO-E)
  !
  !	Find starting values
  !
  XL=L
  XL1=L+1.
  XL3=2.*L+3.
  XL4=3.*L+4.
  XL6=3.*L+6.
  DO I=1,LP2
     S=I
     YS(I)=S*S*C*(1.+S*XL4*B/(XL1*XL6))/XL3
     YS(I)=YS(I)+1.+S*B/XL1+S*S*B*B/(XL1*XL3)
     YS(I)=YS(I)+S*S*S*B*B*B/(XL1*XL3*XL6)
     SS=S/(XL+2.)
     SSS=1.
     DO II=1,L+1
   	SSS=SSS*SS
     enddo
     YS(I)=0.25*YS(I)*SSS
  enddo
  !
  XI=XSTARTS(INT)
  Y(1)=YS(LP2)
  DY(1)=(YS(LP2)-YS(LP2-1))*D/HSUB
  !
  !	Find next point of solution using Runge-Kutta
  !
  WI=Y(1)
  DWI=DY(1)
  D2Y=DWI+(EXP(2.*XI)*(VMT(1)-E)+XL*(XL+1.))*WI
  AK1=H*D2Y
  !
  XI=XI+H/2.
  WI=Y(1)+0.5*H*DY(1)+0.125*H*AK1
  DWI=DY(1)+0.5*AK1
  D2Y=DWI+(EXP(2.0*XI)*(UPPP-E)+XL*(XL+1.))*WI
  AK2=H*D2Y
  !
  DWI=DY(1)+0.5*AK2
  D2Y=DWI+(EXP(2.*XI)*(UPPP-E)+XL*(XL+1.))*WI
  AK3=H*D2Y
  !
  XI=XI+H/2.
  WI=Y(1)+H*DY(1)+0.5*H*AK3
  DWI=DY(1)+AK3
  D2Y=DWI+(EXP(2.*XI)*(VMT(2)-E)+XL*(XL+1.))*WI
  AK4=H*D2Y
  !
  Y(2)=H*(DY(1)+(AK1+AK2+AK3)/6.)+Y(1)
  DY(2)=(AK1+2.*AK2+2.*AK3+AK4)/6.+DY(1)
  !
  !	Solve differential equ. ( Fox and Goodwin method )
  !
  ALPH3=EXP(2.*XGD(2))*(VMT(2)-E)+XL*(XL+1.)
  ALPH2=EXP(2.*XGD(1))*(VMT(1)-E)+XL*(XL+1.)
  !
  INCRSB=INCR+5
  DO N=3,INCRSB
     ALPH1=ALPH2
     ALPH2=ALPH3
     ALPH3=EXP(2.*XGD(N))*(VMT(N)-E)+XL*(XL+1.)
     !
     BETA=1.-H/3.-H*H*ALPH3/9.
     YM2=Y(N-2)
     DYM2=DY(N-2)
     !
     Y(N)=((1.-H/3.+H*H*ALPH1/9.)*YM2+4.*H*(DY(N-1)+0.5*DYM2)/3.+4.*H*H*ALPH2*Y(N-1)/9.)/BETA
     !
     DY(N)=((1.+H/3.+H*H*ALPH3/9.)*DYM2+4.*H*(1.+H*ALPH3/3.)*DY(N-1)/3.+4.*H*ALPH2*Y(N-1)/3.+H*(ALPH3+ALPH1)*YM2/3.)/BETA
     !
     IF (Y(N).LT.1.0D+10) cycle
     DO KP=1,N
	Y(KP)=Y(KP)*1.0D-10
	DY(KP)=DY(KP)*1.0D-10
     enddo
     !
  enddo
  !
  DO N=1,INCRSB
     RNL(N)=EXP(-XGD(N))*Y(N)
     DRNL(N)=EXP(-2.*XGD(N))*(DY(N)-Y(N))
  enddo
  !
  DLOGCX=DRNL(INCR)/RNL(INCR)
  !
  RETURN
END FUNCTION DLOGCX
