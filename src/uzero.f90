SUBROUTINE UZERO (ARRAY,LEN)
  !======================================================================C
  !	Set the contents of a real array to zero
  !       
  !	ARRAY (O)   Array to be zeroed
  !	LEN (I)     The number of words to be zeroed
  !       
  DIMENSION ARRAY(*)
  IF (LEN.LE.0) RETURN
  DO  L=1,LEN
     ARRAY(L)=0
  enddo
  RETURN
END SUBROUTINE UZERO
