SUBROUTINE CALCGAM (CQ,GAMNU,GAMSQUIG,NUMAX,LMAXIN)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_FACTORIALS

  COMPLEX :: CQ(0:IPARLMAX,0:IPARLMAX)
  COMPLEX :: GAMNU(-3:3,0:2,0:IPARLMAX)
  COMPLEX :: GAMSQUIG(-3:3,0:2,0:IPARLMAX)
  real(float) :: FROG1S(-3:3,0:IPARLMAX),FROG2S(-3:3,0:IPARLMAX),TLP1(0:25)
  LOGICAL FIRST
  SAVE FROG1S,FROG2S,FIRST
  DATA TLP1/ &
       1.,3.,5.,7.,9.,11.,13.,15.,17.,19.,21.,23.,25.,27.,29.,31., &
       33.,35.,37.,39.,41.,43.,45.,47.,49.,51./,FIRST/.TRUE./
  IF (FIRST) THEN
     RM1=(-1)**2
     DO MU=-2,2
        DO L=0,IPARLMAX
           IF (IABS(MU).GT.L) GOTO 3
           FROG1S(MU,L)=SQRT(TLP1(L)*FAC(L-MU)/FAC(L+MU))
           FROG2S(MU,L)=TLP1(L)/FROG1S(MU,L)
           FROG1S(MU,L)=RM1*FROG1S(MU,L)
3          CONTINUE
        enddo
        RM1=-RM1
     enddo
     FIRST=.FALSE.
  ENDIF
  DO MU=-NUMAX,NUMAX
     DO NU=0,NUMAX
	DO L=0,LMAXIN
           GAMNU(MU,NU,L)=0
           GAMSQUIG(MU,NU,L)=0
           IF (IABS(MU).LE.L.AND.MU+NU.GE.0.AND.NU.LE.L.AND.NU.LE.L-MU) THEN
              GAMNU(MU,NU,L)=FROG1S(MU,L)*CQ(L,MU+NU)
              GAMSQUIG(MU,NU,L)=FROG2S(MU,L)*CQ(L,NU)
           ENDIF
        enddo
     enddo
  enddo
  RETURN
END SUBROUTINE CALCGAM
