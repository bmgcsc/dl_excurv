SUBROUTINE MULTLEG2 (MJ,MU,F3A,F3,FMAT)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ATMAT
  Use Include_IPA

  COMPLEX FMAT(12,12),F3(9,12),F3A(9,12)
  DO L1=1,MJ
     DO L2=1,MU
	F3A(L1,L2)=0.
        !	F3(L1,L2,IF3A)=0.
     ENDDO
     DO L3=1,MU
 	IF (F3(L1,L3).NE.0.) THEN
           ! 	IF (F3(L1,L3,IF3).NE.0.) THEN
           DO L2=1,MU
              F3A(L1,L2)=F3A(L1,L2)+F3(L1,L3)*FMAT(L3,L2)
              !	  F3(L1,L2,IF3A)=F3(L1,L2,IF3A)+F3(L1,L3,IF3)*FMAT(L3,L2,IFMAT)
           ENDDO
	ENDIF
     ENDDO
  ENDDO
  RETURN
END SUBROUTINE MULTLEG2
