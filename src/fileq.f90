SUBROUTINE FILEQ (K9,INT,*,*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_RCBUF
  Use Common_PLTDSN
  !
  !	Process 'file' command
  !
  !	Allow open,close,rewind the plot file of examine the status of
  !	a file on a given unit number (default for status = plot file)
  !
  !	     K9 (I)   CHARACTER INDICATING OPTION CHOSEN
  !	              = 'O' OPEN NEW PLOT FILE
  !	              = 'R' REWIND PLOT FILE
  !	              = 'C' CLOSE PLOT FILE
  !	              = 'S' FILE STATUS
  !	    INT (I)   UNIT NUMBER FOR STATUS REQUEST (IF 0 THEN
  !
  !	RETURN 1 invalid option
  !	RETURN 2 error in opening file
  !
  !	LOGICAL ERR
  CHARACTER*60 DSN
  CHARACTER*1 K9,OPT(4)
  external fstat
  DATA OPT/'O','R','C','S'/
  !
  !	Find option
  !
  DO  J=1,4
     IF (K9.EQ.OPT(J)) GOTO 20
  enddo
  RETURN 1
  !
  !	Process options
  !
20 GOTO (30,50,60,70),J
  !
  !	Open plot file
  !
30 CALL WTEXT ('Enter filename:')
  CALL SREAD (*30,*40)
  PLTDSN=BUF
  CALL FILEOPEN (PLTDSN,PLOTFILE,'UNFORMATTED',' ',*40)
  RETURN
40 CALL ERRMSG ('Plot file was not opened',*41)
41 RETURN 2
  !
  !	Rewind plot file
  !
50 REWIND(PLOTFILE)
  CALL WTEXT ('Plotfile rewound')
  RETURN
  !
  !	Close plot file
  !
60 CLOSE (PLOTFILE)
  CALL WTEXT ('Plotfile closed')
  RETURN
  !
  !	 File status
  !
70 IF (INT.GT.0) THEN
     CALL FSTAT(INT,IRET,DSN)
     IF (IRET.EQ.0)WRITE (OUTTERM,80)INT
     RETURN
  ELSE
     CALL FSTAT(PLOTFILE,IRET,DSN)
     IF (IRET.EQ.0)WRITE (OUTTERM,90)
  ENDIF
  RETURN
80 FORMAT(' File not opened on unit ',i3)
90 FORMAT(' Plot file not opened on unit 14')
END SUBROUTINE FILEQ
