SUBROUTINE CALCGRID (INT)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_POT
  real(float) EX1,EX2
  DO I=1,IPARGRID
     EX1=DXLS(INT)
     EX1=real(I-1,kind=float)*EX1+XSTARTS(INT)
     EX2=DEXP(EX1)
     XGD(I)=EX1
     GRID(I)=EX2
     G2(I)=EX2*EX2
  enddo
  RETURN
END SUBROUTINE CALCGRID
