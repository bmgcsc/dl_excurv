SUBROUTINE OCDIAD (DN,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - 2-fold rotation about x
  !
  Use Common_convia
  DIMENSION DN(36,*)
  DN(1,2)=PI-DN(1,2)
  DN(1,3)=PI2-DN(1,3)
  DN(1,5)=-DN(1,5)
  DN(1,6)=-DN(1,6)
  IS=IS+1
  RETURN 1
END SUBROUTINE OCDIAD
