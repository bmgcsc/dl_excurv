SUBROUTINE LPC (TH,LX,F)
  !======================================================================C
  !
  !	Calculate the Legendre polynomials
  !
  DIMENSION F(0:*)
  ZF=COS(TH)
  F(0)=1.0
  F(1)=ZF
  DO  J=2,LX
     F(J)=(2.0*J-1.0)*ZF*F(J-1)/J
     F(J)=F(J)-(J-1.0)*F(J-2)/J
  enddo
  RETURN
END SUBROUTINE LPC
