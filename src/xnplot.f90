SUBROUTINE XNPLOT (CNORM,*)
  !======================================================================
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use common_B2
  Use Common_TL
  Use Common_PLFLAG
  Use Common_PMTX
  Use Include_IPA
  Use Include_PA1

  DIMENSION XA(350,20),XB(350,20),XC(350),XD(350),DX(350),XSHIFTS(20),INP(20),IN1(20),IN2(20)
  CHARACTER XTITLE*255,DSN*60,STITLE18(20)*64,CDELETE*6,CREPLOT*6,CSMOOTH*6,CSHIFT*5
  character CRCONT*5,CEDGE*4,CMULT*4,CCONT*4,CQUIT*4,CADD*3,CNORM*(*),COLAB*2
  CHARACTER*4 IP1,IP2,IP3,TIT,AXTYPE,OXTYPE
  LOGICAL ADD
  DATA CADD/'ADD'/,CDELETE/'DELETE'/,CEDGE/'EDGE'/,CMULT/'MULT'/
  data CREPLOT/'REPLOT'/,CSHIFT/'SHIFT'/,CSMOOTH/'SMOOTH'/,CCONT/'CONT'/
  data CQUIT/'QUIT'/,CRCONT/'RCONT'/,TIT/' TIT'/
  CALL ERASE
  IF(IGF(2).NE.3)PLCMD=.TRUE.
  IGFOLD=IGF(2)
  IGF(2)=1
  OXTYPE='EV'
  ICOLR=12
  I=0
  DO IK=1,1000
     I=I+1
     IF (I.GT.20) GOTO 420
     WMIN=99.
     WMAX=-99.
     DMAX=-99.
     IPOS=600
     GOTO 20
10   CLOSE (INFILE)
20   CALL TPOS (45,IPOS)
     CALL WTEXT ('Filename? ')
     CALL TPOS (180,IPOS)
     CALL AREAD (8,DSN,LD,*20,*420)
     ADD=.FALSE.
     IF (DSN(1:1).EQ.'+') THEN
        IF (I.GT.1) ADD=.TRUE.
        DSN(1:)=DSN(2:)
     ENDIF
     CALL TPOS (0,760)
     CALL FILEOPEN (DSN,INFILE,'FORMATTED','dat',*20)
     IPOS=IPOS-25
30   CALL TPOS (5,IPOS)
     WRITE (COLAB,'(I2)') ICOLR
     CALL WTEXT ('Cols? ['//COLAB//']')
     CALL TPOS (160,IPOS)
     CALL CREAD (IP1,IP2,IP3,ICOL,IC,V,*40,*420)
     IF (IC.EQ.0) GOTO 30
     GOTO 50
40   ICOL=ICOLR
50   ICOL1=ICOL/10
     ICOL2=MOD(ICOL,10)
     ICOLR=ICOL
     INP(I)=350
     JP=1
     !	IF (LHEAD.NE.0) JP=-JP
     CALL TPOS (0,760)
     CALL RDSPEC (INFILE,INP(I),XTITLE,XA(1,I),ICOL1,XB(1,I),ICOL2,JP,*10)
     CLOSE (INFILE)
     IF (XTITLE(1:11).EQ.'EXCURVE >>>') XTITLE=XTITLE(81:)
     !	IF (XTITLE(1:12).EQ.' Matrix elem') XTITLE=XTITLE(22:)
     PLOTTITLE=DSN(1:NCSTR(DSN))//':'//XTITLE
     STITLE18(I)=PLOTTITLE
60   CALL TPOS (200,IPOS)
     CALL WTEXT ('Ev,Relative,Hartrees,K?['//OXTYPE(1:2)//']')
     !	CALL WTEXT ('Ev,Relative,Hartrees,K?[ev]')
     CALL TPOS(590,IPOS)
     CALL CREAD (AXTYPE,IP2,IP3,IDUM,IC,XSHIFTS(I),*70,*420)
     OXTYPE=AXTYPE
     GOTO 80
70   AXTYPE=OXTYPE
     IC=0
     XSHIFTS(I)=0.
80   ICR=IC
     IF (AXTYPE(1:1).EQ.'E') THEN
        IMODE=0
     ELSEIF (AXTYPE(1:1).EQ.'H') THEN
        IMODE=0
        DO  J=1,INP(I)
           XA(J,I)=XA(J,I)/EC
        enddo
     ELSEIF (AXTYPE(1:1).EQ.'A') THEN
        IMODE=0
        DO J=1,INP(I)
           XA(J,I)=XA(J,I)/EC*.5
        enddo
     ELSEIF (AXTYPE(1:1).EQ.'R') THEN
        IMODE=1
     ELSEIF (AXTYPE(1:1).EQ.'K') THEN
        IMODE=0
        DO  J=1,INP(I)
           TA=(XA(J,I)+XSHIFTS(I))*EC*2.
           TB=ABS(TA)
           IF (TB.NE.0.) TB=SQRT(TB)
           IF (TA.LT.0.) TB=-TB
           XA(J,I)=TB/RKC
        enddo
        XSHIFTS(I)=0.
     ELSE
        GOTO 60
     ENDIF
120  CALL TPOS (720,IPOS)
     CALL WTEXT ('Abs/Der?[abs]')
     CALL TPOS (920,IPOS)
     CALL CREAD (IP1,IP2,IP3,INTNUM,IC,WEIGHT,*130,*420)
     GOTO 140
130  IP1='ABS'
     IC=0
     WEIGHT=0.

140  IF (IP1(1:1).EQ.'D') THEN
        IDV=1
        PLOTYLAB='dA/dE'
     ELSEIF (IP1(1:1).EQ.'A') THEN
        IDV=0
        PLOTYLAB='absorbance'
     ELSE
        GOTO 120
     ENDIF
     IF (IC.NE.0) THEN
        DO J=1,INP(I)
           XB(J,I)=XB(J,I)*WEIGHT
        enddo
     ENDIF
     IF (ADD) THEN
        IF (INP(I).NE.INP(I-1)) THEN
           CALL WTEXT ('Spectra are incompatible.')
           GOTO 420
        ENDIF
        DO  J=1,INP(I)
           IF (ABS(XC(J)-XA(J,I)).GT..001) THEN
              CALL WTEXT ('Spectra are incompatible.')
              GOTO 420
           ENDIF
           XB(J,I-1)=XB(J,I)+XD(J)
        enddo
        I=I-1
     ENDIF
     !
     !	Save current spectra in XC,XD
     !
     CALL UCOPY (XA(1,I),XC,350)
     CALL UCOPY (XB(1,I),XD,350)
     !
     !     Calc Derivative and find maximum value.
     !
     DX(1)=0.
     DO  J=2,INP(I)
	DX(J)=0.
	IF (XA(J,I).NE.XA(J-1,I)) DX(J)=(XB(J,I)-XB(J-1,I))/(XA(J,I)-XA(J-1,I))
	IF (DX(J).LE.DMAX) cycle
	TX=(XA(J-1,I)+XA(J,I))*.5
	DMAX=DX(J)
     enddo
     !
     !	If derivative requested, replace XB with DX
     !
     IF (IDV.EQ.1) CALL UCOPY (DX,XB(1,I),350)
     !
     !     FIND LIMITS - USE FROM 30 EV BELOW TO XMAX ABOVE THE EDGE
     !
     IN1(I)=1
     IN2(I)=INP(I)
     IF (AXTYPE(1:1).EQ.'R') THEN
        DO  J=1,INP(I)
           XA(J,I)=XA(J,I)-TX
        enddo
     ENDIF
     DO  J=1,INP(I)
	IF (XA(J,I).LE.-30) IN1(I)=J
	IF (XA(J,I).GT.XMAX) GOTO 200
     enddo
     J=INP(I)+1
200  IN2(I)=J-1
     WRITE (LOGFILE,470) DMAX,TX
     IF (ICR.NE.0) THEN
        DO J=1,INP(I)
           XA(J,I)=XA(J,I)+XSHIFTS(I)
        enddo
     ENDIF
     !
     !     FIND MAX AND MIN VALUES OF ABSORBANCE
     !
     DO  J=IN1(I),IN2(I)
	IF (XB(J,I).GT.WMAX) THEN
           WMAX=XB(J,I)
           EM1=XA(J,I)
	ENDIF
	IF (XB(J,I).LT.WMIN) THEN
           WMIN=XB(J,I)
           EM2=XA(J,I)
	ENDIF
     enddo
     WRITE (LOGFILE,460) WMIN,EM1,WMAX,EM2
     IF (CNORM(1:1).NE.'A') THEN
        DO K=1,INP(I)
           XB(K,I)=(XB(K,I)-WMIN)/(WMAX-WMIN)
        enddo
     ENDIF
     IF (I.EQ.1) THEN
        IF (CNORM(1:1).EQ.'A') THEN
           RQ(1)=1.E30
           RQ(2)=-1.E30
           DO  K=1,I
              DO  J=IN1(K),IN2(K)
                 IF (XB(J,K).LT.RQ(1)) THEN
                    RQ(1)=XB(J,K)
                 ELSEIF (XB(J,K).GT.RQ(2)) THEN
                    RQ(2)=XB(J,K)
                 ENDIF
              enddo
           enddo
        ELSE
           RQ(1)=0.
           RQ(2)=1.
        ENDIF
        XTITLE=PLOTYLAB
        CALL GRINIT
        PLOTYLAB=XTITLE
        PLOTXLAB='eV'
        PLOTXSCALE=1.
        PLOTYSCALE=1.
        ICOD=1
     ENDIF
     IPOS=550
250  CALL ADDSHIFT (PLOTTITLE,XSHIFTS(I))
     CALL PLT (XA(1,I),XB(1,I),IN1(I),IN2(I),ICOD,*255)
255  ICOD=0
260  CALL TPOS (45,IPOS)
     CALL WTEXT('Enter Add, Delete, Edge, Multiply, Replot, SHift, SMooth,')
     CALL TPOS (45,IPOS-25)
     CALL WTEXT('Continue or Quit [Replot+Continue]')
     CALL TPOS (330,IPOS-25)
     XSHIFT=0.
     CALL CREAD (IP1,IP2,IP3,IDUM,IC,XSHIFT,*270,*420)
     GOTO 280
270  IP1='RC'
280  IPOS=IPOS-25
     LIP1=NCSTR(IP1)
     IF (IP1(1:1).EQ.CQUIT(1:LIP1)) GOTO 420
     IF (IP1(1:LIP1).EQ.CSHIFT(1:LIP1)) THEN
        XSHIFTS(I)=XSHIFTS(I)+XSHIFT
        DO J=1,INP(I)
           XA(J,I)=XA(J,I)+XSHIFT
        enddo
        GOTO 250
     ELSEIF (IP1(1:LIP1).EQ.CADD(1:LIP1)) THEN
        DO J=1,INP(I)
           XB(J,I)=XB(J,I)+XSHIFT
        enddo
        GOTO 250
     ELSEIF (IP1(1:LIP1).EQ.CDELETE(1:LIP1)) THEN
        IF (I.GT.0) I=I-1
        IF (I.LE.0) GOTO 410
     ELSEIF (IP1(1:LIP1).EQ.CEDGE(1:LIP1)) THEN
        CALL TPOS (45,IPOS-50)
        CALL WTEXT ('Effective Temperature ? [298]')
        CALL CREAD (IP1,IP2,IP3,IDUM,IC,ETEMP,*310,*420)
        GOTO 320
310     ETEMP=298.
320     IF (ETEMP.LE.0.) THEN
           DO J=1,INP(I)
              IF (XA(J,I).LT.XSHIFT) XB(J,I)=0.
           enddo
        ELSE
           DO J=1,INP(I)
              XB(J,I)=XB(J,I)*(.5+ATAN((XA(J,I)-XSHIFT)/(8.6745E-5*ETEMP))/PI)
           enddo
        ENDIF
        GOTO 250
     ELSEIF (IP1(1:LIP1).EQ.CMULT(1:LIP1)) THEN
        DO  J=1,INP(I)
           XB(J,I)=XB(J,I)*XSHIFT
        enddo
        GOTO 250
     ELSEIF (IP1(1:LIP1).EQ.CSMOOTH(1:LIP1)) THEN
        MS=2.*XSHIFT/(XA(2,I)-XA(1,I))+.5
        IF (MS.LT.2) MS=2
        MS1=MS-1
        CONST=2./MS/MS1
        DO  J=1,INP(I)
           SUM=0.
           DO K=1,MS1
              K1=MIN0(J+K,INP(I))
              K2=MAX0(J-K,1)
              SUM=SUM+(XB(K1,I)+XB(K2,I))*(MS-K)
           enddo
           XB(J,I)=(XB(J,I)+SUM*CONST)/3.
        enddo
        GOTO 250
     ELSEIF (IP1(1:LIP1).EQ.CCONT(1:LIP1)) THEN
        GOTO 410
     ELSEIF(IP1(1:LIP1).EQ.CREPLOT(1:LIP1).OR.IP1(1:LIP1).EQ.CRCONT(1:LIP1))THEN
380     IF (CNORM(1:1).EQ.'A') THEN
           RQ(1)=1.E30
           RQ(2)=-1.E30
           DO  K=1,I
              DO  J=IN1(K),IN2(K)
                 IF (XB(J,K).LT.RQ(1)) THEN
                    RQ(1)=XB(J,K)
                 ELSEIF (XB(J,K).GT.RQ(2)) THEN
                    RQ(2)=XB(J,K)
                 ENDIF
              enddo
           enddo
        ELSE
           RQ(1)=0.
           RQ(2)=1.
        ENDIF
        XTITLE=PLOTYLAB
        CALL GRINIT
        PLOTYLAB=XTITLE
        PLOTXLAB='eV'
        PLOTXSCALE=1.
        PLOTYSCALE=1.
        ICOD=1
        RQ(2)=RQ(2)*(1.+real(I,kind=dp)*.045)
        DO  J=1,I
           PLOTTITLE=STITLE18(J)
           CALL ADDSHIFT (PLOTTITLE,XSHIFTS(J))
           CALL PLT (XA(1,J),XB(1,J),IN1(J),IN2(J),ICOD,*450)
           ICOD=0
        enddo
        IPOS=550
        IF (IP1(2:2).NE.'C') GOTO 260
     ENDIF
410  CONTINUE
  enddo
420 IF (I.LE.1) THEN
     IGF(2)=IGFOLD
     RETURN
  ENDIF
  IF (IGFOLD.EQ.1) THEN
     RETURN
  ENDIF
  IGF(2)=IGFOLD
  I=I-1
  IF (CNORM(1:1).EQ.'A') THEN
     RQ(1)=1.E30
     RQ(2)=-1.E30
     DO  K=1,I
        DO  J=IN1(K),IN2(K)
           IF (XB(J,K).LT.RQ(1)) THEN
              RQ(1)=XB(J,K)
           ELSEIF (XB(J,K).GT.RQ(2)) THEN
              RQ(2)=XB(J,K)
           ENDIF
        enddo
     enddo
  ELSE
     RQ(1)=0.
     RQ(2)=1.
  ENDIF
  XTITLE=PLOTYLAB
  CALL GRINIT
  PLOTYLAB=XTITLE
  PLOTXLAB='eV'
  PLOTXSCALE=1.
  PLOTYSCALE=1.
  ICOD=1
  DO  J=1,I
     PLOTTITLE=STITLE18(J)
     CALL ADDSHIFT (PLOTTITLE,XSHIFTS(I))
     CALL PLT (XA(1,J),XB(1,J),IN1(J),IN2(J),ICOD,*450)
     ICOD=0
  enddo
  IGF(2)=IGFOLD
  RETURN
450 RETURN 1
  !
460 FORMAT (' Minimum absorption ',F9.4,' at ',F9.3/' Maximum absorption ',F9.4,' at ',F9.3)
470 FORMAT (' Derivative maximum ',F9.4,' at ',F9.3)
END SUBROUTINE XNPLOT
