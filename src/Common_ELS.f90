MODULE Common_ELS

  !      COMMON /ELS/ELS(-10:103),ITYPEA(10),NAMEA(10),QERCA(10),ITYPEB(10)
  !     *,NAMEB(10),QERCB(10),ITYPEC(10),NAMEC(10),QERCC(10),NSITES,JTYPE
  !     *(10,3)

  Use Definition
  Use Parameters
  private
  character*2, dimension(-10:103), public :: ELS
  data  ELS / &
       '  ','  ','  ','  ','  ','  ','  ','  ','  ','  ','  ', &
       'H' ,'HE','LI','BE','B' ,'C' ,'N' ,'O' ,'F' ,'NE','NA', &
       'MG','AL','SI','P' ,'S' ,'CL','AR','K' ,'CA','SC','TI', &
       'V' ,'CR','MN','FE','CO','NI','CU','ZN','GA','GE','AS', &
       'SE','BR','KR','RB','SR','Y', 'ZR','NB','MO','TC','RU', &
       'RH','PD','AG','CD','IN','SN','SB','TE','I' ,'XE','CS', &
       'BA','LA','CE','PR','ND','PM','SM','EU','GD','TB','DY', &
       'HO','ER','TM','YB','LU','HF','TA','W', 'RE','OS','IR', &
       'PT','AU','HG','TL','PB','BI','PO','AT','RN','FR','RA', &
       'AC','TH','PA','U', 'NP','PU','AM','CM','BK','CF','ES', &
       'FM','MD','NO','LR'/
integer(i4b), dimension(10), public :: ITYPEA,ITYPEB,ITYPEC
character*2,  dimension(10), public :: NAMEA, NAMEB, NAMEC
real(dp),     dimension(10), public :: QERCA, QERCB, QERCC      !!! never used in code ????
integer(i4b), public :: NSITES = 0
integer(i4b), dimension(10,3), public :: JTYPE
END MODULE Common_ELS
