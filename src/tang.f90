FUNCTION TANG (K)
  !======================================================================C
  Use Parameters
  !
  !	Returns ANGLE(K) within range -180 to 180
  !
  Use Include_PA1
  TANG=ANG(K)
  IF (TANG.GT.180.001) TANG=TANG-360.
  RETURN
END FUNCTION TANG
