CHARACTER*25 FUNCTION THTITLE (IN1,I2,I3)
  !======================================================================C
  !
  !	Get title corresponding to theory options
  !
  CHARACTER*25 KTITL(27)
  DATA KTITL/'CW SS Theory',      &
       'CW MS Theory',            &
       'CW AMS Theory',           &
       'SA SS Theory',            &
       'SA MS Theory',            &
       'SA AMS Theory',           &
       'Test SS Theory',          &
       'Test MS Theory',          &
       'Test AMS Theory',         &
       'Pol CW SS Theory',        &
       'Pol CW MS Theory',        &
       'Pol CW AMS Theory',       &
       'Pol SA SS Theory',        &
       'Pol SA MS Theory',        &
       'Pol SA AMS Theory',       &
       'Pol Test SS Theory',      &
       'Pol Test MS Theory',      &
       'Pol Test AMS Theory',     &
       'Exact pol CW SS Theory',  &
       'Exact pol CW MS Theory',  &
       'Exact pol CW AMS Theory', &
       'Exact pol SA SS Theory',  &
       'Exact pol SA MS Theory',  &
       'Exact pol SA AMS Theory', &
       'Exact pol Test SS Theory',&
       'Exact pol Test MS Theory',&
       'Exact pol Test AMS Theory'/
  I1=IN1
  IF (I3.EQ.0) I1=1
  KK=I2*9+(I1-1)*3+I3+1
  IF (KK.LT.1.OR.KK.GT.27) THEN
     THTITLE='GARBAGE'
  ELSE
     THTITLE=KTITL(KK)
  ENDIF
  RETURN
END FUNCTION THTITLE
