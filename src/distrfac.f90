SUBROUTINE DISTRFAC (LISTIN,RSIGMA,NDIST)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_UPU
  Use Common_DISTANCE
  Use Common_WFX
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_XY

  DIMENSION   :: RSIGMA(*)
  CHARACTER*4 :: LABEL1,LABEL2
  LOGICAL     :: LISTIN,LIST
  LIST=LISTIN
  IF (WD+WA.EQ.0.) LIST=.FALSE.
  !	NOBS=NPT(IEXP)*NSPEC
  NDIST=0
  NROBS=NOBS
  IF (LIST) THEN
     DO  IW=OUTTERM,LOGFILE,OUTDIFF
        WRITE (IW,70)
     enddo
  ENDIF
  WEIGHTSUM=0.
  BAWTSUM=0.
  DO I=1,NS
     DO J=0,I-1
	IF (WEIGHTINGS(I,J).NE.0..AND.DISTANCES(I,J).NE.0.) THEN
           WEIGHTSUM=WEIGHTSUM+WEIGHTINGS(I,J)
           NOBS=NOBS+1
           NDIST=NDIST+1
           WFX(NOBS)=0.
	ENDIF
	IF (BAWT(I,J).NE.0..AND.BONDANGS(I,J).NE.0.) THEN
           BAWTSUM=BAWTSUM+BAWT(I,J)
           NOBS=NOBS+1
           WFX(NOBS)=0.
	ENDIF
     enddo
  enddo
  NOBS=NROBS
  IF (WEIGHTSUM.GT.0.) THEN
     DO  I=1,NS
        DO  J=0,I-1
           IF (WEIGHTINGS(I,J).EQ.0..OR.DISTANCES(I,J).EQ.0.) cycle
           NOBS=NOBS+1
           DIST1=(UX(I)-UX(J))
           DIST2=(UY(I)-UY(J))
           DIST3=(UZ(I)-UZ(J))
           DIST=SQRT(DIST1*DIST1+DIST2*DIST2+DIST3*DIST3)
           DIFF=DISTANCES(I,J)-DIST
           WFX(NOBS)=DIFF/DISTANCES(I,J)
           RSIGMA(NOBS)=WEIGHTINGS(I,J)*1./WEIGHTSUM
           IF (LIST) THEN
              ILAB=IATOM(IT(I))
              LABEL1='('//ELS(ILAB)//')'
              ILAB=IATOM(IT(J))
              LABEL2='('//ELS(ILAB)//')'
              DO IW=OUTTERM,LOGFILE,OUTDIFF
                 WRITE (IW,80) I,LABEL1,J,LABEL2,DISTANCES(I,J),DIST,DIFF,WFX(NOBS)*100.,WEIGHTINGS(I,J),RSIGMA(NOBS)*WFX(NOBS)
              enddo
           ENDIF
        enddo
     enddo
  ENDIF
  IF (BAWTSUM.GT.0.) THEN
     DO  I=1,NS
        DO  J=0,I-1
           IF (BAWT(I,J).EQ.0..OR.BONDANGS(I,J).EQ.0.) cycle
           NOBS=NOBS+1
           DIST=ANG(I)
           WFX(NOBS)=ABS((BONDANGS(I,J)-DIST))
           RSIGMA(NOBS)=BAWT(I,J)*.1/BAWTSUM
           IF (LIST) THEN
              ILAB=IATOM(IT(I))
              LABEL1='('//ELS(ILAB)//')'
              ILAB=IATOM(IT(J))
              LABEL2='('//ELS(ILAB)//')'
              DO IW=OUTTERM,LOGFILE,OUTDIFF
                 WRITE (IW,80) I,LABEL1,J,LABEL2,BONDANGS(I,J),DIST,WFX(NOBS),WFX(NOBS),BAWT(I,J),RSIGMA(NOBS)*WFX(NOBS)
              enddo
           ENDIF
        enddo
     enddo
  ENDIF
  RETURN
70 FORMAT (' Atom(I)  Atom(J)  Ideal  Theory  Difference Error % Weighting Error*Weight*1')
80 FORMAT (2(2X,I2,A4),F8.3,F7.3,F11.4,F9.2,F8.3,F10.4)
END SUBROUTINE DISTRFAC
