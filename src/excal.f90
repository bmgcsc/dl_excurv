SUBROUTINE EXCAL (II)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_CL
  Use Common_VTIME
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_SOUPER
  Use Common_MATEL
  Use Common_ATMAT
  Use Common_YLM
  Use Common_RSC
  Use Include_IPA
  Use Include_PA2
  Use Include_XY
  Use Include_XA
  Use Include_FT
  !
  !	Calculate single scattering theory - II is 1 for the lower, or only,
  !	final state, 2 for the upper, if a lower also exists.
  !	Uses the theory of Gurman, Binsted and Ross, for the unpolarised
  !	case (ISING=0 (OFF)), the same theory + small-atom angle-factors
  !	for ISING=1 (ON), or the Lee and Pendry theory for exact polarisation
  !	(ISING=2 (EXACT)).
  !
  parameter(IPART=(IPARTLMAX+1)*(IPARTLMAX+1))

  DIMENSION ANGFAC(IPARNSP),SCWR(IPARNPOINTS),JSYMS(0:IPARNS)
  COMPLEX CI,FPI,CT1,CT2,CT3,ECM(IPARNPOINTS,IPARNP),CWIC,XXC,EE2C, &
       HHC(0:28),TZ,TZQ(IPARNSP),TTZ(3),Z(IPARNSP),V(0:IPARLMAX+3,-IPARLMAX-3:IPARLMAX+3),    &
       W(0:IPARLMAX+3,-IPARLMAX-3:IPARLMAX+3),TK(IPART,7),QK(7,IPART),ZS(-3:3,-3:3),HH(0:28), &
       DZ,TEF,H0
  DATA CI/(0.,1.)/
  IF (DEBUG) THEN
     WRITE (LOGFILE,'(A)') 'Calculating spherical wave theory with:'
     WRITE (LOGFILE,'(4(A,I3))') 'LMAX=',LMAX, ' IMSC=',iset(IMSC),' IEXTYP=',iset(IEXTYP),' ISING=',iset(ISING)
  ENDIF
  CALL GETTIM (1,CPU1,ELAP)
  IF (iset(IAVERAGE).EQ.1) CALL WTEXT ('Warning: domain averaging in operation')
  JS1=JJS+1
  JSM1=JJS-1
  IF (iset(ISING).EQ.2.AND.IOMIN.LE.1) CALL RSM (IEDGE,IEDGE,LMAX)
  !
  !	Loop through clusters of atoms
  !
  DO JCLUS=1,MAXCLUS
     IF (KEXP(JCLUS).NE.IEXP) GOTO 999
     !
     !	IFT is the shell used for calculating Back scattering factors
     !
     JFT=0
     DO JSHELL=1,NS
	IF (ICLUS(JSHELL).EQ.JCLUS) THEN
           JFT=JFT+1
           IF (JFT.EQ.MIN(IFTSET(1),NS)) IFT=JSHELL
	ENDIF
     ENDDO
     CALL GETECM (ECM,JCLUS)
     !
     !	Loop through atom shells
     !
     DO  JSHELL=1,NS
	IF (JSHELL.NE.1.AND.(RN2(JSHELL).EQ.0..OR.IOMIN.GT.1)) cycle
	ITYP=IT(JSHELL)
	ICAP=IABS(ICLUS(JSHELL))
	IF (ICAP.NE.JCLUS.OR.JSHELL.EQ.IRAD(ICAP)) cycle
	ITYP0=ITAD(ICAP)
        !	IF (JSHELL.EQ.1) WRITE (6,*) ITYP0,ITAD(JCLUS)
	IF (RAD(JSHELL).EQ.0.) THEN
           CALL WTEXT ('Scattering atom shell has zero radial coordinate')
           RETURN
	ENDIF
        !
        !	Calculate angle factor if in single crystal mode.
        !
	JSYMS(0)=1
	IF (iset(ISING).NE.0.AND.N(JSHELL).GT.0.AND.ICLUS(JSHELL).GT.0) THEN
           IF (NAT(JCLUS).EQ.0) CALL ERRMSGI ('Space group undefined for cluster ',JCLUS,*230)
           NPG=NPGR(JCLUS)
           DO JSYM1=1,6
              JSYM=JSYM1
              IF (NVALS(JSYM,NPG).NE.0) THEN
                 IF (N(JSHELL).EQ.NVALS(JSYM,NPG)) THEN
                    IF (JSYM.EQ.3.AND.NVALS(3,NPG).EQ.NVALS(6,NPG)) CALL DK56 (JSHELL,JSYM,*141)
                    IF (JSYM.EQ.4.AND.NVALS(4,NPG).EQ.NVALS(5,NPG)) CALL DK45 (JSHELL,JSYM,JCLUS,*141)
                    IF (JSYM.EQ.5.AND.NVALS(5,NPG).EQ.NVALS(6,NPG)) CALL DK56 (JSHELL,JSYM,*141)
                    GOTO 141
                 ENDIF
              ENDIF
           ENDDO
           CALL ERRMSGI ('Invalid Occupation Number:',N(JSHELL),*230)
141        JSYMS(JSHELL)=JSYM
           IF (DEBUG) WRITE (LOGFILE,*) JSHELL,' ',(OPS(K,JSYMS(JSHELL),NPG),K=1,NVALS(JSYMS(JSHELL),NPG))
	ELSE
           NPG=1
           JSYMS(JSHELL)=1
	ENDIF
	TTH=PI-TH2(JSHELL)
	TPHI=PHI2(JSHELL)+PI
	CALL POLANG (ANGFAC,(-1.)**IEDGE,JSHELL,JSYMS,NPG,RN2(IRAD(ICAP)),TH2(JSHELL),PHI2(JSHELL),TTH,TPHI)
	IF (DEBUG) WRITE (LOGFILE,*) 'SHELL',JSHELL,'ANGFAC',ANGFAC(1)
        !	WRITE (6,*) 'SHELL',JSHELL,'ANGFAC',ANGFAC(1)
        !
        !	Calculate complex wave vector
        !
	DO  IEP2=1,JPT
           IE=IEP2+JSM1
           DO I=IEXP,IEXP+JSPEC-1
              Z(I)=0.
           ENDDO
           !
           !	magic numbers: .7128-2,.925-3,1.2-4,1.3-5
           !
           EE2C=ECM(IEP2,ITYP)+ECM(IEP2,ITYP0)
           RADT=RAD(JSHELL)
           IF (iset(IUC).EQ.0.OR.A2(JSHELL).LE.0.) THEN
              IRAMAX=0
              DELTAR=0.
           ELSE
              IRAMAX=11
              DELTAR=SQRT(A2(JSHELL)*.5)*4.*1.3/real(IRAMAX,kind=dp)
           ENDIF
           DO IRA=-IRAMAX,IRAMAX
              IF (iset(IUC).NE.0.AND.A2(JSHELL).GT.0.) THEN
                 RRAD=IRA*DELTAR
                 RGAM=1.33333333*B2(JSHELL)/A2(JSHELL)**3
                 SQUIG=2./A2(JSHELL)**4*(C2(JSHELL)/3.-2.*B2(JSHELL)**2/A2(JSHELL))
                 DELTA=32.*5.*B2(JSHELL)**2/(72.*A2(JSHELL)**5)
                 RNORM=SQRT(A2(JSHELL)*PI)*(1.+.75*A2(JSHELL)**2*(SQUIG+DELTA))
                 TOP=1.+RGAM*RRAD**3+SQUIG*RRAD**4
                 RNRAD=DELTAR*TOP/RNORM*EXP(-RRAD*RRAD/A2(JSHELL))
                 XXC=1.
                 IF (DEBUG.AND.IEP2.EQ.1) WRITE (7,*) 'dr,gamma,eta,top',RRAD,RGAM,SQUIG,TOP
                 !
                 !	Here RRAD depends the IRAth contribution to the integral, + the
                 !	equlibribrium (minimum in the 1-dimensional anharmonic oscillator
                 !	model, and the 3D motion correction (the latter assuming isotropic
                 !	correlation).
                 !
                 IF (iset(JRCOR).NE.0) THEN
                    !
                    !	Phase term for uncorrelated motion normal to bond
                    !
                    RRAD=RADT+RRAD+.25*A2(JSHELL)/RADT
                 ELSE
                    RRAD=RADT+RRAD
                 ENDIF
                 IF (RRAD.LT..1) cycle
              ELSE
                 RNRAD=1.
                 XXC=2.*ECM(IEP2,ITYP)
                 CJ=2./3.*C2(JSHELL)
                 XXC=CEXP(-XXC*A2(JSHELL)+XXC**2*CJ)
                 IF (D2(JSHELL).NE.0.) THEN
                    VQ=ATAN(D2(JSHELL)*SCWR(IE)*2.)
                    XXC=XXC*COS(VQ)*CEXP(CI*COS(VQ))
                 ENDIF
                 IF (iset(JRCOR).EQ.0) THEN
                    RCOR=1.
                 ELSE
                    RCOR=.75
                 ENDIF
                 RRAD=RADT-RCOR*A2(JSHELL)/RADT-2./3.*B2(JSHELL)*REAL(EE2C)
              ENDIF
              IF (DEBUG.AND.IEP2.EQ.1) WRITE (7,*) 'iuc,ira',iset(IUC),IRA,RRAD,RNRAD,DELTAR,A2(JSHELL),B2(JSHELL)
              CWIC=CSQRT(EE2C)
              SCWR(IE)=REAL(CWIC)
              !
              !	Calculate first two Hankel functions explicitly
              !
              !	TEF=CI*CWIC*(RAD(JSHELL)-.5*A2(JSHELL)/RAD(JSHELL))
              !
              !	TEF is ikr
              !
              TEF=CI*CWIC*RRAD
              H0=EXP(TEF)
              !
              !	DZ is (-kr)^-1
              !
              DZ=-1./(CWIC*RRAD)
              HH(0)=CI*H0*DZ
              HH(1)=DZ*(H0-HH(0))
              !
              !	Find LMAX for the point - to avoid calculating unneccessary
              !	Hankel functions which may cause overflows at small kr
              !
              L1MX=LMAXVALS(IEP2,ITYP)
              LMX2=L1MX+IEDGE
              IF (DEBUG.AND.(IEP2.LE.3.OR.IEP2.EQ.JPT)) THEN
                 WRITE (LOGFILE,'(A,I4,2E12.3,2F8.3)') 'Wave vector=',IEP2,CWIC,EE2C
                 WRITE (LOGFILE,'(A,I4,6G10.3)') 'H1(KR)=',IEP2,(HH(J),J=0,1)
                 WRITE (LOGFILE,'(A,2I4)') 'L1MX,LMX2',L1MX,LMX2
              ENDIF
              CL=1.
              DO JL=2,LMX2
                 CL=CL+2.
                 HH(JL)=-HH(JL-1)*DZ*CL-HH(JL-2)
                 IF (ABS(REAL(HH(JL))).GT.1.E18) THEN
                    CALL CZERO (HH(JL),LMX2-JL+1)
                    GOTO 35
                 ENDIF
              enddo
35            DO I=0,LMX2
                 HHC(I)=HH(I)**2
              enddo
              IF (iset(ISING).NE.2) THEN
                 !
                 !	Fast curved wave
                 !
                 IF (IEDGE.EQ.0) THEN
                    TZ=0
                    DO IL=0,L1MX
                       TZ=TZ+(2*IL+1)*HHC(IL)*TMAT(IEP2,IL,ITYP)
                    ENDDO
                 ELSEIF (IEDGE.EQ.1) THEN
                    !
                    !	K-edge
                    !
                    !	if (iep2.eq.jpt.and.jshell.eq.18) then
                    !	write (6,*) l1mx+1,tmat(iep2,l1mx+1,ityp),hhc(l1mx+1+1)
                    !	write (6,*) l1mx,tmat(iep2,l1mx,ityp),hhc(l1mx+1)
                    !	write (6,*) l1mx-1,tmat(iep2,l1mx-1,ityp),hhc(l1mx-1+1)
                    !	endif
                    TZ=HHC(1)*TMAT(IEP2,0,ITYP)
                    IL=1
                    DO  JL=2,L1MX+1
                       TZ=TZ+(IL*HHC(IL-1)+JL*HHC(IL+1))*TMAT(IEP2,IL,ITYP)
                       IL=JL
                    enddo
                 ELSEIF (IEDGE.EQ.2) THEN
                    !
                    !	L2/3 Edge, add L=0 contribution
                    !
                    TZ=CCL2(0,3)*HHC(2)*TMAT(IEP2,0,ITYP)
                    !
                    !	Add L=1 contribution
                    !
                    TZ=TZ+(CCL2(1,2)*HHC(1)+CCL2(1,3)*HHC(3))*TMAT(IEP2,1,ITYP)
                    !
                    !	Now the L=2 to L=LMAX components
                    !
                    IL=2
                    DO  JL=3,L1MX+1
                       TZ=TZ+(CCL2(IL,1)*HHC(IL-2)+CCL2(IL,2)*HHC(IL)+CCL2(IL,3)*HHC(IL+2))*TMAT(IEP2,IL,ITYP)
                       IL=JL
                    enddo
                 ELSEIF (IEDGE.EQ.3) THEN
                    !
                    !	M4/5 Edge, add L=0 contribution
                    !
                    TZ=(CCL3(0,3)*HHC(1)+CCL3(0,4)*HHC(3))*TMAT(IEP2,0,ITYP)
                    TZ=TZ+(CCL3(1,2)*HHC(0)+CCL3(1,3)*HHC(2)+CCL3(1,4)*HHC(4))*TMAT(IEP2,1,ITYP)
                    TZ=TZ+(CCL3(2,2)*HHC(1)+CCL3(2,3)*HHC(3)+CCL3(2,3)*HHC(5)+CCL3(2,4)*HHC(5))*TMAT(IEP2,2,ITYP)
                    DO IL=3,L1MX
                       TZ=TZ+(CCL3(IL,1)*HHC(IL-3)+CCL3(IL,2)*HHC(IL-1)+ &
                            CCL3(IL,3)*HHC(IL+1)+CCL3(IL,4)*HHC(IL+3))*TMAT(IEP2,IL,ITYP)
                    ENDDO
                 ENDIF
                 TZ=TZ*XXC
                 DO JEXP=IEXP,IEXP+JSPEC-1
                    Z(JEXP)=Z(JEXP)+TZ*ANGFAC(JEXP)*RNRAD*RN2(JSHELL)
                 ENDDO
              ELSEIF (iset(ISING).EQ.2) THEN
                 !
                 !	Slow curved wave only, for exact polarisation
                 !
                 DO JEXP=IEXP,IEXP+JSPEC-1
                    TTZ(JEXP)=0.
                 ENDDO
                 IF (N(JSHELL).GT.0.AND.IOMIN.LE.1) THEN
                    DO NAT1=1,NAT(ICAP)
                       IF (ISHELL(NAT1,ICAP).EQ.JSHELL) GOTO 120
                    ENDDO
                    GOTO 150
120                 NAT1=NAT1-1
                    DO K=1,N(JSHELL)
                       NAT1=NAT1+1
                       !	    IF (SINGLES(NAT1,ICAP)) cycle
                       IF (DEBUG) WRITE (LOGFILE,'(A,I3,A,I3)') 'Shell:',JSHELL,' Atom No.:',NAT1
                       CT1=CMPLX(COS(ATH(NAT1,ICAP)),0.)
                       CT2=CMPLX(SIN(ATH(NAT1,ICAP)),0.)
                       CT3=CMPLX(COS(APHI(NAT1,ICAP)),SIN(APHI(NAT1,ICAP)))
                       !	if (iep2.eq.jpt) write (6,*) 'ct',jexp,ct1,ct2,ct3
                       CALL SHM3 (CT1,CT2,CT3,1)
                       IF (DEBUG.AND.IEP2.EQ.JPT) THEN
                          WRITE (7,*) 'COORDS:',ATH(NAT1,ICAP)/AC,APHI(NAT1,ICAP)/AC
                          WRITE (7,'(2F8.4)') YLM(1,1,-1)
                          WRITE (7,'(2F8.4)') YLM(1,1,0)
                          WRITE (7,'(2F8.4)') YLM(1,1,1)
                          DO I=-1,1
                             DO J=-1,1
                                ZS(I,J)=YLM(1,1,I)*YLM(1,1,J)
                             ENDDO
                             WRITE (7,*) (ZS(I,J),J=-1,1)
                          ENDDO
                       ENDIF
                       SL=1.
                       DO L3=0,LMAX+IEDGE
                          DO M3=-L3,L3
                             V(L3,M3)=HH(L3)*YLM(1,L3,-M3)
                             W(L3,-M3)=HH(L3)*YLM(1,L3,-M3)*SL
                          ENDDO
                          SL=-SL
                       ENDDO
                       IC=0
                       IC1=0
                       IC2=0
                       DO M1=-IEDGE,IEDGE
                          IC1=IC1+1
                          IC2=IC2+1
                          IC3=0
                          DO L2=0,LMAX
                             MODL=MOD(L2+IEDGE,2)
                             LSTART=IABS(L2-IEDGE)
                             IF (MOD(LSTART,2).NE.MODL) LSTART=LSTART+1
                             DO M2=-L2,L2
                                IC3=IC3+1
                                TK(IC3,IC2)=0.
                                QK(IC2,IC3)=0.
                                M3=M2-M1
                                DO L3=LSTART,L2+IEDGE,2
                                   IF (IABS(M3).LE.L3) THEN
                                      IC=IC+1
                                      TK(IC3,IC2)=TK(IC3,IC2)+V(L3,M3)*RSC(1,IC)
                                      QK(IC2,IC3)=QK(IC2,IC3)+W(L3,M3)*RSC(2,IC)
                                      if (iep2.eq.jpt+1) write (7,*) 'qk',ic2,ic3,qk(ic2,ic3)
                                   ENDIF
                                ENDDO
                                TK(IC3,IC2)=TMAT(IEP2,L2,ITYP)*TK(IC3,IC2)
                             ENDDO
                          ENDDO
                       ENDDO
                       CALL ZSUM (ZS,QK,TK,TZQ,IPART,LMAX,IEP2.NE.1)
                       DO JEXP=IEXP,IEXP+JSPEC-1
                          IF (DEBUG.AND.IEP2.EQ.JPT) CALL WPOL (JSHELL,ZS,TZQ(JEXP),7,JEXP)
                          TTZ(JEXP)=TTZ(JEXP)+TZQ(JEXP)
                       ENDDO
                    enddo
                 ENDIF
                 DO JEXP=IEXP,IEXP+JSPEC-1
                    IF (IEP2.EQ.JPT.AND.DEBUG) WRITE (6,*) JEXP,ANGFAC(JEXP)
                    Z(JEXP)=Z(JEXP)+TTZ(JEXP)*XXC*ANGFAC(JEXP)*RNRAD
                 ENDDO
              ENDIF
              !
              !	Both theories - calculate back scattering factor
              !	for FT, or general amusement.
              !
           enddo
           IF (JSHELL.EQ.IFT) THEN
              SL=-1
              FPI=0
              DO  IL=0,L1MX
                 SL=-SL
                 FPI=FPI+TMAT(IEP2,IL,ITYP)*SL*real(IL+IL+1,kind=dp)
              enddo
              FPI=FPI*XXC/(CI*CWIC)
              DO JEXP=IEXP,IEXP+JSPEC-1
                 FPIM(IE,JEXP)=CABS(FPI)
                 IF (REAL(FPI).NE.0.) THEN
                    BSCP(IE,JEXP)=ATAN(AIMAG(FPI)/REAL(FPI))
                    IF (REAL(FPI).LT.0.) BSCP(IE,JEXP)=BSCP(IE,JEXP)+PI
                 ELSE
                    BSCP(IE,JEXP)=PID2
                 ENDIF
                 FPIP(IE,JEXP)=BSCP(IE,JEXP)+FCAP(IE)
              ENDDO
           ENDIF
           IF (IOMIN.LT.2) THEN
              DO  JEXP=IEXP,IEXP+JSPEC-1
                 !	  XABT(IE,IEXP)=XABT(IE,IEXP)+Z(IEXP)*CAP(IEP2,ICAP,II)*RN2(IRAD(ICAP))*RATIO(IEP2,II)
                 XABT(IE,JEXP)=XABT(IE,JEXP)+Z(JEXP)*CAP(IEP2,ICAP,II)*RATIO(IEP2,II)
                 IF (IEP2.LE.3.AND.DEBUG) &
                      WRITE (7,'(A,I3,4E10.3,F7.2/)')'ICAP,CAP,ZR,RN2',ICAP,CAP(IEP2,ICAP,II),Z(JEXP),RN2(IRAD(ICAP))
              enddo
           ENDIF
           !
           !	End of energy points loop
           !
        enddo
        !
        !	End of shells loop
        !
150	CONTINUE
     enddo
     DO  JEXP=IEXP,IEXP+JSPEC-1
        !
        !	Add multiple scattering components if relevant
        !
	IF (iset(IMSC).NE.0.AND.IOMAX.GT.1) THEN
           DO  I=MAX(IOMIN,2),IOMAX
              DO  IEP=1,JPT
                 XABT(IEP+JSM1,JEXP)=XABT(IEP+JSM1,JEXP)+XDT(IEP,JEXP,I-1)
              enddo
              IF (DEBUG) THEN
                 DO IEP=1,3
                    WRITE (LOGFILE,*) 'SS,MS for JEXP=',JEXP,' order=',I,XABT(IEP+JSM1,JEXP),XDT(IEP,JEXP,I-1)
                 ENDDO
              ENDIF
           enddo
	ENDIF
	IF (IXA.EQ.3) THEN
           DO IE=JJS,JJF
              XABT(IE,JEXP)=XABT(IE,JEXP)+XA(IE)
           ENDDO
	ENDIF
	IF (iset(IATABS).EQ.1) THEN
           OPEN (87,FILE='atomabs',STATUS='UNKNOWN')
           !	  WRITE (6,*) 'II,JEXP=',II,JEXP,JJS,JJF
           DO IE=JJS,JJF
              IEP=IE-JJS+1
              BACKE=ENER(IE)+OFFSET2
              BACKE=BACKE*.01
              BACKG=PPOST2(0,8)
              BACKG1=PPOST2(JEXP,8)
              DO I=7,0,-1
                 BACKG=BACKG*BACKE+PPOST2(0,I)
                 BACKG1=BACKG1*BACKE+PPOST2(JEXP,I)
              ENDDO
              BACKG=BACKG*BACKG1
              TAT=2.*BACKG*SCWR(IE)*ATABS(IEP,II)
              IF (TEMPQ2.EQ.0.) THEN
                 IF (ENER(IE).LT.0.) TAT=0.
              ELSEIF (TEMPQ2.GT.0.) THEN
                 TAT=TAT*(.5+ATAN(ENER(IE)/(8.6745E-5*TEMPQ2))/PI)
              ENDIF
              TUT=PRE02(0)+PRE12(0)*BACKE+PRE22(0)*BACKE**2
              TUT1=PRE02(JEXP)+PRE12(JEXP)*BACKE+PRE22(JEXP)*BACKE**2
              TUT=TUT+TUT1
              AG1(IE,JEXP)=TAT-TUT
              AG2(IE,JEXP)=XABS(IE,JEXP)/(1.+XABT(IE,JEXP))
              IF (TAT.NE.0.) THEN
                 CG1(IE,JEXP)=(XABS(IE,JEXP)+TUT)/TAT-1.
              ELSE
                 CG1(IE,JEXP)=0.
              ENDIF
              CG2(IE,JEXP)=XABS(IE,JEXP)/AG2(IE,JEXP)-1.
              CG3(IE,JEXP)=XABT(IE,JEXP)
              WRITE (87,*) ENER(IE)/EC,AG1(IE,JEXP),AG2(IE,JEXP),CG1(IE,JEXP),CG2(IE,JEXP)
              !	  XABT(IE,JEXP)=TAT*(1+XABT(IE,JEXP))-BACKG
              XABT(IE,JEXP)=TAT*(1+XABT(IE,JEXP))-TUT
           ENDDO
           CLOSE (87)
	ENDIF
     enddo
     !
     !	Next cluster
999  CONTINUE
     !
  ENDDO
  CALL FIXPI1 (BSCP(1,IEXP))
  CALL FIXPI1 (FPIP(1,IEXP))
  CALL GETTIM (1,CPU2,ELAP)
  VTIME(2)=VTIME(2)+CPU2-CPU1
  IF (BACKGROUND) DEBUG=.FALSE.
  if (SNOISE2.LE.0.) RETURN
  DO JEXP=IEXP,IEXP+JSPEC-1
     DO I=JJS,JJF
	XABT(I,JEXP)=XABT(I,JEXP)+(RANDY(SEED)-.5)*SNOISE2*RK(I,IEXP)**(WNOISE2)
     ENDDO
  ENDDO
230 RETURN
END SUBROUTINE EXCAL
