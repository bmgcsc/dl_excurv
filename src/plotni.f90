SUBROUTINE PLOTNI (UXMIN,UYMIN,IIN,NIN)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_GRAPHICS
  CHARACTER USTRING*14
  IF (NIN.LE.0) RETURN
  WRITE (USTRING,'(I14)') IIN
  CALL FMOVE2 (UXMIN,UYMIN)
  CALL FDRAWSTR (USTRING(14-NIN+1:14))
  RETURN
  ENTRY PLOTNE (UXMIN,UYMIN,EIIN)
  WRITE (USTRING,'(E14.3)') EIIN
  CALL FMOVE2 (UXMIN,UYMIN)
  DO I=1,14
     IF (USTRING(I:I).NE.' ') GOTO 3
  ENDDO
  I=14
3 IFF=14-I+1
  CALL FDRAWSTR (USTRING(I:14))
  RETURN
  ENTRY MAP (UXMIN,UXMAX,UYMIN,UYMAX)
  IENT=0
  GOTO 10
  ENTRY MAPP (UXMIN,UXMAX,UYMIN,UYMAX)
  IENT=1
10 IF (DEBUG) WRITE (7,*) 'MAP:',IENT,UXMIN,UXMAX,UYMIN,UYMAX
  GXMIN=UXMIN
  GXMAX=UXMAX
  GYMIN=UYMIN
  GYMAX=UYMAX
  XDIFF=GXMAX-GXMIN
  YDIFF=GYMAX-GYMIN
  IF (IENT.EQ.1) THEN
     IF (QDQ) THEN
        CALL FQQORTHO2 (UXMIN,UXMAX,UYMIN,UYMAX)
        CALL FQQTEXTSIZE (CSIZE*XDIFF,CSIZE*YDIFF)
     ELSE
        CALL FORTHO2 (UXMIN,UXMAX,UYMIN,UYMAX)
        CALL FTEXTSIZE (CSIZE*XDIFF,CSIZE*YDIFF)
     ENDIF
  ELSE
     CALL FORTHO2 (UXMIN-XDIFF*.125,UXMAX+XDIFF*.05,UYMIN-YDIFF*.1,UYMAX+YDIFF*.01)
     CALL FTEXTSIZE (CSIZE*XDIFF,CSIZE*YDIFF)
  ENDIF
  RETURN
END SUBROUTINE PLOTNI
