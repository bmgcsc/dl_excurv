Module Common_WFX
  !      COMMON /WFX/WFX(IPARNPOINTS*3+IPARNS*(IPARNS+1)+IPARXRD+1)
  Use Definition
  Use Parameters
  implicit none
  real(dp), dimension(:), public :: WFX(IPARNPOINTS*3+IPARNS*(IPARNS+1)+IPARXRD+1)
end Module Common_WFX
