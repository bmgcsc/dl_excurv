SUBROUTINE ONCX (I,TPHI,TTHETA,JCLUS,ISETTING,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Index_INDS
  Use Common_SYMOPS
  !
  !	Set of TTHETA and TPHI for atom on diad // x.
  !	Theta = 90 unless cubic when xx0 etc.
  !
  !	Set default value of TTHETA and TPHI
  !
  TTHETA=PID2
  TPHI=0.
  RADTH2=RADTH(I)*2.
  RADPHI2=RADPHI(I)*2.
  IF (NPGCLASS(JCLUS).EQ.7) THEN
     TPHI=PID2*.5
     IF (ABS(COS(RADTH2)).LT.0.001.AND.ABS(SIN(RADPHI2)).LT..001)  GOTO 50
     IF (ABS(RADTH(I)-PID2).LT..001.AND.ABS(COS(RADPHI2)).LT..001) GOTO 50
  ENDIF
  IF (ABS(RADTH(I)-PID2).GT..001) THEN
     CALL WTEXT (IVARS(inds(INDTH)+I)//' invalid for special position on diad // x. Correct value 90')
     RETURN 1
  ENDIF
  IF (ISETTING.EQ.2) TPHI=PID2/real(IABS(JAXIS),kind=dp)
  !
  !	For Dn and Dnh groups there are two possible
  !	independent sets of diads,
  !
  KAXIS=IABS(JAXIS)
  !
  !	For Dnd and D[35]. there are two orientations so the interval is halved.
  !	For Dnd however JAXIS is -2n so no need to double KAXIS.
  !	If ISETTING has already been determined, only one such setting is valid.
  !
  KKAXIS=KAXIS
  IF (ISETTING.EQ.0) THEN
     IF (PGROUP(JCLUS)(2:2).EQ.'3'.OR.PGROUP(JCLUS)(2:2).EQ.'5') KAXIS=KAXIS*2
  ELSE
     IF (PGROUP(JCLUS)(3:3).EQ.'d') THEN
        KAXIS=KAXIS/2
        KKAXIS=KAXIS
     ENDIF
  ENDIF
  !
  !	Test for one of possible sets.
  !
  TP=PI/real(KAXIS,kind=dp)
  TF=AMOD(RADPHI(I)-TPHI,TP)
  WRITE (7,*) 'TTEST',TP,TF,RADPHI(I),KAXIS,ISETTING
  !
  !	Error if cubic
  !
  IF ((ABS(TF-TP).GT..001.AND.ABS(TF).GT..001).OR.NPGCLASS(JCLUS).EQ.7) THEN
     CALL WTEXT (IVARS(inds(INDPHI)+I)//' invalid for special position on diad //x. Correct value 0 etc')
     RETURN 1
  ENDIF
  !
  !	DNguish orientations pi/2n apart. Test is whether in pi/n set
  !
  ISETTING=1
  IF (PGROUP(JCLUS)(2:2).EQ.'3'.OR.PGROUP(JCLUS)(2:2).EQ.'5'.OR.PGROUP(JCLUS)(3:3).EQ.'d') THEN
     TP2=PI/real(KKAXIS,kind=dp)
     TF=AMOD(RADPHI(I),TP2)
     IF (ABS(TF-TP2).GT..001.AND.ABS(TF).GT..001) THEN
        ISETTING=2
        TPHI=TP2*.5
     ENDIF
  ENDIF
  TP2=2.*PI/real(KKAXIS,kind=dp)
  TF=AMOD(RADPHI(I),TP2)-TPHI
  !
  !	Allow wide margin for this test - DNguishes sets pi/n apart
  !
  IF (ABS(TF-TP2).GT..1.AND.ABS(TF).GT..1) TPHI=TPHI+PI/real(KKAXIS,kind=dp)
50 JCX=1
  JMIR=1
  RETURN
END SUBROUTINE ONCX
