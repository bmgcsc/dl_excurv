SUBROUTINE SORTSP
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PATHS

  IF (NUMPATHS.GE.2) THEN
     NUMP=MIN(NUMPATHS,IPARPATH)
     !
     !	If more than 1 shell is present, put them in order of ascending R
     !
     JSWOP=0
     DO  L=1,NUMP
        J=0
        DO  I=2,NUMP
           IF (PATHPL(I).LT.PATHPL(I-1)) THEN
              CALL SWOPSP (I-1,I)
              J=I
              JSWOP=JSWOP+1
           ENDIF
        enddo
        IF (J.EQ.0) GOTO 30
     enddo
30   IF (JSWOP.GT.0) THEN
        CALL WTEXT ('Shells Have Been Put in Order of Ascending R')
     ENDIF
  ENDIF
  RETURN
END SUBROUTINE SORTSP
