SUBROUTINE HPCNTR (A,M,N,BITS,CONT,NCONT,X,Y,IPTMAX,IDEV,ITHICK,IFLGS)
  !======================================================================
  Use definition
  Use Parameters
  Use Common_PMTX
  !
  !	Map contouring routine
  !
  !	A(M,N) is function to be contoured
  !	M = Number of rows of A
  !	N = Number of column
  !	BITS is logical array dimensioned at least 2*M*N
  !	CONT array of NCONT contour levels
  ! 	X,Y  arrays of dimension IPTMAX used to store contour L
  !
  !	IDIV = Number of line segments between contour level crossings in
  !	unit cell.  If >= 1, then routine finds idiv-1 coordinates between
  !	crossings by linear interpolation on the unit cell.
  !	Contour output via calls to subroutine cntrln (x,y,ipt)
  !	X,Y are coordinates with 0<=x<m and 0<=y<n.
  !
  LOGICAL BITS
  DIMENSION A(*),BITS(*),C(4),CONT(*),X(*),Y(*),IDIR(4),ISIDE(5),IVCT(4),JVCT(4),KVCT(4),LVCT(4),SIDE(4),NVCT(4),IFLGS(*)
  DATA IDIR/3,4,1,2/,ISIDE/1,2,3,4,1/,IVCT/0,-1,0,1/,JVCT/-1,0,1,0/,SIDE/0.,0.,1.0,1.0/,KVCT/4,1,2,3/,LVCT/2,3,4,1/
  !
  CLOLD=1.E28
  CSIZE=CSIZE/2.
  IF (IDEV.NE.3) CALL MAP (0.,28.,0.,28.)
  CSIZE=CSIZE*2.
  IDIV=1
  IDIV=4
  MN=M*N
  MN2=2*MN
  DIV=IDIV
  ICONT=1
10 CL=CONT(ICONT)
  ICONT=ICONT+1
  DO  I=1,MN2
     BITS(I)=.FALSE.
  enddo
  NVCT(1)=0
  NVCT(2)=MN
  NVCT(3)=M
  NVCT(4)=MN+1
  IPT=1
  MM=M-1
  NN=N-1
  !
  !	Search for contour crossing between adjacent column of array a(i,j)
  !
  I=0
  J=1
  ISUB=0
  JSUB=0
  IRTN=1
30 IF (J.GT.N) GOTO 90
40 IF (I.GE.MM) GOTO 80
  I=I+1
  ISUB=ISUB+1
  JSUB=JSUB+1
  IF (A(ISUB)-CL) 50,430,60
50 IF (A(ISUB+1)-CL) 40,40,70
60 IF (A(ISUB+1)-CL) 70,40,40
70 IF (BITS(JSUB+NVCT(1))) GOTO 40
  CXSTART=(CL-A(ISUB))/(A(ISUB+1)-A(ISUB))
  YSTART=0
  GOTO 170
80 I=0
  ISUB=ISUB+1
  JSUB=JSUB+1
  J=J+1
  GOTO 30
  !
  !	Search for contour crossing between adjacent rows of array a(i,j)
  !
90 I=0
  J=1
  JSUB=0
  ISUB=0
  IRTN=2
100 IF (J.GT.NN) GOTO 160
110 IF (I.GE.M) GOTO 150
  I=I+1
  ISUB=ISUB+1
  JSUB=JSUB+1
  IF (A(ISUB)-CL) 120,110,130
120 IF (A(ISUB+M)-CL) 110,110,140
130 IF (A(ISUB+M)-CL) 140,110,110
140 IF (BITS(JSUB+NVCT(2))) GOTO 110
  YSTART=(CL-A(ISUB))/(A(ISUB+M)-A(ISUB))
  CXSTART=0
  GOTO 170
150 I=0
  J=J+1
  GOTO 100
160 IF (ICONT.GT.NCONT) RETURN
  GOTO 10
  !
  !	Begin following contour line... save indicies for return to search
  !
170 ISAVE=I
  JSAVE=J
  ISUBSV=ISUB
  JSUBSV=JSUB
  XSAVE=CXSTART
  YSAVE=YSTART
  X(1)=CXSTART+real(I-1,kind=dp)
  Y(1)=YSTART +real(J-1,kind=dp)
  IENT=IRTN
  IRS=0
  GOTO 220
  !
  !	Dump line and follow contour line on opposite side of starting pio
  !	when used a second time this entry returns to search
  !
180 IRS=1
190 IF (IPT.GT.1) CALL CNTRLN (X,Y,IPT,ICONT-1,ITHICK,IFLGS(ICONT-1))
  IPT=1
  I=ISAVE
  J=JSAVE
  ISUB=ISUBSV
  JSUB=JSUBSV
  CXSTART=XSAVE
  YSTART=YSAVE
  X(1)=CXSTART+real(I-1,kind=dp)
  Y(1)=YSTART +real(J-1,kind=dp)
  IF (IRS.NE.0) GOTO (40,110),IRTN
  IEXIT=IRTN
  IRS=1
  GOTO 210
  !
  !	Return from following contour line through a cell
  !
200 IF (BITS(JSUB+NVCT(IEXIT))) GOTO 180
210 I=I+IVCT(IEXIT)
  J=J+JVCT(IEXIT)
  JSUB=I+(J-1)*M
  ISUB=JSUB
  IENT=IDIR(IEXIT)
220 BITS(JSUB+NVCT(IENT))=.TRUE.
  IF (I.LT.1.OR.I.GT.MM.OR.J.LT.1.OR.J.GT.NN) GOTO 190
  !
  !	Find contour crossing in new cell
  !
  IF (ISUB+1.GT.MN.OR.ISUB+M.GT.MN.OR.ISUB+1+M.GT.MN) GOTO 190
  C(1)=A(ISUB+1)
  C(2)=A(ISUB)
  C(3)=A(ISUB+M)
  C(4)=A(ISUB+1+M)
  JRTN=1
  ICNT=1
  JCNT=1
  DO IROUND=1,4
     IF (IROUND.EQ.IENT) GOTO 270
     I1=ISIDE(IROUND)
     I2=ISIDE(IROUND+1)
     IF (C(I1)-CL) 230,260,240
230  IF (C(I2)-CL) 270,270,250
240  IF (C(I2)-CL) 250,270,270
250  IEXIT=IROUND
     ICNT=ICNT+1
     GOTO 270
260  JEXIT=IROUND
     JCNT=JCNT+1
270  CONTINUE
  enddo
  GOTO (280,290,500,190),JCNT
280 GOTO (190,300,190,520),ICNT
290 GOTO (510,300,190,190),ICNT
300 GOTO (310,320,330,340),IENT
310 GOTO (190,360,390,360),IEXIT
320 GOTO (400,190,400,350),IEXIT
330 GOTO (390,360,190,360),IEXIT
340 GOTO (400,350,400,190),IEXIT
  !
  !	Follow contour line across a cell to a side
  !
350 CXSTART=SIDE(IENT)
360 XFIN=SIDE(IEXIT)
  XINC=(XFIN-CXSTART)/DIV
  XBASE=real(I-1,kind=dp)
  YBASE=real(J-1,kind=dp)
  A1=CL-C(2)
  A2=C(1)-C(2)
  A3=C(3)-C(2)
  A4=C(2)-C(1)+C(4)-C(3)
  DO  INTERP=1,IDIV
     CXSTART=CXSTART+XINC
     YSTART=(A1-A2*CXSTART)/(A3+A4*CXSTART)
     IF (IPT.LT.IPTMAX) GOTO 370
     CALL CNTRLN (X,Y,IPT,ICONT-1,ITHICK,IFLGS(ICONT-1))
     X(1)=X(IPT)
     Y(1)=Y(IPT)
     IPT=1
370  IPT=IPT+1
     X(IPT)=XBASE+CXSTART
     Y(IPT)=YBASE+YSTART
  enddo
  GOTO (200,190,450,480),JRTN
390 YSTART=SIDE(IENT)
  !
  !	Follow contour line across a cell to a top or bottom
  !
400 YFIN=SIDE(IEXIT)
  XBASE=real(I-1,kind=dp)
  YINC=(YFIN-YSTART)/DIV
  YBASE=real(J-1,kind=dp)
  A1=CL-C(2)
  A2=C(3)-C(2)
  A3=C(1)-C(2)
  A4=C(2)-C(1)+C(4)-C(3)
  DO  INTERP=1,IDIV
     YSTART=YSTART+YINC
     CXSTART=(A1-A2*YSTART)/(A3+A4*YSTART)
     IF (IPT.LT.IPTMAX) GOTO 410
     CALL CNTRLN (X,Y,IPT,ICONT-1,ITHICK,IFLGS(ICONT-1))
     X(1)=X(IPT)
     Y(1)=Y(IPT)
     IPT=1
410  IPT=IPT+1
     X(IPT)=XBASE+CXSTART
     Y(IPT)=YBASE+YSTART
  enddo
  GOTO (200,190,450,480),JRTN
  !
  !	Follow contour line from corner to corner
  !
430 K1=ISUB-M
  K2=ISUB+1-M
  K3=ISUB+1
  K4=ISUB+1+M
  K5=ISUB+M
  K6=ISUB-1+M
  K7=ISUB-1
  C1=A(K1)
  C2=A(K2)
  C3=A(K3)
  C4=A(K4)
  C5=A(K5)
  C6=A(K6)
  C7=A(K7)
  C8=A(ISUB)
  IF (ISUB.LT.1.OR.ISUB.GT.MN) GOTO 490
  X(1)=real(I-1,kind=dp)
  Y(1)=real(J-1,kind=dp)
  IF (J.EQ.1.OR.J.EQ.M) GOTO 440
  IF (K1.LT.1.OR.K1.GT.MN) GOTO 440
  IF (K2.LT.1.OR.K2.GT.MN) GOTO 440
  IF (K3.LT.1.OR.K3.GT.MN) GOTO 440
  IF (K4.LT.1.OR.K4.GT.MN) GOTO 440
  IF (K5.LT.1.OR.K5.GT.MN) GOTO 440
  IF (C3.NE.CL) GOTO 440
  IF (C1.EQ.CL.AND.C2.EQ.CL.AND.C4.EQ.CL.AND.C5.EQ.CL) GOTO 440
  X(2)=X(1)+1.
  Y(2)=Y(1)
  CALL CNTRLN (X,Y,2,ICONT-1,ITHICK,IFLGS(ICONT-1))
  GOTO 460
440 IF (J.EQ.1) GOTO 460
  IF (K1.LT.1.OR.K1.GT.MN) GOTO 460
  IF (K2.LT.1.OR.K2.GT.MN) GOTO 460
  IF (K3.LT.1.OR.K3.GT.MN) GOTO 460
  IF (C2.NE.CL) GOTO 460
  IF (C1.EQ.CL.OR.C3.EQ.CL) GOTO 460
  IF (C1.GT.CL.AND.C3.GT.CL.OR.C1.LT.CL.AND.C3.LT.CL) GOTO 460
  C(1)=C2
  C(2)=C1
  C(3)=C8
  C(4)=C3
  J=J-1
  JRTN=3
  IENT=3
  IEXIT=1
  GOTO 390
450 IF (IPT.GT.1) CALL CNTRLN (X,Y,IPT,ICONT-1,ITHICK,IFLGS(ICONT-1))
  IPT=1
  J=J+1
  X(1)=real(I-1,kind=dp)
  Y(1)=real(J-1,kind=dp)
460 IF (J.EQ.M.OR.I.EQ.1) GOTO 470
  IF (K3.LT.1.OR.K3.GT.MN) GOTO 470
  IF (K4.LT.1.OR.K4.GT.MN) GOTO 470
  IF (K5.LT.1.OR.K5.GT.MN) GOTO 470
  IF (K6.LT.1.OR.K6.GT.MN) GOTO 470
  IF (K7.LT.1.OR.K7.GT.MN) GOTO 470
  IF (C5.NE.CL) GOTO 470
  IF (C3.EQ.CL.AND.C4.EQ.CL.AND.C6.EQ.CL.AND.C7.EQ.CL) GOTO 470
  X(2)=X(1)
  Y(2)=Y(1)+1.
  CALL CNTRLN (X,Y,2,ICONT-1,ITHICK,IFLGS(ICONT-1))
  GOTO 490
470 IF (J.EQ.M) GOTO 490
  IF (K3.LT.1.OR.K3.GT.MN) GOTO 490
  IF (K4.LT.1.OR.K4.GT.MN) GOTO 490
  IF (K5.LT.1.OR.K5.GT.MN) GOTO 490
  IF (C4.NE.CL) GOTO 490
  IF (C3.EQ.CL.OR.C5.EQ.CL) GOTO 490
  IF (C3.GT.CL.AND.C5.GT.CL.OR.C3.LT.CL.AND.C5.LT.CL) GOTO 490
  C(1)=C3
  C(2)=C8
  C(3)=C5
  C(4)=C4
  JRTN=4
  IENT=1
  IEXIT=3
  GOTO 390
480 IF (IPT.GT.1) CALL CNTRLN (X,Y,IPT,ICONT-1,ITHICK,IFLGS(ICONT-1))
  IPT=1
  X(1)=real(I-1,kind=dp)
  Y(1)=real(J-1,kind=dp)
490 GOTO (40,110),IRTN
  !
  !    Follow contour line from side to corner or corners
  !
500 JRTN=2
  IOPP=IDIR(IENT)
  I1=ISIDE(IOPP)
  I2=ISIDE(IOPP+1)
  IEXIT=IOPP
  C(I1)=C(KVCT(I1))
  C(I2)=C(LVCT(I2))
  GOTO 300
510 JRTN=2
  IEXIT=JEXIT
  GOTO 300
  !
  !	Follow contour line through saddle point
  !
520 IOPP=IDIR(IENT)
  I1=ISIDE(IENT)
  C1=C(I1)
  I2=ISIDE(IENT+1)
  C2=C(I2)
  I3=ISIDE(IOPP)
  C3=C(I3)
  I4=ISIDE(IOPP+1)
  C4=C(I4)
  IF ((C1-CL)/(C1-C2).EQ.(C4-CL)/(C4-C3)) GOTO 540
  IF ((C1-CL)/(C1-C4).GT.(C2-CL)/(C2-C3)) GOTO 530
  IEXIT=I4
  GOTO 300
530 IEXIT=I2
  GOTO 300
540 C(I3)=C(I2)
  C(I4)=C(I1)
  IEXIT=I3
  GOTO 300
END SUBROUTINE HPCNTR
