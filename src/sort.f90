SUBROUTINE SORT
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Include_IPA
  Use Include_PA1

  IF (NS.LT.2) RETURN
  !
  !	If more than 1 shell is present, put them in order of ascending R
  !
  IF (KEYWORD(1:1).EQ.'D') THEN
     !	WRITE (6,*) 'DELETING ',INTOPT
     JSWOP=0
     DO  L=1,NS
        J=0
        DO  I=2,NS
           !	write (6,*) i,t(i-1),intopt,t(i-1).eq.intopt
           IF (RN(I-1).EQ.0..OR.T(I-1).EQ.INTOPT) THEN
              CALL SWOP (I-1,I,.TRUE.)
              J=I
              JSWOP=JSWOP+1
           ENDIF
        enddo
        IF (J.EQ.0) GOTO 3
     enddo
3    DO I=1,NS
        IF (RN(I).EQ.0..OR.T(I).EQ.INTOPT) GOTO 4
     ENDDO
     I=NS+1
4    CALL WTEXTI ('Number of shells deleted: ',NS-MAX(1,I-1))
     NS=MAX(1,I-1)
     RNS=NS
     CALL FLIM(*171)
171  CALL UTAB (0)
     CALL IUZERO (NAT,IPARNCLUS)
  ENDIF
  DO JCLUS=1,MAXCLUS
     JSWOP=0
     DO  L=1,NS
        IF (IABS(ICLUS(L)).NE.JCLUS) cycle
        J=0
        DO  I=2,NS
           IF (IABS(ICLUS(I)).NE.JCLUS) cycle
           IF (IABS(ICLUS(I)).NE.IABS(ICLUS(I-1))) cycle
           IF (R(I).LT.R(I-1)) THEN
              CALL SWOP (I-1,I,.TRUE.)
              J=I
              JSWOP=JSWOP+1
           ENDIF
        enddo
        IF (J.EQ.0) GOTO 30
     enddo
30   CONTINUE
  ENDDO
  IF (JSWOP.GT.0) THEN
     CALL WTEXT ('Shells Have Been Put in Order of Ascending R')
     CALL IUZERO (NAT,IPARNCLUS)
     !
     !	Call to FLIM updates variables in PA2,IPA etc.
     !
     CALL FLIM(*172)
172  CALL UTAB (0)
  ENDIF
  RETURN
END SUBROUTINE SORT
