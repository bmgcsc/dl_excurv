SUBROUTINE POISON (Z,MAXDEL,PSQ,W,GRID,DELTA,EDXL)
  !======================================================================C
  Use Definition
  Use Parameters
  real(float) A,B,C,C2,D,DELSQR,EDEL,E,F,WW
  !       
  !	THIS ROUTINE SOLVES POISSON'S EQUATION FOR A RADIAL CHARGE
  !	DENSITY. ( NOTE THAT PSQ SHOULD CONTAIN 4*PI*(R**2)*RHO, AND THAT
  !	W IS RETURNED WITH (R**1/2)*VCOULOMB ).
  !       
  DIMENSION PSQ(*),W(*),E(IPARGRID),F(IPARGRID)
  dimension GRID(*),ww(ipargrid)

  DELSQR=DELTA*DELTA
  A= 1.0-DELSQR/48.0
  B=-2.0-DELSQR/ 4.8
  EDEL=SQRT(EDXL)
  C=DELSQR/6.0
  C2=-B/A
  E(1)=0.0
  F(1)=EDEL
  J=MAXDEL-1
  DO I=2,J
     D=C*SQRT(GRID(I))*(EDEL*PSQ(I+1)+10.0*PSQ(I)+PSQ(I-1)/EDEL)
     F(I)=C2-1.0d0/F(I-1)
     E(I)=(D/A+E(I-1))/F(I)
  enddo
  WW(MAXDEL)=2.*Z/SQRT(GRID(MAXDEL))
  W(MAXDEL)=WW(MAXDEL)
  DO I=1,J
     JV=MAXDEL-I
     WW(JV)=E(JV)+WW(JV+1)/F(JV)
     W(JV)=+wW(JV)
  enddo
  !	w(maxdel)=w(maxdel-1)
  RETURN
END SUBROUTINE POISON
