SUBROUTINE DWALI (KDIM,KP,IORD,AN,UPATH,IJMAX,III)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Include_PA2

  DIMENSION :: ANGI(0:11),AN(0:KDIM,0:KDIM,0:KDIM),WALI(0:5,0:5),ZPATH(0:5)
  dimension :: KP(0:6,*),IORD(*),SHAREA(0:5,11),UPATH(*),III(0:5,2),KTYP(0:5)
  DO IJ=1,IJMAX
     J=0
     KTYP(0)=0
     !	K1=5
     !	DO I=1,4
     !	IF (KP(K1,IJ).EQ.0) K1=K1-1
     !	ENDDO
     K1=KP(IORD(IJ),IJ)
     DO K=0,IORD(IJ)
	K2=KP(K,IJ)
	K3=KP(K+1,IJ)
	ANGIT=AN(K1,K2,K3)
	KTYPT=K2
	IF (J.GT.0) THEN
           DO I=0,J-1
              IF (KTYP(I).EQ.KTYPT) THEN
                 TA=ABS(ANGIT-1.)
                 TB=ABS(ANGIT-ANGI(I))
                 TC=ABS(ANGI(I)-1.)
                 IF (TA.LT.1.E-5.OR.TB.LT.1.E-5.OR.TC.LT.1.E-5) THEN
                    !
                    !	Replace pi angles by current one, but not vice-versa
                    !
                    IF (ANGIT.NE.1.) ANGI(I)=ANGIT
                    SHAREA(I,IJ)=SHAREA(I,IJ)+1.
                    GOTO 5
                 ENDIF
              ENDIF
           ENDDO
	ENDIF
	SHAREA(J,IJ)=1.
	ANGI(J)=ANGIT
	KTYP(J)=KTYPT
	KATOM=J
	J=J+1
5	CONTINUE
        !	WRITE (6,*) 'K,K1,K2,K3,J',K,K1,K2,K3,J
        !	WRITE (7,*) 'K,K1,K2,K3,J',K,K1,K2,K3,J
	K1=K2
        !	IF (K3.EQ.0) GOTO 10
     ENDDO
     !   10	WALI(KATOM,KATOM)=0
     WALI(KATOM,KATOM)=0
     DO I=0,KATOM-1
	II=KTYP(I)
	WALI(I,I)=0.
	DO J=I+1,KATOM
           JJ=KTYP(J)
           IF (I.EQ.0) THEN
              WALI(I,J)=A2(III(JJ,1))
           ELSE
              !
              !	Factor of 2 is because of correlation with shared leg
              !
              WALI(I,J)=.5*(A2(III(II,1))+A2(III(JJ,1)))
           ENDIF
           WALI(J,I)=WALI(I,J)
	ENDDO
     ENDDO
     DO I=0,KATOM
	FANGSUM=0.
	DO J=0,KATOM
           IF (I.NE.J) FANGSUM=FANGSUM+ANGI(J)
	ENDDO
	WALISUM=0.
	DO J=0,KATOM
           WALISUM=WALISUM+WALI(I,J)*ANGI(J)
	ENDDO
	ZPATH(I)=ANGI(I)*WALISUM/FANGSUM
     ENDDO
     !
     !	This is a complete mess
     !
     IF (III(2,1).EQ.III(0,1)) THEN
        ZPATH(0)=ZPATH(0)*2.
        DO I=1,KATOM
           IF (KTYP(I).EQ.2) THEN
              ZPATH(I)=ZPATH(I)*2.
           ENDIF
        ENDDO
     ENDIF
     IF (KATOM.GT.2) THEN
        IF (III(3,1).EQ.III(1,1).AND.III(3,2).EQ.III(1,2)) THEN
           ZPATH(1)=ZPATH(1)*2.
           DO I=1,KATOM
              IF (KTYP(I).EQ.3) THEN
                 ZPATH(I)=ZPATH(I)*2.
              ENDIF
           ENDDO
        ENDIF
     ENDIF
     UPATH(IJ)=0.
     DO J=0,KATOM
	UPATH(IJ)=UPATH(IJ)+SHAREA(J,IJ)**2*ZPATH(J)
     ENDDO
     !	write (7,'(5i1,4f4.0,4f8.4,f8.4,i4)') (kp(j,ij),j=1,5),(sharea(j,ij),j=0,3),( angi(j),j=0,3),upath(ij)/dwc,katom
     !	write (6,'(5i1,3f4.0,3f8.4,f8.4)')    (kp(j,ij),j=1,5),(sharea(j,ij),j=0,2),(zpath(j),j=0,2),upath(ij)/dwc
  ENDDO
  RETURN
END SUBROUTINE DWALI
