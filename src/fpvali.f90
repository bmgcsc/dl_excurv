FUNCTION FPVALI (IMAX,CHSTR,ERR)
  !======================================================================C
  !
  !	Convert a character string to a floating point number
  !
  !	CHSTR (I)   Character string containing the floating point
  !	            number (no spaces allowed in string)
  !	FPVAL returns the converted value (0 if syntax error)
  !
  !	INTEGER*1 CHSTR(*),DIG(14)
  INTEGER CHSTR(*),DIG(14)
  LOGICAL END1,ERR
  DATA DIG/69,48,49,50,51,52,53,54,55,56,57,46,43,45/
  FPVALI=0.0
  END1=.FALSE.
  ERR =.FALSE.
  IEXP=0
  M1=2
  M2=14
  I=0
  !
  !	Interpret number
  !
10 S=1.0
  IDIG=0
  IFDEC=0
  X=0.0
20 I=I+1
  IF(I.GT.IMAX)THEN
     END1=.TRUE.
     IF(IEXP.EQ.1)GO TO 50
     GO TO 40
  ENDIF
  !	J=INDEX(DIG(M1:M2),CHSTR(I:I))
  DO J=M1,M2
     IF (CHSTR(I).EQ.DIG(J)) GOTO 30
  ENDDO
  GO TO 60
  !   2	J=J+M1-1
30 M1=1+IEXP
  !
  !	Digit
  !
  IF(J.GE.2.AND.J.LE.11)THEN
     IDIG=IDIG+IFDEC
     X=10.0*X+J-2
     IF(M2.GT.12)M2=12
     GO TO 20
     !
     !	Decimal point
     !
  ELSE IF(J.EQ.12)THEN
     IF(IEXP.EQ.1)GO TO 60
     IFDEC=1
     M2=11
     GO TO 20
     !
     !	Sign
     !
  ELSE IF(J.GE.13)THEN
     IF(J.EQ.14)S=-1.0
     M2=12
     GO TO 20
     !
     !	Exponent
     !
  ELSE
     IEXP=1
     M1=2
     M2=14
  ENDIF
  !
  !	Evaluate number
  !
40 FPVALI=S*X/(10**IDIG)
  IF(END1)RETURN
  GO TO 10
  !
  !	Evaluate and apply exponent
  !
50 IEX=NINT(S*X)
  FPVALI=FPVALI*10.0**IEX
  RETURN
  !
  !	Error condition
  !
60 ERR=.TRUE.
  FPVALI=0.0
  RETURN
END FUNCTION FPVALI
