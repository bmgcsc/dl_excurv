SUBROUTINE IDEALISE
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  CHARACTER TNAME*4,OPTS(2)*6
  LOGICAL SPINNER
  DATA OPTS/'SPIN','NOSPIN'/
  CALL FINDOPT (2,OPTS,IOPT,2,'Unit Number - default all units',*20)
  IF (IOPT.EQ.1) THEN
     SPINNER=.TRUE.
  ELSE
     SPINNER=.FALSE.
  ENDIF
  ISTART=1
  IEND=MG
  IF (INTOPT.NE.0) THEN
     ISTART=INTOPT
     IEND=INTOPT
  ENDIF
  IF (IEND.LT.1) RETURN
  DO J=ISTART,IEND
     CALL WTEXTI ('Idealising unit ',J)
     JPIV=IPIV(J)
     DO I=1,IPARNS
	IF (NU(I).EQ.J.AND.ATLABEL(I).EQ.'CA') THEN
           JPIV=I
           GOTO 10
	ENDIF
     ENDDO
10   IF (JPIV.LE.0) RETURN
     TNAME='$'//UNAME(INTOPT)
     CALL READBASE (TNAME,JPIV,.TRUE.)
     IF (SPINNER) THEN
        KEYWORD='A'
        INTOPT=J
        CALL SPIN
     ENDIF
  ENDDO
  CALL IUZERO (NAT,IPARNCLUS)
20 RETURN
END SUBROUTINE IDEALISE
