SUBROUTINE INTERP (XR,YR,X,Y,IC,IPT,*)
  !======================================================================C
  Use Definition
  real(dp)  XR(*), YR(*), X(*), Y(*)
  STEP=(XR(IC)-XR(1))/real(IPT,kind=dp)
  IF (STEP.LT.1.E-10) RETURN 1
  X1=XR(1)
  X2=X1+STEP
  IB=0
  C1=0.
  DO  I=1,IC
     IF (I.EQ.IC) THEN
        DX=1.0E10
     ELSE
        DX=XR(I+1)-XR(I)
     ENDIF
10   IF (X1+DX-X2) 30,20,20
20   DI=X2-X1
     X1=X2
     C1=C1+DI*YR(I)
     IB=IB+1
     Y(IB)=C1/STEP
     X(IB)=X2-STEP
     DX=DX-DI
     C1=0.
     IF (X2.GT.XR(IC)) RETURN
     X2=X2+STEP
     GOTO 10
30   C1=C1+DX*YR(I)
     X1=X1+DX
  enddo
  RETURN
END SUBROUTINE INTERP
