SUBROUTINE CEXPER (KEXP,ISPEC)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Include_IPA
  !	Use Common_IPA

  CHARACTER*5  :: POWER(6)
  DATA POWER /'*k','*k**2','*k**3','*k**4','*k**5','*k**6'/
  IF (ISPEC.LT.IPARNSP*4) ISPEC=ISPEC+1
  ITLEN=18
  PLOTTITLE='EXAFS (experiment)'
  IF (NSPEC.NE.1) THEN
     PLOTTITLE(ITLEN:)=' '//CHAR(48+KEXP)//')'
     ITLEN=ITLEN+2
  ENDIF
  JQ=IGF(5)-1
  IF (JQ.NE.0) THEN
     IF (ITLEN.GT.1.AND.JQ.LE.6) PLOTTITLE(ITLEN+1:)=POWER(JQ)
  ENDIF
  RETURN
END SUBROUTINE CEXPER
