SUBROUTINE INITVAR
  !======================================================================C
  Use Parameters
  Use Common_LETTERS
  Use Common_VAR
  Use Common_UPU
  Use Index_INDS
  Use Common_DISTANCE
  !
  !	Create variables names dependent on compilation parameters
  !
  CHARACTER*3 LONGNUMS(0:IPARNP+IPARNS+IPARNSP-2)
  inds(INDN)=11
  IMAX=MAX(IPARNS,IPARNP,IPARNSP)
  DO I=0,IMAX
     N0=I/100
     N1=MOD(I/10,10)
     N2=MOD(I,10)
     IF (I.LT.10) THEN
        LONGNUMS(I)=NUMBERS(I)
     ELSEIF (I.LT.100) THEN
        LONGNUMS(I)=NUMBERS(N1)//NUMBERS(N2)
     ELSEIF (I.LT.1000) THEN
        LONGNUMS(I)=NUMBERS(N0)//NUMBERS(N1)//NUMBERS(N2)
     ENDIF
  ENDDO
  DO  I=0,IPARNSV-1
     K=NCSTR(IVARS(I*(IPARNS+1)+inds(INDN)))
     DO  J=0,IPARNS
	IVARS(I*(IPARNS+1)+J+inds(INDN))(K:)=LONGNUMS(J)
	IF (I.EQ.0.AND.J.GT.0) SVARS(J)='S'//LONGNUMS(J)
        !	IF (I.EQ.0) AVARS(J)='ATOM'//LONGNUMS(J)
        !	IF (I.EQ.0) LVARS(J)='LAB'//LONGNUMS(J)
        !	IF (I.EQ.0) NTVARS(J)='ATOM'//LONGNUMS(J)
     enddo
  enddo
  KIND1=(IPARNS+1)*IPARNSV+inds(INDN)
  DO  I=0,IPARNSPV-1
     DO  J=0,IPARNSP
	K=NCSTR(IVARS(KIND1))
	IVARS(KIND1)(K:)=LONGNUMS(J)
	KIND1=KIND1+1
     enddo
  enddo
  DO  I=0,IPARNPV-1
     DO  J=1,IPARNP
	K=NCSTR(IVARS(KIND1))
	IVARS(KIND1)(K:)=LONGNUMS(J)
	KIND1=KIND1+1
     enddo
  enddo
  DO  I=0,IPARNS
     DO  J=0,IPARNS
	WEIGHTVARS(I,J)='W'//LONGNUMS(I)
	DISTVARS(I,J)='D'//LONGNUMS(I)
	BONDANGVARS(I,J)='A'//LONGNUMS(I)
	BAWTVARS(I,J)='V'//LONGNUMS(I)
	BONDVARS(I,J)='B'//LONGNUMS(I)
        !	IF (I.LE.IPARNSC.AND.J.LE.IPARNSC) THEN
        !	  DO KK=1,48
        !	  CORELVARS(I,J,KK)='C'//LONGNUMS(I)//':'
        !	  ENDDO
        !	ENDIF
	K=NCSTR(WEIGHTVARS(I,J))+1
	WEIGHTVARS(I,J)(K:)=':'//LONGNUMS(J)
	DISTVARS(I,J)(K:)=':'//LONGNUMS(J)
	BONDANGVARS(I,J)(K:)=':'//LONGNUMS(J)
	BAWTVARS(I,J)(K:)=':'//LONGNUMS(J)
	BONDVARS(I,J)(K:)=':'//LONGNUMS(J)
        !	IF (I.LE.IPARNSC.AND.J.LE.IPARNSC) THEN
        !	  DO KK=1,48
        !	  CORELVARS(I,J,KK)(K:)=':'//LONGNUMS(J)
        !	  ENDDO
        !	ENDIF
     enddo
  enddo
  DO I=0,IPARNS
     ISEQNUM(I)=0
     ATLABEL(I)=' '
  ENDDO
  RETURN
END SUBROUTINE INITVAR
