MODULE Common_POT

!      COMMON /POT/ NPOTS(0:IPARNP),XSTARTS(0:IPARNP),V0S(0:IPARNP),RHO0S
!     1(0:IPARNP),IHCODE(0:IPARNP),V0EV(0:IPARNP),FEEV(0:IPARNP),RHOZERO(
!     20:IPARNP),RLIFEEV(0:IPARNP),DXLS(0:IPARNP),NENDS(2,0:IPARNP),POT(I
!     3PARGRID,0:IPARNP),RHO(IPARGRID,0:IPARNP),XGD(IPARGRID),GRID(IPARGR
!     4ID),G2(IPARGRID),VMT(IPARGRID),ROAT(IPARGRID),RNL(IPARGRID),DRNL(I
!     5PARGRID),WPL(IPARGRID),DPL(IPARGRID),LASTNEIGH(0:IPARNP),LASTHOLE(
!     60:IPARNP),REMRMTR2(0:IPARNP),IATGROUP(0:IPARNP)

!	data IHCODE/IPARNP1*0/,V0EV/IPARNP1*0./,FEEV/IPARNP1*0./        !   in COMMON /POT/
!	data RHO0S/IPARNP1*0./,RLIFEEV/IPARNP1*0./                      !   in COMMON /POT/

!	data NPOTS/IPARNP1*0/,LASTNEIGH/IPARNP1*0/            !   in COMMON /POT/
!	data LASTHOLE/IPARNP1*-99/                            !   in COMMON /POT/

!	data IATGROUP/IPARNP1*1/                              !   in COMMON /POT/

Use Parameters
Use Definition
implicit none
private

integer(i4b), dimension(:),   public ::   NPOTS(0:IPARNP)
real(dp),     dimension(:),   public :: XSTARTS(0:IPARNP)
real(dp),     dimension(:),   public ::     V0S(0:IPARNP)
real(dp),     dimension(:),   public ::   RHO0S(0:IPARNP)
integer(i4b), dimension(:),   public ::  IHCODE(0:IPARNP)
real(dp),     dimension(:),   public ::    V0EV(0:IPARNP)
real(dp),     dimension(:),   public ::    FEEV(0:IPARNP)
real(dp),     dimension(:),   public :: RHOZERO(0:IPARNP)
real(dp),     dimension(:),   public :: RLIFEEV(0:IPARNP)
real(dp),     dimension(:),   public ::    DXLS(0:IPARNP)
integer(i4b), dimension(:,:), public :: NENDS(2,0:IPARNP)
real(dp),     dimension(:,:), public :: POT(IPARGRID,0:IPARNP)
real(dp),     dimension(:,:), public :: RHO(IPARGRID,0:IPARNP)
real(dp),     dimension(:),   public ::  XGD(IPARGRID)
real(dp),     dimension(:),   public :: GRID(IPARGRID)
real(dp),     dimension(:),   public ::   G2(IPARGRID)
real(dp),     dimension(:),   public ::  VMT(IPARGRID)
real(dp),     dimension(:),   public :: ROAT(IPARGRID)
real(dp),     dimension(:),   public ::  RNL(IPARGRID)
real(dp),     dimension(:),   public :: DRNL(IPARGRID)
real(dp),     dimension(:),   public ::  WPL(IPARGRID)
real(dp),     dimension(:),   public ::  DPL(IPARGRID)
integer(i4b), dimension(:),   public :: LASTNEIGH(0:IPARNP)
integer(i4b), dimension(:),   public ::  LASTHOLE(0:IPARNP)
real(dp),     dimension(:),   public ::  REMRMTR2(0:IPARNP)
integer(i4b), dimension(:),   public ::  IATGROUP(0:IPARNP)

data    IHCODE/IPARNP1*0/
data      V0EV/IPARNP1*0._sp/
data      FEEV/IPARNP1*0._sp/
data     RHO0S/IPARNP1*0._sp/
data   RLIFEEV/IPARNP1*0._sp/
data     NPOTS/IPARNP1*0/
data LASTNEIGH/IPARNP1*0/
data  LASTHOLE/IPARNP1*-99/
data  IATGROUP/IPARNP1*1/


end MODULE Common_POT
