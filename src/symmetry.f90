SUBROUTINE SYMMETRY (QUIET,SORT,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Common_SOUPER
  Use Common_DISTANCE
  Use Common_SYMOPS
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  DIMENSION RATIOS(32,16),THETAS(32,16),PHIS(32,16),NUMBERS(32,16)
  dimension NSPG(16),ITYPES(32,16),JTYPES(2),INDEX(0:IPARNS)
  CHARACTER IP1*6,IP2*6,IP3*6,TSTRUCTS(16)*3
  CHARACTER*10 STRUCTURES(16)
  CHARACTER*1 YNTRIAD,YNDIAD,YNV,YNMIR
  LOGICAL QUIET,SORT
  DATA STRUCTURES /'FCC','HCP','BCC','diamond','halite','fluorite','CsCl','sphalerite','NiAs','zincite',6*' '/
  DATA TSTRUCTS/'fcc','hcp','bcc','dia','hal','flu','csc','sph','nia','zin',6*' '/
  DATA NSPG /46,29,46,44,46,46,46,44,46,44,6*0/
  !	FCC
  DATA (RATIOS(I,1),I=1,32)/ &   
       0.5,1.,1.5,2.,2.5,3.,3.5,4.,4.5,4.5,5.,5.5,6.,6.5,6.5,7.,7.5,8., &
       8.5,9.,9.,9.5,9.5,10.,10.5,11.,11.5,12.,12.5,12.5,12.5,13./
  !	HCP
  DATA (RATIOS(I,2),I=1,32)/ &
       1.,1., 2., 2.6666667, 3*3., 3.6666667, 4., 5., 2*5.6666667,          &
       6., 6.3333333, 6.6666667, 7., 7.3333333, 8.3333333, 9., 9.6666667,   &
       10., 10.3333333, 10.6666667, 11., 11.3333333, 11.6666667,            &
       12., 12.3333333, 13., 13.6666667,14.3333333, 14.6666667/
  !	BCC
  DATA (RATIOS(I,3),I=1,32)/0.75,1.,2.,2.75,3.,4.,4.75,5.,6.,6.75,6.75,8.,8.75,9.,9.,10.,16*0./
  !	DIAMOND
  DATA (RATIOS(I,4),I=1,32)/  &
       0.1875,.5,.6875,1.,1.1875,1.5,1.5,1.6875,1.6875,    &
       2.,2.1875,2.5,2.6875,3.,3.,3.1875,3.1875,3.5,3.5,13*0./
  !	halite
  DATA (RATIOS(I,5),I=1,32)/   &
       0.25,.5,.75,1.,1.25,1.5,2.,2.25,2.25,2.5,2.75,3.,3.25,3.5,4.,4.25, &
       4.25,4.5,4.75,5.,5.25,5.5,6.,6.25,8*0./
  !	fluorite
  DATA (RATIOS(I,6),I=1,32)/ &
       0.1875,.5,.6875,1.,1.1875,1.5,1.6875,1.6875,2.,2.1875, &
       2.5,2.6875,3.,3.1875,3.1875,3.5,16*0./
  !	CsCl
  DATA (RATIOS(I,7),I=1,32)/0.75,1.,2.,2.75,3.,4.,4.75,5.,6.,6.75,6.75,8.,8.75,9.,9.,10.,16*0./
  !	sphalerite
  DATA (RATIOS(I,8),I=1,32)/ &
       0.1875,.5,.6875,1.,1.1875,1.5,1.5,1.6875,1.6875, &
       2.,2.1875,2.5,2.6875,3.,3.,3.1875,3.1875,3.5,3.5,13*0./
  DATA (RATIOS(I,9), I=1,32)/32*0./
  DATA (RATIOS(I,10),I=1,32)/32*0./
  DATA (RATIOS(I,11),I=1,32)/32*0./
  DATA (RATIOS(I,12),I=1,32)/32*0./
  DATA (RATIOS(I,13),I=1,32)/32*0./
  DATA (RATIOS(I,14),I=1,32)/32*0./
  DATA (RATIOS(I,15),I=1,32)/32*0./
  DATA (RATIOS(I,16),I=1,32)/32*0./
  !	FCC
  DATA (THETAS(I,1),I=1,32)/ &
       90., 90., 35.2643897, 90., 90., 54.7356103, 74.4986404, 0.,  &
       90., 19.4712206, 90., 64.7605982, 35.2643897, 90., 38.328818, &
       24.0948426, 120.96, 90., 99.336, 90., 98.876, 107.55, 98.479,125.26, 115.10,7*0./
  !	HCP
  DATA (THETAS(I,2),I=1,32)/ &
       35.2643897,90., 54.7356103, 0.,61.874494, 90., 90., 31.4821541, &
       90., 68.5832860, 2*46.6861434, 70.5287794, 13.2626720, 50.768480, &
       72.0247162,16*0./
  !	BCC
  DATA (THETAS(I,3),I=1,32)/  &
       54.73561032, 0., 90., 25.2394018, 54.73561032, 0., 76.7373240, &
       90., 35.2643897, 54.73561032, 15.7931691, 90., 32.3115332,     &
       90., 109.4712206,90.,16*0./
  !	DIAMOND
  DATA (THETAS(I,4),I=1,32)/ &
       54.7356103, 90., 154.760598, 0., 76.737324, 35.2643897, 44.735610,125.2643897,15.7931691, &
       90., 120.470360, 18.4349488, 40.3155422, 54.7356103, 125.2643897, &
       81.9505330,168.5799268,36.6992252,57.688,13*0./
  !	halite
  DATA (THETAS(I,5),I=1,32)/ &
       90.,90.,54.7356103,90.,90.,35.2643897,90.,0.,70.5287794,90., &
       25.2394018, 54.7356103, 90., 56.6884668,0.,43.3138567,16*0./
  !	fluorite
  DATA (THETAS(I,6),I=1,32)/ &
       54.7356103, 90., 25.2394018, 0., 76.737324, 35.2643897, &
       54.7356103, 15.7931691, 90., 32.3115332, 90., &
       40.3155422, 54.7356103, 81.9505330, 11.4211863, 57.6884668,16*0./
  !	CsCl
  DATA (THETAS(I,7),I=1,32)/ &
       54.73561032, 0., 90., 25.2394018, 54.73561032, 0., 76.7373240, &
       90., 35.2643897, 54.73561032, 15.7931691, 90., 32.3115332,     &
       90., 109.4712206,90.,16*0./
  !	sphalerite
  DATA (THETAS(I,8),I=1,32)/ &
       54.7356103, 90., 154.760598, 0., 76.737324, 35.2643897, &
       144.735610,125.2643897,15.7931691,                      &
       90., 120.470360, 18.4349488, 40.3155422, 54.7356103, 125.2643897, &
       81.9505330,168.5799268,36.6992252,57.688,13*0./
  DATA (THETAS(I,9), I=1,32)/32*0./
  DATA (THETAS(I,10),I=1,32)/32*0./
  DATA (THETAS(I,11),I=1,32)/32*0./
  DATA (THETAS(I,12),I=1,32)/32*0./
  DATA (THETAS(I,13),I=1,32)/32*0./
  DATA (THETAS(I,14),I=1,32)/32*0./
  DATA (THETAS(I,15),I=1,32)/32*0./
  DATA (THETAS(I,16),I=1,32)/32*0./
  !	FCC
  DATA (PHIS(I,1),I=1,32)/ &
       45., 0., 45., 45., 18.4349488, 45., 33.6900675, 0., &
       45.,45., 26.5650512, 45., 45., 11.3099325, 18.4349488, 26.5650512, &
       45., 180., 189.46, 198.43, 231.34, 198.43, 206.57, 225., 231.34, &
       7*0./
  !	HCP
  DATA (PHIS(I,2),I=1,32)/ &
       60., 30., 0., 0., 40.8933947, 0., 60., 30., 30., 13.8978863, &
       0., 60., 60., 60., 30., 36.5867756,16*0./ 
  !	BCC
  DATA (PHIS(I,3),I=1,32)/ &
       45., 90., 45., 45., 45., 90., 45., 26.5650512, 45.,  &
       45., 45., 45., 18.4349488, 0., 45., 18.4349488,16*0./
  !	DIAMOND
  DATA (PHIS(I,4),I=1,32)/ &
       45.,45.,45.,0.,45.,45.,45.,45.,45., &
       45.,11.3099325,0.,45.,45.,45.,45.,45.,26.5650512,71.565,13*0./
  !	halite
  DATA (PHIS(I,5),I=1,32)/ &
       0.,45.,45.,0.,26.5650512,45.,45.,0.,45.,18.4349488,45.,45., &
       33.6900675,18.4349488,0.,45.,16*0./
  !	fluorite
  DATA (PHIS(I,6),I=1,32)/ &
       45., 45., 45., 0., 45., 45., 45., 45., 45., 18.4349488, &
       18.4349488, 45., 45., 45., 45., 18.4349488,16*0./
  !	CsCl
  DATA (PHIS(I,7),I=1,32)/ &
       45., 90., 45., 45., 45., 90., 45., 26.5650512, 45., &
       45., 45., 45., 18.4349488, 0., 45., 18.4349488,16*0./
  !	sphalerite
  DATA (PHIS(I,8),I=1,32)/ &
       45.,45.,45.,0.,45.,45.,45.,45.,45., &
       45.,11.3099325,0.,45.,45.,45.,45.,45.,26.5650512,71.565,13*0./
  DATA (PHIS(I,9), I=1,32)/32*0./
  DATA (PHIS(I,10),I=1,32)/32*0./
  DATA (PHIS(I,11),I=1,32)/32*0./
  DATA (PHIS(I,12),I=1,32)/32*0./
  DATA (PHIS(I,13),I=1,32)/32*0./
  DATA (PHIS(I,14),I=1,32)/32*0./
  DATA (PHIS(I,15),I=1,32)/32*0./
  DATA (PHIS(I,16),I=1,32)/32*0./
  DATA (NUMBERS(I,1),I=1,32)/ &
       12,6,24,12,24,8,48,6,12,24,24,24,24,24,48,48,12,48,6,24,24,48,24, &
       48,24,48,8,84,24,24,96,6/
  !	HCP
  DATA (NUMBERS(I,2),I=1,32)/6,6,6,2,12,3,3,12,6,12,6,6,6,6,12,12,16*0/
  !	BCC
  DATA (NUMBERS(I,3),I=1,32)/8,6,12,24,8,6,24,24,24,8,24,12,48,6,24,24,16*0/
  !	DIAMOND
  DATA (NUMBERS(I,4),I=1,32)/4,12,12,6,12,12,12,4,12,12,24,24,12,4,4,12,12,24,24,13*0/
  !	halite
  DATA (NUMBERS(I,5),I=1,32)/6,12,8,6,24,24,12,6,24,24,24,8,24,48,6,24,16*0/
  !	fluorite
  DATA (NUMBERS(I,6),I=1,32)/8,12,24,6,24,24,8,24,12,48,24,24,8,24,24,48, 16*0/
  !	CsCl
  DATA (NUMBERS(I,7),I=1,32)/8,6,12,24,8,6,24,24,24,8,24,12,48,6,24,24,16*0/
  !	sphalerite
  DATA (NUMBERS(I,8),I=1,32)/4,12,12,6,12,12,12,4,12,12,24,24,12,4,4,12,12,24,24,13*0/
  DATA (NUMBERS(I,9) ,I=1,32)/32*0/
  DATA (NUMBERS(I,10),I=1,32)/32*0/
  DATA (NUMBERS(I,11),I=1,32)/32*0/
  DATA (NUMBERS(I,12),I=1,32)/32*0/
  DATA (NUMBERS(I,13),I=1,32)/32*0/
  DATA (NUMBERS(I,14),I=1,32)/32*0/
  DATA (NUMBERS(I,15),I=1,32)/32*0/
  DATA (NUMBERS(I,16),I=1,32)/32*0/
  !	fcc
  DATA (ITYPES(I,1),I=1,32)/32*1/
  !	hcp
  DATA (ITYPES(I,2),I=1,32)/32*1/
  !	bcc
  DATA (ITYPES(I,3),I=1,32)/32*1/
  !	diamond
  DATA (ITYPES(I,4),I=1,32)/32*1/
  !	halite
  DATA (ITYPES(I,5),I=1,32)/2,1,2,1,2,1,1,2,2,1,2,1,2,1,1,2,16*1/
  !	fluorite
  DATA (ITYPES(I,6),I=1,32)/2,1,2,1,2,1,2,2,1,2,1,2,1,2,2,1,16*1/
  !	CsCl
  DATA (ITYPES(I,7),I=1,32)/2,1,1,2,1,1,2,1,1,2,2,1,2,1,1,1,16*1/
  !	sphalerite
  DATA (ITYPES(I,8),I=1,32)/2,1,2,1,2,1,1,2,2,1,2,1,2,1,1,2,16*1/
  !
  DATA (ITYPES(I,9),I=1,32)/32*1/
  DATA (ITYPES(I,10),I=1,32)/32*1/
  DATA (ITYPES(I,11),I=1,32)/32*1/
  DATA (ITYPES(I,12),I=1,32)/32*1/
  DATA (ITYPES(I,13),I=1,32)/32*1/
  DATA (ITYPES(I,14),I=1,32)/32*1/
  DATA (ITYPES(I,15),I=1,32)/32*1/
  DATA (ITYPES(I,16),I=1,32)/32*1/
  !
  DO  I=1,50
     YNTRIAD='n'
     IF (ITRIADS(I).NE.1) YNTRIAD='y'
     YNDIAD='n'
     IF (ICXS(I).NE.1) YNDIAD='y'
     YNV='n'
     IF (ISXS(I).NE.1) YNV='y'
     YNMIR='n'
     IF (IMIRS(I).NE.1) YNMIR='y'
     IF (DEBUG) WRITE (LOGFILE,10) PGROUPS(I)(1:4),YNTRIAD,YNDIAD,YNV,YNMIR,KROTS(I),(NVALS(J,I),J=1,6)
10   FORMAT (5(A,' '),7I6)
  enddo
  CALL IUZERO (NAT,IPARNCLUS)
  IF (KEYWORD(1:1).EQ.'Q') THEN
     QUIET=.TRUE.
     KEYWORD=' '
  ENDIF
  CALL UZERO (AR,IPARNCLUS*(IPARATOMS+1))
  CALL UZERO (ATH,IPARNCLUS*(IPARATOMS+1))
  CALL UZERO (APHI,IPARNCLUS*(IPARATOMS+1))
  CALL UZERO (AX,IPARNCLUS*(IPARATOMS+1))
  CALL UZERO (AY,IPARNCLUS*(IPARATOMS+1))
  CALL UZERO (AZ,IPARNCLUS*(IPARATOMS+1))
  DO  JCLUS=1,MAXCLUS
     IF (.NOT.QUIET.OR.DEBUG) CALL WTEXTI ('Cluster number:',JCLUS)
30   IF (JCLUS.EQ.1.AND.KEYWORD.NE.' ') THEN
        PGROUP(JCLUS)=KEYWORD
     ELSEIF (KEYWORD.NE.' '.OR.PGROUP(JCLUS).EQ.' ') THEN
40      IF (LRIS+LMDSN.EQ.0) CALL WTEXTI ('Enter Point Group ( or ?) for cluster ',JCLUS)
        CALL CREAD (PGROUP(JCLUS),IP1,IP2,IDUM,IC,V,*40,*280)
     ENDIF
     DO  I=2,3
	J=ICHAR(PGROUP(JCLUS)(I:I))
	IF (J.GT.64.AND.J.LT.91) PGROUP(JCLUS)(I:I)=CHAR(J+32)
     enddo
     IF (PGROUP(JCLUS)(1:1).EQ.'?') THEN
        WRITE (OUTTERM,290) PGROUPS
        WRITE (OUTTERM,300)
        WRITE (OUTTERM,310) (STRUCTURES(I),PGROUPS(NSPG(I)),I=1,8)
        IF (PGROUP(JCLUS)(2:2).EQ.'l') THEN
           WRITE (OUTTERM,*)
           WRITE (OUTTERM,*) '----------------------------------'
           NLINES=2
           DO  JCLASS=0,7
              DO  I=1,50
                 IF (ICLASS(I).NE.JCLASS) cycle
                 WRITE (OUTTERM,*) I,'    ',PGROUPS(I),INTERNAT(I),NVALS(1,I)
                 NLINES=NLINES+1
                 IF (NLINES.EQ.19.OR.NLINES.EQ.38) THEN
                    CALL CREAD (IP1,IP2,IP3,INT,IC,VAL,*60,*280)
                 ENDIF
60               CONTINUE
              enddo
              WRITE (6,*) '----------------------------------'
              NLINES=NLINES+1
           enddo
        ENDIF
        PGROUP(JCLUS)=' '
        KEYWORD=' '
        GOTO 30
     ELSE
        DO  NPG=1,50
           IF (PGROUP(JCLUS).EQ.PGROUPS(NPG)) GOTO 120
        enddo
        J=ICHAR(PGROUP(JCLUS)(1:1))
        !
        !	Convert first char to upper case
        !
        IF (J.GT.64.AND.J.LT.91) PGROUP(JCLUS)(1:1)=CHAR(J+32)
        DO  NPG=1,8
           IF (PGROUP(JCLUS)(1:3).EQ.TSTRUCTS(NPG)) GOTO 100
        enddo
        CALL WTEXT ('Point group '//PGROUP(JCLUS)//'not found')
        GOTO 275
100     R1=R(1)/SQRT(RATIOS(1,NPG))
        JTYPES(1)=IT(2)
        JTYPES(2)=IT(1)
        DO  I=1,IPARNS
           R(I)=R1*SQRT(RATIOS(I,NPG))
           TH(I)=THETAS(I,NPG)
           PHI(I)=PHIS(I,NPG)
           UN(I)=0.
           RN(I)=NUMBERS(I,NPG)
           CLUS(I)=1.
           T(I)=JTYPES(ITYPES(I,NPG))
        enddo
        NPG=NSPG(NPG)
        PGROUP(JCLUS)=PGROUPS(NPG)
        CALL FLIM (*121)
121     CALL UTAB (0)
120     CONTINUE
     ENDIF
     NPGR(JCLUS)=NPG
!     	WRITE (OUTTERM,*) 'npg=',NPG
     NPGCLASS(JCLUS)=ICLASS(NPG)
     ISETTING=0
     ITRIAD=ITRIADS(NPG)
     ICX=ICXS(NPG)
     ISX=ISXS(NPG)
     IMIR(JCLUS)=IMIRS(NPG)
     KROT(JCLUS)=KROTS(NPG)
     IAXIS(JCLUS)=IABS(KROT(JCLUS))
     KCENT(JCLUS)=KCENTS(NPG)
     IF (SORT) CALL SORT1 (INDEX)
     DO  IINDEX=0,NS
	I=INDEX(IINDEX)
  	IF (N(I).LT.1.OR.IABS(ICLUS(I)).NE.JCLUS) cycle
	IF (I.EQ.IRAD(JCLUS)) cycle
	DO  JSYM=1,6
           IF (N(I).EQ.NVALS(JSYM,NPG)) GOTO 140
        enddo
        !	CALL WTEXT (' ')
	CALL ERRMSGI ('Invalid Occupation Number in Space Group '//PGROUP(JCLUS)(1:4)//': ',N(I),*275)
        !	GOTO 275
140	JTRIAD=ITRIAD
	JCX=ICX
	JSX=ISX
	JMIR=IMIR(JCLUS)
	JROT=KROT(JCLUS)
	JAXIS=IAXIS(JCLUS)
150     IF (JSYM.EQ.1) THEN
           CALL GENERAL (I,TPHI,TTHETA,*280)
	ELSEIF (JSYM.EQ.2) THEN
           CALL ONTRIAD (I,TPHI,TTHETA,JCLUS,*280)
	ELSEIF (JSYM.EQ.3) THEN
           IF (NVALS(3,NPG).EQ.NVALS(6,NPG)) CALL DK56 (I,JSYM,*150)
           IF (NVALS(3,NPG).EQ.NVALS(4,NPG)) CALL DK34 (I,JSYM,*150)
           CALL ONCX (I,TPHI,TTHETA,JCLUS,ISETTING,*280)
	ELSEIF (JSYM.EQ.4) THEN
           IF (NVALS(4,NPG).EQ.NVALS(5,NPG)) CALL DK45 (I,JSYM,JCLUS,*150)
           CALL ONSX (I,TPHI,TTHETA,JCLUS,*280)
	ELSEIF (JSYM.EQ.5) THEN
           IF (NVALS(5,NPG).EQ.NVALS(6,NPG)) CALL DK56 (I,JSYM,*150)
           CALL ONMIR (I,TPHI,TTHETA,JCLUS,*280)
	ELSEIF (JSYM.EQ.6) THEN
           IF (JTRIAD.NE.3) THEN
              CALL ONAXIS (I,TPHI,TTHETA,*280)
           ELSE
              DO  J=1,4
                 IF (NAT(JCLUS).GE.IPARATOMS) THEN
                    CALL WTEXT ('Error, more than IPARATOMS atoms in list')
                    GOTO 260
                 ENDIF
                 NAT(JCLUS)=NAT(JCLUS)+1
                 APHI(NAT(JCLUS),JCLUS)=real(J-1,kind=dp)*PID2
                 ATH(NAT(JCLUS),JCLUS)=PID2
!                 	    WRITE (OUTTERM,*) JCLUS,NAT(JCLUS),I,RAD(I),DC
                 AR(NAT(JCLUS),JCLUS)=RAD(I)/DC
                 ISYM(NAT(JCLUS),JCLUS)=J
                 IF (J.GT.2.AND.JROT.NE.4) ISYM(NAT(JCLUS),JCLUS)=J-2
                 NSYM(NAT(JCLUS),JCLUS)=J
                 IDEG(NAT(JCLUS),JCLUS)=JAXIS
                 ISHELL(NAT(JCLUS),JCLUS)=I
                 IF (JROT.LT.0) IDEG(NAT(JCLUS),JCLUS)=IDEG(NAT(JCLUS),JCLUS)/2
                 ANORM(NAT(JCLUS),JCLUS)=SQRT(real(IDEG(NAT(JCLUS),JCLUS),kind=dp))
                 CALL POLTOCAR (AR(NAT(JCLUS),JCLUS),ATH(NAT(JCLUS),JCLUS),APHI(NAT(JCLUS),JCLUS), &
                      AX(NAT(JCLUS),JCLUS),AY(NAT(JCLUS),JCLUS),AZ(NAT(JCLUS),JCLUS))
              enddo
              DO  J=1,2
                 IF (NAT(JCLUS).GE.IPARATOMS) THEN
                    CALL ERRMSG ('Error, more than IPARATOMS atoms in list',*260)
                    !	      GOTO 260
                 ENDIF
                 NAT(JCLUS)=NAT(JCLUS)+1
                 !	    WRITE (OUTTERM,*) 'nat=',NAT(JCLUS)
                 APHI(NAT(JCLUS),JCLUS)=0
                 ATH(NAT(JCLUS),JCLUS)=0
                 !	    WRITE (OUTTERM,*) JCLUS,NAT(JCLUS),I,RAD(I),DC
                 AR(NAT(JCLUS),JCLUS)=RAD(I)/DC
                 ISYM(NAT(JCLUS),JCLUS)=1
                 NSYM(NAT(JCLUS),JCLUS)=J+4
                 IF (J.EQ.2) THEN
                    IF (JMIR.EQ.2) ISYM(NAT(JCLUS),JCLUS)=-1
                    ATH(NAT(JCLUS),JCLUS)=PI
                 ENDIF
                 IDEG(NAT(JCLUS),JCLUS)=1
                 ANORM(NAT(JCLUS),JCLUS)=SQRT(real(JMIR,kind=dp))
                 ISHELL(NAT(JCLUS),JCLUS)=I
                 CALL POLTOCAR (AR(NAT(JCLUS),JCLUS),ATH(NAT(JCLUS),JCLUS),APHI(NAT(JCLUS),JCLUS), &
                      AX(NAT(JCLUS),JCLUS),AY(NAT(JCLUS),JCLUS),AZ(NAT(JCLUS),JCLUS))
              enddo
              GOTO 250
           ENDIF
	ENDIF
	IREV=1
	IF (JROT.LT.0.AND.MOD(JAXIS,2).EQ.0) THEN
           IREV=2
           JROT=-JROT/2
           JAXIS=JAXIS/2
	ENDIF
	NNSYM=0
	DO  L1=1,JTRIAD
           TTHETA1=TTHETA
           TPHI1=TPHI
           DO  L2=1,JCX
              TTHETA2=TTHETA1
              TPHI2=TPHI1
              DO  L3=1,JSX
                 TTHETA3=TTHETA2
                 TPHI3=TPHI2
                 DO  L4=1,IREV
                    TTHETA4=TTHETA3
                    TPHI4=TPHI3
                    LSYM=0
                    DO  L5=1,JMIR
                       TPHI5=TPHI4
                       TTHETA5=TTHETA4
                       DO  L6=1,JAXIS
                          IF (NAT(JCLUS).GE.IPARATOMS) THEN
                             CALL WTEXT ('Error, more than IPARATOMS atoms in list')
                             GOTO 260
                          ENDIF
                          NAT(JCLUS)=NAT(JCLUS)+1.
                          NNSYM=NNSYM+1
                          LSYM=LSYM+1
                          AR(NAT(JCLUS),JCLUS)=RAD(I)/DC
                          ATH(NAT(JCLUS),JCLUS)=TTHETA5
                          APHI(NAT(JCLUS),JCLUS)=TPHI5
                          IF (ABS(ATH(NAT(JCLUS),JCLUS)).LT.0.001.OR.ABS(ATH(NAT(JCLUS),JCLUS)-PI).LT.0.001) THEN
                             APHI(NAT(JCLUS),JCLUS)=0.
                          ELSEIF (APHI(NAT(JCLUS),JCLUS).GE.PI2) THEN
                             APHI(NAT(JCLUS),JCLUS)=APHI(NAT(JCLUS),JCLUS)-PI2
                          ELSEIF (APHI(NAT(JCLUS),JCLUS).LT.0) THEN
                             APHI(NAT(JCLUS),JCLUS)=APHI(NAT(JCLUS),JCLUS)+PI2
                          ENDIF
                          CALL POLTOCAR (AR(NAT(JCLUS),JCLUS),ATH(NAT(JCLUS),JCLUS),APHI(NAT(JCLUS),JCLUS), &
                               AX(NAT(JCLUS),JCLUS),AY(NAT(JCLUS),JCLUS),AZ(NAT(JCLUS),JCLUS))
                          ISYM(NAT(JCLUS),JCLUS)=LSYM
                          NSYM(NAT(JCLUS),JCLUS)=NNSYM
                          ISHELL(NAT(JCLUS),JCLUS)=I
                          IDEG(NAT(JCLUS),JCLUS)=JAXIS
                          IF (L5.EQ.2) IDEG(NAT(JCLUS),JCLUS)=-IDEG(NAT(JCLUS),JCLUS)
                          ANORM(NAT(JCLUS),JCLUS)=SQRT(real(JAXIS*JMIR,kind=dp))
                          TPHI5=TPHI5+PI2/real(JAXIS,kind=dp)
                          IF (TPHI5.GT.PI2) TPHI5=TPHI5-PI2
                          IF (JROT.LT.0) TTHETA5=PI-TTHETA5
                       enddo
                       TTHETA4=PI-TTHETA4
                    enddo
                    TTHETA3=PI-TTHETA3
                    TPHI3=TPHI3+PI/real(JAXIS,kind=dp)
                    IF (TPHI3.GT.PI2) TPHI3=TPHI3-PI2
                 enddo
                 IF (PGROUP(JCLUS).EQ.'Cx') THEN
                    TPHI2=PI-TPHI2
                 ELSE
                    TPHI2=PI2-TPHI2
                 ENDIF
                 IF (PGROUP(JCLUS).EQ.'Td') TPHI2=TPHI2+PID2
              enddo
              TTHETA1 =PI-TTHETA1
              IF (ISETTING.NE.2) THEN
                 TPHI1 =PI2-TPHI1
              ELSE
                 TPHI1=PI-TPHI1
                 IF (TPHI1.LT.0.) TPHI1=TPHI1+PI2
              ENDIF
           enddo
           CALL POLTOCAR (AR(NAT(JCLUS),JCLUS),TTHETA,TPHI,TX,TY,TZ)
           TU=TZ
           TZ=TY
           TY=TX
           TX=TU
           IF (PGROUP(JCLUS)(1:1).EQ.'O'.AND.N(I).EQ.12.AND.L1.EQ.2) TZ=-TZ
           CALL CARTOPOL (TX,TY,TZ,AR(NAT(JCLUS),JCLUS),TTHETA,TPHI)
        enddo
250     CONTINUE
     enddo
260  IF (NAT(JCLUS).NE.0) CALL GEOMCAL (6,QUIET,JCLUS,*280)
  enddo
  !	LC=IOR(LC,67)
  RETURN
275 PGROUP(JCLUS)=' '
  NPGR(JCLUS)=0
280 RETURN 1
290 FORMAT (1X,3A6/1X,7A6/1X,5A6/1X,5A6/1X,5A6/1X,6A6/1X,5A6/1X,11A6)
300 FORMAT ('Structures:'/)
310 FORMAT (4(1X,A10,' (',A3,') '))
END SUBROUTINE SYMMETRY
