MODULE Common_SETKW

  !      COMMON /SETKW/ SETKW,SETKWO,MINUS,GSETKW,GSETKWO,ISETMAX,
  !     1FTSETKW,FTSETKWO,IFTMINUS,IFTSETMAX
  !
  !      DIMENSION MINUS(IPARSETOPTS),ISETMAX(IPARSETOPTS),
  !     1IFTMINUS(IPARFTOPTS),IFTSETMAX(IPARFTOPTS)
  !
  !      CHARACTER*16 SETKW(IPARSETOPTS),SETKWO(5,IPARSETOPTS),GSETKW(IPARG
  !     1SET),GSETKWO(5,IPARGSET),FTSETKW(IPARFTOPTS),FTSETKWO(5,IPARFTOPTS)

  USE Definition
  Use Parameters
  implicit none
  private
  character*16, dimension(IPARSETOPTS), public :: SETKW  = (/ &
       'FINAL_STATE ', &
       'QUINN       ', &
       'DISORDER    ', &
       'WEIGHTING   ', &
       'POLARISATION', &
       'THEORY      ', &
       'CORRELATION ', &
       'ATOMIC_ABS  ', &
       'AVERAGE     ', &
       'RELCOR      ', &
       'WAVEVEC     ', &
       'RCOR        ', &
       'MS          ', &
       'MAPSIZE     ', &
       'EXCHANGE    ', &
       'DEBUG       ', &
       'GROUND_STATE', &
       'CONSTANT    ', &
       'SYM         ', &
       'SIGMA       ', &
       'VIEW        '/)
  character*16, dimension(5,IPARSETOPTS), public :: SETKWO 
  data SETKWO  /'ALL','L-1','L+1',' ',' ',        &
       'OFF','ON',' ',' ',' ',                    &
       'APPROX','EXACT_SS',' ',' ',' ',           &
       'NONE','K','K2','K3',' ',                  &
       'OFF','ON','EXACT',' ',' ',                &
       'CURVED_WAVE','SMALL_ATOM','TEST',' ',' ', &
       'OFF','ON','ALL',' ',' ',                  &
       'OFF','ON',' ',' ',' ',                    &
       'OFF','ON',' ',' ',' ',                    &
       'OFF','ON','LARGE',' ',' ',                &
       'AVERAGE','CENTRAL','SCATT',' ',' ',       &
       'OFF','ON',' ',' ',' ',                    &
       'OFF','ON','ALL',' ',' ',                  &
       'SMALL','LARGE',' ',' ',' ',               &
       'HEDIN-LUNDQVIST','XALPHA',' ',' ',' ',    &
       'OFF','ON',' ',' ',' ',                    &
       'XALPHA','VON_BARTH',' ',' ',' ',          &
       'NONE','V','RHO','FERMI_ENERGY','CHARGE',  &
       'OFF','ON',' ',' ',' ',                    &
       'OFF','ON',' ',' ',' ',                    &
       'AUTO','CURRENT',' ',' ',' '/
  character*16, dimension(IPARGSET),    public :: GSETKW                       !!!! never used    ????
  character*16, dimension(5,IPARGSET),  public :: GSETKWO 
  data GSETKWO /'FIXED','SELECT',' ',' ',' ',        &
       'TERMINAL','PLOTTER','NOTERM',' ',' ',        &
       'ALL','EXPERIMENT','THEORY','DIFFERENCE',' ', &
       'K','ENERGY',' ',' ',' ',                     &
       'NONE','K','K2','K3','K4',                    &
       'ON','OFF','BOTTOM',' ',' ',                  &
       'EMIN/EMAX','SELECT','YMIN/YMAX',' ',' ',     &
       'SELANAR','PERICOM','T4010','XT','XWIN',      &
       'LONG','MEDIUM','SHORT','NONE',' ',           &
       'NORMAL','SYMBOL','THICK',' ',' ',            &
       'OFF','ON',' ',' ',' ',                       &
       'ON','OFF',' ',' ',' ',                       &
       'ON','OFF','LARGE',' ',' ',                   &
       'ON','OFF','LARGE',' ',' ',                   &
       'HP','PS','HP_FILE','PS_FILE',' '/
  character*16, dimension(  IPARFTOPTS), public :: FTSETKW
  data FTSETKW  /'FTPHASE','FTWEIGHT','FTWIND','FTMULT','FTCENT','RDF','TRANSFORM','NOTHING'/
  character*16, dimension(5,IPARFTOPTS), public :: FTSETKWO 
  data  FTSETKWO /'FIRST-SHELL','SECOND','NONE',' ',' ',           &
       'NONE','K','K2','K3','k/fpim',                              &
       'BLACKMAN-HARRIS','GAUSSIAN','HANNING','KAISER-BESSEL',' ', &
       'ONE','R-SQUARED',' ',' ',' ',                              &
       'AUTO','K','ENERGY',' ',' ',                                &
       'ATOMIC','ELECTRON','ATOMIC/R^2',' ',' ',                   &
       'MODULUS','SINE+MODULUS',' ',' ',' ',                       &
       'OFF','ON',' ',' ',' '/
  integer(i4b), dimension(IPARSETOPTS), public :: MINUS     = (/1,1,1,1,1,0,1,1,1,1,0,1,1,1,0,1,1,1,1,1,1/)
  integer(i4b), dimension(IPARSETOPTS), public :: ISETMAX   = (/3,2,2,4,3,3,3,2,2,3,3,4,3,2,2,2,2,5,2,2,2/)
  integer(i4b), dimension(IPARFTOPTS),  public :: IFTMINUS  = (/0,1,0,0,0,0,0,1/)
  integer(i4b), dimension(IPARFTOPTS),  public :: IFTSETMAX = (/3,5,4,2,3,3,2,1/)
END MODULE Common_SETKW
