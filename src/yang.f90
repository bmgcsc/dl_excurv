FUNCTION YANG (K)
  !======================================================================C
  Use Parameters
  Use Include_IPA
  Use Include_PA1

  J=IPLA(K)
  YANG=ANG(J)
  IF (YANG.GT.180.) YANG=YANG-360.
  IF (J.EQ.0) YANG=0.
  RETURN
END FUNCTION YANG
