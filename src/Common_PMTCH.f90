MODULE Common_PMTCH

  !      COMMON /PMTCH/ IAL(IPARGSET),GTITLE
  !      CHARACTER IAL*12,GTITLE*48

  Use Definition
  Use Parameters
  implicit none
  private
  character*12, dimension(IPARGSET), public :: IAL
  data IAL /'FRAMES','DEVICE'  ,'SPECTRUM','X-AXIS','PWEIGHT'  ,'AXES'  , &
            'RANGE' ,'TERMINAL','TABLE'   ,'LINE'  ,'MAXOUTPUT','PROMPT', &
            'LABELS',' '       ,'PLOTTER'/
  character*48, public :: GTITLE
end MODULE Common_PMTCH
