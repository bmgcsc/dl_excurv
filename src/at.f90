COMPLEX FUNCTION AT (ZZ)
  !======================================================================C
  !	Returns t-matrix element .5 exp(2i ZZ)-1. ZZ=phaseshift delta(l)
  !       
  COMPLEX ZZ
  AT=0.5*(CEXP(2.0*CMPLX(-AIMAG(ZZ),REAL(ZZ)))-1.0)
  RETURN
END FUNCTION AT
