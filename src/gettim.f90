SUBROUTINE GETTIM (IFLAG,CPU,ELAPS)
  !======================================================================C
  !       
  !	Return cpu time and elapsed time. Times are intervals
  !	from the initial call with iflag=0 and are in seconds.
  !       
  !       IFLAG (I/O) =0, INITIALISE, =1, RETURN TIMES, =-1 DUMMY CALL
  !       RETURNS -1 IF TIME NOT AVAILABLE
  !       CPU (O)   CPU TIME IN SECONDS
  !       ELAPS (O)   ELAPSED TIME IN SECONDS
  !       
  !	If time not available then iflag is returned as -1 and cpu and
  !	elaps are set to zero
  !       
  INTEGER   TIM0,STIME
  DIMENSION TARRAY(2)
  COMMON /TIMSAV/ TIM0,CPUX
  SAVE TIMSAV
  EXTERNAL STIME
  IF (IFLAG.EQ.0) THEN
     ELAPS = 0.0
     CPU   = 0.0
     CPUX  = DTIME(TARRAY)
     CPUX  = 0.0
     TIM0  = STIME()
  ELSE
     ELAPS = STIME()-TIM0
     CPU   = DTIME(TARRAY)+CPUX
     CPUX  = CPU
  ENDIF
  RETURN
END SUBROUTINE GETTIM
