Module Common_GEOMETRY
  !      COMMON /GEOMETRY/DIST(IPARATOMS,IPARATOMS,IPARNCLUS),ANGLE(IPARATO
  !     1MS,IPARATOMS,IPARNCLUS)
  Use Definition
  Use Parameters
  implicit none
  private
  real(dp), dimension(:,:,:), public ::  DIST(IPARATOMS,IPARATOMS,IPARNCLUS)
  real(dp), dimension(:,:,:), public :: ANGLE(IPARATOMS,IPARATOMS,IPARNCLUS)
end Module Common_GEOMETRY

