SUBROUTINE FINDPAR (CPAR,IFINDPAR,*)
  !======================================================================C
  Use Parameters
  Use Common_VAR
  Use Include_PA1

  CHARACTER*(*) CPAR
  DO I=1,IPARNPARAMS
     IF (CPAR.EQ.IVARS(I)) GOTO 10
  ENDDO
  L=NCSTR(CPAR)
  IF (CPAR(L:L).EQ.'0') THEN
     L=L-1
     DO I=1,IPARNPARAMS
        IF (CPAR(1:L).EQ.IVARS(I)) GOTO 10
     ENDDO
  ENDIF
  RETURN 1
10 IFINDPAR=I
  RETURN
END SUBROUTINE FINDPAR
