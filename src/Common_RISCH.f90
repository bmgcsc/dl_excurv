MODULE Common_risch

  ! Module to replace COMMON block /RISCH/
  !     COMMON /RISCH/ RIS,MDSN

  USE Definition
  implicit none
  private
  character*120, public :: RIS  = ' '
  character*60 , public :: MDSN = ' '
END MODULE Common_risch
