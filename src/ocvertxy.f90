SUBROUTINE OCVERTXY (DN,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about plane //z at 45 to x
  !
  Use Common_convia
  DIMENSION DN(36,*)
  DN(1,3)=PID2-DN(1,3)
  TU=DN(1,4)
  DN(1,4)=DN(1,5)
  DN(1,5)=TU
  IS=IS+1
  RETURN 1
END SUBROUTINE OCVERTXY
