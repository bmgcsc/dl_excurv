DOUBLE PRECISION FUNCTION VXALPH (DEXV,D,PI)
  Use Definition
  IMPLICIT real(float) (A-H,O-Z)
  SAVE
  !       
  !	D=density
  !	VXALPHA in Hartrees , based on formula 17.26 in Ashcroft
  !	and Mermin, Solid State Physics.
  !       
  VXALPH=-3.0*DEXV*((3.0*D/(8.0*PI))**(1.0/3.0))
  RETURN
END FUNCTION VXALPH
