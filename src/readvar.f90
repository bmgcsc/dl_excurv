SUBROUTINE READVAR
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_ICF
  Use Include_PA1
  Use Common_DLV

  CHARACTER*64 FILE

  CALL FFOPENQ (INFILE,trim(DSN_path)//'excurve.def',1,1,*20)
  J=1
  I1=11+IPARNSV*(IPARNS+1)
  I2=I1+IPARNSPV*(IPARNSP+1)
  I3=I2+IPARNP*IPARNPV
10 IF (J.GE.11.AND.J.LT.I1) THEN
     LEN=IPARNS+1
  ELSEIF (J.GE.I1.AND.J.LT.I2) THEN
     LEN=IPARNSP+1
  ELSEIF (J.GE.I2.AND.J.LT.I3) THEN
     LEN=IPARNP
  ELSE
     LEN=1
  ENDIF
  READ (INFILE,30) IVARS(J),PA1(J),RMS(J),RMX(J),ICF(J),LCS(J)
  IF (LEN.GT.1) THEN
     DO I=J+1,J+LEN-1
        IVARS(I)=IVARS(J)
        PA1(I)=PA1(J)
        RMS(I)=RMS(J)
        RMX(I)=RMX(J)
        ICF(I)=ICF(J)
        LCS(I)=LCS(J)
     ENDDO
  ENDIF
  J=J+LEN
  IF (J.LE.IPARNPARAMS) GOTO 10
  R(0)=0.
  UX(0)=0.
  CLUS(0)=-1.
  RN(0)=1.
  CLOSE (INFILE)
20 RETURN
30 FORMAT (1X,A8,3E12.4,2I3)
END SUBROUTINE READVAR
