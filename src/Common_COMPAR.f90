MODULE Common_COMPAR

  !      COMMON /COMPAR/ KEYWORD,CHAROPT,NUMFLAG,INTOPT,FP
  !     1OPT,LKEYWORD,COMWORD
  !      CHARACTER KEYWORD*64,CHAROPT*24,COMWORD*12


  Use Definition
  implicit none
  private
  character*64, public :: KEYWORD
  character*24, public :: CHAROPT
  integer(i4b), public :: NUMFLAG
  integer(i4b), public :: INTOPT
  real(dp),     public :: FPOPT
  integer(i4b), public :: LKEYWORD
  character*12, public :: COMWORD
end MODULE Common_COMPAR
