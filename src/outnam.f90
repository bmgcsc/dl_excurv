SUBROUTINE OUTNAM (ITYP,DSN,MEMBER,*)
  !======================================================================C
  Use Common_DLV
  Use Parameters
  Use Common_extension
  Use Common_B1
  Use Common_B2
  Use Common_NUMF
  Use Common_RCBUF
  !
  !	Set up file names (supplying appropriate defaults) for the
  !	output files from excurve. the file names returned must be
  !	compatible with the requirements of the subroutine 'filopn' which
  !	will be used to open the files. additionaly or alternatively, user
  !	defined file names may be read in by the subroutine.
  !
  !	PARAMETERS
  !
  !	   ITYP (I)   FILE TYPE  =11    OUTPUT SPECTRUM FILE
  !	                         =12    OUTPUT FFT FILE
  !	                         =13    OUTPUT PARAMETERS FILE
  !	                         =14    OUTPUT PLOT FILE
  !	                         =15    FILE FOR BACKGROUND JOB
  !	                         =16    OUTPUT MATRIX ELEMENTS
  !	                         =17    OUTPUT COORDINATES
  !	                         =18    OUTPUT XANES CONTROL DATA
  !	                         =19    SPARE
  !	                         =20+   OUTPUT PHASE SHIFTS FILES
  !	                                (1 to IPARNP )
  !	    DSN (I/O) FILE NAME. TYPE=CHARACTER. MAY BE UP
  !	              TO 60 CHARACTERS IN LENGTH AND MAY CONTAIN
  !	              TRAILING SPACES. ALWAYS SET ON OUTPUT. ON INPUT
  !	              IT NEED ONLY BE SET FOR PHASE SHIFT FILES (ITYP =
  !	              20 TO 20+IPARNP-1 ) WHEN DSN SHOULD BE
  !	              SET TO BLANK FOR THE FIRST MEMBER OF A SET OF PHASE
  !	              SHIFT FILES AND SHOULD BE SET TO THE PREVIOUSLY
  !	              RETURNED VALUE FOR SUBSEQUENT MEMBERS
  !	 MEMBER (I/O) MEMBER NAME IF RELEVANT. TYPE=CHARACTER. MAX OF
  !	              12 CHARACTERS ALLOWED. MAY CONTAIN TRAILING
  !	              SPACES. FOR PHASE SHIFT OUTPUT FILES ONLY (ITYP = 20
  !	              -25) A DEFAULT MEMBER NAME WILL BE SUPPLIED
  !	              WHEN THE ROUTINE IS CALLED.
  !
  ! SEQUENCE NUMBERS DERIVED FROM A TWO CHARACTER RUN CODE ARE AVAILABLE
  ! FOR INCREMENTING AND CONVERTING BACK TO A RUN CODE OF THE FORM
  ! xn (WHERE x IS AN ALPHABETIC CHARACTER AND n IS A DIGIT FROM 1 TO
  ! 9) TO BE USED IN THE DEFAULT FILE NAMES. a1=1,a2=2...z9=234. THE
  ! SEQUENCE NUMBERS ARE INITIALISED IN THE SUBROUTINE 'OPTION' TO THE
  ! START SEQUENCE NUMBER -1. THE SEQUENCE NUMBERS ARE HELD FOR THE
  ! FOLLOWING FILES.
  !	                 NUMF(1) OUTPUT SPECTRUM FILE
  !	                 NUMF(2) OUTPUT FFT FILE
  !	                 NUMF(3) OUTPUT PARAMETERS FILE
  !	                 NUMF(4) OUTPUT PLOT FILE
  !	                 NUMF(5) OUTPUT FILE FOR BACKGROUND JOB AND
  !	                         OUTPUT FILES FROM BACKGROUND JOB
  !	                 NUMF(6) OUTPUT MATRIX ELEMENTS FILE
  !	                 NUMF(7) OUTPUT COORDINATES
  !	                 NUMF(8) OUTPUT XANES CONTROL DATA
  !	                 NUMF(9) SPARE
  !	                 NUMF(10)OUTPUT PHASE SHIFTS FILE
  !
  ! IN THIS VERSION THE DEFAULT FILE NAMES ARE AS FOLLOWS WHERE a1 IS
  ! AN EXAMPLE OF THE RUN CODE. THE RUN CODE IS IN GENERAL INCREMENTED
  ! TILL A NEW FILE NAME IS FOUND. THE SUBROUTINE ALLOWS THE DEFAULT NAMES
  ! TO BE REPLACED BY THE USER'S CHOICE OF NAME IF REQUIRED.
  !
  !	     exouta1.dat         SPECTRUM FILE
  !	     exffta1.dat         FFT FILE
  !	     expara1.dat         PARAMETERS DATA FILE
  !	     explota1.unf        PLOT FILE
  !	     exca1.unf           FILE FOR BACKGROUND JOB
  !	     exmtxa1.dat         MATRIX ELEMENTS
  !	     exbrka1.dat         BROOKHAVEN COORDINATES
  !	     exanesa1.con        XANES CONTROL DATA
  !	     extaba1.unf         TABLES
  !	     exphsa1.xxx         THE FILE EXTENSION xxx IS DERIVED FROM
  !	                         THE MEMBER NAME BY TAKING THE FIRST
  !	                         THREE CHARACTERS OR USING at2, at3 ETC.
  !	                         IF THE MEMBER NAMES ARE atom2, atom3 ETC
  CHARACTER*(*) DSN,MEMBER
  CHARACTER*60 DSN1,DSN2
  CHARACTER*80 PROMPT
  CHARACTER*39 NAM
  CHARACTER*12 MEM1
  CHARACTER*16 FNAM(13)
  CHARACTER*8 EXT
  CHARACTER*4 IP1,IP2,IP3
  LOGICAL FILOLD
  DATA NAM/'abcdefghijklmnopqrstuvwxyz#@_0123456789'/
  DATA FNAM/ &
       'exouta1.dat','exffta1.dat','expara1.dat','explota1.ps', &
       'exca1.unf','exmtxa1.dat','exbrka1.dat','exanesa1.con','extaba1.dat', &
       'exphsa1.xxx','exouta1.xrd','exlista1','excora1.dat'/
  !
  !	Prepare file names
  !
  JJ=ITYP-10
  NUMOLD=NUMF(JJ)
  IF (ITYP.EQ.20) CALL LWCASE(MEMBER)
  IF (ITYP.EQ.20.AND.DSN.NE.' ') THEN
     DO I1=NCSTR(DSN),1,-1
        IF (DSN(I1:I1).EQ.'.') GOTO 20
     ENDDO
     I1=0
20   DSN(I1+1:)=MEMBER
     GOTO 21
  ENDIF
  DSN=FNAM(JJ)
10 NUMF(JJ)=NUMF(JJ)+1
  IF (NUMF(JJ).GT.234) NUMF(JJ)=234
  I1=6
  IF (JJ.EQ.5) I1=4
  IF (JJ.EQ.4.OR.JJ.EQ.8.OR.JJ.EQ.12) I1=7
  IALPH=(NUMF(JJ)-1)/9
  DSN(I1:I1)=NAM(IALPH+1:IALPH+1)
  IDIG=NUMF(JJ)-9*IALPH
  DSN(I1+1:I1+1)=NAM(IDIG+30:IDIG+30)
  IF (ITYP.EQ.20) DSN(I1+3:)=MEMBER
  IF (FILOLD(DSN).AND.NUMF(JJ).NE.234) GOTO 10
21 IF (ITYP.EQ.15) THEN
     DSN(7:10)=EXTLOG
     IF (FILOLD(DSN).AND.NUMF(JJ).NE.234) GOTO 10
     DSN(7:10)=EXTDATA
     IF (FILOLD(DSN).AND.NUMF(JJ).NE.234) GOTO 10
     DSN(7:10)=EXTUNFM
  ELSEIF (ITYP.EQ.18) THEN
     DSN(10:13)=EXTATOM
     IF (FILOLD(DSN).AND.NUMF(JJ).NE.234) GOTO 10
     DSN(10:13)=EXTLOG
     IF (FILOLD(DSN).AND.NUMF(JJ).NE.234) GOTO 10
     DSN(10:13)=EXTDATA
     IF (FILOLD(DSN).AND.NUMF(JJ).NE.234) GOTO 10
     DSN(10:13)=EXTCONT
     !	ELSEIF (ITYP.EQ.20) THEN
     !	  I1=NCSTR(DSN)
  ELSE
     I1=NCSTR(DSN)
     !	  IF (ITYP.NE.14) I1=I1+6
  ENDIF
  !
  !	Read in user defined file name to override default if required
  !
29 IF (ITYP.NE.14) THEN
30   PROMPT='Filename: ['//DSN(1:I1+1)//'] ?'
40   if(.not.DLV_flag) CALL WTEXT (PROMPT)
     CALL AREAD(ITYP,DSN1,LLD,*50,*100)
     GOTO 60
50   IF (BUF.NE.' ') GOTO 40
     GOTO 70
60   IF (ITYP.NE.20) THEN
        DSN1(LLD+1:)=DSN(I1+2:)
        DSN=DSN1
        I1=LLD-1
     ELSE
        DSN=DSN1
        I1=NCSTR(DSN)
     ENDIF
  ENDIF
70 continue
  if(.not.DLV_flag)then
     IF (FILOLD(DSN)) THEN
        CALL ERRS (*80)
80      CALL WTEXT ('File already exists: do you wish to overwrite ?')
        CALL CREAD (IP1,IP2,IP3,INT,IC,V,*80,*29)
        IF (IP1(1:1).NE.'Y') GOTO 29
     ENDIF
  endif
  IF (ITYP.EQ.15) THEN
     DSN2=DSN(1:LLD)//'.'//EXTDATA
     IF (FILOLD(DSN2)) THEN
        CALL ERRS (*90)
90      CALL WTEXT ('Delete '//DSN2(1:LLD+5)//' ?')
        CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*90,*29)
        IF (IP1(1:1).NE.'Y') GOTO 29
        !-st	    ISTAT=SYSTEM('/bin/rm '//DSN2//char(0))
     ENDIF
  ENDIF
  IF (JJ.NE.5) NUMF(JJ)=NUMOLD
  RETURN
100 IF (JJ.NE.5) NUMF(JJ)=NUMOLD
  RETURN 1
END SUBROUTINE OUTNAM
