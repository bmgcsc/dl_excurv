SUBROUTINE TABLE
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_COMPAR
  Use Common_IP
  Use Common_SPARS
  Use Index_INDS
  Use Common_LSQ
  Use Common_RCBUF
  Use Common_PRDSN
  Use Include_PA1

  DIMENSION ILIST(0:IPARNS)
  DIMENSION LABS(100),ICOL(100),ELABS(100)
  CHARACTER*120 TTITLE
  CHARACTER*15 IP1,IP2,IP3,LABS,ELABS,CLABS(100),TLABS
  CHARACTER*1 DASHES(256)
  LOGICAL OPEN,ERRORS
  DATA DASHES/256*'_'/

  OPEN=.FALSE.
  ERRORS=.FALSE.
  IF (KEYWORD(1:1).EQ.'E') ERRORS=.TRUE.
  JFIELD=11
  IF (ERRORS) JFIELD=18
  CALL OUTNAM(19,PRDSN,' ',*10)
  CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*10)
  OPEN=.TRUE.
10 IF (LRIS+LMDSN.EQ.0) CALL WTEXT ('Enter title []')
  CALL SREAD (*20,*40)
20 TTITLE=BUF
  CALL WTEXT ('Enter column labels or ''='' to end list:')
  DO I=1,100
     IF (LRIS+LMDSN.EQ.0) CALL WTEXTI('Enter label for column ',I)
     LABS(I)=' '
     CALL SREAD (*30,*40)
     NLAB=NCSTR(BUF)
     IDIFF=MAX0(1,MIN0(15,JFIELD)-NLAB)
     LABS(I)(IDIFF:)=BUF
30   DO II=1,15
	IF (LABS(I)(II:II).EQ.'_') LABS(I)(II:II)=' '
     ENDDO
  ENDDO
40 NGEN=I-1
  IF (NGEN.LE.0) GOTO 100
  CALL WTEXT ('Enter variables to be listed or ''='' to end list:')
  DO J=1,100
50   IF (LRIS+LMDSN.EQ.0) CALL WTEXTI('Enter variable for row ',J)
     CALL CREAD (IP1,IP2,IP3,INT,IC,V,*50,*70)
     CLABS(J)=IP1
     LIP1=NCSTR(IP1)
     IP3=IP1
     LIP3=LIP1
     CALL GETLIST (IP3,LIP3,NLIST,ILIST)
     DO I=1,IPARNPARAMS
	IF (IP3.EQ.IVARS(I)) GOTO 60
     ENDDO
     IF (IP1(1:LIP1).EQ.'FI') THEN
        I=IPARNPARAMS+1
     ELSEIF (IP1(1:LIP1).EQ.'R') THEN
        I=IPARNPARAMS+2
     ELSEIF (IP1(1:LIP1).EQ.'REX') THEN
        I=IPARNPARAMS+3
     ELSEIF (IP1(1:LIP1).EQ.'RD') THEN
        I=IPARNPARAMS+4
     ELSEIF (IP1(1:LIP1).EQ.'NPT') THEN
        I=IPARNPARAMS+5
     ELSEIF (IP1(1:LIP1).EQ.'CHI') THEN
        I=IPARNPARAMS+6
     ELSE
        CALL ERRMSG ('Variable not found: '//IP1,*50)
     ENDIF
60   ICOL(J)=I
  ENDDO
70 CONTINUE
  IF (J.LE.1) GOTO 100
  !-st	KK=MIN0(NSAVE,NGEN)
  KK=MIN(NSAVE,NGEN)
  NGENI=NGEN*JFIELD+12
  !	WRITE (88,*) NGEN,NGENI,JFIELD,DASHES
  IF (NGENI.GT.80) CALL COL132
  WRITE (OUTTERM,'(12X,A)') TTITLE(1:NCSTR(TTITLE))
  IF (ERRORS) THEN
     WRITE (OUTTERM,120) (LABS(I),I=1,NGEN)
  ELSE
     WRITE (OUTTERM,110) (LABS(I),I=1,NGEN)
  ENDIF
  WRITE (OUTTERM,130) (DASHES(II),II=1,NGENI)
  WRITE (OUTTERM,*)
  IF (OPEN) THEN
     WRITE (OUTFILE,'(12X,A/)') TTITLE(1:NCSTR(TTITLE))
     IF (ERRORS) THEN
        WRITE (OUTFILE,120) (LABS(I),I=1,NGEN)
     ELSE
        WRITE (OUTFILE,110) (LABS(I),I=1,NGEN)
     ENDIF
     WRITE (OUTFILE,130) (DASHES(II),II=1,NGENI)
     WRITE (OUTFILE,*) ' '
  ENDIF
  DO I=1,J-1
     K=ICOL(I)
     IF (K.LT.inds(INDN).OR.K.GE.inds(INDPHI)+IPARNS+1) THEN
        JSHELL=-1
     ELSE
        JSHELL=MOD(K-inds(INDN),IPARNS+1)
     ENDIF
     IF (K.LE.IPARNPARAMS) THEN
        JJ=0
        IP1=' '
        DO II=1,15
           IF (CLABS(I)(II:II).NE.'['.AND.CLABS(I)(II:II).NE.']') THEN
              JJ=JJ+1
              IP1(JJ:JJ)=CLABS(I)(II:II)
              IF (IP1(JJ:JJ).EQ.',') IP1(JJ:JJ)='/'
           ENDIF
        ENDDO
        IF (K.GE.inds(INDANG).AND.K.LT.inds(INDANG)+IPARNS+1) THEN
           IP1='B0-1-'//IVARS(K)(4:5)
        ENDIF
        IF (K.EQ.inds(INDNIND))  IP1='Ni'
        IF (K.EQ.inds(INDNPARS)) IP1='p'
     ELSEIF (K.EQ.IPARNPARAMS+1) THEN
        IP1='Fit*10^3'
     ELSEIF (K.EQ.IPARNPARAMS+2) THEN
        IP1='R(total)'
     ELSEIF (K.EQ.IPARNPARAMS+3) THEN
        IP1='R(exafs)'
     ELSEIF (K.EQ.IPARNPARAMS+4) THEN
        IP1='R(distance)'
     ELSEIF (K.EQ.IPARNPARAMS+5) THEN
        IP1='NPT'
     ELSEIF (K.EQ.IPARNPARAMS+6) THEN
        IP1='Chi^2*10^6'
     ENDIF

     DO II=1,KK
	ELABS(II)=' '
	EE=-1.
	IF (ERRORS) THEN
           DO JJ=1,IPARNVARS
              TLABS=SIVCH(JJ,II)
              IF (TLABS.EQ.' ') GOTO 90
              DO KI=1,15
                 IF (TLABS(KI:KI).EQ.'[') THEN
                    I1=KI+1
                    TLABS(KI:KI)=TLABS(I1:I1)
                    IF (TLABS(I1+1:I1+1).GE.'0'.AND.TLABS(I1+1:I1+1).LE.'9') THEN
                       I1=I1+1
                       TLABS(KI+1:KI+1)=TLABS(I1:I1)
                    ENDIF
                    TLABS(I1:)=' '
                    GOTO 80
                 ENDIF
              ENDDO
80            IF (TLABS.EQ.IVARS(K)) THEN
                 EE=SEVEC(JJ,II)
                 GOTO 90
              ENDIF
           ENDDO
	ENDIF
90	IF (SPARS(IPARNPARAMS+1,II).NE.-1.) THEN
           IF (JSHELL.GE.0.AND.SPARS(JSHELL+inds(INDN),II).EQ.0.) THEN
              LABS(II)=' '
           ELSE
              IF (K.GE.inds(INDANG).AND.K.LT.inds(INDANG)+IPARNS+1) THEN
                 WRITE (LABS(II),'(f11.1)') SPARS(K,II)
                 IF (EE.NE.-1.) THEN
                    IF (EE.LT.99.95) THEN
                       WRITE (ELABS(II),'('' ('',F4.1,'')'')') EE
                    ELSE
                       WRITE (ELABS(II),'('' ('',F4.0,'')'')') EE
                    ENDIF
                    CALL FILLIN (ELABS(II)(4:7))
                 ENDIF
	      ELSEIF ((K.GE.inds(INDA).AND.K.LT.inds(INDUN)).OR.K.EQ.IPARNPARAMS+1) THEN
                 WRITE (LABS(II),'(f11.3)') SPARS(K,II)
                 IF (EE.NE.-1.) THEN
                    IEE=ifix(EE*1000.)
                    IF (IEE.LT.1000) THEN
                       WRITE (ELABS(II),'('' ('',1x,I3,'')'')') IEE
                    ELSE
                       WRITE (ELABS(II),'('' ('',F4.2,'')'')') EE
                    ENDIF
                    CALL FILLIN (ELABS(II)(4:7))
                 ENDIF
              ELSE
                 WRITE (LABS(II),'(f11.2)') SPARS(K,II)
                 IF (EE.NE.-1.) THEN
                    IF (EE.LT.9.995) THEN
                       WRITE (ELABS(II),'('' ('',F4.2,'')'')') EE
                    ELSE
                       WRITE (ELABS(II),'('' ('',F4.1,'')'')') EE
                    ENDIF
                    CALL FILLIN (ELABS(II)(4:7))
                 ENDIF
              ENDIF
           ENDIF
	ELSE
           IF (K.GE.inds(INDXE).OR.K.LT.11) THEN
              LABS(II)=' '
           ELSEIF (K.GE.inds(INDANG).AND.K.LT.inds(INDANG)+IPARNS+1) THEN
              WRITE (LABS(II),'(''('',f3.1,'')'')') SPARS(K,II)
              IF (EE.NE.-1.) THEN
                 IF (EE.LT.99.95) THEN
                    WRITE (ELABS(II),'('' ('',F4.1,'')'')') EE
                 ELSE
                    WRITE (ELABS(II),'('' ('',F4.0,'')'')') EE
                 ENDIF
                 CALL FILLIN (ELABS(II)(2:7))
              ENDIF
           ELSEIF (K.LT.inds(INDUN)) THEN
              WRITE (LABS(II),'(''('',f4.3,'')'')') SPARS(K,II)
              IF (EE.NE.-1.) THEN
                 IEE=ifix(EE*1000.)
                 IF (IEE.LT.1000) THEN
                    WRITE (ELABS(II),'('' ('',1x,I3,'')'')') IEE
                 ELSE
                    WRITE (ELABS(II),'('' ('',F4.2,'')'')') EE
                 ENDIF
                 CALL FILLIN (ELABS(II)(4:7))
              ENDIF
           ELSE
              WRITE (LABS(II),'(''('',f3.2,'')'')') SPARS(K,II)
              IF (EE.NE.-1.) THEN
                 IF (EE.LT.9.995) THEN
                    WRITE (ELABS(II),'('' ('',F4.2,'')'')') EE
                 ELSE
                    WRITE (ELABS(II),'('' ('',F4.1,'')'')') EE
                 ENDIF
                 CALL FILLIN (ELABS(II)(4:7))
              ENDIF
           ENDIF
	ENDIF
     ENDDO
     IF (IP1(1:1).NE.IP2(1:1).OR.(IP1(2:2).LE.'9'.AND.IP2(2:2).GT.'9').AND.OPEN) WRITE (OUTFILE,*)
     IF (.NOT.ERRORS) THEN
        IF (OPEN) WRITE (OUTFILE,140) IP1,(LABS(J),J=1,KK)
        WRITE (OUTTERM,140) IP1,(LABS(J),J=1,KK)
     ELSE
        IF (OPEN) WRITE (OUTFILE,150) IP1,(LABS(J),ELABS(J),J=1,KK)
        WRITE (OUTTERM,150) IP1,(LABS(J),ELABS(J),J=1,KK)
     ENDIF
     IP2=IP1

  ENDDO
  IF (OPEN) WRITE (OUTFILE,130) (DASHES(II),II=1,NGENI)
  IF (NGENI.GT.80) CALL COL80
100 IF (OPEN) THEN
     CALL WTEXT (' ')
     CALL EFILE ('Parameter Table')
  ENDIF
  RETURN
110 FORMAT (13X,11A11/)
120 FORMAT (13X,11A18/)
130 FORMAT (256A1)
140 FORMAT (A12,11A11)
150 FORMAT (A12,11(A11,A7))
END SUBROUTINE TABLE
