SUBROUTINE COVAR (A,N,M,FSA,DSTEP,FSM,E,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_DLV
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  Use Common_IP
  Use Common_PRDSN
  !
  !	Calculate covarience matrix and statistical errors from
  !	the jacobian approximation. Similar to method of Cox (1981).
  !	Only works if near a minimum and step parameter well chosen
  !
  DIMENSION :: A(N,M),B(IPARNVARS,IPARNVARS),CQ(IPARNVARS,IPARNVARS*2)
  dimension :: V(IPARNVARS,IPARNVARS),DST(IPARNVARS),E(IPARNVARS),LP(IPARNVARS)
  real(float) :: B,CQ,DIV,RAT,TT,DT
  CHARACTER*6 :: PRINTLAB6

  ICOUT=LOGFILE
  ICOUTDIFF=1
  IF (.NOT.BACKGROUND) THEN
     if(.not.DLV_flag) CALL OUTNAM (23,PRDSN,' ',*3)
     if(.not.DLV_flag) CALL FILEOPEN (PRDSN,OUTFILE,'FORMATTED',' ',*3)
     ICOUT=OUTFILE
3    ICOUTDIFF=ICOUT-OUTTERM
  ENDIF
  CALL DZERO (B,IPARNVARS*IPARNVARS)
  CALL DZERO (CQ,IPARNVARS*IPARNVARS*2)

  DO I=1,NCALL
     B(I,I)=1.
  enddo
  DO I=1,NCALL
     DO J=1,NCALL
	TT=SF(I)*SF(J)
	DO K=1,M
           CQ(I,J)=CQ(I,J)+A(I,K)*A(J,K)/TT
        enddo
     enddo
  enddo
  WRITE (LOGFILE,*) FSA,(SF(I),I=1,NCALL)
  DO  I=1,NCALL
     WRITE (LOGFILE,'(A,10E14.6)') 'IN ',(CQ(I,J),J=1,NCALL)
  enddo
  CALL CROUT (IPARNVARS,NCALL,0,CQ,1.D-12,DT,IERR,LP)
  DO I=1,NCALL
     DO J=1,NCALL
	B(I,J)=CQ(I,J+NCALL)
     ENDDO
  ENDDO
  !	DO 60 I=1,NCALL
  !	DIV=CQ(I,I)
  !	IF (DIV.EQ.0.) THEN
  !	  WRITE (LOGFILE,170) I
  !	  WRITE (OUTTERM,170) I
  !	  DIV=1E-10
  !	ENDIF
  !	DO 30 J=1,NCALL
  !	CQ(I,J)=CQ(I,J)/DIV
  !  30	B(I,J)=B(I,J)/DIV
  !	DO 50 J=1,NCALL
  !	IF (J.EQ.I) GOTO 50
  !	RAT=CQ(J,I)
  !	DO 40 K=1,NCALL
  !	CQ(J,K)=CQ(J,K)-RAT*CQ(I,K)
  !  40	B(J,K)=B(J,K)-RAT*B(I,K)
  !  50	CONTINUE
  !  60	CONTINUE
  DO I=1,NCALL
     WRITE (LOGFILE,'(A,10E14.6)') 'OUT',(B(I,J),J=1,NCALL)
  enddo
  DO  I=1,NCALL
     DO  J=1,NCALL
   	B(I,J)=B(I,J)*FSA/real(M-NCALL,kind=dp)
     enddo
  enddo
  DO I=1,NCALL
     IF (B(I,I).LE.0) GOTO 120
     E(I)=SQRT(B(I,I))*2.
     REME(IVNUMS(I))=E(I)/DSTEP
     DST(I)=DSTEP*SF(I)
  enddo
  DO  I=1,NCALL
     DO  J=1,NCALL
   	V(I,J)=B(I,J)/SQRT(B(I,I)*B(J,J))
     enddo
  enddo
  NCOLS=NCALL
  DO IW=OUTTERM,ICOUT,ICOUTDIFF
     IF (IW.EQ.OUTFILE) THEN
        if(DLV_flag)cycle
     endif
     WRITE (IW,190)
     WRITE (IW,200) (PRINTLAB6(IVCH(I)),I=1,NCALL)
     DO I=1,NCALL
	WRITE (IW,210) PRINTLAB6(IVCH(I)),(V(I,J),J=1,NCOLS)
     enddo
     !	WRITE (IW,*) ' Unnormalised covariance matrix'
     !	WRITE (IW,210) (IVCH(I),I=1,NCALL)
     !	DO 110 I=1,NCALL
     !	WRITE (IW,220) IVCH(I),(V(I,J),J=1,NCOLS)
     ! 110	CONTINUE
     WRITE (IW,220) (RU(I),I=1,NCALL)
     WRITE (IW,230) (DST(I),I=1,NCALL)
     WRITE (IW,250) (E(I),I=1,NCALL)
     WRITE (IW,260) FSM
  enddo
  JQ=iset(IWEIGHT)
  WRITE (LOGFILE,181) JQ,FITINDEX,RFAC,CHISQU,REXAFS,RDISTANCE
  GOTO 165
120 continue
  DO IW=OUTTERM,ICOUT,ICOUTDIFF
     IF (IW.EQ.OUTFILE) THEN
        if(DLV_flag)cycle
     endif
     WRITE (IW,180) M,NCALL
  enddo
  WRITE (LOGFILE,*) DIV,RAT,FSA,(SF(I),I=1,NCALL)
  DO I=1,NCALL
     WRITE (LOGFILE,240) 'A',(A(I,J),J=1,NCALL)
  enddo
  DO I=1,NCALL
     WRITE (LOGFILE,240) 'B',(B(I,J),J=1,NCALL)
  enddo
  DO I=1,NCALL
     WRITE (LOGFILE,240) 'C',(CQ(I,J),J=1,NCALL)
  enddo
165 continue
  DO  IW=OUTTERM,ICOUT,ICOUTDIFF
     IF (IW.EQ.OUTFILE) THEN
        if(DLV_flag)cycle
     endif
     WRITE (IW,1510) ICALL,FSMIN
     DO  LQ=1,NCALL
  	WRITE (IW,1460) IVCH(LQ)(1:MAX(10,NCSTR(IVCH(LQ)))),RU(LQ),E(LQ)
     enddo
  enddo
  IF (BACKGROUND) THEN
     WRITE (PLOTFILE,*) ' &ERR'
     WRITE (PLOTFILE,290) (IVCH(LQ)(1:MAX(10,NCSTR(IVCH(LQ)))),E(LQ),LQ=1,N)
     WRITE (PLOTFILE,*) ' &END'
  ENDIF
  IF (ICOUT.EQ.OUTFILE) THEN
     if(.not.DLV_flag) CALL EFILE ('Correlation matrix')
  ENDIF
  RETURN 1
  !
170 FORMAT ('For I=',I4,' DIV= 0')
180 FORMAT ('Error in covar',2I10)
190 FORMAT (/' *** Correlation matrix and statistical errors ***'/)
200 FORMAT (7X,25(1X,A5))
210 FORMAT (1X,A6,25F6.2/7X,25F6.2)
220 FORMAT (/' XM    ',25F6.3/7X,25F6.3)
230 FORMAT (' DSTEP ',25F6.3/7X,25F6.3)
240 FORMAT (1X,A,5X,12F12.3,3(/7X,12F12.3))
250 FORMAT (/1X,'2SIGMA',25F6.3/7X,25F6.3)
260 FORMAT (/' Value of fit index at predicted minimum',F9.4/)
181 FORMAT (/'Fit index * k**',I1,' :',G14.4,' R-factor :',F8.4,' Chisqu :',G14.4/,'Rexafs ',f8.4,' Rdist ',F8.4)
290 FORMAT (4(1X,A8,'  ',F11.5))
1460 FORMAT (1X,A,'=',F12.5,' +/-',F9.5,' (2 sigma)')
1510 FORMAT (' Call',I4,', R(min)=',F9.4/' Best Values Are :')
END SUBROUTINE COVAR
