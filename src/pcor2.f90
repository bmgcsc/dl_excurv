SUBROUTINE PCOR2 (J,OLD,OLD2,JTN,MS,JCLUS)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_VAR
  Use Common_UPU
  Use Index_INDS
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2

  DIMENSION CX(0:IPARNS),CY(0:IPARNS),CZ(0:IPARNS)
  real(float) WR2,WR,AA,BB,CC,RR,ROOT,ROOT1,ROOT2
  real(float) YROOT1,YROOT2,SIGN1,SIGN2,CALCDSQRT
  LOGICAL LERR
  !
  !	NR(I) IS THE SHELL INDEX FOR THE NEAREST ATOM IN UNIT I
  !	NU(I) IS THE UNIT NUMBER FOR SHELL I ( ZERO NO UNIT)
  !	DR(I) IS THE DISTANCE FROM R(I) TO R(NR(I)) IN angstroms
  !	JT INDICATES THE TYPE OF VARIABLE - 3 FOR R, 4 FOR ALPHA ETC.
  !	MS IS THE SHELL INDEX OF VARIABLE J
  !	IG IS THE UNIT NUMBER FOR VARIABLE J
  !	NF IS NR(IG)
  !
  IF (DEBUG) WRITE (LOGFILE,*) 'pcor2: jtn,ms,old ',IVARS(J),JTN,MS,OLD
  IF (DEBUG) WRITE (OUTTERM,*) 'pcor2: jtn,ms,old ',IVARS(J),JTN,MS,OLD,inds(INDR),inds(INDTILT)
  IF (JTN.LT.inds(INDR)  .OR. JTN.GT.inds(INDTILT)) RETURN
  IF (JTN.GT.inds(INDR)  .AND.JTN.LT.inds(INDANG))  RETURN
  IF (JTN.GE.inds(INDPIV).AND.JTN.LT.inds(INDTWST)) RETURN
  IG=NU(MS)
  NF=NR(IG)
  DIFF=PA1(J)-OLD
  DIFF2=PA2(J)-OLD2
  JRAD=IRAD(JCLUS)
  RPHI=PHI(MS)
  RPHI2=PHI2(MS)
  RNEW=PA1(J)
  RNEW2=PA2(J)
  PA1(J)=OLD
  NTRIES=0
  FANG=DIFF2
  CALL GETAXIS (MS,JTN,NF,IG,RX,RY,RZ,FANG,*80)
  !
  !   1    2    3    4    5    6    7    8    9   10   11   12   13   14
  !   N    T    R    A    B    C    D   UN  ANG   TH  PHI   UX   UY   UZ
  !
  !  15   16   17   18   19   20   21   22   23   24   25   26   27
  ! ROT  PIV  PLA  PLB TORA TORB TORC  PAT PANG  UOC TWST TILT CLUS
  !
  WRITE (7,10) 'rx,ry,rz,fang',RX,RY,RZ,FANG/AC
10 FORMAT (A,4F10.4)
  DO  I=0,IPARNS
     IF (NU(I).NE.IG) cycle
     CX(I)=UX(I)-UX(NF)
     CY(I)=UY(I)-UY(NF)
     CZ(I)=UZ(I)-UZ(NF)
     IF (JTN.NE.inds(INDANG).AND.JTN.LT.inds(INDTWST)) then
        CALL XROTATE (RX,RY,RZ,-FANG,CX(I),CY(I),CZ(I))
     endif
     WRITE (7,20) I,CX(I),CY(I),CZ(I)
20   FORMAT (I4,3F10.3,4X,3F10.3)
  enddo
  U=UX(NF)
  V=UY(NF)
  W=UZ(NF)
  IF (JTN.EQ.inds(INDX)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing X')
     U=U-RNEW
  ELSEIF (JTN.EQ.inds(INDY)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing Y')
     V=V-RNEW
  ELSEIF (JTN.EQ.inds(INDZ)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing Z')
     W=W-RNEW
  ENDIF
  CALL XROTATE(RX,RY,RZ,-FANG,U,V,W)
  WR2=CX(MS)*CX(MS)+CY(MS)*CY(MS)
  WR=DSQRT(WR2)
  IF (JTN.EQ.inds(INDR)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing R')
     RR=.5*(R(NF)*R(NF)-RNEW*RNEW+WR2+CZ(MS)*CZ(MS)+2.*W*CZ(MS))
     AA=V*V+U*U
     BB=2.*U*RR
     CC=RR*RR-V*V*WR2
     CALL QUAD (AA,BB,CC,ROOT1,ROOT2,NTRIES,*80)
     YROOT1=WR2-ROOT1*ROOT1
     YROOT1=CALCDSQRT(YROOT1,'yroot1',LERR)
     IF (LERR) GOTO 80
     YROOT2=WR2-ROOT2*ROOT2
     YROOT2=CALCDSQRT(YROOT2,'yroot2',LERR)
     IF (LERR) GOTO 80
     IF (V.EQ.0.) THEN
        YROOT2=-YROOT1
     ELSE
        SIGN1=-(RR+U*ROOT1)/V
        SIGN2=-(RR+U*ROOT2)/V
        YROOT1=DSIGN(YROOT1,SIGN1)
        YROOT2=DSIGN(YROOT2,SIGN2)
     ENDIF
  ELSEIF (JTN.EQ.inds(INDTH)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing TH')
     CALL WTEXT ('cant do thetas yet')
     GOTO 80
  ELSEIF (JTN.GE.inds(INDPHI).AND.JTN.LE.inds(INDZ)) THEN
     NTRIES=1
     U1=0
     V1=0
     W1=0
     IF (JTN.EQ.inds(INDPHI)) THEN
        IF (DEBUG) CALL WTEXT ('PCOR2: Changing PHI')
        IF (ABS(SIN(RPHI2)).LT..7071) THEN
           U1=-TAN(RPHI2)
           V1=1.
        ELSE
           U1=-1.
           !	      V1=TAN(SNGL(PID2-RPHI2))
           V1=TAN(real(PID2-RPHI2, kind = dp))
        ENDIF
     ELSEIF (JTN.EQ.inds(INDX)) THEN
        IF (DEBUG) CALL WTEXT ('PCOR2: Changing X')
        U1=1.
     ELSEIF (JTN.EQ.inds(INDY)) THEN
        IF (DEBUG) CALL WTEXT ('PCOR2: Changing Y')
        V1=1.
     ELSEIF (JTN.EQ.inds(INDZ)) THEN
        IF (DEBUG) CALL WTEXT ('PCOR2: Changing Z')
        W1=1.
     ENDIF
     CALL XROTATE (RX,RY,RZ,-FANG,U1,V1,W1)
     DD=U*U1+V*V1+W*W1
     ZED=-CZ(MS)
     IF (ABS(ZED).LT.1.E-6) ZED=0.
     DD=-DD+W1*ZED
     AA=V1*V1+U1*U1
     BB=-2.*DD*U1
     CC=DD*DD-WR2*V1*V1
     CALL QUAD (AA,BB,CC,ROOT1,ROOT2,NTRIES,*80)
     IF (ABS(V1).LT.1.E-6) THEN
        SIGN1=0.
        SIGN2=0.
     ELSE
        SIGN1=(DD-U1*ROOT1)/V1
        SIGN2=(DD-U1*ROOT2)/V1
     ENDIF
     YROOT1=CALCDSQRT(WR2-ROOT1*ROOT1,'yroot1',LERR)
     YROOT2=CALCDSQRT(WR2-ROOT2*ROOT2,'yroot2',LERR)
     YROOT1=DSIGN(YROOT1,SIGN1)
     YROOT2=DSIGN(YROOT2,SIGN2)
  ELSEIF (JTN.EQ.inds(INDROT)) THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing ROT')
     JVECA=IVECA
     JVECB=IVECB
     DO I=0,IPARNS
        IF (NU(I).EQ.IG.AND.I.NE.JRAD) CALL RROTATE (I,DIFF,JVECA,JVECB)
     ENDDO
     ROT(MS)=RNEW
     RETURN
  ELSEIF (JTN.EQ.inds(INDANG).OR.JTN.EQ.inds(INDTWST).OR.JTN.EQ.inds(INDTILT))THEN
     IF (DEBUG) CALL WTEXT ('PCOR2: Changing ANG/TWST/TILT')
     TX=UX(NF)
     TY=UY(NF)
     TZ=UZ(NF)
     DO  I=0,IPARNS
        IF (NU(I).NE.IG.OR.I.EQ.JRAD) cycle
        CALL XROTATE (RX,RY,RZ,DIFF2,CX(I),CY(I),CZ(I))
        UX(I)=CX(I)+TX
        UY(I)=CY(I)+TY
        UZ(I)=CZ(I)+TZ
        UX2(I)=UX(I)*DC
        UY2(I)=UY(I)*DC
        UZ2(I)=UZ(I)*DC
     enddo
     PA1(J)=RNEW
     RETURN
  ENDIF
  !	WRITE (7,*) 'a,b,c',AA,BB,CC
  !	WRITE (7,*)  'new',ROOT1,YROOT1,' old',CX(MS),CY(MS)
  !	WRITE (7,*)  'new',ROOT2,YROOT2,' old',CX(MS),CY(MS)
  ROTANG1=(CX(MS)*ROOT1+CY(MS)*YROOT1)/WR2
  ROTANG2=(CX(MS)*ROOT2+CY(MS)*YROOT2)/WR2
  AXIS1=DSIGN(1.0D0,CX(MS)*YROOT1-ROOT1*CY(MS))
  AXIS2=DSIGN(1.0D0,CX(MS)*YROOT2-ROOT2*CY(MS))
  ROTANG1=CALCACOS(ROTANG1,'rotang1',LERR)
  IF (LERR) GOTO 80
  ROTANG2=CALCACOS(ROTANG2,'rotang2',LERR)
  IF (LERR) GOTO 80
  WRITE (7,*) 'rotang1',ROTANG1/AC,AXIS1
  WRITE (7,*) 'rotang2',ROTANG2/AC,AXIS2
  IF (ABS(ROTANG2).LT.ABS(ROTANG1)) THEN
     ROTANG=ROTANG2
     AXIS=AXIS2
     JANG=2
  ELSE
     ROTANG=ROTANG1
     AXIS=AXIS1
     JANG=1
  ENDIF
50 TTX=UX(NF)
  TTY=UY(NF)
  TTZ=UZ(NF)
  DO  I=0,IPARNS
     IF (NU(I).NE.IG.OR.I.EQ.JRAD) cycle
     TX=CX(I)
     TY=CY(I)
     TZ=CZ(I)
     CALL XROTATE (0.,0.,AXIS,ROTANG,TX,TY,TZ)
     CALL XROTATE (RX,RY,RZ,FANG,TX,TY,TZ)
     TX=TX+TTX
     TY=TY+TTY
     TZ=TZ+TTZ
     WRITE (7,60) 'tx,ty,tz',TX,TY,TZ,UX(I),UY(I),UZ(I)
     UX(I)=TX
     UY(I)=TY
     UZ(I)=TZ
     TX=R(I)
     TY=TH(I)
     TZ=PHI(I)
     CALL CARTOPOLD (UX(I),UY(I),UZ(I),R(I),TH(I),PHI(I))
     WRITE (7,60) 'r,th,phi',TX,TY,TZ,R(I),TH(I),PHI(I)
60   FORMAT (A,3F10.3,2X,3F10.3)
  enddo
  TPHI1=RPHI
  TPHI2=PHI(MS)
  WRITE (7,*) 'ntries,tphi1,tphi2',NTRIES,TPHI1,TPHI2
  IF (ABS(ABS(TPHI1)-360.).LT..1) TPHI1=0.
  IF (ABS(ABS(TPHI2)-360.).LT..1) TPHI2=0.
  IF (NTRIES.GT.0.AND.ABS(TPHI1-TPHI2).GT.90.) THEN
     NTRIES=NTRIES-1
     IF (JANG.EQ.1) THEN
        AXIS=AXIS2
        ROTANG=ROTANG2
     ELSE
        AXIS=AXIS1
        ROTANG=ROTANG1
     ENDIF
     GOTO 50
  ENDIF
  RETURN
  !
  !	restore old values after error
  !
80 PA1(J)=OLD
  PA2(J)=OLD2
  CALL WTEXT ('Invalid rotation requested - parameter not changed')
  RETURN
END SUBROUTINE PCOR2
