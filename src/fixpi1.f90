SUBROUTINE FIXPI1 (BSF)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B1
  Use Include_XY
  DIMENSION BSF(*)
  !
  !	Fix PI shifts
  !
  DO JU=1,2
     TEMP=BSF(JF(IEXP))
     DO  KK=JF(IEXP)-1,JS(IEXP),-1
	QT=BSF(KK)-TEMP
	IF (ABS(QT).LT.2.5) GOTO 30
        !-st	QT=DSIGN(PI,DBLE(QT))
	QT=SIGN(PI,real(QT,kind=dp))
	NF=0
	DO  M=JS(IEXP),KK
           BSF(M)=BSF(M)-QT
           IF (REAL(BSF(M)).LT.0.) NF=1
        enddo
	IF (NF.EQ.0) GOTO 30
	DO  MM=JS(IEXP),JF(IEXP)
           BSF(MM)=BSF(MM)+PI
        enddo
30	TEMP=BSF(KK)
     enddo
  ENDDO
  RETURN
END SUBROUTINE FIXPI1
