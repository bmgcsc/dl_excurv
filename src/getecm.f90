SUBROUTINE GETECM (ECM,JCLUS)
  !======================================================================C
  Use Parameters
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_UPU
  Use Common_ATMAT
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA2
  Use Include_XY
  !
  !	Calculates complex energy ECM (atomic units) - two options exist
  !	for treating cases where the complex self energy differs according
  !	to atom type, due to irregularites in phaseshift calculation.
  !	GETECM must be called whenever the cluster number JCLUS changes,
  !	as one of the options is dependent on the current central atom.
  !	ECM is indexed starting with the first point to be calculated, as
  !	with the T-matrix, etc. The real energy must be at least .01
  !
  COMPLEX ECM(IPARNPOINTS,*)
  I=ITAD(JCLUS)
  DO KK=1,IPARNP
     IF (KK.GT.NPS.AND.IATOM(KK).EQ.0) cycle
     K=KK
     IF (iset(IWAVE).EQ.2) K=I
     DO  IE=1,JPT
	E1=MAX(ENER(IE+JJS-1)+EF02(0)+EF02(IEXP)-REAL(EREFP(IE,K)),.001)
	V1=-(VPI02(0)+VPI02(IEXP))-AIMAG(EREFP(IE,K))
	ECM(IE,KK)=CMPLX(E1,V1)
     enddo
  enddo
  RETURN
END SUBROUTINE GETECM
