SUBROUTINE MULTLEG1 (MJ,MU,F3,FK,FNEW)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ATMAT
  Use Include_IPA

  COMPLEX FNEW(9,12),FK(9,9),F3(9,12)
  DO L3=1,MJ
     DO L2=1,MU
	F3(L3,L2)=0.
     ENDDO
     DO L1=1,MJ
	IF (FK(L3,L1).NE.0) THEN
           DO L2=1,MU
              F3(L3,L2)=F3(L3,L2)+FK(L3,L1)*FNEW(L1,L2)
           ENDDO
	ENDIF
     ENDDO
  ENDDO
  RETURN
END SUBROUTINE MULTLEG1
