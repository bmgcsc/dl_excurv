SUBROUTINE CNTRLN (X,Y,IPT,N,ITHICK,IFLGSN)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PMTX
  !
  !	Draw a contour line
  !
  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***
  !
  DIMENSION :: X(*),Y(*)
  NPE=IPT
  DO K=1,NPE
     IF (X(K).LT.0.) X(K)=0.
     IF (Y(K).LT.0.) Y(K)=0.
     IF (X(K).GT.28.) X(K)=28.
     IF (Y(K).GT.28.) Y(K)=28.
  enddo
  AMID=15.
  IF (IGF(2).EQ.3) RETURN
  CALL THICK (ITHICK)
  CALL PTJOIN (X,Y,1,NPE,1)
  CALL THICK (1)
  IF (ITHICK.EQ.3.OR.NPE.LE.2.OR.IFLGSN.EQ.1) RETURN
  NPM=NPE-1
  DO K=1,NPM
     IF (Y(K).GT.AMID.OR.Y(K+1).LT.AMID) GOTO 50
     XA1=(X(K)+X(K+1))/2.
     CALL PLOTNI (XA1,AMID,N,2)
     IFLGSN=1
     RETURN
50   CONTINUE
  enddo
  RETURN
END SUBROUTINE CNTRLN
