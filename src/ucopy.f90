SUBROUTINE UCOPY (A,B,N)
  !======================================================================C
  !
  !	Copy a number of words from one array to another (real or integer)
  !
  !	A (I) Array from which words are to be copied
  !	B (O) Array into which words are to be copied
  !	N (I) The number of words to be copied
  !
  DIMENSION A(*),B(*)
  IF (N.LE.0)RETURN
  DO  I=1,N
     B(I)=A(I)
  enddo
  RETURN
END SUBROUTINE UCOPY
