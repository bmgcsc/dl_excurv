Module Common_MAPS
!      COMMON /MAPS/ AP(IPARMAP,IPARMAP),IMAP,JMAP,X1,Y1,X2,Y2,MAPPOINTS,
!     1C(29,29),H(20),FMIN,XM,YM,MAPVARS(2)
!      CHARACTER*6 MAPVARS
Use Definition
Use Parameters
implicit none
private
real(dp), dimension(:,:), public :: AP(IPARMAP,IPARMAP)
integer(i4b),             public :: IMAP,JMAP
real(dp),                 public :: X1,Y1,X2,Y2
integer(i4b),             public :: MAPPOINTS
real(dp), dimension(:,:), public :: C(29,29)
real(dp), dimension(:),   public :: H(20)
real(dp),                 public :: FMIN,XM,YM
character*6, dimension(:),public :: MAPVARS(2)
end Module Common_MAPS

