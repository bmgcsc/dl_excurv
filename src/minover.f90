SUBROUTINE MINOVER (UXM,POSITIVE,JCLUS)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_VIEW
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  DIMENSION TX(0:IPARATOMS),TY(0:IPARATOMS),AMAG(0:IPARATOMS),RADIUS(0:IPARATOMS),AMAG1(0:IPARATOMS),INDEX(0:IPARATOMS)
  COMPLEX P3D,TXYZ,TXY2
  LOGICAL POSITIVE
  MINNOVER=1E6
  MINNKILL=MINNOVER
  MINNNEAR=MINNOVER
  IF (KCENT(JCLUS).EQ.1.OR.POSITIVE) THEN
     IMIN=0
  ELSE
     IMIN=-5
  ENDIF
  DO  IX=5,IMIN,-1
     DO  IY=5,IMIN,-1
	DO  IZ=5,IMIN,-1
           IF (IX.EQ.0.AND.IY.EQ.0.AND.IZ.EQ.0) cycle
           SCALER=IX*IX+IY*IY+IZ*IZ
           SCALER=6./SQRT(SCALER)*UXM
           VVX=IX*SCALER
           VVY=IY*SCALER
           VVZ=IZ*SCALER
           IF (VVX.EQ.0..AND.VVY.EQ.0.) THEN
              VVX=1.E-6
              VVY=1.E-6
           ENDIF
           TXYZ=P3D(0.,0.,0.)
           RADIUS(0)=AMAX1(RMTR(1)*SIZE,.025*UXM)
           TX(0)=REAL(TXYZ)
           TY(0)=AIMAG(TXYZ)
           AMAG(0)=1
           DO  I=1,NAT(JCLUS)
              V1=SQRT(VVX*VVX+VVY*VVY+VVZ*VVZ)
              ASIZE=AMAX1(RMTR(IT(ISHELL(I,JCLUS)))*SIZE,.025*UXM)
              VV1=VVX-AX(I,JCLUS)
              VV2=VVY-AY(I,JCLUS)
              VV3=VVZ-AZ(I,JCLUS)
              V2=SQRT(VV1*VV1+VV2*VV2+VV3*VV3)
              AMAG(I)=V1/V2
              RADIUS(I)=ASIZE*AMAG(I)
              TXYZ=P3D(AX(I,JCLUS),AY(I,JCLUS),AZ(I,JCLUS))
              TX(I)=REAL(TXYZ)
              TY(I)=AIMAG(TXYZ)
           enddo
           CALL UCOPY (AMAG,AMAG1,NAT(JCLUS)+1)
           CALL SORTTAB (AMAG1,NAT(JCLUS)+1,INDEX)
           DO  I=0,NAT(JCLUS)
              INDEX(I)=INDEX(I)-1
           enddo
           NOVER=0
           NKILL=0
           NNEAR=0
           DO  I=NAT(JCLUS)-1,0,-1
              J=INDEX(I)
              DO  K=I+1,NAT(JCLUS)
                 JJ=INDEX(K)
                 DX=TX(J)-TX(JJ)
                 DY=TY(J)-TY(JJ)
                 DIST=DX*DX+DY*DY
                 IF (DIST.NE.0.) DIST=SQRT(DIST)
                 !	IF (MINNOVER.EQ.1E6) THEN
                 !	  WRITE (LOGFILE,*) I,K,J,JJ,DIST,RADIUS(J),RADIUS(JJ)
                 !	ENDIF
                 IF (DIST.LT.RADIUS(J)+RADIUS(JJ)) THEN
                    NOVER=NOVER+1
                    NNEAR=NNEAR+1
                    DRAD=RADIUS(J)-RADIUS(JJ)
                    IF (DIST.LT.ABS(DRAD).AND.DRAD.LT.0.) NKILL=NKILL+1
                 ELSEIF (DIST.LT.(RADIUS(J)+RADIUS(JJ))*2.) THEN
                    NNEAR=NNEAR+1
                 ENDIF
              enddo
           enddo
           IF (NOVER.LT.MINNOVER) THEN
              MINNOVER=NOVER
              IXS=IX
              IYS=IY
              IZS=IZ
           ELSEIF (NOVER.EQ.MINNOVER) THEN
              IF (NKILL.LT.MINNKILL) THEN
                 MINNKILL=NKILL
                 IXS=IX
                 IYS=IY
                 IZS=IZ
              ELSEIF (NNEAR.LT.MINNNEAR) THEN
                 MINNNEAR=NNEAR
                 IF (MINNNEAR.EQ.0) GOTO 50
                 IXS=IX
                 IYS=IY
                 IZS=IZ
              ENDIF
           ENDIF
        enddo
     enddo
  enddo
50 SCALER=IXS*IXS+IYS*IYS+IZS*IZS
  SCALER=6./SQRT(SCALER)
  VVX=IXS*SCALER
  VVY=IYS*SCALER
  VVZ=IZS*SCALER
  IF (DEBUG) THEN
     IF (JCLUS.EQ.1) THEN
        WRITE (LOGFILE,*) 'VX:',VVX,' VY:',VVY,' VZ:',VVZ
     ENDIF
     WRITE (LOGFILE,*) 'VX:',VVX,' VY:',VVY,' VZ:',VVZ
     WRITE (LOGFILE,*) 'IXS,IYS,IZS',IXS,IYS,IZS,MINNOVER,MINNKILL, MINNNEAR
  ENDIF
  VVX=VVX*UXM
  VVY=VVY*UXM
  VVZ=VVZ*UXM
  RETURN
END SUBROUTINE MINOVER
