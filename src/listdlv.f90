SUBROUTINE LDLV (K5,FILEOPT)
  !======================================================================C
  Use Common_DLV
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Common_SETKW
  Use Common_F
  Use Common_PMTCH
  Use Common_ELS
  Use Common_VAR
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_POT
  Use Common_PMTX
  Use Common_UPU
  Use Common_COMPAR
  Use Index_INDS
  Use Common_DISTANCE
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  !
  !	This subroutine processes the 'LIST' command
  !
  !	K5(C) Keyword
  !	FILEOPT(C) TERM or LOG
  !
  DIMENSION DISTLIST(0:IPARNS),ILIST(IPARNS+1),PPP(IPARNS+1),EX(IPARNSP),EY(IPARNSP),EZ(IPARNSP)
  dimension ER(IPARNSP),ETH(IPARNSP),EPHI(IPARNSP),IEXITELIST(10),ICLUSLIST(IPARNCLUS)
  CHARACTER OPTS(24)*9,KP1*1,KP2*1,KP3*1,K5*(*),FILEOPT*(*),BUF*80
  DATA OPTS/ &
       'UPARS',  'SPARS',   'ALL',   'FILES','TITLE','NSHELL','CART',  &
       'SETOPTS','GSETOPTS','FTOPTS','D:',   'W:',   'A:',    'V:',    &
       'B:',     'DF1',     'DF2',   'PHASE','RADIAL','FRAC', 'MSPARS',&
       'BACK',   'LINKS',   'CLUSMAP'/
  if(.not.DLV_flag) then
     CALL WTEXT ('Command LDLV  not found.')
     return
  end if
  I=0
  IP=0
  IO=NS
  ISTARTPAR=inds(INDR)
  IF(FILEOPT.EQ.'LOG'.OR.K5.EQ.'LOG') THEN
     KW=LOGFILE
     IF(K5.EQ.'LOG') K5=FILEOPT
  ELSE
     KW=OUTTERM
  ENDIF
  LENK5=NCSTR(K5)
  IF(LENK5.LE.1) GOTO 50
  CALL GETLIST (K5,LENK5,NLIST,ILIST)
  !
  !	Single parameter
  !
  CALL FINDPAR (K5,I,*11)
  IF(I.GE.inds(INDEMIN0).AND.I.LE.inds(INDEMIN0)+IPARNSP) THEN
     DO J=0,MAX(0,NLIST-1)
        WRITE (KW,530) IVARS(I+J),PA1(I+J),'KMIN',RKMIN
     ENDDO
  ELSEIF(I.GE.inds(INDEMAX0).AND.I.LE.inds(INDEMAX0)+IPARNSP) THEN
     DO J=0,MAX(0,NLIST-1)
        WRITE (KW,530) IVARS(I),PA1(I),'KMAX',RKMAX
     ENDDO
  ELSEIF(I.GE.inds(INDD1A).AND.I.LE.inds(INDD2B)) THEN
     WRITE (KW,*) IVARS(I),' = ',IVARS(IPA(I))
  ELSEIF(I.GE.inds(INDATOM).AND.I.LT.inds(INDION)) THEN
     IF(NLIST.EQ.0) THEN
        WRITE (KW,*) IVARS(I),' = ',ELS(IPA(I)),' (',IPA(I),')'
     ELSE
        IOFF=I-ILIST(1)
        DO K=1,NLIST
           IK=ILIST(K)+IOFF
           WRITE (KW,*) IVARS(IK),' = ',ELS(IPA(IK)),' (',IPA(IK),')'
        ENDDO
     ENDIF
  ELSE
     IF(NLIST.LE.0) THEN
        IF(I.GE.inds(INDANG).AND.I.LE.inds(INDANG)+IPARNS) THEN
           PPA=TANG(I-inds(INDANG))
        ELSEIF(I.GE.inds(INDPANG).AND.I.LE.inds(INDPANG)+IPARNS) THEN
           PPA=YANG(I-inds(INDPANG))
        ELSE
           PPA=PA1(I)
        ENDIF
        JLEN=MAX(8,LKEYWORD)
        WRITE (KW,510) KEYWORD(1:JLEN),PPA
     ELSE
        IOFF=I-ILIST(1)
        DO K=1,NLIST
           IF (I.GE.inds(INDANG).AND.I.LE.inds(INDANG)+IPARNS) THEN
              PPP(K)=TANG(ILIST(K))
           ELSEIF (I.GE.inds(INDPANG).AND.I.LE.inds(INDPANG)+IPARNS) THEN
              PPP(K)=YANG(ILIST(K))
           ELSE
              PPP(K)=PA1(ILIST(K)+IOFF)
           ENDIF
        ENDDO
        WRITE (KW,500) (IVARS(ILIST(K)+IOFF),PPP(K),K=1,NLIST)
     ENDIF
  ENDIF
  RETURN
  !	ENDIF
  !  10	CONTINUE
  !
  !	Shell symbol Sn
  !
11 DO I=1,IPARNS
     IF (K5.EQ.SVARS(I)) THEN
        IP=I
        IO=I
        GOTO 390
     ENDIF
  enddo
  DO  I=1,IPARSETOPTS
     IF (K5(1:LENK5).EQ.SETKW(I)(1:LENK5)) THEN
        IF (I.EQ.21) THEN
           WRITE (KW,'(4A,3F5.1,A)') SETKW(I),': ',SETKWO(ISET(I)+MINUS(I),I),'(',VX,VY,VZ,')'
        ELSE
           WRITE (KW,*) SETKW(I),': ',SETKWO(ISET(I)+MINUS(I),I)
        ENDIF
        RETURN
     ENDIF
  enddo
  DO  I=1,IPARFTOPTS
     IF (K5(1:LENK5).EQ.FTSETKW(I)(1:LENK5)) THEN
        WRITE (KW,*) FTSETKW(I),': ',FTSETKWO(IFTSET(I)+IFTMINUS(I),I)
	RETURN
     ENDIF
  enddo
  DO I=1,IPARGSET
     IF (K5(1:LENK5).EQ.IAL(I)(1:LENK5)) THEN
        WRITE (KW,*) IAL(I),': ',GSETKWO(IGF(I),I)
        RETURN
     ENDIF
  enddo
  IF ((K5(1:1).EQ.'D'.OR.K5(1:1).EQ.'W'.OR.K5(1:1).EQ.'A'.OR.K5(1:1).EQ.'V').AND.LENK5.GE.4) THEN
     !
     !	Dn:m or Wn:m
     !
     I=0
     J=0
     I=NUMCHAR(K5(2:),K)
     IF (K+1.LT.LENK5) J=NUMCHAR(K5(K+2:),KJ)
     IF (I.GE.1.AND.I.LE.IPARNS+1.AND.J.GE.0.AND.J.LE.IPARNS+1) THEN
	IF (I.LT.J) RETURN
	IF (K5(1:1).EQ.'D') THEN
           WRITE (KW,510) DISTVARS(I,J),DISTANCES(I,J)
	ELSEIF (K5(1:1).EQ.'W') THEN
           WRITE (KW,510) WEIGHTVARS(I,J),WEIGHTINGS(I,J)
	ELSEIF (K5(1:1).EQ.'A') THEN
           WRITE (KW,510) BONDANGVARS(I,J),BONDANGS(I,J)
	ELSE
           WRITE (KW,510) BAWTVARS(I,J),BAWT(I,J)
	ENDIF
	RETURN
     ENDIF
  ENDIF
  !
  !	Test for Menu Options
  !
50 CALL FINDOPT (24,OPTS,IOPT,6,'None',*111)
  GOTO (70,120,60,370,380,140,130,210,180,200,230,260,280,310,330,150,160,410,420,430,434,436,437,438),IOPT
  !
  !	List ALL
  !
60 IO=IPARNS
  IP=0
  GOTO 390
  !
  !	List UNIT
  !
70 IF (MG.EQ.0.) THEN
     CALL WTEXT ('No unit defined')
80   RETURN
  ENDIF
  IS=1
  IF1=MG
  IF (INTOPT.GT.0.AND.INTOPT.LE.MG) THEN
     IS=INTOPT
     IF1=INTOPT
  ELSEIF (INTOPT.GT.MG) THEN
     return
  ENDIF
  DO  L=IS,IF1
     IF (NT(L).LT.1) cycle
     LL=IRAD(IABS(ICLUS(NR(L))))
     WRITE (KW,490) L,IRESNUM(L),UNAME(L),UOC(L),IPIV(L),LL,IPIV(L),IPLA(L),IPLB(L),TORC(L)
     DO  J=1,NS
        IF (NU(J).NE.L) cycle
        WRITE (KW,480) J,IVARS(inds(INDT)+J),IT(J),ELS(IATOM(IT(J))),IVARS(inds(INDR)+J), &
             R(J),IVARS(inds(INDA)+J),A(J),'DR  ',DR(J),IVARS(inds(INDANG)+J),TANG(J)
     enddo
     DO  J=1,NS
        IF (NU(J).NE.L) cycle
        WRITE (KW,600) ATLABEL(J),(IVARS(K+J),PA1(K+J),K=inds(INDTH),inds(INDZ),IPARNS+1)
     enddo
     XX1=UX(IPLA(L))-UX(IPIV(L))
     XX2=UX(IPLB(L))-UX(IPIV(L))
     YY1=UY(IPLA(L))-UY(IPIV(L))
     YY2=UY(IPLB(L))-UY(IPIV(L))
     ZZ1=UZ(IPLA(L))-UZ(IPIV(L))
     ZZ2=UZ(IPLB(L))-UZ(IPIV(L))
     XX=YY1*ZZ2-YY2*ZZ1
     YY=ZZ1*XX2-ZZ2*XX1
     ZZ=XX1*YY2-XX2*YY1
     RR=XX*XX+YY*YY+ZZ*ZZ
     IF (RR.NE.0) THEN
        RR=SQRT(RR)
        XX=XX/RR
        YY=YY/RR
        ZZ=ZZ/RR
     ENDIF
     WRITE (KW,440) 'Plane(',IPIV(L),',',IPLA(L),',',IPLB(L),')=',XX,YY,ZZ,      &
          ' t(',NTORA(1,L),'-',NTORA(2,L),'-',NTORA(3,L),'-',NTORA(4,L),TORA(L), &
          ' t(',NTORB(1,L),'-',NTORB(2,L),'-',NTORB(3,L),'-',NTORB(4,L),TORB(L)
  enddo
111 RETURN
120 NGROUPS=(NSPEC+2)/3-1
  DO I=1,MAX(3,NSPEC)
     EX(I)=-COS(BEW02(I))*SIN(BEPHI02(I))+SIN(BEW02(I))*COS(BETH02(I))*COS(BEPHI02(I))
     EY(I)=-COS(BEW02(I))*COS(BEPHI02(I))+SIN(BEW02(I))*COS(BETH02(I))*SIN(BEPHI02(I))
     EZ(I)=-SIN(BEW02(I))*SIN(BETH02(I))
     CALL CARTOPOL (EX(I),EY(I),EZ(I),ER(I),ETH(I),EPHI(I))
  ENDDO
  DO IG=0,NGROUPS*3,3
     IMIN=IG+1
     IMAX=MIN(IG+3,MAX(3,NSPEC))
     WRITE (KW,620) (I,I=IMIN,IMAX)
     WRITE (KW,610) (IVARS(I),PA1(I),I=inds(INDBETH0) +IMIN,inds(INDBETH0) +IMAX)
     WRITE (KW,610) (IVARS(I),PA1(I),I=inds(INDBEPHI0)+IMIN,inds(INDBEPHI0)+IMAX)
     WRITE (KW,610) (IVARS(I),PA1(I),I=inds(INDBEW0)  +IMIN,inds(INDBEW0)  +IMAX)
     WRITE (KW,*)
     WRITE (KW,610) ('EX'//CHAR(48+I)//'   ',EX(I),I=IMIN,IMAX)
     WRITE (KW,610) ('EY'//CHAR(48+I)//'   ',EY(I),I=IMIN,IMAX)
     WRITE (KW,610) ('EZ'//CHAR(48+I)//'   ',EZ(I),I=IMIN,IMAX)
     WRITE (KW,*)
     WRITE (KW,610) ('ETH'//CHAR(48+I)//'  ',ETH(I)/AC,I=IMIN,IMAX)
     WRITE (KW,610) ('EPHI'//CHAR(48+I)//' ',EPHI(I)/AC,I=IMIN,IMAX)
     IF (IG.NE.NGROUPS*3) CALL CREAD (KP1,KP2,KP3,INT,IC,VAL,*122,*125)
122  CONTINUE
  ENDDO
125 RETURN
  !
  !	Here for normal ordinary list
  !
130 ISTARTPAR=inds(INDX)
140 continue
  call wtext('#dlv')
  write(kw,'(F10.5)')  PA1(inds(INDEF0) + 0 )    ! Fermi Energy
  if(.not.DLV_flag) WRITE (KW,505) (IVARS(K),PA1(K),K=6,10)
  NSPEC1=0
  IF (NSPEC.GT.1) NSPEC1=NSPEC
  DO JEXP=0,NSPEC
     if(.not.DLV_flag) WRITE(KW,505) &
          IVARS(inds(INDEF0)  +JEXP),PA1(inds(INDEF0) +JEXP), &
          IVARS(inds(INDVPI0) +JEXP),PA1(inds(INDVPI0)+JEXP), &
          IVARS(inds(INDAFAC0)+JEXP),AFAC0(0+JEXP), &
          IVARS(inds(INDEMIN0)+JEXP),EMIN0(0+JEXP), &
          IVARS(inds(INDEMAX0)+JEXP),EMAX0(0+JEXP)
  ENDDO
  IF (iset(ISING).GT.0) THEN
     DO I=1,NSPEC
        IF (iset(ISING).NE.0)  &
             WRITE(KW,505) IVARS(inds(INDBETH0) +I),BETH0(I),IVARS(inds(INDBEPHI0)+I),BEPHI0(I),IVARS(inds(INDBEW0)+I),BEW0(I)
     ENDDO
  ENDIF
  GOTO 390
  !
  !	Test for Difference variable
  !
150 I=1
  GOTO 170
160 I=2
170 WRITE (KW,520)  &
       DVARS(I)(1:3),': ',                &
       IVARS(IPA(inds(INDD1B)-1+I)),'-',  &
       IVARS(IPA(inds(INDD1A)-1+I)),'=',  &
       PA1(IPA(inds(INDD1B)-1+I))-PA1(IPA(inds(INDD1A)-1+I))
  RETURN
  !
  !	Graphics options
  !
180 DO I=1,IPARGSET
     WRITE (KW,*) IAL(I),': ',GSETKWO(IGF(I),I),'(',IGF(I),')'
  enddo
  RETURN
  !
  !	FT options
  !
200 DO I=1,IPARFTOPTS
     WRITE (KW,*) FTSETKW(I),': ',FTSETKWO(IFTSET(I),I),'(',IFTSET(I),')'
  enddo
  RETURN
210 DO I=1,IPARSETOPTS
     WRITE (KW,*) SETKW(I),': ',SETKWO(ISET(I)+MINUS(I),I)
  enddo
  WRITE (KW,*) 'TITLE           : ',GTITLE
  RETURN
  !
  !	table of Distance parameters for restrained refinement
  !
230 IF (NS.GT.5) CALL COL132
  WRITE (KW,590) (I,I=0,NS-1)
  DO I=1,NS
     DO J=0,I-1
        V1=UX(I)-UX(J)
        V2=UY(I)-UY(J)
        V3=UZ(I)-UZ(J)
        V=V1*V1+V2*V2+V3*V3
        DISTLIST(J)=SQRT(V)
     enddo
     WRITE (KW,550) I,(DISTANCES(I,J),DISTLIST(J),J=0,I-1)
  enddo
  WRITE (KW,590) (I,I=0,NS-1)
  !	IF (LMDSN+LRIS.EQ.0) READ (INTERM,*)
  IF (NS.GT.5) CALL COL80
  RETURN
  !
  !	Table of weightings for restrained refinement
  !
260 IF (NS.GT.10) CALL COL132
  WRITE (KW,580) (I,I=0,NS-1)
  DO I=1,NS
     WRITE (KW,570) ' W',I,(WEIGHTINGS(I,J),J=0,I-1)
  enddo
  WRITE (KW,580) (I,I=0,NS-1)
  IF (NS.GT.10) CALL COL80
  RETURN
  !
  !	table of Angle parameters for restrained refinement
  !
280 IF (NS.GT.5) CALL COL132
  WRITE (KW,590) (I,I=0,NS-1)
  DO I=1,NS
     DO J=0,I-1
        V=ANG(I)
        DISTLIST(J)=V
     enddo
     WRITE (KW,560) I,(BONDANGS(I,J),DISTLIST(J),J=0,I-1)
  enddo
  WRITE (KW,590) (I,I=0,NS-1)
  !	    IF (LMDSN+LRIS.EQ.0) READ (INTERM,*)
  IF (NS.GT.5) CALL COL80
  RETURN
  !
  !	Table of weightings for restrained refinement
  !
310 WRITE (KW,580) (I,I=0,NS-1)
  DO  I=1,NS
     WRITE (KW,570) ' V',I,(BAWT(I,J),J=0,I-1)
  enddo
  WRITE (KW,580) (I,I=0,NS-1)
  RETURN
  !
330 WRITE (KW,580) (I,I=0,NS-1)
  DO JCLUS=1,MAXCLUS
     DO KCLUS=1,NS
        IF (ICLUS(KCLUS).EQ.JCLUS) GOTO 340
     ENDDO
     cycle
340  DO I=1,MAX(NS,NAT(JCLUS))
        WRITE (KW,570) ' B',I,(BONDS(I,J),J=0,I-1)
     enddo
     WRITE (KW,580) (I,I=0,MAX(NS,NAT(JCLUS))-1)
  enddo
  RETURN
370 WRITE (KW,540) (LFS(I),I=1,NPS+IPARNSP+1)
  RETURN
380 WRITE (KW,540) GTITLE
  RETURN
  !
390 IF (INTOPT.GT.0.AND.INTOPT.LE.NS) IO=INTOPT
  write(kw,*) IO
  DO L1=IP,IO
     write(kw,'(f10.5)') RN(L1)
     write(kw,'(f10.5/f10.5/f10.5)') (PA1(L1+K),K=ISTARTPAR,ISTARTPAR+(IPARNS+1)*2,IPARNS+1)
     !     pause
     !-st     WRITE (KW,460) L1,IVARS(L1+inds(INDN)),RN(L1),IVARS(L1+inds(INDT)),IT(L1),ELS(IATOM(IT(L1))), &
     !-st          (IVARS(L1+K),PA1(L1+K),K=ISTARTPAR,ISTARTPAR+(IPARNS+1)*2,IPARNS+1),ICLUS(L1),NU(L1)
     IF (K5(1:1).NE.'S') cycle
     WRITE (KW,470) L1,(IVARS(L1+K),PA1(L1+K),K=5*(IPARNS+1)+11,8*(IPARNS+1)+11,IPARNS+1)
  enddo
     write(kw,'(f10.5)') fitindex*1E3_dp
     write(kw,'(f10.5)') rfac
  call wtext('#dlv')
  RETURN
410 CALL PHLIST (1,IPARNP)
  RETURN
420 IO=NS
  IF (INTOPT.GT.0.AND.INTOPT.LE.NS) IO=INTOPT
  DO L1=0,IO
     WRITE (KW,460) L1,IVARS(L1+inds(INDN)),RN(L1),IVARS(L1+inds(INDT)),IT(L1),ELS(IATOM(IT(L1))), &
          IVARS(L1+inds(INDR)),RAD(L1)/DC,IVARS(L1+inds(INDTH)),RADTH(L1)/AC,IVARS(L1+inds(INDPHI)),RADPHI(L1)/AC
  ENDDO
  RETURN
  !
  !	LIST FRAC
  !
430 WRITE (KW,'(A,F12.4,2F13.4)') 'Cell parameters:',ACELL,BCELL,CCELL
  WRITE (KW,*) '     Shell         X            Y            Z'
  DO L1=0,NS
     CALL FRACCORD (L1,FRACX,FRACY,FRACZ,INTX,INTY,INTZ,KW)
  ENDDO
  RETURN
434 WRITE (KW,'(4(A,F8.3,'' ''))')  &
       IVARS(inds(INDOMIN)),OMIN,   &
       IVARS(inds(INDOMAX)),OMAX,   &
       IVARS(inds(INDPLMIN)),PLMIN, &
       IVARS(inds(INDPLMAX)),PLMAX 
  WRITE (KW,'(4(A,F8.3,'' ''))')       &
       IVARS(inds(INDMINANG)),RMINANG, & 
       IVARS(inds(INDMINMAG)),RMINMAG, &
       IVARS(inds(INDDLMAX)),DLMAX,    &
       IVARS(inds(INDTLMAX)),TLMAX
  WRITE (KW,'(4(A,F8.3,'' ''))')      &
       IVARS(inds(INDATMAX)), ATMAX,  &
       IVARS(inds(INDNUMAX)), RNUMAX, &
       IVARS(inds(INDOUTPUT)),OUTPUT
  RETURN
436 DO K=0,NSPEC
     IF (K.EQ.0) THEN
        WRITE (KW,'(A)') 'Variables common to all spectra:'
        WRITE (KW,'(4(A6,F12.7,1X))') IVARS(inds(INDOFFSET)),PA1(inds(INDOFFSET)), IVARS(inds(INDTEMP)),  PA1(inds(INDTEMP))
     ELSE
        WRITE (KW,'(A,I2)') 'Variables effecting only spectrum ',K
     ENDIF
     WRITE (KW,'(4(A6,F12.7,1X))') &
          (IVARS(I+K),PA1(I+K),I=inds(INDPOST0),inds(INDPOST8),IPARNSP+1), &
          (IVARS(I+K),PA1(I+K),I=inds(INDPRE0) ,inds(INDPRE2) ,IPARNSP+1)
  ENDDO
  RETURN
  !
  !	Here for LIST LINKS
  !
437 DO I=0,MAXCLUS
     WRITE (KW,'(A,I2,A,3F9.3)') 'Cluster ',I,' Offset vector: ',(COFFSET(J,I),J=1,3)
  ENDDO
  WRITE (KW,*) 'Shell Clus   To From     Shell Clus   To From'
  DO I=0,NS/2
     WRITE (KW,'(2(I6,3I5,4X))') I,ICLUS(I),LINK(I),LINK(LINK(I)),I+NS/2+1,ICLUS(I+NS/2+1),LINK(I+NS/2+1),LINK(LINK(I+NS/2+1))
  ENDDO
  RETURN
438 CALL WTEXTI ('Number of experiments:',NSPEC)
  DO IEXP=1,NSPEC
     JSPEC=1
     IF (IEXP.NE.NSPEC) THEN
        DO J=IEXP+1,NSPEC
           IF (IPOL(J).LE.IPOL(IEXP)) GOTO 106
           JSPEC=JSPEC+1
        ENDDO
106     CONTINUE
     ENDIF
     KCLUS=1
     DO JEXP=1,NSPEC
        IF (NCLUS(JEXP).GT.0) THEN
           DO I=1,NCLUS(JEXP)
              KEXP(KCLUS)=JEXP
              KCLUS=KCLUS+1
           ENDDO
        ELSEIF (KCLUS.GT.1) THEN
           DO I=1,KCLUS-1
              KEXP(I)=IEXP
           ENDDO
        ENDIF
     ENDDO
     NL=NCSTR(LFS(IEXP))
     BUF='Experiment '//CHAR(48+IEXP)//' ('//LFS(IEXP)(1:NL)//')'
     CALL WTEXT (' ')
     CALL WTEXT (BUF)
     WRITE (OUTTERM,' (A,A,1X,A,8(A,I3))')  &
          'Edge ',ELS(IZED(IEXP)),EDGECODE(IEXP),' Iedge', &
          IEDGECODE(IEXP),' Ni',NINITIAL(IEXP),' Li',LINITIAL(IEXP),' Ji',JINITIAL(IEXP), &
          ' Z',IZED(IEXP),' NPOL',JSPEC,' IPOL',IPOL(IEXP),' NCLUS',NCLUS(IEXP)
     CALL IUZERO (ICLUSLIST,IPARNSP)
     INCLUS=0
     DO J=1,MAXCLUS
        IF (KEXP(J).EQ.IEXP) THEN
           INCLUS=INCLUS+1
           ICLUSLIST(INCLUS)=J
        ENDIF
     ENDDO
     IF (INCLUS.GT.0) THEN
        WRITE (OUTTERM,'(''Clusters:'',20I3)') (ICLUSLIST(J),J=1,INCLUS)
     ELSE
        CALL WTEXT ('No clusters')
     ENDIF
  ENDDO
  IG=1
  CALL IUZERO (ICLUSLIST,IPARNSP)
  DO I=1,NPS
     IF (I.NE.1.AND.LASTHOLE(I).NE.-99) IG=IG+1
     ICLUSLIST(IG)=ICLUSLIST(IG)+1
  ENDDO
  WRITE (OUTTERM,'(I2,A)') IG,' groups of atoms'
  J=1
  DO I=1,IG
     WRITE (OUTTERM,'(A,I2,A,I2,A,I2)')'Group ',I,': atoms ',J,' to ',J+ICLUSLIST(IG)-1
     J=J+ICLUSLIST(IG)
  ENDDO
  WRITE (OUTTERM,'(A,20I2)') 'Last used IATGROUP:',(IATGROUP(I),I=1,NPS)
  RETURN
440 FORMAT (/3(A,I3),A,2(F5.2,','),F5.2,4(A,I3),')=',F6.1,4(A,I3),')=',F6.1)
450 FORMAT ('Fourier transform options:'/' PS =',I3/' WT =',I3/' ND =',I3/' MF =',I3/' CN =',I3/' BC =',I3/' TR =',I3)
460 FORMAT ('Shell',I3,1X,A3,'=',F8.3,1X,A3,'=',I3,'(',A2,')',3(1X,A3,' =',F8.3),2I2)
470 FORMAT ('Shell',I3,1X,A6,' = ',F4.0,4(1X,A6,' = ',F8.3))
480 FORMAT ('Shell',I3,1X,A3,'=',I1,'(',A,')',3(1X,A3,'=',F8.4),1X,A5,'=',F9.3)
490 FORMAT (/'Unit',I3,' (',I4,') Name ',A,'Occ =',F5.1,' Pivotal atom',I3,' t(',3(I3,'-'),I3,')',F6.1/)
500 FORMAT (5(1X,A,'=',F8.3))
505 FORMAT (5(1X,A5,'=',F8.3))
510 FORMAT (1X,A,' =',F10.4)
520 FORMAT (1X,6A,F10.4)
530 FORMAT (1X,A6,' =',F10.4,1X,'(',A6,' =',F9.4,')')
540 FORMAT (1X,A48)
550 FORMAT (' D',I2,': ',20(F6.3,'(',F6.3,')'))
560 FORMAT (' A',I2,': ',20(F6.2,'(',F6.2,')'))
570 FORMAT (A,I2,': ',20F6.2/8X,20F6.2)
580 FORMAT (7X,32(I2,'/',3X))
590 FORMAT (7X,32(I2,'/',11X))
600 FORMAT (A,2(1X,A5,'=',F8.3),3(1X,A3,'=',F8.3))
610 FORMAT (3(3X,A6,'=',F8.3))
620 FORMAT (/'   Spectrum ',I1,'        Spectrum ',I1,'        Spectrum ',I1/)
END SUBROUTINE LDLV
