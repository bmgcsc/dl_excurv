SUBROUTINE SUBMITQ
  !======================================================================C
  Use Common_extension
  CHARACTER*60 DSN,DSN1,DSN2
  CHARACTER*12 IP1,IP2,IP3
  LOGICAL FILOLD
10 CALL WTEXT ('Enter filename, without extension. ( e.g. exouta1 )')
  CALL AREAD (0,DSN,LD,*10,*50)
20 CALL WTEXT ('Enter Time in Seconds or "=" to Escape')
  CALL CREAD (IP1,IP2,IP3,NSEC,IC,V,*20,*50)
  IF (DSN(1:3).EQ.'exc') THEN
     ITYP=1
  ELSEIF (DSN(1:6).EQ.'explot') THEN
     ITYP=5
  ELSEIF (DSN(1:6).EQ.'exanes') THEN
     ITYP=6
  ELSE
30   CALL WTEXT ('Enter file type :')
     CALL WTEXT ('  1  calculation')
     CALL WTEXT ('  5  plot')
     CALL WTEXT ('  6  xanes')
     CALL CREAD (IP1,IP2,IP3,ITYP,IC,V,*30,*50)
     IF (ITYP.NE.1.AND.ITYP.NE.5.AND.ITYP.NE.6) GOTO 30
  ENDIF
  NC=NCSTR(DSN)
  IF (ITYP.LE.5) THEN
     DSN1=DSN(1:NC)//'.'//EXTUNFM
  ELSE
     DSN1=DSN(1:NC)//'.'//EXTATOM
  ENDIF
  DSN2=DSN(1:NC)//'.'//EXTDATA
  IF (FILOLD(DSN2)) THEN
40   CALL WTEXT ('Delete '//DSN2(1:NC+5)//' ?')
     CALL CREAD (IP1,IP2,IP3,IDUM,IC,V,*40,*50)
     IF (IP1(1:1).NE.'Y') RETURN
     !-st	  ISTAT=SYSTEM('/bin/rm '//DSN2//char(0))
  ENDIF
  CALL SUBMIT (ITYP,DSN1,DSN2,NSEC,*50)
50 RETURN
END SUBROUTINE SUBMITQ
