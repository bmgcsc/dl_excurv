SUBROUTINE ERRS (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_PMTCH
  Use Common_PMTX
  Use Include_XY
  !
  !	ERRS is called after an error to ensure that the default mode
  !	is entered for various program options.
  !
  !	Reset defaults
  !
  !	Cancel noprompt override flag
  !
  IPR=0
  !
  !	Mark input buffer as empty
  !
  LRIS=0
  !
  !	Free macro file
  !
  IF (LMDSN.NE.0) CLOSE (COMFILE)
  !
  !	Mark macro as not active
  !
  LMDSN=0
  !
  !	Return to error handling address.
  !
  RETURN 1
END SUBROUTINE ERRS
