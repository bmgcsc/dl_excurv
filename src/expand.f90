SUBROUTINE EXPAND
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_UPU
  Use Common_COMPAR
  Use Common_SPAREPA1
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1

  DIMENSION  :: TEMPA(0:IPARNS),TEMPT(0:IPARNS)
  if (keyword(1:1).eq.'L') then
     call expand1
     return
  endif
  JCLUS=1
  IF (NAT(JCLUS).LT.1) THEN
     KEYWORD=' '
     CALL SYMMETRY (.TRUE.,.TRUE.,*20)
  ENDIF
  CALL UCOPY (PA1,SPAREPA1,IPARNPARAMS)
  J=0
  DO I=1,MAXCLUS
     J=J+NAT(I)
  ENDDO
  IF (J.GT.IPARNS) CALL WTEXT ('Too many shells - list truncated')
  CALL UCOPY (A,TEMPA,IPARNS)
  CALL UCOPY (T,TEMPT,IPARNS)
  DO I=1,MAXCLUS
     PGROUP(I)='C1'
     NPGR(I)=1
  ENDDO
  NS=-1
  DO JCLUS=1,MAXCLUS
     DO I=0,NAT(JCLUS)
	IF (NS.GE.IPARNS) cycle
	NS=NS+1
	UX(NS)=AX(I,JCLUS)
	UY(NS)=AY(I,JCLUS)
	UZ(NS)=AZ(I,JCLUS)
	R(NS)=AR(I,JCLUS)
	A(NS)=TEMPA(ISHELL(I,JCLUS))
	T(NS)=TEMPT(ISHELL(I,JCLUS))
	CLUS(NS)=JCLUS
	IF (I.EQ.0) CLUS(NS)=-CLUS(NS)
	RN(NS)=1.
	UN(NS)=JCLUS
     enddo
  ENDDO
  RNS=NS
  CALL FLIM (*11)
11 CALL UTAB (1)
  CALL IUZERO (NAT,IPARNCLUS)
  CALL SYMMETRY (.TRUE.,.TRUE.,*20)
20 RETURN
END SUBROUTINE EXPAND
