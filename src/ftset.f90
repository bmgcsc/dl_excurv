SUBROUTINE FTSET (IP1,K4,INT,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_SETKW
  Use Common_PMTCH
  !-st	Use Common_COMPAR
  Use Common_ICF
  Use Common_RCBUF
  Use Common_VIEW
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XA
  !
  !	Set program options
  !
  CHARACTER*(*) K4
  CHARACTER*12 IP1,K3,KP3,MSG
  CHARACTER CRESET*5,CQUIT*4,CLIST*4
  INTEGER(i4b) :: JSET(IPARFTOPTS),LCSTAT(IPARFTOPTS)
  LOGICAL IEXIT
  DATA CRESET/'RESET'/,CQUIT/'QUIT'/,CLIST/'LIST'/,LCSTAT/2,0,0,0,0,0,0,0/
  !
  !	Set FT and RDF options
  !
  IEXIT=IP1.NE.' '.AND.K4.NE.' '
  DO I=1,IPARFTOPTS
     JSET(I)=IFTSET(I)
  enddo
  IF (IP1.NE.' ') GOTO 90
  GOTO 70
20 DO I=1,IPARFTOPTS
     IFTSET(I)=1-IFTMINUS(I)
  enddo
  RETURN 1
40 CALL WTEXT  ('FTSET OPTIONS-------- ')
  CALL WTEXT  (' ')
  CALL WTEXT  ('KEYWORD        OPTIONS')
  CALL WTEXT  ('               1             2          3       '//'4  5       NOW')
  CALL WTEXTI ('FTPHASE        First-shell   Second     None    '//'          ',IFTSET(1)+IFTMINUS(1))
  CALL WTEXTI ('FTWEIGHT       None          K          K2      '//'K3 K/fpim ',IFTSET(2)+IFTMINUS(2))
  CALL WTEXTI ('FTWIND         Blackman-Harr Gaussian   Hanning '//'Kaiser-B  ',IFTSET(3)+IFTMINUS(3))
  CALL WTEXTI ('FTMULT         One           R-squared          '//'          ',IFTSET(4)+IFTMINUS(4))
  CALL WTEXTI ('FTCENT         Auto          K          Energy  '//'          ',IFTSET(5)+IFTMINUS(5))
  CALL WTEXTI ('RDF            Atomic        Electron   Atomic/r'//'^2        ',IFTSET(6)+IFTMINUS(6))
  CALL WTEXTI ('TRANSFORM      Modulus       Sine+Modulus       '//'          ',IFTSET(7)+IFTMINUS(7))
  !	CALL WTEXTI ('ATOMIC_ABS     Off           On                 '//'          ',IFTSET(8)+IFTMINUS(8))
  GOTO 70
50 CALL ERRMSG ('Invalid keyword '//IP1,*70)
  !
  !	FTN77 needs the next line
  !
60 MSG=K4
  CALL ERRMSG ('Invalid option '//MSG,*70)
70 IF (IEXIT) GOTO 160
  IF (LRIS.GT.0) GOTO 80
  CALL WTEXT (' ')
  CALL WTEXT ('Enter keyword + option, LIST, RESET or QUIT ')
80 CALL CREAD (IP1,K4,KP3,INT,IC,V,*70,*70)
90 NIP1=NCSTR(IP1)
  NIP4=MIN(NIP1,4)
  NK4=NCSTR(K4)
  IF (IP1(1:NIP4).EQ.CRESET(1:NIP4)) GOTO 20
  IF (IP1(1:NIP4).EQ.CLIST(1:NIP4).OR.IP1(1:1).EQ.'?') GOTO 40
  IF (IP1(1:NIP4).EQ.CQUIT(1:NIP4)) GOTO 160
  DO J=1,IPARFTOPTS
     IF (IP1(1:NIP1).EQ.FTSETKW(J)(1:NIP1)) GOTO 120
  enddo
  CALL ERRS (*50)
120 IF (K4.EQ.' '.AND.INT.EQ.0) THEN
130  CALL WTEXT ('Option ?')
     CALL CREAD (K4,K3,KP3,INT,IC,V,*130,*70)
     NK4=NCSTR(K4)
  ENDIF
  IF (INT.NE.0) THEN
     IF (INT.LT.0.OR.INT.GT.IFTSETMAX(J)) CALL ERRS (*60)
  ELSE
     DO INT=1,IFTSETMAX(J)
        IF (K4(1:NK4).EQ.FTSETKWO(INT,J)(1:NK4)) GOTO 150
     enddo
     INT=0
     GOTO 60
150  CONTINUE
  ENDIF
  IF (LMDSN.EQ.0) WRITE (OUTTERM,170) FTSETKW(J),FTSETKWO(INT,J),FTSETKWO(JSET(J)+IFTMINUS(J),J)
  IFTSET(J)=INT-IFTMINUS(J)
  INT=0
  GOTO 70
160 CONTINUE
  DO I=1,IPARFTOPTS
     IF (IFTSET(I).NE.JSET(I)) THEN
        LF=2
        LC=IOR(LCSTAT(I),LC)
     ENDIF
  ENDDO
  RETURN
170 FORMAT(' Keyword: ',A14,' Option: ',A14,' ( previously: ',A14,')')
END SUBROUTINE FTSET
