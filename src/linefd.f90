SUBROUTINE LINEFD (NIN)
  !======================================================================C
  !-st	Use Parameters
  Use Common_PMTX
  IF (QDQ) THEN
     CALL FQQGETGP2 (TXMIN,TYMIN)
     TYMIN=TYMIN+NIN*.75
     CALL FQQMOVE2 (0.,TYMIN)
  ELSE
     CALL FGETGP2 (TXMIN,TYMIN)
     TYMIN=TYMIN+NIN
     CALL FMOVE2 (0.,TYMIN)
  ENDIF
  RETURN
END SUBROUTINE LINEFD
