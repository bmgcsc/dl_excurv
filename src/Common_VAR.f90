MODULE Common_VAR

  !      COMMON /VAR/IVARS(IPARNPARAMS)/SVAR/SVARS(IPARNS)/DVARS/DVARS(2)
  !      CHARACTER*8 IVARS,SVARS,DVARS
  !      data DVARS/'DF1','DF2'/

  Use Definition
  Use Parameters
  implicit none
  private

  character*8, dimension(IPARNPARAMS), public :: IVARS
  character*8, dimension(IPARNS),      public :: SVARS
  character*8, dimension(2),           public :: DVARS = (/'DF1','DF2'/)
end MODULE Common_VAR
