SUBROUTINE XNSUB (IP1,*)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_PGROUP
  Use Common_ATOMCORD
  Use Common_COMPAR
  Use Common_MATEL
  Use Common_P
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY

  CHARACTER*(*) IP1
  CHARACTER*160 XTITLE
  CHARACTER*60 DSN,OLDDSN
  DATA OLDDSN /'exmtxa1.dat'/
10 CONTINUE
  IF (NME.NE.0) THEN
     CALL WTEXT ('Enter Filename for Matrix Elements [current data].'  )
  ELSE
     CALL WTEXT ('Enter Filename for Matrix Elements ['//OLDDSN(1:20)//'].')
  ENDIF
  CALL AREAD (1,DSN,LD,*20,*110)
  GOTO 30
20 IF (NME.GT.10) GOTO 40
  DSN=OLDDSN
30 CALL FILEOPEN (DSN,INFILE,'FORMATTED','dat',*10)
  NME=IPARPHPOINTS
  LH=1
  !	IF (LHEAD.NE.0) LH=-1
  CALL RDSPEC (INFILE,NME,XTITLE,XME,1,RME,2,LH,*100)
  CLOSE (INFILE)
  OLDDSN=DSN
40 TXMIN=XME(1)+.001
  DO  K=1,NS
     IF (NU(K).LT.1) cycle
     IF (EN(1,IT(K)).GT.XMIN2) TXMIN=EN(1,IT(K))
  enddo
  IF (TXMIN.GT.XMIN2) XMIN2=TXMIN
  XMIN=XMIN2/EC
  IOUT1=OUTFILE
  IOUT2=OUTFILE
  CALL GETSHELL (OUTFILE,*80,*90)
  DO I=1,NPS
     NM=NPH(I)
     IF (IATOM(I).EQ.0) THEN
        WRITE (OUTFILE,130) I
     ELSE
        WRITE (OUTFILE,140) ELS(IATOM(I)),I
     ENDIF
     WRITE (OUTFILE,120) NM
     DO J=1,NM
        WRITE (OUTFILE,150) EN(J,I),EREF(J,I),((PHS(J,I,L)),L=1,IXLMAX+1)
     enddo
     IF (I.EQ.1) THEN
        WRITE (OUTFILE,120) NME
        WRITE (OUTFILE,160) (XME(K),RME(K,1),K=1,NME)
     ENDIF
  enddo
  CALL EFILE ('XANES Parameters and Phaseshifts ')
  IF (IP1(1:1).EQ.'S') THEN
     KEYWORD='XANES'
     CALL BSUB (0,0,0)
  ENDIF
  RETURN
80 CLOSE (OUTFILE)
90 CLOSE (OUTFILE)
  RETURN 1
100 CLOSE (INFILE)
110 RETURN 1
  !
120 FORMAT (2I5)
130 FORMAT (' Phaseshifts for Atom Type',I2)
140 FORMAT (' Phaseshifts for ',A2,' ( atom type',I2,' )')
150 FORMAT (27E12.5)
160 FORMAT (2E12.5)
END SUBROUTINE XNSUB
