MODULE Common_IPA

  !      COMMON /IPA/ IEQ,IVPI,IQQQQ,IRRRR,ISSSS,LMAX,IDLMAX,ITLMAX,IWP,NS,
  !     *N(0:IPARNS),IT(0:IPARNS),IR(0:IPARNS),IA(0:IPARNS),IB(0:IPARNS),IC
  !     *Q(0:IPARNS),IDQ(0:IPARNS),NU(0:IPARNS),IANG(0:IPARNS),ITH(0:IPARNS
  !     *),IPHI(0:IPARNS),IUX(0:IPARNS),IUY(0:IPARNS),IUZ(0:IPARNS),IROT(0:
  !     *IPARNS),IPIV(0:IPARNS),IPLA(0:IPARNS),IPLB(0:IPARNS),ITORA(0:IPARN
  !     *S),ITORB(0:IPARNS),ITORC(0:IPARNS),IPAT(0:IPARNS),IPANG(0:IPARNS),
  !     *IUOC(0:IPARNS),ITWST(0:IPARNS),ITILT(0:IPARNS),ICLUS(0:IPARNS),LIN
  !     *K(0:IPARNS),IPOSX(0:IPARNS),IPOSY(0:IPARNS),IPOSZ(0:IPARNS),IBI(0
  !     *:IPARNS),IPOST0(0:IPARNSP),IPOST1(0:IPARNSP),IPOST2(0:IPARNSP),IPO
  !     *ST3(0:IPARNSP),IPOST4(0:IPARNSP),IPOST5(0:IPARNSP),IPOST6(0:IPARNS
  !     *P),IPOST7(0:IPARNSP),IPOST8(0:IPARNSP),IPRE0(0:IPARNSP),IPRE1(0:IP
  !     *ARNSP),IPRE2(0:IPARNSP),IAFAC0(0:IPARNSP),IWEX0(0:IPARNSP),IEF0(0:
  !     *IPARNSP),IBETH0(0:IPARNSP),IBEPHI0(0:IPARNSP),IBEW0(0:IPARNSP),IEM
  !     *IN0(0:IPARNSP),IEMAX0(0:IPARNSP),IESUM0(0:IPARNSP),
  !     *IVPI0(0:IPARNSP),IMOPG0(0:IPARNSP),IXE(IPARNP),IXV
  !     *(IPARNP),IATOM(IPARNP),ION(IPARNP)
  !      COMMON /IPA/ MTR(IPARNP),IALF(IPARNP),ICMAG(IPARNP),ISDX(IPARNP),I
  !     *SDY(IPARNP),ISDZ(IPARNP),IPERCA(IPARNP),IPERCB(IPARNP),IPERCC(IPAR
  !     *NP),KMIN,KMAX,IPMIN,IPMAX,IOMIN,IOMAX,IRMIN,IRMAX,IWIND,ISPARE12,I
  !     *SPARE13,ISPARE14,ISPARE15,ISPARE16,ISPARE17,ISPARE18,ISPARE19,ISPA
  !     *RE20,IDWMIN,IDWMAX,IXMIN,IXMAX,IYMIN,IYMAX,ITMIN,ITMAX,ID1A,ID1B,I
  !     *D2A,ID2B,IOUTPUT,IBOND,ISIZE,IVX,IVY,IVZ,MEM(10),IXEMIN,IXEMAX,IXN
  !     *P,IXLMAX,IXLOUT,IXOPT,MINDIST,IWD,IWA,IR4W,MINANG,IPLMIN,IPLMAX,NG
  !     *RID,IDFAC,NPARS,ISNOISE,MINMAG,ISPARE9,ISPARE8,IWNOISE,INPEAKS,ISP
  !     *ARE11,IATMAX,IVECA,IVECB,IE0,IBFAC,ININD,IV0G,IFE0G,IRHO0G,IZMIN,I
  !     *ZMAX,ISURREL,ITRANSX,ITRANSY,ITRANSZ,ISURROT,IACELL,IBCELL,ICCELL,
  !     *ICELLALPHA,ICELLBETA,ICELLGAMMA,IAH,IBH,ICH,IBACK0,IBACK1,IBACK2,I
  !     *BACK3,IBACK4,IBACK5,IBACK6,IBACK7,IBACK8,IOFFSET,ICENT,MIXA,MIXB,N
  !     *SPEC,MAXRAD,ISPARE1,ISPARE2,ISPARE3,ITEMPQ,ICLE,NUMAX
  !      INTEGER IPA(IPARNPARAMS)
  !      EQUIVALENCE (IPA(1),IEQ)


  USE Definition
  Use Parameters
  implicit none
  !  private
  !  integer(i4b), dimension(:),allocatable, public :: IPA
  !  integer(i4b), dimension(:),public :: IPA(IPARNPARAMS)
  !  integer(i4b), dimension(:),public :: IPA(10 + 32*(IPARNS  + 1) + 23*(IPARNSP + 1) + 13*IPARNP + 115)
  integer(i4b), dimension(:),public :: IPA(10 + 32*(IPARNS  + 1) + 23*(IPARNSP + 1) + 13*IPARNP + 115)
  integer(i4b), public :: IEQ, IVPI, IQQQQ, IRRRR, ISSSS, LMAX, IDLMAX, ITLMAX, IWP, NS
  integer(i4b), dimension(:), public ::    N(0:IPARNS),   IT(0:IPARNS),   IR(0:IPARNS),   IA(0:IPARNS),   IB(0:IPARNS), &
       ICQ(0:IPARNS),  IDQ(0:IPARNS),   NU(0:IPARNS), IANG(0:IPARNS),  ITH(0:IPARNS), &
       IPHI(0:IPARNS),  IUX(0:IPARNS),  IUY(0:IPARNS),  IUZ(0:IPARNS), IROT(0:IPARNS), &
       IPIV(0:IPARNS), IPLA(0:IPARNS), IPLB(0:IPARNS),ITORA(0:IPARNS),ITORB(0:IPARNS), &
       ITORC(0:IPARNS), IPAT(0:IPARNS),IPANG(0:IPARNS), IUOC(0:IPARNS),ITWST(0:IPARNS), &
       ITILT(0:IPARNS),ICLUS(0:IPARNS), LINK(0:IPARNS),IPOSX(0:IPARNS),IPOSY(0:IPARNS), &
       IPOSZ(0:IPARNS),  IBI(0:IPARNS)
  integer(i4b), dimension(:), public ::   IPOST0(0:IPARNSP), IPOST1(0:IPARNSP),IPOST2(0:IPARNSP),IPOST3(0:IPARNSP),IPOST4(0:IPARNSP), &
       IPOST5(0:IPARNSP), IPOST6(0:IPARNSP),IPOST7(0:IPARNSP),IPOST8(0:IPARNSP), IPRE0(0:IPARNSP), &
       IPRE1(0:IPARNSP),  IPRE2(0:IPARNSP),IAFAC0(0:IPARNSP), IWEX0(0:IPARNSP),  IEF0(0:IPARNSP), &
       IBETH0(0:IPARNSP),IBEPHI0(0:IPARNSP), IBEW0(0:IPARNSP),IEMIN0(0:IPARNSP),IEMAX0(0:IPARNSP), &
       IESUM0(0:IPARNSP),  IVPI0(0:IPARNSP),IMOPG0(0:IPARNSP)
  integer(i4b), dimension(:), public ::  IXE(1:IPARNP),   IXV(1:IPARNP),  IATOM(1:IPARNP),    ION(1:IPARNP),    MTR(1:IPARNP), &
       IALF(1:IPARNP), ICMAG(1:IPARNP),   ISDX(1:IPARNP),   ISDY(1:IPARNP),   ISDZ(1:IPARNP), &
       IPERCA(1:IPARNP),IPERCB(1:IPARNP), IPERCC(1:IPARNP)
  integer(i4b), public :: KMIN,       KMAX,    IPMIN,    IPMAX,    IOMIN,    IOMAX,     IRMIN,    IRMAX,   IWIND,   ISPARE12, &
       ISPARE13,   ISPARE14, ISPARE15, ISPARE16, ISPARE17, ISPARE18,  ISPARE19, ISPARE20,  IDWMIN,     IDWMAX, &
       IXMIN,      IXMAX,    IYMIN,    IYMAX,    ITMIN,    ITMAX,      ID1A,     ID1B,    ID2A,       ID2B, &
       IOUTPUT,      IBOND,    ISIZE,      IVX,      IVY,      IVZ
  integer(i4b), dimension(:), public :: MEM(1:10)
  integer(i4b), public ::  IXEMIN,     IXEMAX,     IXNP, &
       IXLMAX,     IXLOUT,    IXOPT,  MINDIST,      IWD,      IWA,      IR4W,   MINANG,  IPLMIN,     IPLMAX, &
       NGRID,      IDFAC,    NPARS,  ISNOISE,   MINMAG,  ISPARE9,   ISPARE8,  IWNOISE, INPEAKS,   ISPARE11, &
       IATMAX,      IVECA,    IVECB,      IE0,    IBFAC,    ININD,      IV0G,    IFE0G,  IRHO0G,      IZMIN, &
       IZMAX,    ISURREL,  ITRANSX,  ITRANSY,  ITRANSZ,  ISURROT,    IACELL,   IBCELL,  ICCELL, ICELLALPHA, &
       ICELLBETA, ICELLGAMMA,      IAH,      IBH,      ICH,   IBACK0,    IBACK1,   IBACK2,  IBACK3,     IBACK4, &
       IBACK5,     IBACK6,   IBACK7,   IBACK8,  IOFFSET,    ICENT,      MIXA,     MIXB,   NSPEC,     MAXRAD, &
       ISPARE1,    ISPARE2,  ISPARE3,   ITEMPQ,     ICLE,    NUMAX
  integer(i4b),parameter,private :: k1 = 10
  integer(i4b),parameter,private :: k2 = k1 + 32*(IPARNS  + 1)
  integer(i4b),parameter,private :: k3 = k2 + 23*(IPARNSP + 1)
  integer(i4b),parameter,private :: k4 = k3 + 13*IPARNP
  integer(i4b),parameter,private :: k5 = k4 + 115
contains
  !  subroutine allocate_IPA
  !    integer(I4B) :: error
  !    allocate( IPA(IPARNPARAMS), Stat = error)
  !    if( error /= 0) then
  !       call errvrs(error, 'allocate_std', 'Memory allocation for Experiment')
  !    endif
  !  end subroutine allocate_IPA

  subroutine all_IPA
    IPA(   1:k1) =  (/ IEQ, IVPI, IQQQQ, IRRRR, ISSSS, LMAX, IDLMAX, ITLMAX, IWP, NS /)
    IPA(k1+1:k2) =  (/   N(0:IPARNS),   IT(0:IPARNS),   IR(0:IPARNS),   IA(0:IPARNS),   IB(0:IPARNS), &
         ICQ(0:IPARNS),  IDQ(0:IPARNS),   NU(0:IPARNS), IANG(0:IPARNS),  ITH(0:IPARNS), &
         IPHI(0:IPARNS),  IUX(0:IPARNS),  IUY(0:IPARNS),  IUZ(0:IPARNS), IROT(0:IPARNS), &
         IPIV(0:IPARNS), IPLA(0:IPARNS), IPLB(0:IPARNS),ITORA(0:IPARNS),ITORB(0:IPARNS), &
         ITORC(0:IPARNS), IPAT(0:IPARNS),IPANG(0:IPARNS), IUOC(0:IPARNS),ITWST(0:IPARNS), &
         ITILT(0:IPARNS),ICLUS(0:IPARNS), LINK(0:IPARNS),IPOSX(0:IPARNS),IPOSY(0:IPARNS), &
         IPOSZ(0:IPARNS),  IBI(0:IPARNS) /)
    IPA(k2+1:k3) =  (/  IPOST0(0:IPARNSP), IPOST1(0:IPARNSP),IPOST2(0:IPARNSP),IPOST3(0:IPARNSP),IPOST4(0:IPARNSP), &
         IPOST5(0:IPARNSP), IPOST6(0:IPARNSP),IPOST7(0:IPARNSP),IPOST8(0:IPARNSP), IPRE0(0:IPARNSP), &
         IPRE1(0:IPARNSP),  IPRE2(0:IPARNSP),IAFAC0(0:IPARNSP), IWEX0(0:IPARNSP),  IEF0(0:IPARNSP), &
         IBETH0(0:IPARNSP),IBEPHI0(0:IPARNSP), IBEW0(0:IPARNSP),IEMIN0(0:IPARNSP),IEMAX0(0:IPARNSP), &
         IESUM0(0:IPARNSP),  IVPI0(0:IPARNSP),IMOPG0(0:IPARNSP) /)
    IPA(k3+1:k4) =  (/  IXE(1:IPARNP),   IXV(1:IPARNP),  IATOM(1:IPARNP),    ION(1:IPARNP),    MTR(1:IPARNP), &
         IALF(1:IPARNP), ICMAG(1:IPARNP),   ISDX(1:IPARNP),   ISDY(1:IPARNP),   ISDZ(1:IPARNP), &
         IPERCA(1:IPARNP),IPERCB(1:IPARNP), IPERCC(1:IPARNP) /)
    IPA(k4+1:k5) =  (/    KMIN,       KMAX,    IPMIN,    IPMAX,    IOMIN,    IOMAX,     IRMIN,    IRMAX,   IWIND,   ISPARE12, &
         ISPARE13,   ISPARE14, ISPARE15, ISPARE16, ISPARE17, ISPARE18,  ISPARE19, ISPARE20,  IDWMIN,     IDWMAX, &
         IXMIN,      IXMAX,    IYMIN,    IYMAX,    ITMIN,    ITMAX,      ID1A,     ID1B,    ID2A,       ID2B, &
         IOUTPUT,      IBOND,    ISIZE,      IVX,      IVY,      IVZ, MEM(1:10),   IXEMIN,  IXEMAX,       IXNP, &
         IXLMAX,     IXLOUT,    IXOPT,  MINDIST,      IWD,      IWA,      IR4W,   MINANG,  IPLMIN,     IPLMAX, &
         NGRID,      IDFAC,    NPARS,  ISNOISE,   MINMAG,  ISPARE9,   ISPARE8,  IWNOISE, INPEAKS,   ISPARE11, &
         IATMAX,      IVECA,    IVECB,      IE0,    IBFAC,    ININD,      IV0G,    IFE0G,  IRHO0G,      IZMIN, &
         IZMAX,    ISURREL,  ITRANSX,  ITRANSY,  ITRANSZ,  ISURROT,    IACELL,   IBCELL,  ICCELL, ICELLALPHA, &
         ICELLBETA, ICELLGAMMA,      IAH,      IBH,      ICH,   IBACK0,    IBACK1,   IBACK2,  IBACK3,     IBACK4, &
         IBACK5,     IBACK6,   IBACK7,   IBACK8,  IOFFSET,    ICENT,      MIXA,     MIXB,   NSPEC,     MAXRAD, &
         ISPARE1,    ISPARE2,  ISPARE3,   ITEMPQ,     ICLE,    NUMAX  /)
  end subroutine all_IPA


  subroutine IPA_all
    IEQ      = IPA( 1)
    IVPI     = IPA( 2)
    IQQQQ    = IPA( 3)
    IRRRR    = IPA( 4)
    ISSSS    = IPA( 5)
    LMAX     = IPA( 6)
    IDLMAX   = IPA( 7)
    ITLMAX   = IPA( 8)
    IWP      = IPA( 9)
    NS       = IPA(10)

    N(0:IPARNS)     = IPA(k1                 + 1:k1 +  1*(IPARNS+1))
    IT(0:IPARNS)    = IPA(k1 +  1*(IPARNS+1) + 1:k1 +  2*(IPARNS+1))
    IR(0:IPARNS)    = IPA(k1 +  2*(IPARNS+1) + 1:k1 +  3*(IPARNS+1))
    IA(0:IPARNS)    = IPA(k1 +  3*(IPARNS+1) + 1:k1 +  4*(IPARNS+1))
    IB(0:IPARNS)    = IPA(k1 +  4*(IPARNS+1) + 1:k1 +  5*(IPARNS+1))
    ICQ(0:IPARNS)   = IPA(k1 +  5*(IPARNS+1) + 1:k1 +  6*(IPARNS+1))
    IDQ(0:IPARNS)   = IPA(k1 +  6*(IPARNS+1) + 1:k1 +  7*(IPARNS+1))
    NU(0:IPARNS)    = IPA(k1 +  7*(IPARNS+1) + 1:k1 +  8*(IPARNS+1))
    IANG(0:IPARNS)  = IPA(k1 +  8*(IPARNS+1) + 1:k1 +  9*(IPARNS+1))
    ITH(0:IPARNS)   = IPA(k1 +  9*(IPARNS+1) + 1:k1 + 10*(IPARNS+1))
    IPHI(0:IPARNS)  = IPA(k1 + 10*(IPARNS+1) + 1:k1 + 11*(IPARNS+1))
    IUX(0:IPARNS)   = IPA(k1 + 11*(IPARNS+1) + 1:k1 + 12*(IPARNS+1))
    IUY(0:IPARNS)   = IPA(k1 + 12*(IPARNS+1) + 1:k1 + 13*(IPARNS+1))
    IUZ(0:IPARNS)   = IPA(k1 + 13*(IPARNS+1) + 1:k1 + 14*(IPARNS+1))
    IROT(0:IPARNS)  = IPA(k1 + 14*(IPARNS+1) + 1:k1 + 15*(IPARNS+1))
    IPIV(0:IPARNS)  = IPA(k1 + 15*(IPARNS+1) + 1:k1 + 16*(IPARNS+1))
    IPLA(0:IPARNS)  = IPA(k1 + 16*(IPARNS+1) + 1:k1 + 17*(IPARNS+1))
    IPLB(0:IPARNS)  = IPA(k1 + 17*(IPARNS+1) + 1:k1 + 18*(IPARNS+1))
    ITORA(0:IPARNS) = IPA(k1 + 18*(IPARNS+1) + 1:k1 + 19*(IPARNS+1))
    ITORB(0:IPARNS) = IPA(k1 + 19*(IPARNS+1) + 1:k1 + 20*(IPARNS+1))
    ITORC(0:IPARNS) = IPA(k1 + 20*(IPARNS+1) + 1:k1 + 21*(IPARNS+1))
    IPAT(0:IPARNS)  = IPA(k1 + 21*(IPARNS+1) + 1:k1 + 22*(IPARNS+1))
    IPANG(0:IPARNS) = IPA(k1 + 22*(IPARNS+1) + 1:k1 + 23*(IPARNS+1))
    IUOC(0:IPARNS)  = IPA(k1 + 23*(IPARNS+1) + 1:k1 + 24*(IPARNS+1))
    ITWST(0:IPARNS) = IPA(k1 + 24*(IPARNS+1) + 1:k1 + 25*(IPARNS+1))
    ITILT(0:IPARNS) = IPA(k1 + 25*(IPARNS+1) + 1:k1 + 26*(IPARNS+1))
    ICLUS(0:IPARNS) = IPA(k1 + 26*(IPARNS+1) + 1:k1 + 27*(IPARNS+1))
    LINK(0:IPARNS)  = IPA(k1 + 27*(IPARNS+1) + 1:k1 + 28*(IPARNS+1))
    IPOSX(0:IPARNS) = IPA(k1 + 28*(IPARNS+1) + 1:k1 + 29*(IPARNS+1))
    IPOSY(0:IPARNS) = IPA(k1 + 29*(IPARNS+1) + 1:k1 + 30*(IPARNS+1))
    IPOSZ(0:IPARNS) = IPA(k1 + 30*(IPARNS+1) + 1:k1 + 31*(IPARNS+1))
    IBI(0:IPARNS)   = IPA(k1 + 31*(IPARNS+1) + 1:k1 + 32*(IPARNS+1))

    IPOST0(0:IPARNSP)  = IPA(k2                  + 1:k2 +  1*(IPARNSP+1))
    IPOST1(0:IPARNSP)  = IPA(k2 +  1*(IPARNSP+1) + 1:k2 +  2*(IPARNSP+1))
    IPOST2(0:IPARNSP)  = IPA(k2 +  2*(IPARNSP+1) + 1:k2 +  3*(IPARNSP+1))
    IPOST3(0:IPARNSP)  = IPA(k2 +  3*(IPARNSP+1) + 1:k2 +  4*(IPARNSP+1))
    IPOST4(0:IPARNSP)  = IPA(k2 +  4*(IPARNSP+1) + 1:k2 +  5*(IPARNSP+1))
    IPOST5(0:IPARNSP)  = IPA(k2 +  5*(IPARNSP+1) + 1:k2 +  6*(IPARNSP+1))
    IPOST6(0:IPARNSP)  = IPA(k2 +  6*(IPARNSP+1) + 1:k2 +  7*(IPARNSP+1))
    IPOST7(0:IPARNSP)  = IPA(k2 +  7*(IPARNSP+1) + 1:k2 +  8*(IPARNSP+1))
    IPOST8(0:IPARNSP)  = IPA(k2 +  8*(IPARNSP+1) + 1:k2 +  9*(IPARNSP+1))
    IPRE0(0:IPARNSP)   = IPA(k2 +  9*(IPARNSP+1) + 1:k2 + 10*(IPARNSP+1))
    IPRE1(0:IPARNSP)   = IPA(k2 + 10*(IPARNSP+1) + 1:k2 + 11*(IPARNSP+1))
    IPRE2(0:IPARNSP)   = IPA(k2 + 11*(IPARNSP+1) + 1:k2 + 12*(IPARNSP+1))
    IAFAC0(0:IPARNSP)  = IPA(k2 + 12*(IPARNSP+1) + 1:k2 + 13*(IPARNSP+1))
    IWEX0(0:IPARNSP)   = IPA(k2 + 13*(IPARNSP+1) + 1:k2 + 14*(IPARNSP+1))
    IEF0(0:IPARNSP)    = IPA(k2 + 14*(IPARNSP+1) + 1:k2 + 15*(IPARNSP+1))
    IBETH0(0:IPARNSP)  = IPA(k2 + 15*(IPARNSP+1) + 1:k2 + 16*(IPARNSP+1))
    IBEPHI0(0:IPARNSP) = IPA(k2 + 16*(IPARNSP+1) + 1:k2 + 17*(IPARNSP+1))
    IBEW0(0:IPARNSP)   = IPA(k2 + 17*(IPARNSP+1) + 1:k2 + 18*(IPARNSP+1))
    IEMIN0(0:IPARNSP)  = IPA(k2 + 18*(IPARNSP+1) + 1:k2 + 19*(IPARNSP+1))
    IEMAX0(0:IPARNSP)  = IPA(k2 + 19*(IPARNSP+1) + 1:k2 + 20*(IPARNSP+1))
    IESUM0(0:IPARNSP)  = IPA(k2 + 20*(IPARNSP+1) + 1:k2 + 21*(IPARNSP+1))
    IVPI0(0:IPARNSP)   = IPA(k2 + 21*(IPARNSP+1) + 1:k2 + 22*(IPARNSP+1))
    IMOPG0(0:IPARNSP)  = IPA(k2 + 22*(IPARNSP+1) + 1:k2 + 23*(IPARNSP+1))

    IXE(1:IPARNP)     = IPA(k3             + 1:k3 +  1*IPARNP)
    IXV(1:IPARNP)     = IPA(k3 +  1*IPARNP + 1:k3 +  2*IPARNP)
    IATOM(1:IPARNP)   = IPA(k3 +  2*IPARNP + 1:k3 +  3*IPARNP)
    ION(1:IPARNP)     = IPA(k3 +  3*IPARNP + 1:k3 +  4*IPARNP)
    MTR(1:IPARNP)     = IPA(k3 +  4*IPARNP + 1:k3 +  5*IPARNP)
    IALF(1:IPARNP)    = IPA(k3 +  5*IPARNP + 1:k3 +  6*IPARNP)
    ICMAG(1:IPARNP)   = IPA(k3 +  6*IPARNP + 1:k3 +  7*IPARNP)
    ISDX(1:IPARNP)    = IPA(k3 +  7*IPARNP + 1:k3 +  8*IPARNP)
    ISDY(1:IPARNP)    = IPA(k3 +  8*IPARNP + 1:k3 +  9*IPARNP)
    ISDZ(1:IPARNP)    = IPA(k3 +  9*IPARNP + 1:k3 + 10*IPARNP)
    IPERCA(1:IPARNP)  = IPA(k3 + 10*IPARNP + 1:k3 + 11*IPARNP)
    IPERCB(1:IPARNP)  = IPA(k3 + 11*IPARNP + 1:k3 + 12*IPARNP)
    IPERCC(1:IPARNP)  = IPA(k3 + 12*IPARNP + 1:k3 + 13*IPARNP)

    KMIN        = IPA(k4 +   1)
    KMAX        = IPA(k4 +   2)
    IPMIN       = IPA(k4 +   3)
    IPMAX       = IPA(k4 +   4)
    IOMIN       = IPA(k4 +   5)
    IOMAX       = IPA(k4 +   6)
    IRMIN       = IPA(k4 +   7)
    IRMAX       = IPA(k4 +   8)
    IWIND       = IPA(k4 +   9)
    ISPARE12    = IPA(k4 +  10)
    ISPARE13    = IPA(k4 +  11)
    ISPARE14    = IPA(k4 +  12)
    ISPARE15    = IPA(k4 +  13)
    ISPARE16    = IPA(k4 +  14)
    ISPARE17    = IPA(k4 +  15)
    ISPARE18    = IPA(k4 +  16)
    ISPARE19    = IPA(k4 +  17)
    ISPARE20    = IPA(k4 +  18)
    IDWMIN      = IPA(k4 +  19)
    IDWMAX      = IPA(k4 +  20)
    IXMIN       = IPA(k4 +  21)
    IXMAX       = IPA(k4 +  22)
    IYMIN       = IPA(k4 +  23)
    IYMAX       = IPA(k4 +  24)
    ITMIN       = IPA(k4 +  25)
    ITMAX       = IPA(k4 +  26)
    ID1A        = IPA(k4 +  27)
    ID1B        = IPA(k4 +  28)
    ID2A        = IPA(k4 +  29)
    ID2B        = IPA(k4 +  30)
    IOUTPUT     = IPA(k4 +  31)
    IBOND       = IPA(k4 +  32)
    ISIZE       = IPA(k4 +  33)
    IVX         = IPA(k4 +  34)
    IVY         = IPA(k4 +  35)
    IVZ         = IPA(k4 +  36)
    MEM(1:10)   = IPA(k4 +  37:k4 +  46)
    IXEMIN      = IPA(k4 +  47)
    IXEMAX      = IPA(k4 +  48)
    IXNP        = IPA(k4 +  49)
    IXLMAX      = IPA(k4 +  50)
    IXLOUT      = IPA(k4 +  51)
    IXOPT       = IPA(k4 +  52)
    MINDIST     = IPA(k4 +  53)
    IWD         = IPA(k4 +  54)
    IWA         = IPA(k4 +  55)
    IR4W        = IPA(k4 +  56)
    MINANG      = IPA(k4 +  57)
    IPLMIN      = IPA(k4 +  58)
    IPLMAX      = IPA(k4 +  59)
    NGRID       = IPA(k4 +  60)
    IDFAC       = IPA(k4 +  61)
    NPARS       = IPA(k4 +  62)
    ISNOISE     = IPA(k4 +  63)
    MINMAG      = IPA(k4 +  64)
    ISPARE9     = IPA(k4 +  65)
    ISPARE8     = IPA(k4 +  66)
    IWNOISE     = IPA(k4 +  67)
    INPEAKS     = IPA(k4 +  68)
    ISPARE11    = IPA(k4 +  69)
    IATMAX      = IPA(k4 +  70)
    IVECA       = IPA(k4 +  71)
    IVECB       = IPA(k4 +  72)
    IE0         = IPA(k4 +  73)
    IBFAC       = IPA(k4 +  74)
    ININD       = IPA(k4 +  75)
    IV0G        = IPA(k4 +  76)
    IFE0G       = IPA(k4 +  77)
    IRHO0G      = IPA(k4 +  78)
    IZMIN       = IPA(k4 +  79)
    IZMAX       = IPA(k4 +  80)
    ISURREL     = IPA(k4 +  81)
    ITRANSX     = IPA(k4 +  82)
    ITRANSY     = IPA(k4 +  83)
    ITRANSZ     = IPA(k4 +  84)
    ISURROT     = IPA(k4 +  85)
    IACELL      = IPA(k4 +  86)
    IBCELL      = IPA(k4 +  87)
    ICCELL      = IPA(k4 +  88)
    ICELLALPHA  = IPA(k4 +  89)
    ICELLBETA   = IPA(k4 +  90)
    ICELLGAMMA  = IPA(k4 +  91)
    IAH         = IPA(k4 +  92)
    IBH         = IPA(k4 +  93)
    ICH         = IPA(k4 +  94)
    IBACK0      = IPA(k4 +  95)
    IBACK1      = IPA(k4 +  96)
    IBACK2      = IPA(k4 +  97)
    IBACK3      = IPA(k4 +  98)
    IBACK4      = IPA(k4 +  99)
    IBACK5      = IPA(k4 + 100)
    IBACK6      = IPA(k4 + 101)
    IBACK7      = IPA(k4 + 102)
    IBACK8      = IPA(k4 + 103)
    IOFFSET     = IPA(k4 + 104)
    ICENT       = IPA(k4 + 105)
    MIXA        = IPA(k4 + 106)
    MIXB        = IPA(k4 + 107)
    NSPEC       = IPA(k4 + 108)
    MAXRAD      = IPA(k4 + 109)
    ISPARE1     = IPA(k4 + 110)
    ISPARE2     = IPA(k4 + 111)
    ISPARE3     = IPA(k4 + 112)
    ITEMPQ      = IPA(k4 + 113)
    ICLE        = IPA(k4 + 114)
    NUMAX       = IPA(k4 + 115)
  end subroutine IPA_all


  !  subroutine free_IPA
  !    deallocate(IPA)
  !  end subroutine free_IPA

  subroutine ERRVRS(IERR,NAMZ,MZSS)
    Use Definition
    implicit none
    integer(i4b)  :: IERR
    character*(*) :: NAMZ
    character*(*) :: MZSS
    if(IERR.eq.0)then
       write(*,1)trim(NAMZ),trim(MZSS)
1      format(' NO ERROR **** ',A,' **** ',A)
    else
       write(*,2)trim(NAMZ),trim(MZSS)
2      format(' ERROR IN SYSTEM ROUTINE ATTEMPTTING TO DO ALLOCATION **** ',A,' **** ',A)
    endif
    return
  end subroutine ERRVRS

end MODULE Common_IPA
