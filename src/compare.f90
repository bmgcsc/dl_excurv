SUBROUTINE COMPARE (*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_F
  Use Common_ICF
  Use Index_INDS
  Use Include_XY
  Use Include_FT
  Use Include_OLD
  !
  !	Subroutine to compare spectra
  !
  CHARACTER*60 :: WSPACE
  CHARACTER*12 :: K2,K3,KP3
  CALL SETUP (*80)
  !
  !	Update ft if required
  !
  IF (LF.GT.0) CALL FOURS (1)
  IF (IXA.EQ.3) THEN
     IXA=2
     CALL WTEXT ('XADD switched off after theory update')
  ENDIF
  !
  !	STORE ENER , XABS , NP etc.
  !
  IEXP=1
  OLDRMS=RMS(inds(INDEMIN0)+IEXP)
  OLDRMX=RMX(inds(INDEMAX0)+IEXP)
  !
  CALL UCOPY (XY,OLDXY,(2+IPARNSP*8)*IPARNPOINTS+3*IPARNSP+3)
  !
  !	STORE EXP FT
  !
  CALL UCOPY (FE,OLDFE,IPARNA)
  GOTO 20
10 CALL ERRS (*20)
20 CALL WTEXT ('Enter filename')
  CALL AREAD (1,WSPACE,LD,*20,*70)
  CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','dat',*10)
  LFSAV=WSPACE
30 CALL WTEXT ('Point frequency ?')
  CALL CREAD (K2,K3,KP3,ISP,IC,V,*30,*70)
  IF (ISP.GT.0.AND.ISP.LE.100) GOTO 40
  CALL ERRMSGI ('Invalid point frequency: ',ISP,*30)
40 CALL WTEXT ('Column combination ?')
  CALL CREAD (K2,K3,KP3,ICOL,IC,V,*40,*70)
  IF (ICOL.GE.12.AND.ICOL.LT.100.AND.MOD(ICOL,11).NE.0) GOTO 50
  CALL ERRMSGI ('Invalid column combination: ',ICOL,*40)
  !
  !	Read experiment if required, return 1 occurs on error
  !
50 CALL RDS (1,ICOL,ISP,*70)
  RMS(inds(INDEMIN0)+IEXP)=ENER(1)
  RMX(inds(INDEMAX0)+IEXP)=ENER(NP)
  DO  I=1,NP
     ENER(I)=ENER(I)*EC
  enddo
  !
  !	Free experimental file
  !
  CLOSE (INFILE)
  CALL FLIM (*70)
  CALL UTAB (0)
  CALL CPLOT (INTOPT,*70)
  !
  !	Retrieve old spectra after compare
  !
70 CALL UCOPY (OLDXY,XY,(2+IPARNSP*8)*IPARNPOINTS+3*IPARNSP+3)
  RMS(inds(INDEMIN0)+IEXP)=OLDRMS
  RMX(inds(INDEMAX0)+IEXP)=OLDRMX
  LC=IOR(LC,71_i4b)
  LF=2
  CALL FLIM (*80)
  CALL UTAB (0)
  RETURN
80 RETURN 1
END SUBROUTINE COMPARE
