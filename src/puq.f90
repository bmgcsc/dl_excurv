SUBROUTINE PUQ (K,CLEAR,UXM)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Index_IGF
  Use Common_PMTX
  Use Common_UPU
  Use Include_IPA
  Use Include_PA1
  !
  !	Plot a UNIT - called by PU
  !
  !      *** INCLUDES CALLS TO GHOST LIBRABRY ROUTINES ***
  !
  DIMENSION RADIUS(999),IBONDS(IPARNS),JBONDS(IPARNS)
  CHARACTER LABEL*4,KABEL*5
  LOGICAL CLEAR
  IF (CLEAR) THEN
     UXM=0.
     DO  I=1,NS
        IF (NU(I).NE.K) cycle
        IF (ABS(UX(I)).GT.UXM) UXM=ABS(UX(I))
        IF (ABS(UY(I)).GT.UXM) UXM=ABS(UY(I))
     enddo
     UXM=MAX(.5,UXM)
     X1=UXM*1.10
     CALL MAP (-X1,X1,-X1,X1)
     CALL BORDER
     IF (IGF(6).NE.2) CALL SCALES
  ENDIF
  !	CALL MAP (-X1,X1,-X1,X1)
  NBOND=0
  !	CALL POSITN (0.,0.)
  ASIZE=AMAX1(RMTR(IT(1))*SIZE,.3)
  CALL FCIRCLE (0.,0.,ASIZE)
  DO  I=1,NS
     IF (NU(I).NE.K) cycle
     !	CALL POSITN (UX(I),UY(I))
     ASIZE=AMAX1(RMTR(IT(I))*SIZE,.3)
     CALL FCIRCLE (UX(I),UY(I),ASIZE)
     IF (CLEAR.AND.IGF(13).NE.2) THEN
        !	  ASIZE=CSIZE*UXM
        !	  CALL CTRSIZ (ASIZE)
        !	  CALL PLOTNI (UX(I)-ASIZE*2,UY(I)-ASIZE*2,I,2)
        TTX=UX(I)-ASIZE*2
        TTY=UY(I)-ASIZE*2
        ILAB=IATOM(IT(I))
        KABEL=ELS(ILAB)
        IF (KABEL(2:2).EQ.' ') THEN
           NDIGITS=1
        ELSE
           NDIGITS=2
           KABEL(2:2)=CHAR(ICHAR(KABEL(2:2))+32)
        ENDIF
        KABEL(NDIGITS+1:NDIGITS+1)='\\'
	III=I
        IF (III.GT.99) THEN
           NDIGITS=NDIGITS+1
           KABEL(NDIGITS+1:)=CHAR(48+III/100)
           III=MOD(III,100)
        ENDIF
        IF (III.GT.9) THEN
           NDIGITS=NDIGITS+1
           KABEL(NDIGITS+1:)=CHAR(48+III/10)
           III=MOD(III,10)
        ENDIF
        KABEL(NDIGITS+2:)=CHAR(48+III)
        CALL PLOTCSX (TTX,TTY,KABEL)
        !	  LABEL='('//ELS(ILAB)//')'
        !	  CALL TYPECS (LABEL)
     ENDIF
     CALL THICK (3)
     IF (R(I).LE.BOND) THEN
        CALL POSITN (0.,0.)
        CALL JOIN (UX(I),UY(I))
     ENDIF
     DO  J=I,NS
	IF (I.EQ.J.OR.NU(J).NE.K) cycle
	TR=(UX(J)-UX(I))**2+(UY(J)-UY(I))**2+(UZ(J)-UZ(I))**2
	TR=SQRT(TR)
	IF (TR.GT.BOND) cycle
 	CALL POSITN(UX(I),UY(I))
	CALL JOIN (UX(J),UY(J))
	NBOND=NBOND+1
	IBONDS(NBOND)=I
	JBONDS(NBOND)=J
	RADIUS(NBOND)=TR
     enddo
     CALL THICK (1)
  enddo
  IF (CLEAR.AND.IGF(9).NE.4) THEN
     CALL SETTAB (0)
     CALL PLACE (1,5)
     IF (IGF(2).EQ.1.AND.IGF(ITNUM).NE.5) THEN
        CALL TYPECS ('          R    DR     ANG    PHI    TH ')
     ELSE
        CALL TYPECS ('          R     DR      ANG     PHI     TH ')
     ENDIF
     CALL CRLNFD
     DO  I=1,NS
        IF (NU(I).NE.K) cycle
        CALL CRLNFD
        CALL TYPENI (I,2)
        CALL TYPENF (R(I),3,7)
        CALL TYPENF (DR(I),3,5)
        QX=TANG(I)
        CALL TYPENF (QX,1,6)
        CALL TYPENF (PHI(I),1,5)
        CALL TYPENF (TH(I),1,5)
     enddo
     CALL CRLNFD
     CALL LINEFD (2)
     CALL SPACE(8)
     IF (IGF(2).EQ.1.AND.IGF(ITNUM).NE.5) THEN
        CALL TYPECS ('Atom1 Atom2 Distance ')
     ELSE
        CALL TYPECS ('Atom1   Atom2   Distance ')
     ENDIF
     CALL CRLNFD
     CALL CRLNFD
     DO  I=1,NBOND
        CALL SPACE(8)
        CALL TYPECS (ELS(IATOM(IT(IBONDS(I)))))
        CALL TYPENI (IBONDS(I),3)
        CALL SPACE(1)
        CALL TYPECS (ELS(IATOM(IT(JBONDS(I)))))
        CALL TYPENI (JBONDS(I),3)
        CALL SPACE(2)
        CALL TYPENF (RADIUS(I),4,7)
        CALL CRLNFD
     enddo
  ENDIF
  QDQ=.FALSE.
  return
END SUBROUTINE PUQ
