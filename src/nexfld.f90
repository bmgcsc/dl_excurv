SUBROUTINE NEXFLD (BUF,IL,M1,M2,*)
  !======================================================================C
  !	Get next field from a record - separators are spaces. J.W.Campbell
  !
  !	    BUF (I)   CHARACTER TEXT BUFFER
  !	     IL (I/O) POSITION OF LAST CHARACTER PROCESSED
  !	     M1 (O)   START POSITION OF NEXT FIELD
  !	     M2 (O)   END POSITION OF NEXT FIELD
  !    RETURN 1   END OF STRING
  !
  CHARACTER*(*) BUF
  LOGICAL QUOTE
  !
  !	Get next field
  !
  NBUF=LEN(BUF)
  M1=0
  M2=0
  QUOTE=.FALSE.
10 IL=IL+1
  IF (IL.GT.NBUF) RETURN 1
  IF (BUF(IL:IL).EQ.' ') GOTO 10
  M1=IL
20 M2=IL
  IF (BUF(IL:IL).EQ.CHAR(39)) QUOTE=.NOT.QUOTE
  IL=IL+1
  IF (IL.GT.NBUF) RETURN
  IF (BUF(IL:IL).NE.' '.OR.QUOTE) GOTO 20
  IL=IL-1
  RETURN
END SUBROUTINE NEXFLD
