SUBROUTINE POLAR (*)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_CL
  Use Common_POL
  Use Include_IPA
  Use Include_PA2
  !
  !	Calculate  angular part of dipole operator for exact polarisation
  !	dependent case.
  !
  COMPLEX EXPIPHI,A,B,DD
  !
  !	Calculate angle factors
  !
  DO JEXP=1,NSPEC
     EXPIPHI=CMPLX(COS(BEPHI02(JEXP)),-SIN(BEPHI02(JEXP)))
     A=EXPIPHI*CMPLX(-SIN(BEW02(JEXP))*COS(BETH02(JEXP)),COS(BEW02(JEXP)))
     B=-SQRT(2.)*SIN(BEW02(JEXP))*SIN(BETH02(JEXP))
     DO M0=-LINITIAL(JEXP),LINITIAL(JEXP)
	DO MF=-IEDGE,IEDGE
           M2=M0-MF
           DD=CMPLX(DM(LINITIAL(JEXP),IEDGE,M0,MF),0.)
           IF (M2.EQ.-1) THEN
              CCANG1(M0,MF,JEXP)=DD*A
           ELSEIF (M2.EQ.0) THEN
              CCANG1(M0,MF,JEXP)=DD*B
           ELSEIF (M2.EQ.1) THEN
              CCANG1(M0,MF,JEXP)=-DD*CONJG(A)
           ELSE
              CCANG1(M0,MF,JEXP)=0.
           ENDIF
           CCANG2(MF,M0,JEXP)=CONJG(CCANG1(M0,MF,JEXP))
	ENDDO
     ENDDO
  ENDDO
  !	WRITE (6,'(a,2(6f8.3/))') 'ccang',(ccang1(0,j,1),j=-1,1),(ccang2(j,0,1),j=-1,1)
  RETURN
END SUBROUTINE POLAR
