SUBROUTINE MULTLEG3 (MJ,MU,F3,FTERM,TZ)
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ATMAT
  Use Include_IPA

  COMPLEX FTERM(12,9),F3(9,12),TZ
  TZ=0
  DO L1=1,MJ
     !	F2(L1,L1)=0.
     DO L3=1,MU
        !	F2(L1,L1)=F2(L1,L1)+F3(L1,L3)*FTERM(L3,L1)
	TZ=TZ+F3(L1,L3)*FTERM(L3,L1)
	IF (.FALSE.) WRITE (7,*) L1,L3
     ENDDO
     !	TZ=TZ+F2(L1,L1)
  ENDDO
  RETURN
END SUBROUTINE MULTLEG3
