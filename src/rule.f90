SUBROUTINE RULE
  !======================================================================C
  Use Parameters
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_VAR
  Use Common_RULES
  Use Common_COMPAR
  Use Common_RCBUF
  Use Include_IPA
  Use Include_PA1
  !
  !	Define or modify a rule relating parameters
  !
  DIMENSION VEC(10)
  CHARACTER IP1*64,IP2*4,IP3*4,OPTS(2)*16
  DATA OPTS/'(parameter_name)','DELETE'/
  NNRULES=NRULES
  CALL FINDOPT (2,OPTS,IOPT,0,'None',*290)
  IF (IOPT.EQ.2) THEN
     NRULES=0
     RETURN
  ENDIF
  IF (KEYWORD.EQ.' ') THEN
     IF (NRULES.LE.0) RETURN
     CALL WTEXT ('The following rules are defined:')
     DO I=1,NRULES
        VEC(1)=0.
        L1=NCSTR(RULEX(I))
        L2=NCSTR(RULEY(I))
        WRITE (6,'(1X,A,''='',A,''   ['',F9.4,'']'')') RULEX(I)(1:L1),RULEY(I)(1:L2),EXPRESS(RULEY(I),IC,VEC)
     ENDDO
     RETURN
  ENDIF
  DO J=1,IPARNPARAMS
     IF (KEYWORD.EQ.IVARS(J)) GOTO 5
  ENDDO
  CALL ERRMSG (KEYWORD(1:LKEYWORD)//' is not a valid parameter name',*290)
5 IF (NRULES.GT.0) THEN
     DO I=1,NRULES
        IF (KEYWORD.EQ.RULEX(I)) GOTO 10
     ENDDO
  ENDIF
  NRULES=NRULES+1
  I=NRULES
10 RULEX(I)=KEYWORD
  IRULE(I)=J
  IF (CHAROPT(1:3).EQ.'DEL') THEN
     IF (NRULES.GT.I) THEN
        DO K=I+1,NRULES
           RULEX(K-1)=RULEX(K)
           RULEY(K-1)=RULEY(K)
           IRULE(K-1)=IRULE(K)
        ENDDO
     ENDIF
     NRULES=NRULES-1
     RETURN
  ENDIF
20 IF (LRIS.EQ.0) CALL WTEXT ('Enter rule for '//RULEX(I))
  !	CALL SREAD (*20,*290)
  CALL CREAD(IP1,IP2,IP3,IDUM,IC,V,*20,*290)
  RULEY(I)=IP1
  VEC(1)=0.
  L1=NCSTR(RULEX(I))
  L2=NCSTR(RULEY(I))
  WRITE (6,'(1X,A,''='',A,''   ['',F9.4,'']'')') RULEX(I)(1:L1),RULEY(I)(1:L2),EXPRESS(RULEY(I),IC,VEC)
  RETURN
290 NRULES=NNRULES
  RETURN
END SUBROUTINE RULE
