SUBROUTINE OPROT (APHI,KROT,IS,*)
  !======================================================================
  !	Performs symmetry operation - KROT fold rotation about z
  Use Definition
  Use Common_convia
  APHI=APHI+2.*PI/real(KROT,kind=dp)
  IS=IS+2
  RETURN 1
END SUBROUTINE OPROT
