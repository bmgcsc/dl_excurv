SUBROUTINE PLACE (IUXMIN,IUYMIN)
  !======================================================================C
  !-st	Use Parameters
  Use Common_PMTX
  TXMIN=IUXMIN
  TYMIN=IUYMIN
  IF (QDQ) THEN
     CALL FQQMOVE2 (TXMIN,TYMIN)
  ELSE
     CALL FMOVE2 (TXMIN,TYMIN)
  ENDIF
  RETURN
END SUBROUTINE PLACE
