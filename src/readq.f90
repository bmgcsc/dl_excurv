SUBROUTINE READQ
  !======================================================================C
  Use Common_DLV
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_MTR
  Use Common_F
  Use Common_ELS
  Use Common_PLFLAG
  Use Common_VAR
  Use Common_COMPAR
  Use Common_ICF
  Use Common_PATHS
  Use Index_INDS
  Use Include_IPA
  !	Use Common_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  use Module_SCFPOT
  !
  !	Read reads input files.
  !
  LOGICAL      :: MESSAGE
  CHARACTER*60 :: WSPACE
  CHARACTER*12 :: KP1,KP2,KP3,KP4,KP5
  !  CHARACTER*11 :: OPT(9)
  !  DATA OPT/'ALL','EXPERIMENT','PARAMETERS','PHASESHIFTS','MS','POTENTIALS','SIGMA','SPECTRA','ME'/
  CHARACTER*11 :: OPT(11)
  DATA OPT/'ALL','EXPERIMENT','PARAMETERS','PHASESHIFTS','MS','POTENTIALS','SIGMA','SPECTRA','ME','SCFPOT', 'SPOT'/
  !-st  print *, opt,iopt
  !-st  pause
  !  CALL FINDOPT (9,OPT,IOPT,1,'Spectrum Number',*190)
  CALL FINDOPT (11,OPT,IOPT,1,'Spectrum Number',*190)
  !-st  print *, opt,iopt
  !-st  pause
  GOTO ( 30, 30,110,150,110,200,210,220,229, 301, 320),IOPT
  !
  !	Error in reading experimental file
  !
10 CLOSE (INFILE)
  if(DLV_flag) return
20 if(DLV_flag) then
     CALL ERRS ()
     return
  else
     CALL ERRS (*30)
  endif
  !
  !	Read experiment
  !
30 IF (INTOPT.EQ.0) THEN
     IEXP=1
  ELSE
     IEXP=INTOPT
  ENDIF
  IF (IEXP.GT.IPARNSP) IEXP=1
  if(.not.DLV_flag) CALL WTEXT ('Filename for Experiment '//CHAR(48+IEXP)//' ?')
  CALL AREAD (1,WSPACE,LD,*30,*80)
  CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','dat',*20)
  if(.not.DLV_flag)  WRITE (OUTTERM,250) WSPACE(1:NCSTR(WSPACE))
40 ISP=1
  if(.not.DLV_flag) CALL WTEXT ('Point frequency [1] ?')
  CALL CREAD (KP1,KP2,KP3,IDUM,IC,V,*41,*80)
  ISP=IDUM
41 IF (ISP.GT.0.AND.ISP.LE.100) GOTO 50
  CALL ERRMSGI ('Invalid point frequency: ',ISP,*40)
50 ICOL=12
  if(.not.DLV_flag) CALL WTEXT ('Column combination [12] ?')
  CALL CREAD (KP1,KP2,KP3,IDUM,IC,V,*51,*80)
  ICOL=IDUM
51 IF (ICOL.GE.12.AND.ICOL.LT.100.AND.MOD(ICOL,11).NE.0) GOTO 55
  CALL ERRMSGI ('Invalid column combination: ',ICOL,*50)
55 KP4=EDGENAME
  KP5=EDGECODE(IEXP)
  NK4=NCSTR(KP4)
  NK5=NCSTR(KP5)
  if(.not.DLV_flag) CALL WTEXT ('Edge ? ['//KP4(1:NK4)//' '//KP5(1:NK5)//']')
  CALL CREAD (KP1,KP2,KP3,IDUM,IC,V,*56,*80)
  KP4=KP1
  KP5=KP2
56 DO I=1,9
     IF (KP5.EQ.EDGECODES(I)) GOTO 57
  ENDDO
  CALL ERRMSG ('Invalid edge: '//KP5,*55)
57 IEDGECODE(IEXP)=I
  DO J=1,103
     IF (KP4.EQ.ELS(J)) GOTO 59
  ENDDO
  CALL ERRMSG ('Invalid element: '//KP4,*55)
59 CONTINUE
  EDGENAME=KP4
  EDGECODE(IEXP)=KP5
  NINITIAL(IEXP)=NINITIALS(I)
  LINITIAL(IEXP)=LINITIALS(I)
  JINITIAL(IEXP)=JINITIALS(I)
  IZED(IEXP)=J
  if(.not.DLV_flag) CALL WTEXT ('Sequence-number in polarisation set [0]')
  IPOL(IEXP)=0
  CALL CREAD (KP1,KP2,KP3,IDUM,IC,V,*58,*80)
  IPOL(IEXP)=IDUM
58 if(.not.DLV_flag) CALL WTEXT ('Number of clusters for this experiment [1]')
  NCLUS(IEXP)=1
  CALL CREAD (KP1,KP2,KP3,IDUM,IC,V,*60,*80)
  NCLUS(IEXP)=IDUM
  !
  !	Read experiment if required, return 1 occurs on error
  !
60 CALL RDS (INFILE,ICOL,ISP,*10)
  RMS(inds(INDEMIN0)+IEXP)=ENER(1)
  RMX(inds(INDEMAX0)+IEXP)=ENER(NP)
  RMS(inds(INDEMIN0))=MIN(RMS(inds(INDEMIN0)),RMS(inds(INDEMIN0)+IEXP))
  RMX(inds(INDEMAX0))=MAX(RMX(inds(INDEMAX0)),RMX(inds(INDEMAX0)+IEXP))
  EMIN0(IEXP)=ENER(1)
  EMAX0(IEXP)=ENER(NP)
  NSIG=0
  DO I=1,NP
     ENER(I)=(ENER(I))*EC
     TT=ENER(I)+.5
     IF (TT.GT.1.E-10) THEN
        RK(I,IEXP)=SQRT(2.*TT)
        TRK(I,IEXP)=RK(I,IEXP)
     ELSE
        RK(I,IEXP)=1.E-5
        IF (TT.EQ.0.) THEN
           TRK(I,IEXP)=0.
        ELSEIF (TT.GT.0.) THEN
           TRK(I,IEXP)=SQRT(2.)*1.E-5
        ELSE
           TRK(I,IEXP)=-SQRT(-2.*TT)
        ENDIF
     ENDIF
  enddo
  !
  !	Set RNSPEC to highest spectrum number so far
  !	FLIM will set NSPEC from RNSPEC
  !
  RNSPEC=MAX(IEXP,NSPEC)
  NSPEC=RNSPEC
  !
  !	Reset the experiment-group markers
  !
  IEXGROUP(1)=1
  IG=1
  IF (NSPEC.GT.1) THEN
     DO I=2,NSPEC
	IF (NINITIAL(I).NE.NINITIAL(I-1).OR.  &
             LINITIAL(I).NE.LINITIAL(I-1).OR.  &
             JINITIAL(I).NE.JINITIAL(I-1)) IG=IG+1
	IEXGROUP(I)=IG
     ENDDO
  ENDIF
  CLOSE (INFILE)
  LFS(IEXP)=WSPACE
80 IF (IOPT.NE.1) GOTO 180
  GOTO 110
90 CLOSE (INFILE)
  !
  !	Here for an error in reading a parameter file
  !
100 CALL ERRS (*110)
  !
  !	Entry for read parameters or read ms - read parameter file
  !
110 IF (LRIS+LMDSN.EQ.0) CALL WTEXT ('Filename for parameters ?')
  CALL AREAD (2,WSPACE,LD,*100,*140)
  CALL FILEOPEN (WSPACE,INFILE,'FORMATTED','dat',*100)
  TEMPX=0.
  DO I=1,IPARNP
     TEMPX=TEMPX+XE(I)
  enddo
  IF (CHAROPT(1:1).EQ.'B') THEN
     JCLUS=1
123  CALL WTEXT ('Enter Cluster Number: [1]')
     CALL CREAD (KP1,KP2,KP3,IDUM,IC,V,*125,*80)
     IF (IDUM.LT.1.OR.IDUM.GT.IPARNCLUS) CALL ERRMSGI ('Invalid cluster number: ',I,*123)
     JCLUS=IDUM
125  CALL RDBROOK (*90,JCLUS)
  ELSEIF (CHAROPT(1:1).EQ.'C') THEN
     CALL RDCAR (*90)
  ELSE
     CALL RDPARM (*90)
  ENDIF
  LFS(IPARNSP+1)=WSPACE
  DO I=1,IPARNP
     TEMPX=TEMPX-XE(I)
  enddo
  IF (ABS(TEMPX).GE.1.E-4) THEN
     IPR=1
     CALL WTEXT ('*WARNING*: XE changed by parameter file')
     IPR=0
  ENDIF
  !
  !	Close file
  !
  CLOSE (INFILE)
  WRITE (OUTTERM,260) LFS(IPARNSP+1)
  IF (IOPT.EQ.5) CALL XADD ('READ',-1,*180)
140 IF (IOPT.NE.1) GOTO 180
  !
  !	Entry for read phase - read phaseshifts
  !
  !	Phaseshifts must be read if NPS=0 : defaults have been overwritten
  !	and an uncorrectable error has occurred. Otherwise step may be ski
  !     pped.
  !
150 CALL WTEXT ('Enter number of phaseshift files')
  CALL CREAD (KP1,KP2,KP3,NPX,IC,V,*150,*170)
  IF (NPX.LT.1.OR.NPX.GT.IPARNP) CALL ERRMSG ('Number of phaseshifts invalid',*150)
  !
  !	If temporary value ok, pass it to NPS, otherwise keep old value.
  !
  NPS=NPX
  !
  !	Read each of nps phaseshift files. individual atoms may be
  !	skipped if already read in.
  !
  DO I=1,NPS
     CALL RPS (I)
  enddo
170 IF (NPS.LT.1) GOTO 150
  !
  !	FLIM Checks energy ranges, performs initial unit conversion and
  !	sets up wave vector. Return 1 occurs on non-correctable error.
  !
180 CALL FLIM (*181)
  !
  !	UTAB checks unit parameters and calculates angular coordinates
  !
181 CALL UTAB (0)
190 RETURN
200 CALL READPOT
  RETURN
301 CALL READSCFPOT
  RETURN
320 CALL READSPOT
  RETURN
210 CALL READSTAT
  RETURN
220 CALL WTEXT ('Filename for Spectra ?')
  CALL AREAD (1,WSPACE,LD,*220,*190)
  CALL FILEOPEN (WSPACE,INFILE,'UNFORMATTED','f88',*190)
  READ (INFILE,END=240) NUMPATHS,MESSAGE,JS(IEXP),JF(IEXP),NPATHS
  NPT(IEXP)=JF(IEXP)-JS(IEXP)+1
  IF (MESSAGE.OR.NUMPATHS.GT.IPARPATH) CALL WTEXT ('Warning : list truncated')
  NUMPATHS=MIN(NUMPATHS,IPARPATH)
  NPATHS=MIN(NPATHS,IPARPATH1)
  READ (INFILE,END=240) (RK(I,IEXP),I=JS(IEXP),JF(IEXP))
  READ (INFILE,END=240) ((SPECTRA(K,I,1),K=1,NPT(IEXP)),I=1,NUMPATHS)
  READ (INFILE,END=240) (PATHMAX(I,1),I=1,NPATHS)
  READ (INFILE,END=240) (PATHINDEX(I),I=1,NUMPATHS)
  READ (INFILE,END=240) ((IPATHINDS(K,I),K=1,5),I=1,NUMPATHS)
  READ (INFILE,END=240) ((JPATHINDS(K,I),K=1,5),I=1,NUMPATHS)
  READ (INFILE,END=240) (PATHPL(I),I=1,NUMPATHS)
  READ (INFILE,END=240) (PATHANG(I),I=1,NUMPATHS)
  READ (INFILE,END=240) (IPATHOCC(I),I=1,NUMPATHS)
  CALL WTEXTI ('Number of spectra read:',NUMPATHS)
  CALL WTEXTI ('Total paths in cluster:',NPATHS)
240 CLOSE (INFILE)
  RETURN
229 CALL README
  RETURN
250 FORMAT ('Experimental spectrum in ',A)
260 FORMAT ('Parameters in ',A)
END SUBROUTINE READQ
