CHARACTER*6 FUNCTION PRINTLAB6 (LAB)
  !======================================================================C
  !
  !	Truncate parameter strings ABC[1,2,3] etc. to 6 characters
  !
  CHARACTER*(*) LAB
  J=0
  PRINTLAB6=' '
  ILEN=MIN0(6,LEN(LAB))
  DO I=1,ILEN
     IF (LAB(I:I).EQ.'['.OR.LAB(I:I).EQ.']') cycle
     J=J+1
     IF (LAB(I:I).EQ.',') THEN
        PRINTLAB6(J:J)='/'
     ELSE
        PRINTLAB6(J:J)=LAB(I:I)
     ENDIF
  enddo
  RETURN
END FUNCTION PRINTLAB6
