SUBROUTINE XTCLEAR
  !======================================================================C
  Use Parameters
  Use Common_B1
  !
  !	Clears an Xterminal in TEK mode
  !
  CHARACTER*2 XCLR
  XCLR(1:1)=CHAR(27)
  XCLR(2:2)=CHAR(12)
  WRITE (OUTTERM,20) XCLR
20 FORMAT(A)
  RETURN
END SUBROUTINE XTCLEAR
