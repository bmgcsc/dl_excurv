SUBROUTINE FFOPENQ (IUN,FILNAM,ISTAT,ITYPE,*)
  !======================================================================C
  Use Definition
  Use Parameters
  Use Common_B1
  Use Common_B2
  !
  !	Open logical file FILNAM with unit number IUN.
  !
  !	IUN      Unit number
  !	FILNAM   Logical file name (up to 8 characters)
  !	ISTAT    File status flag =1, 'UNKNOWN'
  !	                          =2, 'SCRATCH'
  !	                          =3, 'OLD'
  !	                          =4, 'NEW'
  !	                          =5, 'READONLY'
  !	                          =6, 'PRINTER'
  !	ITYPE    File type flag   =1, 'formatted'
  !	                          =2, 'unformatted'
  !
  CHARACTER*(*) FILNAM
  CHARACTER*12 FRM
  CHARACTER*7 STAT(6),ST,CCNTRL
  DATA STAT/'UNKNOWN','SCRATCH','OLD','NEW','OLD','UNKNOWN'/
  IF (ISTAT.LT.1.OR.ISTAT.GT.6.OR.ITYPE.LT.1.OR.ITYPE.GT.2) GOTO 10
  ST=STAT(ISTAT)
  FRM='FORMATTED'
  IF (ITYPE.EQ.2) FRM='UNFORMATTED'
  IF (ISTAT.EQ.6) THEN
     CCNTRL='FORTRAN'
  ELSE
     CCNTRL='LIST'
  ENDIF
  IF (ISTAT.EQ.5) THEN
     OPEN(UNIT=IUN,FILE=FILNAM,STATUS=ST,ACCESS='SEQUENTIAL',FORM=FRM,IOSTAT=IOS)
  ELSE
     OPEN(UNIT=IUN,FILE=FILNAM,STATUS=ST,ACCESS='SEQUENTIAL',FORM=FRM,IOSTAT=IOS)
  ENDIF
  IF (DEBUG) THEN
     WRITE (6,'(A,I2,A,A,A,A,I2)') 'FFOPENQ: UNIT=',IUN,' FILE=',FILNAM,' '//FRM(1:NCSTR(FRM)),' IOSTAT= ',IOS
  ENDIF
  IF (IOS.LE.0) RETURN
  !
  !	Error return
  !
10 WRITE (LOGFILE,20) IUN,FILNAM
  IF (BACKGROUND) STOP '**FFOPENQ ERROR**'
  RETURN 1
  !
20 FORMAT(/,' Error in opening unit',I3,' file name: ',A)
END SUBROUTINE FFOPENQ
