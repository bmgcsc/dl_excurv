SUBROUTINE RHL (ERL,EIM,XK,RS,SRS,WP)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Index_ISET
  Use Common_B1
  Use Common_B2
  Use Include_IPA
  !
  !	Real part interpolated. Imaginary part calculated analytically.
  !	it uses hl to calculate values at the mesh points for the inter
  !	polation of the real part. polynomial in rs has a 3/2 power term.
  !
  !	written by jose mustre
  !
  !	COMMON/INTER1/WPFE,WPFESQ
  !
  !       for the right branch the interpolation has the form:
  !       hl(rs,x) = e/x + f/x**2 + g/x**3
  !         where e is known and
  !               f = sum (i=1,3) ff(i) rs**(i+1)/2
  !               g = sum (i=1,3) gg(i) rs**(i+1)/2
  !
  !       lrs=number of rs panels, in this case one has 4 panels
  !       nrs=number of standard rs values, also order of rs expansion
  !       if you change nrs you need to change the expansion of hl
  !       in powers of rs that only has 3 terms!
  !       nleft=number of coefficients for x<x0
  !       nright=number of coefficients for x>x0
  !
  PARAMETER (LRS=4,NRS=3,NLEFT=4,NRIGHT=2)
  DIMENSION RCFL(LRS,NRS,NLEFT),RCFR(LRS,NRS,NRIGHT),CLEFT(NLEFT),CRIGHT(NRIGHT)
  LOGICAL CUBIC
  DATA CONV /1.9191583/
  DATA RCFR &
       /-0.173963D+00,-0.173678D+00,-0.142040D+00,-0.101030D+00, &
       -0.838843D-01,-0.807046D-01,-0.135577D+00,-0.177556D+00,  &
       -0.645803D-01,-0.731172D-01,-0.498823D-01,-0.393108D-01,  &
       -0.116431D+00,-0.909300D-01,-0.886979D-01,-0.702319D-01,  &
       0.791051D-01,-0.359401D-01,-0.379584D-01,-0.419807D-01,   &
       -0.628162D-01, 0.669257D-01, 0.667119D-01, 0.648175D-01/
  DATA RCFL &
       / 0.590195D+02, 0.478860D+01, 0.812813D+00, 0.191145D+00, &
       -0.291180D+03,-0.926539D+01,-0.858348D+00,-0.246947D+00,  &
       0.363830D+03, 0.460433D+01, 0.173067D+00, 0.239738D-01,   &
       -0.181726D+03,-0.169709D+02,-0.409425D+01,-0.173077D+01,  &
       0.886023D+03, 0.301808D+02, 0.305836D+01, 0.743167D+00,   &
       -0.110486D+04,-0.149086D+02,-0.662794D+00,-0.100106D+00,  &
       0.184417D+03, 0.180204D+02, 0.450425D+01, 0.184349D+01,   &
       -0.895807D+03,-0.318696D+02,-0.345827D+01,-0.855367D+00,  &
       0.111549D+04, 0.156448D+02, 0.749582D+00, 0.117680D+00,   &
       -0.620411D+02,-0.616427D+01,-0.153874D+01,-0.609114D+00,  &
       0.300946D+03, 0.109158D+02, 0.120028D+01, 0.290985D+00,   &
       -0.374494D+03,-0.535127D+01,-0.261260D+00,-0.405337D-01/
  !
  !	calcualte hl using interplation coefficients
  !
  !	writen by j. mustre march 1988 based on analytical expression derived by john rehr.
  !
  ICUSP=0
  XKSQ=XK*XK
  XKCUB=XK*XKSQ
  !	XKP1SQ=(XK+1.)**2
  XK2=XK+XK
  XKP1SQ=XKSQ+XK2+1.
  XKM1SQ=XKSQ-XK2+1.
  !	XKM1SQ=(XK-1.)**2
  !
  !	wp is given in units of the fermi energy in the formula below.
  !
  WPFE=WP
  WPFESQ=WP*WP
  EIM=0.
  IF (XK.GT.1.) THEN
     XS=WPFESQ-(XKSQ-1.)**2
     IF ( XS.LT.0.) THEN
        !-st	  Q2SQ=SQRT(.4444444444444-XS)-.6666666666667
        Q2SQ=SQRT(4./9. - XS)-2./3.
        QU=MIN(Q2SQ,XKP1SQ)
        IF (QU.GT.XKM1SQ) EIM=FFQ2(WPFE,WPFESQ,QU,XKM1SQ)
        CALL CUBIC2 (XK,WPFESQ,QPLUS,QMINUS,*10)
        IF (QPLUS.GT.XKP1SQ) EIM=EIM*FFQ2(WPFE,WPFESQ,QPLUS,XKP1SQ)
        IF (XKM1SQ.GT.QMINUS) THEN
           EIM=EIM*FFQ2(WPFE,WPFESQ,XKM1SQ,QMINUS)
           !
           !	beginning of the imaginary part and position of the cusp x0
           !
           ICUSP=1
        ENDIF
        IF (EIM.NE.0.) EIM=(.239894787*WPFE/(RS*XK))*LOG(EIM)
     ENDIF
  ENDIF
  IF (iset(IQUINN).EQ.1) THEN
     CALL QUINN (WP,XK,RS,EI)
     IF (EIM.GT.EI) EIM=EI
  ENDIf
  !
  !	EIM ALREADY HAS A FACTOR OF EF IN IT J.M.
  !	EIM ALSO GIVES THE POSITION OF THE CUSP
  !
  !	Calculate right hand side coefficients
  !
10 IF (RS.LT..2) THEN
     MRS=1
  ELSEIF (RS.LT.1.) THEN
     MRS=2
  ELSEIF (RS.LT.5.) THEN
     MRS=3
  ELSE
     MRS=4
  ENDIF
  IF (ICUSP.EQ.0) THEN
     !  $DIR SCALAR
     DO  J=1,NLEFT
        CLEFT(J)=RCFL(MRS,1,J)+RCFL(MRS,2,J)*SRS+RCFL(MRS,3,J)*RS
     enddo
     ERL=CLEFT(1)+CLEFT(2)*XK+CLEFT(3)*XKSQ+CLEFT(4)*XKCUB
  ELSE
     !
     !	Right branch
     !
     ERL=-.409240948*WPFE/XK
     DO J=1,NRIGHT
        CRIGHT(J)=RCFR(MRS,1,J)+RCFR(MRS,2,J)*SRS+RCFR(MRS,3,J)*RS
     enddo
     ERL=ERL+CRIGHT(1)/XKSQ+CRIGHT(2)/XKCUB
  ENDIF
  ERL=ERL*1.84158429/RS
  RETURN
END SUBROUTINE RHL
