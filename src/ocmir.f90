SUBROUTINE OCMIR (DN,IS,*)
  !======================================================================C
  !
  !	Performs symmetry operation - reflection about horiz plane
  !
  Use Common_convia
  DIMENSION DN(36,*)
  DN(1,2)=PI-DN(1,2)
  DN(1,6)=-DN(1,6)
  IS=IS+1
  RETURN 1
END SUBROUTINE OCMIR
