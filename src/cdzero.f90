SUBROUTINE CDZERO (ARRAY,LEN)
  !======================================================================C
  Use Definition
  !
  !	SET THE CONTENTS OF A REAL ARRAY TO ZERO
  !
  !	  ARRAY (O)   ARRAY TO BE ZEROED
  !	    LEN (I)   THE NUMBER OF WORDS TO BE ZEROED
  !
  COMPLEX(imag2) :: ARRAY(*)
  IF (LEN.LE.3) RETURN
  DO L=1,LEN/4
     ARRAY(L)=0
  enddo
  RETURN
END SUBROUTINE CDZERO
