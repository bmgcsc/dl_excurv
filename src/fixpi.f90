SUBROUTINE FIXPI (K,PISHIFT)
  !======================================================================C
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_P
  Use Include_PA1

  LOGICAL PISHIFT
  !
  !	Fix PI shifts
  !
  DO NK=NPH(K),1,-1
     IF (REAL(PHS(NK,K,1)).NE.0.) GOTO 2
  ENDDO
  NK=NPH(K)
  IF (NK.EQ.0) RETURN
2 IF (PISHIFT) THEN
     DO J=1,6
        DO JU=1,2
           TEMP=PHS(NK,K,J)
           DO  KK=NK-1,1,-1
              QT=PHS(KK,K,J)-TEMP
              IF (ABS(QT).LT..5) goto 30
              !
              !	Allow bigger jumps if gradient positive
              !
              if (QT.GT.-2.6.AND.QT.LT.0.) GOTO 30
              !-st	  QT=DSIGN(PI,DBLE(QT))
              QT=SIGN(PI,real(QT,kind=dp))
              NF=0
              DO  M=1,KK
                 PHS(M,K,J)=PHS(M,K,J)-QT
                 IF (REAL(PHS(M,K,J)).LT.-.31) NF=1
              enddo
              IF (NF.EQ.0) GOTO 30
              DO  MM=1,NK
                 PHS(MM,K,J)=PHS(MM,K,J)+PI
              enddo
30            TEMP=PHS(KK,K,J)
           enddo
        ENDDO
     enddo
  ENDIF
  !
  !	First phaeshift can come out quite atrongly -ve in HL - make sure there isnt an extra PI shift
  !
  IF (ATOM(K).LE.36.) THEN
     DO IL=2,3
        IF (IL.EQ.3.OR.ATOM(K).LE.18.) THEN
           DO NN=1,2
              IF (REAL(PHS(NK,K,IL)).GE.PI) THEN
                 DO I=1,NK
                    PHS(I,K,IL)=PHS(I,K,IL)-PI
                 ENDDO
              ENDIF
           ENDDO
        ENDIF
     ENDDO
  ENDIF
  !
  !	Three passes of checks to make sure the set of L phaseshifts are
  !	never less than the L-1 phaseshifts.  Facilitates comparisons.
  !
  !	return
  DO  KK=1,3
     NF=0
     DO I=2,5
   	IF (REAL(PHS(NK,K,I)).GT.REAL(PHS(NK,K,I-1))) NF=I-1
     enddo
     IF (NF.EQ.0) GOTO 70
     DO  J=1,NF
	DO  I=1,NK
           PHS(I,K,J)=PHS(I,K,J)+PI
        enddo
     enddo
  enddo
70 continue
  !   70IF (PHS(NK,K,3).EQ.0.) THEN
  !	  DO I=1,2
  !	  DO J=1,NK
  !	  PHS(J,K,I)=PHS(J,K,I)+PI
  !	  ENDDO
  !	  ENDDO
  !	ENDIF
  RETURN
END SUBROUTINE FIXPI
