SUBROUTINE UTAB (ICODE)
  !======================================================================
  Use Parameters
  Use Common_convia
  Use Common_B1
  Use Common_B2
  Use Common_ELS
  Use Common_POT
  Use Common_UPU
  Use Common_ICF
  Use Include_IPA
  Use Include_PA1
  Use Include_PA2
  Use Include_XY
  !
  !	UTAB checks unit parameters , calculates anular coordinates
  !	and generates parameter lists in atomic units and integers.
  !	If ICODE is 0, the spherical polar coordinates are used
  !	If ICODE is 1, the cartesian coordinates are used
  !	UTAB does its own update of PA2 and IPA so there is no need to call
  !	FLIM after UTAB
  !
  DIMENSION RM(IPARNUNITS),NNR(IPARNUNITS)
  call eqv_convia
  !
  !	NR(I) is the shell index for the nearest atom in unit i
  !	NT(I) is the number of shells associated with unit i
  !
  IF (DEBUG) WRITE (LOGFILE,*) 'UTAB: ICODE=',ICODE
  !
  !	Zero unit parameter table
  !
  MG=0
  MAXCLUS=0
  DO  I=1,IPARNUNITS
     NR(I)=0
     NNR(I)=0
     NT(I)=0
     RM(I)=99.
  enddo
  CALL UZERO (UOC,IPARNS+1)
  !	MAXEXITE=0
  DO I=NS,0,-1
     IF (ICLUS(I).LT.0) THEN
        IRAD(-ICLUS(I))=I
        !	  MAXEXITE=MAXEXITE+1
        !	  IEXITELIST(MAXEXITE)=I
        IF (LINK(I).GT.NS) LINK(I)=0
        IF (LINK(I).GT.NS) RLINK(I)=0
     ENDIF
     JCLUS=IABS(ICLUS(I))
     MAXCLUS=MAX(MAXCLUS,JCLUS)
  ENDDO
  KCLUS=1
  DO JEXP=1,NSPEC
     IF (NCLUS(JEXP).GT.0) THEN
        DO I=1,NCLUS(JEXP)
           KEXP(KCLUS)=JEXP
           KCLUS=KCLUS+1
        ENDDO
     ENDIF
  ENDDO
  DO I=1,MAXCLUS
     ITAD(I)=IT(IRAD(I))
     IF (IATOM(ITAD(I)).LT.0) THEN
        IZD=IZED(KEXP(I))
        JEXP=1
        ITAD(I)=0
        DO J=1,NPS
           IF (LASTHOLE(J).NE.-99.AND.J.NE.1) JEXP=JEXP+1
           IF (IATOM(J).EQ.IZD) THEN
              IF (JEXP.EQ.KEXP(I).AND.LASTHOLE(J).NE.-99) THEN
                 ITAD(I)=J
              ENDIF
              IF (ITAD(I).EQ.0) ITAD(I)=J
           ENDIF
        ENDDO
     ENDIF
     IF (DEBUG) WRITE (6,*) 'I,ITAD(I)',I,ITAD(I)
  ENDDO
  DO I=0,IPARNS
     !	IF (ABS(R(I)-99.).LT.1.E-6) THEN
     !	  LINK(LINK(I))=0.
     !	  LINK(I)=0.
     !	ENDIF
     IF (ICODE.EQ.0) THEN
        CALL POLTOCARD (R(I),TH(I),PHI(I),UX(I),UY(I),UZ(I))
     ELSE
        CALL CARTOPOLD (UX(I),UY(I),UZ(I),R(I),TH(I),PHI(I))
     ENDIF
     ANG(I)=0.
     IF (I.NE.0) DR(I)=0.
     IF (PHI(I).LT.0.) PHI(I)=PHI(I)+360.
     IF (PHI(I).GT.360) PHI(I)=PHI(I)-360.
     IF (TH(I).LT.0.) TH(I)=TH(I)+360.
     IF (TH(I).GT.360) TH(I)=TH(I)-360.
     JCLUS=IABS(ICLUS(I))
     RADX(I)=UX(I)-UX(IRAD(JCLUS))
     RADY(I)=UY(I)-UY(IRAD(JCLUS))
     RADZ(I)=UZ(I)-UZ(IRAD(JCLUS))
     !	IF (IEXITE.NE.MAXEXITE) THEN
     !	  RADX(I)=UX(I)-UX(IEXITELIST(IEXITE))
     !	  RADY(I)=UY(I)-UY(IEXITELIST(IEXITE))
     !	  RADZ(I)=UZ(I)-UZ(IEXITELIST(IEXITE))
     !	ENDIF
     CALL CARTOPOL (RADX(I),RADY(I),RADZ(I),RAD(I),RADTH(I),RADPHI(I))
     !
     !	This is the proper correction
     !
     RAD(I)=RAD(I)*DC
     RADX(I)=RADX(I)*DC
     RADY(I)=RADY(I)*DC
     RADZ(I)=RADZ(I)*DC
     !	IF (I.GT.NS.OR.I.EQ.IEXITELIST(IEXITE)) cycle
     IF (I.GT.NS) cycle
     K=NU(I)
     IF (K.LT.1.OR.K.GT.IPARNUNITS) cycle
     IF (RAD(I).LT.RM(K)) THEN
        RM(K)=RAD(I)
        NNR(K)=I
     ENDIF
     NT(K)=NT(K)+1
     IF (K.GT.MG) MG=K
  enddo
  !	IEXITE=1
  IF (MG.EQ.0) GOTO 40
  DO I=1,MG
     IF (IPIV(I).GT.0.AND.IPIV(I).LE.IPARNS.AND.NU(IPIV(I)).EQ.I) THEN
        NR(I)=IPIV(I)
     ELSE
        NR(I)=NNR(I)
     ENDIF
     UOC(I)=RN(NR(I))
  ENDDO
  DO I=0,IPARNS
     NF=NR(NU(I))
     JRAD=IRAD(IABS(ICLUS(I)))
     IF (NU(I).LT.1.OR.NU(I).GT.IPARNUNITS.OR.NF.EQ.I.OR.I.EQ.JRAD) cycle
     V1=UX(I)-UX(NF)
     V2=UY(I)-UY(NF)
     V3=UZ(I)-UZ(NF)
     V=V1*V1+V2*V2+V3*V3
     IF (V.GT.0.) DR(I)=SQRT(V)
     IF (V.LT..5.AND.I.LE.NS) THEN
        CALL WTEXTI ('Invalid interatomic distance for shell ',I)
        WRITE (OUTTERM,*) 'Interatomic distance = ',DR(I)
     ENDIF
     ANG2(I)=CALCANG(UX(JRAD)-UX(NF),UY(JRAD)-UY(NF),UZ(JRAD)-UZ(NF),V1,V2,V3,S)
     ANG(I)=ANG2(I)/AC
     IF (I.EQ.IPLA(NU(I))) THEN
        PANG2(NU(I))=ANG2(I)
        PANG(NU(I))=ANG(I)
     ENDIF
     RN(I)=RN(NF)
  enddo
40 DO I=1,IPARNPARAMS
     PA2(I)=PA1(I)*CONVIA(ICF(I))
     !	call all_IPA
     IPA(I)=NINT(PA1(I))
     !	call IPA_all
  ENDDO
  IF (MG.LE.0) GOTO 100
  DO I=1,MG
     JCLUS=IABS(ICLUS(NR(I)))
     CALL CTOR (NTORA(1,I),NTORA(2,I),NTORA(3,I),NTORA(4,I),TORA(I),TORA2(I))
     CALL CTOR (NTORB(1,I),NTORB(2,I),NTORB(3,I),NTORB(4,I),TORB(I),TORB2(I))
     CALL CTOR (IRAD(JCLUS),IPIV(I),IPLA(I),IPLB(I),TORC(I),TORC2(I))
  enddo
100 IF (NS.LT.IPARNS) THEN
     DO I=NS+1,IPARNS
        UN(I)=0.
        NU(I)=0
        LINK(I)=0
        RLINK(I)=0
     ENDDO
  ENDIF
  RETURN
END SUBROUTINE UTAB
